﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class CompRulesAudit1 : System.Web.UI.Page
    {
        public static string auditStates = "";
        public static string auditCities = "";
        public static string auditZips = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            ForAudit();
        }

        public static void ForAudit()
        {
            clsConnection cls = new clsConnection();

            auditStates = "";
            auditCities = "";
            auditZips = "";

            string qry = "select distinct state from tbl_VPR_Workable_Registration_PFC where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from tbl_VPR_Workable_Registration_REO where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from tbl_VPR_Workable_Registration_Property where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from tbl_VPR_Workable_Registration_PropReq where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from tbl_VPR_Workable_Registration_Cost where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from tbl_VPR_Workable where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from tbl_VPR_Workable_Inspection where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from tbl_VPR_Workable_Municipality where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from dbo.tbl_VPR_Workable_Deregistration where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct state from dbo.tbl_VPR_Workable_Deregistration where tag = 'for audit' ";

            DataTable dtAudit = cls.GetData(qry);

            if (dtAudit.Rows.Count > 0)
            {
                for (int x = 0; x <= dtAudit.Rows.Count - 1; x++)
                {
                    auditStates += "'" + dtAudit.Rows[x][0].ToString() + "',";
                }

                auditStates = auditStates.Remove(auditStates.Length - 1, 1);
            }

            string qry2 = "select distinct city from tbl_VPR_Workable_Registration_PFC where tag = 'for audit' ";
            qry2 += "UNION ";
            qry2 += "select distinct city from tbl_VPR_Workable_Registration_REO where tag = 'for audit' ";
            qry2 += "UNION ";
            qry2 += "select distinct city from tbl_VPR_Workable_Registration_Property where tag = 'for audit' ";
            qry2 += "UNION ";
            qry2 += "select distinct city from tbl_VPR_Workable_Registration_PropReq where tag = 'for audit' ";
            qry2 += "UNION ";
            qry2 += "select distinct city from tbl_VPR_Workable_Registration_Cost where tag = 'for audit' ";
            qry2 += "UNION ";
            qry2 += "select distinct city from tbl_VPR_Workable where tag = 'for audit' ";
            qry2 += "UNION ";
            qry2 += "select distinct city from tbl_VPR_Workable_Inspection where tag = 'for audit' ";
            qry2 += "UNION ";
            qry2 += "select distinct city from tbl_VPR_Workable_Municipality where tag = 'for audit' ";
            qry += "UNION ";
            qry += "select distinct city from dbo.tbl_VPR_Workable_Deregistration where tag = 'for audit' ";

            DataTable dtAudit2 = cls.GetData(qry2);

            if (dtAudit2.Rows.Count > 0)
            {
                for (int y = 0; y <= dtAudit2.Rows.Count - 1; y++)
                {
                    auditCities += "'" + dtAudit2.Rows[y][0].ToString() + "',";
                }

                auditCities = auditCities.Remove(auditCities.Length - 1, 1);
            }
        }

        [WebMethod]
        public static string GetState()
        {
            clsConnection cls = new clsConnection();
            DataTable dtState = new DataTable();

            string query = "select DISTINCT(LTRIM(RTRIM(State))) [State] from tbl_DB_US_States (nolock) where isStateActive = 1 and State is not null and State <> '' and State in (" + auditStates + ") order by [State]";

            dtState = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtState } });
        }

        [WebMethod]
        public static string GetCity(string state)
        {
            clsConnection cls = new clsConnection();
            DataTable dtCity = new DataTable();

            string query = "";

            if (state != "")
            {
                query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and LTRIM(RTRIM(State)) = '" + state + "' and City is not null and City <> '' and City in (" + auditCities + ") order by [City]";
            }
            else
            {
                query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where isCityActive = 1 and City is not null and City <> '' order by [City]";
            }

            dtCity = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCity } });
        }

        [WebMethod]
        public static string GetZip(string city)
        {
            clsConnection cls = new clsConnection();
            DataTable dtZip = new DataTable();

            string query = "";

            if (city != "")
            {
                query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where isZipActive = 1 and LTRIM(RTRIM(City)) = '" + city + "' and ZipCode is not null and ZipCode <> '' order by [ZipCode]";
            }
            else
            {
                query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where isZipActive = 1 and ZipCode is not null and ZipCode <> '' order by [ZipCode]";
            }


            dtZip = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtZip } });
        }

        [WebMethod]
        public static string GetOrdinance(string state, string city, string hist)
        {
            clsConnection cls = new clsConnection();
            DataTable dtOrdinance = new DataTable();

            string query = "";

            if (hist != "" && hist != "undefined")
            {
                if (hist == "hist")
                {
                    query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date], added_by, date_added, modified_by, date_modified, deleted_by, date_deleted from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "'";
                }
                else
                {
                    query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date] from tbl_VPR_Workable_Ordinance (nolock) where id = '" + hist + "'";
                }
            }
            else
            {
                query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date] from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "' and date_deleted is null";
            }

            dtOrdinance = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtOrdinance } });
        }

        [WebMethod]
        public static void DeleteOrdinance(string id)
        {
            clsConnection cls = new clsConnection();

            string query = "update tbl_VPR_Workable_Ordinance set date_deleted = GETDATE() where id = '" + id + "'";

            try
            {
                cls.ExecuteQuery(query);

                ArrayList arrPages = new ArrayList();
                arrPages.Add("Ordinance Settings");
                arrPages.Add("");

                cls.FUNC.Audit("Deleted Ordinance Id: " + id, arrPages);
            }
            catch (Exception)
            {

            }
        }

        [WebMethod]
        public static string GetData(string state, string city)
        {
            clsConnection cls = new clsConnection();
            string qryRegPFC, qryRegREO, qryRegPropType, qryRegPropReq, qryRegCost, qryRegCont, qryInspect, qryMunicipal, qryDereg, qryNotif = "";

            DataTable dtRegPFC, dtRegREO, dtRegPropType, dtRegPropReq, dtRegCost, dtRegCont, dtInspect, dtMunicipal, dtDereg, dtNotif = new DataTable();

            qryRegPFC = "select top 1 * from tbl_VPR_Workable_Registration_PFC (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegPFC = cls.GetData(qryRegPFC);

            qryRegREO = "select top 1  * from tbl_VPR_Workable_Registration_REO (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegREO = cls.GetData(qryRegREO);

            qryRegPropType = "select top 1 * from tbl_VPR_Workable_Registration_Property (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegPropType = cls.GetData(qryRegPropType);

            qryRegPropReq = "select top 1 a.*, b.* from tbl_VPR_Workable_Registration_PropReq a (nolock) join tbl_VPR_Workable_Registration_PropReq_Contact b (nolock) " +
                            "on a.state = b.state where a.state = '" + state + "' and a.city = '" + city + "' order by a.id desc, a.date_modified desc";
            dtRegPropReq = cls.GetData(qryRegPropReq);

            qryRegCost = "select top 1 * from tbl_VPR_Workable_Registration_Cost (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegCost = cls.GetData(qryRegCost);

            qryRegCont = "select top 1 * from tbl_VPR_Workable (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtRegCont = cls.GetData(qryRegCont);

            qryInspect = "select top 1 * from tbl_VPR_Workable_Inspection (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtInspect = cls.GetData(qryInspect);

            qryMunicipal = "select top 1 * from tbl_VPR_Workable_Municipality (nolock) where state = '" + state + "' and city = '" + city + "' order by id desc, date_modified desc";
            dtMunicipal = cls.GetData(qryMunicipal);

            qryDereg = "select top 1 * from tbl_VPR_Workable_Deregistration (nolock) where state = '" + state + "' and city = '" + city + "'  order by id desc, date_modified desc";
            dtDereg = cls.GetData(qryDereg);

            qryNotif = "select top 1 * from tbl_VPR_Workable_Notification (nolock) where state = '" + state + "' and city = '" + city + "'  order by id desc, date_modified desc";
            dtNotif = cls.GetData(qryNotif);

            return JsonConvert.SerializeObject(new
            {
                Success = true,
                Message = "Success",
                data = new
                {
                    regPFC = dtRegPFC,
                    regREO = dtRegREO,
                    regPropType = dtRegPropType,
                    regPropReq = dtRegPropReq,
                    regCost = dtRegCost,
                    regCont = dtRegCont,
                    inspect = dtInspect,
                    municipal = dtMunicipal,
                    dereg = dtDereg,
                    notif = dtNotif
                }
            });
        }

        [WebMethod]
        public static string SaveWorkable(List<string> data, string user)
        {
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            string query = "";
            string tag = "";
            if (data[0] == "regPFC")
            {
                query = "update tbl_VPR_Workable_Registration_PFC set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Registration_PFC ([state], city, pfc_city_notice, pfc_default, def_reg_timeline1, " +
                        "def_reg_timeline2, def_reg_timeline3, def_reg_timeline4, pfc_def_foreclosure_timeline1, pfc_def_foreclosure_timeline2, " +
                        "pfc_def_foreclosure_timeline3, pfc_def_foreclosure_timeline4, pfc_foreclosure_vacant, pfc_foreclosure_vacant_timeline1, " +
                        "pfc_foreclosure_vacant_timeline2, pfc_foreclosure_vacant_timeline3, pfc_foreclosure_vacant_timeline4, pfc_boarded, " +
                        "pfc_foreclosure, pfc_other_timeline1, pfc_other_timeline2, pfc_other_timeline3, pfc_other_timeline4, pfc_vacant, " +
                        "pfc_vacant_timeline1, pfc_vacant_timeline2, pfc_vacant_timeline3, pfc_vacant_timeline4, pfc_code_violation, " +
                        "special_requirements, payment_type, type_of_registration, vms_renewal, upload_path, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                        "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                        "'" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', " +
                        "'" + data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', " +
                        "'" + data[25].ToString() + "', '" + data[26].ToString() + "', '" + data[27].ToString() + "', '" + data[28].ToString() + "', " +
                        "'" + data[29].ToString() + "', '" + data[30].ToString() + "', '" + data[31].ToString() + "', '" + data[32].ToString() + "', " +
                        "'" + data[33].ToString() + "', '" + data[34].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE())";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Registration");
                        arrPages.Add("PFC");

                        cls.FUNC.Audit("Audited PFC settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regREO")
            {
                query = "update tbl_VPR_Workable_Registration_REO set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Registration_REO ([state], city, reo_bank_owed, reo_bank_owed_timeline1, " +
                        "reo_bank_owed_timeline2, reo_bank_owed_timeline3, reo_bank_owed_timeline4, reo_boarded_only, reo_city_notice, " +
                        "reo_code_violation, reo_distressed_abandoned, reo_distressed_abandoned1, reo_distressed_abandoned2, " +
                        "reo_distressed_abandoned3, reo_distressed_abandoned4, reo_other_timeline1, reo_other_timeline2, reo_other_timeline3, " +
                        "reo_other_timeline4, reo_vacant, reo_vacant_timeline1, reo_vacant_timeline2, reo_vacant_timeline3, reo_vacant_timeline4, " +
                        "rental_registration, rental_form, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                        "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                        "'" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', " +
                        "'" + data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', " +
                        "'" + data[25].ToString() + "', '" + data[26].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE())";

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Registration");
                        arrPages.Add("REO");

                        cls.FUNC.Audit("Audited REO settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }

            }
            else if (data[0] == "regProperty")
            {
                query = "update tbl_VPR_Workable_Registration_Property set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Registration_Property ([state], city, residential,rental,commercial,condo, " +
                         "townhome,vacant_lot,mobile_home,tag, modified_by, date_modified) values " +
                         "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                         "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                         "'" + data[9].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE())";

                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Registration");
                        arrPages.Add("Property Type");

                        cls.FUNC.Audit("Audited Property Type settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }

            }
            else if (data[0] == "regPropReq")
            {

                query = "update tbl_VPR_Workable_Registration_PropReq set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";
                query += "update tbl_VPR_Workable_Registration_PropReq_Contact set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Registration_PropReq ([state], city,  addn_info, presale_definition, first_time_vacancy_date, " +
                        "secured_required, local_contact_required, additional_signage_required, pictures_required, gse_exclusion, mobile_vin_number_required, " +
                        "insurance_required, parcel_number_required, foreclosure_action_information_needed, legal_description_required, " +
                        "foreclosure_case_information_needed, block_and_lot_number_required, foreclosure_deed_required, attorney_information_required, " +
                        "bond_required, broker_information_required_if_reo, bond_amount, mortgage_contact_name_required, maintenance_plan_required, " +
                        "client_tax_number_required, no_trespass_form_required, signature_required, utility_information_required, notarization_required, " +
                        "winterization_required, recent_inspection_date, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                        "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                        "'" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', " +
                        "'" + data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', " +
                        "'" + data[25].ToString() + "', '" + data[26].ToString() + "', '" + data[27].ToString() + "', '" + data[28].ToString() + "', " +
                        "'" + data[29].ToString() + "', '" + data[30].ToString() + "', '" + data[31].ToString() + "', 'for approval', '" + user.ToString() + "', " +
                        "GETDATE());";

                query += "insert into tbl_VPR_Workable_Registration_PropReq_Contact (state, city, lcicompany_name, lcifirst_name, lcilast_name, lcititle, " +
                        "lcibusiness_license_num, lciphone_num1, lciphone_num2, lcibusiness_phone_num1, lcibusiness_phone_num2, lciemergency_phone_num1, " +
                        "lciemergency_phone_num2, lcifax_num1, lcifax_num2, lcicell_num1, lcicell_num2, lciemail, lcistreet, lcistate, lcicity, lcizip, " +
                        "lcihours_from, lcihours_to, tag) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', " +
                        "'" + data[32].ToString() + "', '" + data[33].ToString() + "', '" + data[34].ToString() + "', '" + data[35].ToString() + "', " +
                        "'" + data[36].ToString() + "', '" + data[37].ToString() + "', '" + data[38].ToString() + "', '" + data[39].ToString() + "', " +
                        "'" + data[40].ToString() + "', '" + data[41].ToString() + "', '" + data[42].ToString() + "', '" + data[43].ToString() + "', " +
                        "'" + data[44].ToString() + "', '" + data[45].ToString() + "', '" + data[46].ToString() + "', '" + data[47].ToString() + "', " +
                        "'" + data[48].ToString() + "', '" + data[49].ToString() + "', '" + data[50].ToString() + "', '" + data[51].ToString() + "', " +
                        "'" + data[52].ToString() + "', '" + data[53].ToString() + "', 'for approval')";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Registration");
                        arrPages.Add("Property Requirements");

                        cls.FUNC.Audit("Audited Property Requirements settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regCost")
            {
                query = "update tbl_VPR_Workable_Registration_Cost set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Registration_Cost ([state], city,  reg_cost, reg_cost_amt, reg_cost_curr, reg_cost_standard, " +
                        "is_renewal_cost_escal1, renewal_fee_amt, renewal_fee_curr, com_reg_fee, com_reg_curr, com_fee_standard, is_renew_cost_escal2, " +
                        "com_renew_cost_amt, com_renew_cost_curr, is_reg_cost_standard, reg_escal_service_type, reg_escal_amount, reg_escal_curr, " +
                        "reg_escal_succeeding, com_escal_service_type, com_escal_amount, com_escal_curr, com_escal_succeeding, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "',  '" + data[6].ToString() + "',  '" + data[7].ToString() + "',  '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "',  '" + data[10].ToString() + "',  '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                        "'" + data[13].ToString() + "',  '" + data[14].ToString() + "',  '" + data[15].ToString() + "',  '" + data[16].ToString() + "', " +
                        "'" + data[17].ToString() + "',  '" + data[18].ToString() + "',  '" + data[19].ToString() + "',  '" + data[20].ToString() + "', " +
                        "'" + data[21].ToString() + "',  '" + data[22].ToString() + "',  '" + data[23].ToString() + "',  '" + data[24].ToString() + "', " +
                        "'for approval', '" + user.ToString() + "', GETDATE());";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Registration");
                        arrPages.Add("Cost");

                        cls.FUNC.Audit("Audited Cost settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "regCont")
            {
                query = "update tbl_VPR_Workable set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable ([state], city, cont_reg, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE());";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Registration");
                        arrPages.Add("Continuing Registration");

                        cls.FUNC.Audit("Audited Continuing Registration for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "inspect")
            {
                query = "update tbl_VPR_Workable_Inspection set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Inspection ([state], city, inspection_update_req, inspection_criteria_occ, " +
                        "inspection_criteria_vac, inspection_fee_payment_freq, inspection_fee_req, inspection_fee_amount, inspection_fee_curr, " +
                        "inspection_reporting_freq, criteria_occ_freq, criteria_occ_cycle_1, criteria_occ_cycle_2, criteria_occ_cycle_refresh_time, " +
                        "criteria_occ_cycle_day_1, criteria_occ_cycle_day_2, criteria_occ_cycle_week_1, criteria_occ_cycle_week_2, " +
                        "criteria_occ_cycle_month_1, criteria_occ_cycle_month_2, criteria_occ_cycle_month_3, criteria_occ_cycle_year_1, " +
                        "criteria_occ_cycle_year_2, criteria_vac_freq, criteria_vac_cycle_1, criteria_vac_cycle_2, criteria_vac_cycle_refresh_time, " +
                        "criteria_vac_cycle_day_1, criteria_vac_cycle_day_2, criteria_vac_cycle_week_1, criteria_vac_cycle_week_2, " +
                        "criteria_vac_cycle_month_1, criteria_vac_cycle_month_2, criteria_vac_cycle_month_3, criteria_vac_cycle_year_1, " +
                        "criteria_vac_cycle_year_2, fee_payment_freq, fee_payment_cycle_1, fee_payment_cycle_2, fee_payment_cycle_refresh_time, " +
                        "fee_payment_cycle_day_1, fee_payment_cycle_day_2, fee_payment_cycle_week_1, fee_payment_cycle_week_2, fee_payment_cycle_month_1, " +
                        "fee_payment_cycle_month_2, fee_payment_cycle_month_3, fee_payment_cycle_year_1, fee_payment_cycle_year_2, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                        "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                        "'" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', " +
                        "'" + data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', " +
                        "'" + data[25].ToString() + "', '" + data[26].ToString() + "', '" + data[27].ToString() + "', '" + data[28].ToString() + "', " +
                        "'" + data[29].ToString() + "', '" + data[30].ToString() + "', '" + data[31].ToString() + "', '" + data[32].ToString() + "', " +
                        "'" + data[33].ToString() + "', '" + data[34].ToString() + "', '" + data[35].ToString() + "', '" + data[36].ToString() + "', " +
                        "'" + data[37].ToString() + "', '" + data[38].ToString() + "', '" + data[39].ToString() + "', '" + data[40].ToString() + "', " +
                        "'" + data[41].ToString() + "', '" + data[42].ToString() + "', '" + data[43].ToString() + "', '" + data[44].ToString() + "', " +
                        "'" + data[45].ToString() + "', '" + data[46].ToString() + "', '" + data[47].ToString() + "', '" + data[48].ToString() + "', " +
                        "'" + data[49].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE());";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Inspection");
                        arrPages.Add("");

                        cls.FUNC.Audit("Audited Inspection settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }

                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "municipalityInfo")
            {
                query = "update tbl_VPR_Workable_Municipality set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Municipality ([state], city, municipality_department, municipality_phone_num, " +
                        "municipality_email, municipality_st, municipality_city, municipality_state, municipality_zip, contact_person, title, " +
                        "department, phone_num, email, address, ops_hrs_from, ops_hrs_to, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                        "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
                        "'" + data[17].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE())";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Municipality Contact Information");
                        arrPages.Add("");

                        cls.FUNC.Audit("Audited Municipality Contact Information settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "dereg")
            {
                query = "update tbl_VPR_Workable_Deregistration set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Deregistration ([state], city, dereg_req, conveyed, occupied, how_to_dereg, upload_file, " +
                        "new_owner_info_req, proof_of_conveyance_req, date_of_sale_req, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE())";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Deregistration");
                        arrPages.Add("");

                        cls.FUNC.Audit("Audited Deregistration settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "ordinance")
            {
                query = "insert into tbl_VPR_Workable_Ordinance (state, city, ordinance_num, ordinance_name, description, section, source, enacted_date, revision_date, ordinance_file, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + user.ToString() + "', GETDATE())";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Ordinance Settings");
                        arrPages.Add("");

                        cls.FUNC.Audit("Added new Ordinance settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "ordinance2")
            {
                query = "update tbl_VPR_Workable_Ordinance set state = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', " +
                                "ordinance_num = '" + data[3].ToString() + "', ordinance_name = '" + data[4].ToString() + "', description = '" + data[5].ToString() + "', " +
                                "section = '" + data[6].ToString() + "', source = '" + data[7].ToString() + "', enacted_date = '" + data[8].ToString() + "', " +
                                "revision_date = '" + data[9].ToString() + "', ordinance_file = '" + data[10].ToString() + "', " +
                                "modified_by = '" + user.ToString() + "', date_modified = GETDATE() where id = '" + data[11].ToString() + "'";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Ordinance Settings");
                        arrPages.Add("");

                        cls.FUNC.Audit("Updated Ordinance settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "zip")
            {
                query = "update tbl_DB_US_States set " +
                                "isZipActive = " + data[4].ToString() + " " +
                                "where state = '" + data[1].ToString() + "' and " +
                                "city = '" + data[2].ToString() + "' and " +
                                "ZipCode = '" + data[3].ToString() + "'";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Zip Codes");
                        arrPages.Add("");

                        if (data[4].ToString() == "1")
                        {
                            cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Active", arrPages);
                        }
                        else
                        {
                            cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Inactive", arrPages);
                        }

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }
            else if (data[0] == "notif")
            {
                query = "update tbl_VPR_Workable_Notification set tag = 'audited' where tag = 'for audit' and state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

                query += "insert into tbl_VPR_Workable_Notification ([state], city, reg_send_reminder_num, reg_send_reminder_type, reg_send_reminder_days, " +
                        "reg_send_reminder_before, reg_send_reminder_time_frame, ren_send_reminder_num, ren_send_reminder_type, ren_send_reminder_days, " +
                        "ren_send_reminder_before, ren_send_reminder_time_frame, web_notif_url, tag, modified_by, date_modified) values " +
                        "('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
                        "'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
                        "'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
                        "'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " + 
                        "'" + data[17].ToString() + "', 'for approval', '" + user.ToString() + "', GETDATE())";
                try
                {
                    int exec = cls.ExecuteQuery(query);

                    if (exec > 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("Notification");
                        arrPages.Add("");

                        cls.FUNC.Audit("Audited Notification settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);

                        tag = "1";
                    }
                    else
                    {
                        tag = "0";
                    }
                }
                catch (Exception)
                {
                    tag = "0";
                }
            }

            return tag;
        }
    }
}