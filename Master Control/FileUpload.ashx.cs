﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace Master_Control
{
	/// <summary>
	/// Summary description for FileUpload
	/// </summary>
	public class FileUpload : IHttpHandler
	{

		public void ProcessRequest(HttpContext context)
		{

			clsConnection cls = new clsConnection();

            //string query = "select id, [filename] from tbl_VPR_Mapping_PDF order by id desc";

            //DataTable dt = new DataTable();
            //dt = cls.GetData(query);

            //string fn = dt.Rows[0][1].ToString();

			if (context.Request.Files.Count > 0)
			{
				HttpFileCollection files = context.Request.Files;
                string fname = files.AllKeys[0].ToString();
				for (int i = 0; i < files.Count; i++)
				{
                    HttpPostedFile file = files[i];
					
                    //if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    //{
                    //    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    //}
                    //else
                    //{
                    //    fname = fname;
                    //}

                    string path = Path.Combine(context.Server.MapPath("~/Files/PDF Files/"), fname + ".pdf");
					//string path = Path.Combine(@"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\PDF Files\" + dt.Rows[0]["filename"].ToString() + ".pdf");

					file.SaveAs(path);

					////string update = "update tbl_VPR_Mapping_PDF set pdf_file = '" + fname + "' where id = " + dt.Rows[0][0].ToString();

					//try
					//{
					//	cls.ExecuteQuery(update);
					//}
					//catch (Exception) { }

				}
			}
			context.Response.ContentType = "text/plain";
			context.Response.Write("File Uploaded Successfully!");
		}

		public bool IsReusable
		{
			get
			{
				return false;
			}
		}
	}
}