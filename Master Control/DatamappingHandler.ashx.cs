﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Master_Control
{
    /// <summary>
    /// Summary description for DatamappingHandler
    /// </summary>
    public class DatamappingHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string dirFullPath = HttpContext.Current.Server.MapPath("~/flatfiles/");
			//string sharedPath = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\";
            string[] files;
            int numFiles;
            files = System.IO.Directory.GetFiles(dirFullPath);
            numFiles = files.Length + 1;
            string str_image = "";
            string fileName = "", pathToSave_100 = "";
            string fileExtension = "";
            string file_name = (DateTime.Now.Year.ToString() + DateTime.Now.Day.ToString() + DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString() + DateTime.Now.Millisecond.ToString()).Trim();
            foreach (string s in context.Request.Files){
                HttpPostedFile file = context.Request.Files[s];
                fileName = file.FileName;
                fileExtension = file.ContentType;
                if (!String.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    str_image = Convert.ToString(file_name) + fileExtension;
                    pathToSave_100 = HttpContext.Current.Server.MapPath("~/flatfiles/") + str_image;
					//string pathToSave = sharedPath + fileName;
					//file.SaveAs(pathToSave);
                    file.SaveAs(pathToSave_100);
                }
            }
			context.Response.Write(fileName + "*" + file_name + fileExtension);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}