﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control
{
    public class AuditClass
    {
        public string id { get; set; }
        public string page { get; set; }
        public string sub_page { get; set; }
        public string sub_sub_page { get; set; }
        public string action { get; set; }
        public string action_by { get; set; }
        public string action_date { get; set; }
        public string action_time { get; set; }

    }

    public class AuditList
    {
        public List<AuditClass> tblAuditList { get; set; }
    }

    public class AuditParam
    {
        public string timeFrame { get; set; }
        public string by { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
    }
}