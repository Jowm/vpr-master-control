﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control
{
    public class ProcessSettings
    {
    }
    public class ProcessLocation
    {
        public string id { get; set; }
        public string stateName { get; set; }
        public string cityName { get; set; }
        public string setting { get; set; }
        public string isActive { get; set; }
    }
    public class ProcessFunction
    {
        public string funcName { get; set; }
    }
}