﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MasterReports
{
    public class clsCreateReport
    {
        public List<fields> fields { get; set; }
        public List<whereData> where { get; set; }
    }

    public class fields {
        public string name { get; set; }
        public string alias { get; set; }
    }

    public class whereData {
        public string field { get; set; }
        public string field_operator { get; set; }
        public string field_values { get; set; }
    }

	public class Settings
	{
		public string id { get; set; }
		public string type { get; set; }

		public List<SettingsVal> tblSettings { get; set; }
	}

	public class SettingsVal
	{
		public string id { get; set; }
		public string saveAs { get; set; }
		public string reportName { get; set; }
		public string description { get; set; }
		public string dateValid { get; set; }
		public string type { get; set; }
		public string bs { get; set; }
		public string bu { get; set; }
		public string empCat { get; set; }
		public string empname { get; set; }
	}
}