﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control
{
	public class PrioClass
	{
		public List<PrioMain> tblPrioMain { get; set; }
		public List<PrioList> tblPrio { get; set; }
	}

	public class PrioList
	{
		public string id { get; set; }
		public string isActive { get; set; }
		public string sortBy { get; set; }
		public string orderBy { get; set; }
		public string appliedTo { get; set; }
	}

    public class PrioMain
    {
        public string id { get; set; }
        public string level { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string location { get; set; }
        public string bunit { get; set; }
        public string bsegment { get; set; }
        public string actOn { get; set; }
        public string actUntil { get; set; }
        public string keepAct { get; set; }
        public string users { get; set; }
        public string do_not_end { get; set; }
        public string created_date { get; set; }
        public string created_by { get; set; }
        
    }

    public class PrioMainList
    {
        public List<PrioMain> tblPrioMain { get; set; }
    }
}