﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Master_Control
{
    public class myParameters
    {
        public string ParameterName { get; set; }
        public dynamic Value
        {
            get
            {
                return _myProperty;
            }
            set
            {
                if ((value == null))
                {
                    _myProperty = "";
                }
                else
                {
                    _myProperty = value;
                }
            }
        }
        public SqlDbType mytype { get; set; }

        public dynamic _myProperty { get; set; }
    }
}