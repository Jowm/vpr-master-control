﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Master_Control.Models
{
    public class Status
    {
        public string StatusId { get; set; }
        public string StatusValue { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Electricity { get; set; }
        public string Water { get; set; }
        public string Gas { get; set; }
        public string Result { get; set; }
    }
}