﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.NetworkInformation;
using System.Web;
using System.Web.Script.Serialization;

namespace Insurance_Maker.App_Code
{
	public class clsFunctions
	{

		public string[] idHTML = new string[] { "COUNTRY", "REGIONSTATE", "CITY", "RESIDENTIALSTATUS", "NOOFCARSOWNED", "TYPEOFEMPLOYMENT", "RANK", "INDUSTRY", "TENUREYRS", "COMPANY", "SECURITYBANKACCOUNTHOLDER", "CREDITCARDHOLDER", "EXITPAYRANGE", "SALARYRANGE", "CREDITSCORE", "APPICANTCRIMNALOFFENSE", "NOOFOFFENSE", "DISACTIONCOUNT", "ATTRIPOSSISCORE", "ATTRIPOSSIINDEX", "GENDER", "AGERANGE", "TAXSTATUS", "DEPENDENTDET", "AGE", "HIGHESTEDUCATTAIN", "LOANTYPEPERSONAL", "LOANTYPECAR", "LOANTYPEHOUSING", "LOANOFFEREDBY", "LOANOFFEREDDATE", "LOANOFFEREDVIEWED", "LOANOFFEREDACCEPTED", "undefined", "UNDERWRITER", "TOTALLOANAMOUNT", "NUMBEROFLOANS", "DELINQUENCYRATE", "TOTALRECEIVABLETILDATE", "TOTALRECEIVED" };
		public string[] idDB = new string[] { "emp.country", "emp.region", "emp.city", "emp.ResidentialStatus", "emp.car", "emp.TypeOfEmployment", "emp.Rank", "emp.Industry", "tenure_yrs", "emp.Company", "emp.SecurityBankHolder", "emp.CreditCard", "emp.ExitPay", "emp.[Monthly Rate] [salaryrange]", "emp.[Credit Score]", "emp.CriminalOffense", "emp.CriminalOffense", "emp.DisciplinaryAction", "CAST(emp.[Atrrition Probability Rate] as varchar) + '%' [AttritionProbabilityScore]", "CAST(emp.[Employee Reliability Index] as varchar) + '%' [EmployeeReliabilityIndex]", "emp.Gender", "DATEDIFF(YEAR,emp.DOB,GETDATE()) [age]", "emp.[Tax Status]", "'' [dependent1]", "'' [dependent1_age]", "emp.Education", "LOANTYPEPERSONAL", "LOANTYPECAR", "LOANTYPEHOUSING", "created_by", "CAST(date_created as varchar) [Date]", "CASE WHEN Ads.Viewed IS NULL then 0 ELSE Ads.Viewed end [Viewed] ", "CASE WHEN Ads.Accepted IS NULL THEN 0 ELSE Ads.Accepted END [Accepted] ", "", "Ads.underwriter [Underwriter]", "left(CONVERT(varchar(50), CAST(Ads.TotalLoanAmount AS money), 1), LEN(CONVERT(varchar(50), CAST(Ads.TotalLoanAmount AS money), 1)) -3) [Total Loan Amount]", "left(CONVERT(varchar(50), CAST(Ads.NumberOfLoans AS money), 1), LEN(CONVERT(varchar(50), CAST(Ads.NumberOfLoans AS money), 1)) -3) [Number of Loans]", "Ads.DelinquencyRate [Delinquency Rate]", "left(CONVERT(varchar(50), CAST(Ads.TotalReceivableTilDate AS money), 1), LEN(CONVERT(varchar(50), CAST(Ads.TotalReceivableTilDate AS money), 1)) -3) [Total Receivable Til Date]", "left(CONVERT(varchar(50), CAST(Ads.TotalReceived AS money), 1), LEN(CONVERT(varchar(50), CAST(Ads.TotalReceived AS money), 1)) -3) [Total Received]" };
		public string[] idDBNoAlias = new string[] { "emp.country", "emp.region", "emp.city", "emp.ResidentialStatus", "emp.car", "emp.TypeOfEmployment", "emp.Rank", "emp.Industry", "tenure_yrs", "emp.Company", "emp.SecurityBankHolder", "emp.CreditCard", "emp.ExitPay", "emp.[Monthly Rate]", "emp.[Credit Score]", "emp.CriminalOffense", "emp.CriminalOffense", "emp.DisciplinaryAction", "emp.[Atrrition Probability Rate]", "emp.[Employee Reliability Index]", "emp.Gender", "age", "emp.[Tax Status]", "'' [dependent1]", "'' [dependent1_age]", "emp.Education", "LOANTYPEPERSONAL", "LOANTYPECAR", "LOANTYPEHOUSING", "created_by", "date_created", "CASE WHEN Ads.Viewed IS NULL then 0 ELSE Ads.Viewed end [Viewed] ", "CASE WHEN Ads.Accepted IS NULL THEN 0 ELSE Ads.Accepted END [Accepted] ", "", "Ads.underwriter", "Ads.TotalLoanAmount", "Ads.NumberOfLoans", "Ads.DelinquencyRate", "Ads.TotalReceivableTilDate", "Ads.TotalReceived" };


		public string sqlString(string str)
		{

			if (str != null && str != string.Empty && str != "")
			{
				str = "'" + str + "'";
			}
			else
			{
				str = "NULL";
			}

			return str;
		}

		public string getName(string str)
		{

			string[] vidHTML = idHTML;
			string[] vidDB = idDB;
			var rt = "";
			try
			{
				rt = idHTML[Array.IndexOf(idDB, str)].ToString();
			}
			catch (Exception)
			{
				rt = str;
			}

			return rt;
		}

		public string FetchMacId()
		{
			string macAddresses = "";

			foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
			{
				if (nic.OperationalStatus == OperationalStatus.Up)
				{
					macAddresses += nic.GetPhysicalAddress().ToString();
					break;
				}
			}
			return macAddresses;
		}


        public int Audit(string user_action, ArrayList pages)
        {
            try
            {
                var rt = 0;
                var user = HttpContext.Current.Session["user"].ToString();
                var url = HttpContext.Current.Request.Url.AbsolutePath.Remove(0, 1);
                var page = "";
                if (url.IndexOf(".aspx") < 0)
                {
                    page = "List.aspx";
                }
                else
                {
                    page = url.Remove(url.IndexOf(".aspx/"));
                }


                var cls = new clsConnection();
                var sql = "INSERT INTO tbl_VPR_Audit(page, sub_page, sub_sub_page, action, action_by, action_date, action_time) VALUES";

                string asd = "";

                foreach (string s in pages)
                {
                    asd += sqlString(s) + ",";
                }

                asd = asd.Remove(asd.Length - 1, 1);

                sql += "(" + sqlString(page) + ", " + asd + ", " + sqlString(user_action) + ", " + sqlString(user) + ", CAST(GETDATE() as date), CAST(GETDATE() as time))";

                rt = cls.ExecuteQuery(sql);

                return rt;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return -1;
            }

        }


		public string DataTableToJSONWithJavaScriptSerializer(DataTable table)
		{
			JavaScriptSerializer jsSerializer = new JavaScriptSerializer();
			List<Dictionary<string, object>> parentRow = new List<Dictionary<string, object>>();
			Dictionary<string, object> childRow;
			foreach (DataRow row in table.Rows)
			{
				childRow = new Dictionary<string, object>();
				foreach (DataColumn col in table.Columns)
				{
					childRow.Add(col.ColumnName, row[col]);
				}
				parentRow.Add(childRow);
			}
			return jsSerializer.Serialize(parentRow);
		}

		public DataTable convertAllColumnsToString(DataTable dt)
		{
			DataTable dtCloned = dt.Clone();
			for (int i = 0; i <= dt.Columns.Count - 1; i++)
			{
				dtCloned.Columns[i].DataType = typeof(string);
			}

			foreach (DataRow row in dt.Rows)
			{
				dtCloned.ImportRow(row);
			}

			return dtCloned;
		}

		public List<string> GetAllColumnName(DataTable dt)
		{
			var rt = new List<string>();
			foreach (DataColumn column in dt.Columns)
			{
				rt.Add(column.ColumnName);
			}

			return rt;
		}

        public void VPR_Audit(string user_action,string details)
        {

            try
            {
                var rt = 0;
                var user = HttpContext.Current.Session["userID"].ToString();
                var url = HttpContext.Current.Request.Url.AbsolutePath.Remove(0, 1);
                var page = url.Remove(url.IndexOf(".aspx/"));

                var cls = new clsConnection();
                //var sql = "INSERT INTO tbl_InsuranceMaker_Audit(Date_Occurred, UserID, Page, User_Action, Details) VALUES";

                //sql += "(GETDATE(), " + sqlString(user) + "," + sqlString(page) + "," + sqlString(user_action) + "," + sqlString(details) + ")";

                //cls.ExecuteQuery(sql);

                
            }
            catch (Exception)
            {
                
            }
        }

	}
}