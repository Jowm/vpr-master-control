﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class Process : System.Web.UI.Page
    {
        public static string stateName;
        public static DataTable dtCity;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<ProcessLocation> popState()
        {
            clsConnection cls = new clsConnection();
            List<ProcessLocation> ls = new List<ProcessLocation>();
            DataTable dt = new DataTable();

            dt = cls.GetData("select distinct(RTRIM(LTRIM(State))) from tbl_DB_US_States where isStateActive = 1 order by RTRIM(LTRIM(State))");

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProcessLocation st = new ProcessLocation();
                st.stateName = dt.Rows[i][0].ToString();
                ls.Add(st);

            }

            return ls;

        }


        [WebMethod]
        public static List<ProcessLocation> popCity(string state)
        {
            clsConnection cls = new clsConnection();
            List<ProcessLocation> ls = new List<ProcessLocation>();
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            string qry = "select distinct(City) from tbl_DB_US_States a where isCityActive=1 and RTRIM(LTRIM(a.State)) = '"+ state +"'";
            string qry2 = "select * from tbl_VPR_Process_AssignCity where state = '"+ state +"' order by city";
            dt = cls.GetData(qry);
            dt2 = cls.GetData(qry2);

            if(dt2.Rows.Count > 0)
            {
                dtCity = new DataTable();
                dtCity = dt2;
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    ProcessLocation st = new ProcessLocation();
                    st.id = dt2.Rows[i][0].ToString();
                    st.cityName = dt2.Rows[i][1].ToString();
                    st.setting = dt2.Rows[i][3].ToString();
                    st.isActive = dt2.Rows[i][4].ToString();
                    ls.Add(st);

                }
            }
            else
            {
                dtCity = dt2;
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    ProcessLocation st = new ProcessLocation();
                    st.id = state.Replace(" ", "") + "_" + dt.Rows[i][0].ToString().Replace(" ", "");
                    st.cityName = dt.Rows[i][0].ToString();
                    st.setting = "PDF";
                    st.isActive = "False";
                    ls.Add(st);

                }
            }
            

            stateName = state;
            return ls;

        }

        [WebMethod]
        public static List<ProcessFunction> popFunction()
        {
            List<ProcessFunction> pf = new List<ProcessFunction>();
            clsConnection cls = new clsConnection();
            DataTable dt = new DataTable();
            dt = cls.GetData("select funcname from tbl_VPR_Access_Role_Function where functype = 'Skill Level' and tag = 'VPR'");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ProcessFunction p = new ProcessFunction();
                p.funcName = dt.Rows[i][0].ToString();
                pf.Add(p);
            }

            return pf;
        }

        [WebMethod]
        public static string updateProcess(List<ProcessLocation> pl)
        {
            clsConnection cls = new clsConnection();
            string qry = "";
            DataTable dt = new DataTable();
            
            if(dtCity.Rows.Count > 0 ) 
            {
                for (int i = 0; i < pl.Count; i++)
                {
                        qry += "UPDATE tbl_VPR_Process_AssignCity set isActive ='" + pl[i].isActive + "', setting = '" + pl[i].setting + "'  where state = '" + stateName + "' and city='" + pl[i].cityName + "';";
                }
            }
            else
            {
                for (int i = 0; i < pl.Count; i++)
                {
                        qry += "INSERT tbl_VPR_Process_AssignCity(state,city,setting,isActive)";
                        qry += " Values('" + stateName + "','" + pl[i].cityName + "','" + pl[i].setting + "'," + pl[i].isActive + ")";
   
                }
            }
           
            cls.ExecuteQuery(qry);


            ArrayList arrPages = new ArrayList();
            arrPages.Add("User");
            arrPages.Add("Access");


            cls.FUNC.Audit("Updated Process", arrPages);

            return stateName;
        }

    }
}