﻿$(document).ready(function () {
    //asd
    getState();
    $('.datepicker').datepicker({
        autoclose: true
    });

    $(".timepicker").timepicker({
        showInputs: false
    });

    $('#slctAddnInfo').multiselect({
        numberDisplayed: 1,
        includeSelectAllOption: true,
    });

    $("input[type=number]").keypress(function (event) {
        if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
            return false;
        }
    });

    var x;

    $('#slctRenewEveryNum, #slctREORenewEveryNum').empty();
    $('#slctRenewEveryNum, #slctREORenewEveryNum').append('<option value="" selected="selected">N/A</option>');
    for (x = 1; x <= 100; x++) {
        $('#slctRenewEveryNum, #slctREORenewEveryNum').append('<option value="' + x + '">' + x + '</option>');
    }

    var months = getMonths();

    $('#slctRenewOnMonths, #slctREORenewOnMonths').empty();
    $('#slctRenewOnMonths, #slctREORenewOnMonths').append('<option value="" selected="selected">N/A</option>');
    $.each(months, function (idx, val) {
        $('#slctRenewOnMonths, #slctREORenewOnMonths').append('<option value="' + val + '">' + val + '</option>');
    });

    var x;

    $('#slctRenewOnNum, #slctREORenewOnNum').empty();
    $('#slctRenewOnNum, #slctREORenewOnNum').append('<option value="" selected="selected">N/A</option>');
    for (x = 1; x <= 31; x++) {
        $('#slctRenewOnNum, #slctREORenewOnNum').append('<option value="' + x + '">' + x + '</option>');
    }

    var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
    var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
    var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
    var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

    rdRenewal1.prop('checked', true);
    divRenewOn1.prop('disabled', true);
    divRenewOn2.prop('disabled', true);

    $('.renewYes').hide();

    var rdREORenewal1 = $('input[name=rdREORenewal]:eq(0)');
    var rdREORenewal2 = $('input[name=rdREORenewal]:eq(1)');
    var divREORenewOn1 = rdREORenewal2.closest('div').next().find('select');
    var divREORenewOn2 = rdREORenewal2.closest('div').next().next().find('select');

    rdREORenewal1.prop('checked', true);
    divREORenewOn1.prop('disabled', true);
    divREORenewOn2.prop('disabled', true);

    $('.renewREOYes').hide();

    

});

$("#Reordvery").click(function () {

    $('#slctREORenewOnMonths').prop('disabled', true);
    $('#slctREORenewOnNum').prop('disabled', true);



    $('#slctREORenewEveryNum').prop('disabled', false);
    $('#slctREORenewEveryYears').prop('disabled', false);

    $('#slctREORenewOnMonths').val('');
    $('#slctREORenewOnNum').val('');
});

$("#Reordon").click(function () {

    $('#slctREORenewOnMonths').prop('disabled', false);
    $('#slctREORenewOnNum').prop('disabled', false);



    $('#slctREORenewEveryNum').prop('disabled', true);
    $('#slctREORenewEveryYears').prop('disabled', true);

    $('#slctREORenewEveryNum').val('');
    $('#slctREORenewEveryYears').val('');
});


$("#RDEVERY").click(function () {

    $('#slctRenewOnMonths').prop('disabled', true);
    $('#slctRenewOnNum').prop('disabled', true);



    $('#slctRenewEveryNum').prop('disabled', false);
    $('#slctRenewEveryYears').prop('disabled', false);

    $('#slctRenewOnMonths').val('');
    $('#slctRenewOnNum').val('');
});

$("#RDON").click(function () {

    $('#slctRenewOnMonths').prop('disabled', false);
    $('#slctRenewOnNum').prop('disabled', false);



    $('#slctRenewEveryNum').prop('disabled', true);
    $('#slctRenewEveryYears').prop('disabled', true);

    $('#slctRenewEveryNum').val('');
    $('#slctRenewEveryYears').val('');
});

function getSettings() {
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetSettings',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                console.log(records);
                $('.slctCopyMuni').empty();
                $('.slctCopyMuni').append('<option value="">--Select One--</option>');
                $.each(records, function (idx, val) {
                    $('.slctCopyMuni').append('<option value="' + val.municipality_code + '">' + val.municipality_name + '</option>');
                });
            }
        }
    });
}

function getState() {
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblState tbody').empty();
                $.each(records, function (idx, val) {
                    $('#tblState tbody').append(
						'<tr class="rowHover" onclick="getMunicipality(this);">' +
                            //'<td data-id=>' + val.State + '</td>' +
							'<td data-id="' + val.State_Abbr + '">' + val.State + '</td>' +
						'</tr>'
					);
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function getMunicipality(val) {

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

    //var state = $(val).find('td').text();
    var state = $(val).find('td').attr('data-id');
    $.each($('#tblState tbody tr'), function () {
        $(this).removeClass('activeRow');
    })
    $(val).addClass('activeRow');

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetMunicipality',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblMunicipality tbody').empty();
                $('#tblZip tbody').empty();
                $.each(records, function (idx, val) {
                    $('#tblMunicipality tbody').append(
						'<tr class="rowHover" onclick="getZip(this);">' +
							'<td data-id="' + val.municipality_code + '" >' + val.municipality_name + '</td>' +
						'</tr>'
					);

                });

                //getLCIState();
                //getLCICity('');
                //getLCIZip('');

                //getMuniState();
                //getMuniCity('');
                //getMuniZip('');
            }


            $('#divSettings').hide();

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

//function getCity(val) {
//    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });

//    var state = $(val).find('td').text();
//    //var state = $(val).find('td').attr('data-id');
//    $.each($('#tblState tbody tr'), function () {
//        $(this).removeClass('activeRow');
//    })
//    $(val).addClass('activeRow');

//    $.ajax({
//        type: 'POST',
//        url: 'CompRulesEncode.aspx/GetCity',
//        data: '{state: "' + state + '"}',
//        contentType: 'application/json; charset=utf-8',
//        success: function (data) {

//            var d = $.parseJSON(data.d);

//            if (d.Success) {
//                var records = d.data.record;
//                //console.log(records);
//                $('#tblCity tbody').empty();
//                $('#tblZip tbody').empty();
//                $.each(records, function (idx, val) {
//                    $('#tblCity tbody').append(
//						'<tr class="rowHover" onclick="getZip(this);">' +
//							'<td>' + val.City + '</td>' +
//						'</tr>'
//					);

//                });

//                //getLCIState();
//                //getLCICity('');
//                //getLCIZip('');

//                //getMuniState();
//                //getMuniCity('');
//                //getMuniZip('');
//            }


//            $('#divSettings').hide();

//            $('#modalLoading').modal('hide');
//        }, error: function (response) {
//            console.log(response.responseText);
//        }, failure: function (response) {
//            console.log(response.responseText);
//        }
//    });
//}

var state, city;

function getZip(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow').text();
    state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $(val).find('td').text();
    city = $(val).find('td').attr('data-id');
    $.each($('#tblMunicipality tbody tr'), function () {
        $(this).removeClass('activeRow');
    })
    $(val).addClass('activeRow');

    $('#divSettings input[type=text]').val('');
    $('#divSettings input[type=number]').val('');
    $('#divSettings input[type=checkbox]').prop('checked', false);
    $('#divSettings input[type=radio]').prop('checked', false);
    $('#modalEdit input[type=text]').val('');
    $('#divSettings select').each(function () {
        $(this).find('option:first-child').prop('selected', true);
    });

    //getData(state, city);

    getDataPFC(state, city);

    getSettings();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetZip',
        data: '{city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblZip tbody').empty();
                $.each(records, function (idx, val) {
                    var checked = '';
                    if (val.isZipActive) {
                        checked = 'checked';
                    }

                    $('#tblZip tbody').append(
						'<tr>' +
							'<td style="width: 10%;"><input data-id="' + val.ZipCode + '" type="checkbox" class="toggle" ' + checked + ' /></td>' +
							'<td><label>' + val.ZipCode + '</label></td>' +
						'</tr>'
					);
                });
                $('#modalLoading').modal('hide');
            }

            //getOrdinance('');

            $('.toggle').bootstrapToggle({
                on: 'Active',
                off: 'Inactive'
            });

            $('#divSettings').show();


        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}


$('#btnSavePDF').click(function () {

    if ($('#inUploadPath').val() == '') {

        alertify.alert('Please select PDF!')

    } else {
        //$('#exampleModal').hide();
        //var usr = $('#usr').text();
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        serv = "Property Registration";
        var name = $('#inUploadPath').val().replace('C:\\fakepath\\', '');
        $('#linkPDF').text(name);
        var client = 'PFC';
        var myData = [name, client, serv, $("#inUploadPath").get(0)]

        pdfUpload(myData);
    }
})

$('#btnSavePDFRenew').click(function () {

    if ($('#inUploadPath1').val() == '') {

        alertify.alert('Please select PDF!')

    } else {
        //$('#exampleModal').hide();
        //var usr = $('#usr').text();
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        serv = "Renewal";
        var name = $('#inUploadPath1').val().replace('C:\\fakepath\\', '');
        $('#linkPDFRenewal').text(name);
        var client = 'PFC';
        var myData = [name, client, serv, $('#inUploadPath1').get(0)]

        pdfUpload(myData);
    }
})

$('#btnSavePDF2').click(function () {

    if ($('#inREOUploadPath').val() == '') {

        alertify.alert('Please select PDF!')

    } else {
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        //var usr = $('#usr').text();
        serv = "Property Registration";
        var name = $('#inREOUploadPath').val().replace('C:\\fakepath\\', '');
        $('#linkPDFREO').text(name);
        var client = 'REO';
        var myData = [name, client, serv, $('#inREOUploadPath').get(0)]

        pdfUpload(myData);
    }
})

$('#btnSavePDFRenewREO').click(function () {

    if ($('#Reoupload1').val() == '') {

        alertify.alert('Please select PDF!')

    } else {
        //var name = $('#tblState tbody tr.activeRow td').attr('data-id') + " - " + $('#tblMunicipality tbody tr.activeRow td').text();
        //var code = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
        //var client = $('#navReg .active').text().trim();
        //var usr = $('#usr').text();
        serv = "Renewal";
        var name = $('#Reoupload1').val().replace('C:\\fakepath\\', '');
        $('#linkPDFRenewalREO').text(name);
        var client = 'REO';
        var myData = [name, client, serv, $('#Reoupload1').get(0)]

        pdfUpload(myData);
    }
})

$('#slctReoCodeViolation').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReoviolationwith').prop('disabled', false);
        $('#slctReoViolationtimeline1').prop('disabled', false);
        $('#slctReoviolationtimeline2').prop('disabled', false);
        $('#slctReoviolationdaystimeline3').prop('disabled', false);
        $('#slctReoviolationvacancytimeline4').prop('disabled', false);
    } else {
        $('#slctReoviolationwith').prop('disabled', true);
        $('#slctReoViolationtimeline1').prop('disabled', true);
        $('#slctReoviolationtimeline2').prop('disabled', true);
        $('#slctReoviolationdaystimeline3').prop('disabled', true);
        $('#slctReoviolationvacancytimeline4').prop('disabled', true);
    }
});

$('#slctReoBoardedOnly').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReoBoardedwith').prop('disabled', false);
        $('#slctReoBoardedtimeline1').prop('disabled', false);
        $('#slctReoBoardedtimeline2').prop('disabled', false);
        $('#slctReoBoardeddaystimeline3').prop('disabled', false);
        $('#slctReoBoardedtimeline4').prop('disabled', false);
    } else {
        $('#slctReoBoardedwith').prop('disabled', true);
        $('#slctReoBoardedtimeline1').prop('disabled', true);
        $('#slctReoBoardedtimeline2').prop('disabled', true);
        $('#slctReoBoardeddaystimeline3').prop('disabled', true);
        $('#slctReoBoardedtimeline4').prop('disabled', true);
    }
});

$('#slctRentalRegistration').change(function () {
    if ($(this).val() == 'true') {
        $('#slctRentalFormwithin').prop('disabled', false);
        $('#slctReorentaltimeline1').prop('disabled', false);
        $('#slctReorentaltimeline2').prop('disabled', false);
        $('#slctReorentaltimeline3').prop('disabled', false);
        $('#slctReorentaltimeline4').prop('disabled', false);
    } else {
        $('#slctRentalFormwithin').prop('disabled', true);
        $('#slctReorentaltimeline1').prop('disabled', true);
        $('#slctReorentaltimeline2').prop('disabled', true);
        $('#slctReorentaltimeline3').prop('disabled', true);
        $('#slctReorentaltimeline4').prop('disabled', true);
    }
});

$('#slctReoOther').change(function () {
    if ($(this).val() == 'true') {
        $('#slctreootherwithin').prop('disabled', false);
        $('#inReoOtherTimeline1').prop('disabled', false);
        $('#slctReoOtherTimeline2').prop('disabled', false);
        $('#slctReoOtherTimeline3').prop('disabled', false);
        $('#slctReoOtherTimeline4').prop('disabled', false);
    } else {
        $('#slctreootherwithin').prop('disabled', true);
        $('#inReoOtherTimeline1').prop('disabled', true);
        $('#slctReoOtherTimeline2').prop('disabled', true);
        $('#slctReoOtherTimeline3').prop('disabled', true);
        $('#slctReoOtherTimeline4').prop('disabled', true);
    }
});

function pdfUpload(myData) {



    var client, regType = '';

    if (myData[1] == 'PFC') {
        client = 'P';
    } else if (myData[1] == 'REO') {
        client = 'R';
    } else if (myData[1] == 'RESI') {
        client = 'RE';
    }

    if (myData[2] == 'Property Registration') {
        regType = 'I';
    } else if (myData[2] == 'Delist') {
        regType = 'D';
    } else if (myData[2] == "Renewal") {
        regType = 'R';
    }

    var filename = myData[0];
    var fileUpload = myData[3];

    //if (myData[1] == 'PFC') {
    //    if (regType == "P") {
    //        fileUpload = $("#inUploadPath").get(0);
    //    } else if (regType == "R") {
    //        fileUpload = $("#inUploadPath1").get(0);
    //    }


    //} else if (myData[1] == 'REO') {
    //    if(regType == "")
    //    fileUpload = $("#inREOUploadPath").get(0);
    //}

    var files = fileUpload.files;
    var test = new FormData();
    for (var i = 0; i < files.length; i++) {
        test.append(filename, files[i]);
    }
    $.ajax({
        url: "FileUpload1.ashx",
        type: "POST",
        contentType: false,
        processData: false,
        data: test,
        success: function (result) {
            //$('#modalLoading').modal('hide');
            alertify.alert(result);
        },
        error: function (result) {
            alertify.alert(result);
            //$('#modalLoading').modal('hide');
        }
    });
}

function openPDF(val) {
    $('#modalPDF').modal({ backdrop: 'static', keyboard: false });
    $('#lblPDF').text('PDF Name: ' + $(val).text());

    $('#pdf').replaceWith('<iframe id="pdf" style="width: 100%; height: 682px; border: 0;"></iframe>');
    $('#pdf').attr('src', '../Files/MC Official PDF Files/' + $(val).text());
}

$('#btntabREO').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetDataREO',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regREO = d.data.regREO;

                if (regREO.length > 0) {
                    if (regREO[0].tag == 'PASSED') {
                        $('#tabREO div.tag').remove();
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);

                        //BANKED-OWED
                        $('#slctReoBankOwed').val('' + regREO[0].reo_bank_owed + '');
                        $('#Drpreobanktime').val(regREO[0].reo_bank_owed_timeline1);
                        $('#inReoBankOwed1').val(regREO[0].reo_bank_owed_timeline2);
                        $('#slctReoBankOwed2').val(regREO[0].reo_bank_owed_timeline3);
                        $('#slctReoBankOwed3').val(regREO[0].reo_bank_owed_timeline4);
                        $('#slctReoBankOwed4').val(regREO[0].reo_bank_owed_timeline5);

                        //VACANT
                        $('#slctReoVacant').val('' + regREO[0].reo_vacant + '');
                        $('#slctReowith').val(regREO[0].reo_vacant_timeline1);
                        $('#inReoVacantTimeline1').val(regREO[0].reo_vacant_timeline2);
                        $('#slctReoVacantTimeline2').val(regREO[0].reo_vacant_timeline3);
                        $('#slctReoVacantTimeline3').val(regREO[0].reo_vacant_timeline4);
                        $('#slctReoVacantTimeline4').val(regREO[0].reo_vacant_timeline5);

                        //CITY NOTICE
                        $('#slctReoCityNotice').val('' + regREO[0].reo_city_notice + '');
                        $('#slctReocitynoticewith').val(regREO[0].reo_city_notice_timeline1);
                        $('#slctReocitytimeline1').val(regREO[0].reo_city_notice_timeline2);
                        $('#slctReoBusinesstimeline2').val(regREO[0].reo_city_notice_timeline3);
                        $('#slctReodaystimeline3').val(regREO[0].reo_city_notice_timeline4);
                        $('#slctReovacancytimeline4').val(regREO[0].reo_city_notice_timeline5);

                        //CODE VIOLATION
                        $('#slctReoCodeViolation').val('' + regREO[0].reo_code_violation + '');
                        $('#slctReoviolationwith').val(regREO[0].reo_code_violation_timeline1);
                        $('#slctReoViolationtimeline1').val(regREO[0].reo_code_violation_timeline2);
                        $('#slctReoviolationtimeline2').val(regREO[0].reo_code_violation_timeline3);
                        $('#slctReoviolationdaystimeline3').val(regREO[0].reo_code_violation_timeline4);
                        $('#slctReoviolationvacancytimeline4').val(regREO[0].reo_code_violation_timeline5);

                        //BOARDED ONLY
                        $('#slctReoBoardedOnly').val('' + regREO[0].reo_boarded_only + '');
                        $('#slctReoBoardedwith').val(regREO[0].reo_boarded_only_timeline1);
                        $('#slctReoBoardedtimeline1').val(regREO[0].reo_boarded_only_timeline2);
                        $('#slctReoBoardedtimeline2').val(regREO[0].reo_boarded_only_timeline3);
                        $('#slctReoBoardeddaystimeline3').val(regREO[0].reo_boarded_only_timeline4);
                        $('#slctReoBoardedtimeline4').val(regREO[0].reo_boarded_only_timeline5);

                        //DISTRESSED ABANDONED
                        $('#slctReoDistressedAbandoned').val('' + regREO[0].reo_distressed_abandoned + '');
                        $('#DrpReoAbandoned').val(regREO[0].reo_distressed_abandoned1);
                        $('#inReoDistressedAbandoned1').val(regREO[0].reo_distressed_abandoned2);
                        $('#slctReoDistressedAbandoned2').val(regREO[0].reo_distressed_abandoned3);
                        $('#slctReoDistressedAbandoned3').val(regREO[0].reo_distressed_abandoned4);
                        $('#slctReoDistressedAbandoned4').val(regREO[0].reo_distressed_abandoned5);

                        //RENTAL REGISTRATION
                        $('#slctRentalRegistration').val('' + regREO[0].rental_registration + '');
                        $('#slctRentalFormwithin').val(regREO[0].rental_registration_timeline1);
                        $('#slctReorentaltimeline1').val(regREO[0].rental_registration_timeline2);
                        $('#slctReorentaltimeline2').val(regREO[0].rental_registration_timeline3);
                        $('#slctReorentaltimeline3').val(regREO[0].rental_registration_timeline4);
                        $('#slctReorentaltimeline4').val(regREO[0].rental_registration_timeline5);

                        //OTHERS
                        $('#slctReoOther').val(regREO[0].reo_other);
                        $('#slctreootherwithin').val(regREO[0].reo_other_timeline1);
                        $('#inReoOtherTimeline1').val(regREO[0].reo_other_timeline2);
                        $('#slctReoOtherTimeline2').val(regREO[0].reo_other_timeline3);
                        $('#slctReoOtherTimeline3').val(regREO[0].reo_other_timeline4);
                        $('#slctReoOtherTimeline4').val(regREO[0].reo_other_timeline5);

                        $('#slctREOPaymentType').val(regREO[0].payment_type);
                        $('#slctREOTypeOfRegistration').val(regREO[0].type_of_registration);
                        $('#slctREOVmsRenewal').val('' + regREO[0].vms_renewal + '');

                        $('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                        $('#slctREORenewOnNum').val(regREO[0].renew_on_num);
                        $('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                        $('#slctREORenewEveryYears').val(regREO[0].renew_every_years);


                        if (regREO[0].vms_renewal == false || regREO[0].vms_renewal == '') {
                            $('.renewREOYes').hide();
                        } else {
                            $('.renewREOYes').show();

                            if (regREO[0].renew_every == 'false') {

                                $('#slctREORenewOnMonths').prop('disabled', false);
                                $('#slctREORenewOnNum').prop('disabled', false);

                                $('#Reordon').prop('checked', true);
                                $('#Reordvery').prop('checked', false);

                                $('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                                $('#slctREORenewOnNum').val(regREO[0].renew_on_num);
                                $('#slctREORenewEveryNum').val('');
                                $('#slctREORenewEveryYears').val('');

                                $('#slctREORenewEveryNum').prop('disabled', true);
                                $('#slctREORenewEveryYears').prop('disabled', true);

                            } else {
                                $('#slctREORenewEveryNum').prop('disabled', false);
                                $('#slctREORenewEveryYears').prop('disabled', false);

                                $('#Reordvery').prop('checked', true);
                                $('#Reordon').prop('checked', false);

                                $('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                                $('#slctREORenewEveryYears').val(regREO[0].renew_every_years);
                                $('#slctREORenewOnMonths').val('');
                                $('#slctREORenewOnNum').val('');

                                $('#slctREORenewOnMonths').prop('disabled', true);
                                $('#slctREORenewOnNum').prop('disabled', true);
                            }
                        }

                        if (regREO[0].type_of_registration == 'PDF') {
                            var fn = regREO[0].upload_path;

                            $('#slctREOTypeOfRegistration').closest('div').next().show();

                            $('#linkPDFREO').text(fn);
                            $('#btnSavePDF2').show();
                            //$('#tabREO div.file-caption-main div.file-caption-name').attr('title', fn);
                            //$('#tabREO div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctREOTypeOfRegistration').closest('div').next().hide();
                            $('#btnSavePDF2').hide();
                            $('#tabREO div.file-caption-main div.file-caption-name').removeAttr('title');
                            $('#tabREO div.file-caption-main div.file-caption-name').html('');
                        }

                        if (regREO[0].type_of_registration == 'PDF' && regREO[0].vms_renewal == 'true' && regREO[0].reo_renew_uploadpath != '') {
                            var fname = regREO[0].reo_renew_uploadpath;

                            $('#linkPDFRenewalREO').text(fname);
                            $('#slctREOVmsRenewal').closest('div').next().show();
                            $('#btnSavePDFRenewREO').show();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fname);
                            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fname);
                        }
                        else {
                            $('#slctREOVmsRenewal').closest('div').next().hide();
                            $('#btnSavePDFRenewREO').hide();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                        }

                        if (regREO[0].no_reo_reg == true) {
                            $('#reoReg').prop('checked', true);
                            $('#tabREO select').prop('disabled', true);
                            $('#tabREO input[type=number]').prop('disabled', true);
                            $('#tabREO input[type=text]').prop('disabled', true);
                            $('#tabREO select').val('');
                            $('#tabREO input[type=number]').val('');
                            $('#tabREO input[type=text]').val('');
                        } else {
                            $('#reoReg').prop('checked', false);
                            $('#tabREO select').prop('disabled', false);
                            $('#tabREO input[type=number]').prop('disabled', false);
                            $('#tabREO input[type=text]').prop('disabled', false);
                        }

                        if (regREO[0].statewide_reg == true) {
                            $('#reoStatewideReg').prop('checked', true);
                        } else {
                            $('#reoStatewideReg').prop('checked', false);
                        }

                    } else {
                        $('#tabREO div.tag').remove();
                        $('#tabREO').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regREO[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                    }

                    if ($('#Reordvery').is(':checked')) {
                        $('#slctREORenewEveryNum').prop('disabled', false);
                        $('#slctREORenewEveryYears').prop('disabled', false);
                        $('#slctREORenewOnMonths').prop('disabled', true);
                        $('#slctREORenewOnNum').prop('disabled', true);

                    } else if ($('#Reordon').is(':checked')) {
                        $('#slctREORenewOnMonths').prop('disabled', false);
                        $('#slctREORenewOnNum').prop('disabled', false);
                        $('#slctREORenewEveryNum').prop('disabled', true);
                        $('#slctREORenewEveryYears').prop('disabled', true);
                    } else {
                        $('#slctREORenewOnMonths').prop('disabled', true);
                        $('#slctREORenewOnNum').prop('disabled', true);
                        $('#slctREORenewEveryNum').prop('disabled', true);
                        $('#slctREORenewEveryYears').prop('disabled', true);
                    }

                    $('#modalLoading').modal('hide');

                } else {
                    $('#tabREO div.tag').remove();

                    $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                }

                $('#modalLoading').modal('hide');
            }
        }
    });
});

$('#btntabProperty').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetDataPropType',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regPropType = d.data.regPropType;

                if (regPropType.length > 0) {
                    if (regPropType[0].tag == 'PASSED') {
                        $('#tabPropType div.tag').remove();
                        $('#tabPropType div:last button:eq(0)').prop('disabled', false);

                        $('#slctPropResidential').val('' + regPropType[0].residential + '');
                        $('#slctSingleFamily').val('' + regPropType[0].single_family + '');
                        $('#slctMultiFamily').val('' + regPropType[0].multi_family + '');
                        $('#slct2units').val('' + regPropType[0].unit2 + '');
                        $('#slct3units').val('' + regPropType[0].unit3 + '');
                        $('#slct4units').val('' + regPropType[0].unit4 + '');
                        $('#slct5units').val('' + regPropType[0].unit5 + '');
                        $('#slctPropRental').val('' + regPropType[0].rental + '');
                        $('#slctPropCommercial').val('' + regPropType[0].commercial + '');
                        $('#slctPropCondo').val('' + regPropType[0].condo + '');
                        $('#slctPropTownhome').val('' + regPropType[0].townhome + '');
                        $('#slctPropVacantLot').val('' + regPropType[0].vacant_lot + '');
                        $('#slctPropMobilehome').val('' + regPropType[0].mobile_home + '');
                    } else {
                        $('#tabPropType div.tag').remove();
                        $('#tabPropType').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regPropType[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabPropType div:last button:eq(0)').prop('disabled', true);
                    }

                    $('#modalLoading').modal('hide');
                } else {
                    $('#tabPropType div.tag').remove();

                    $('#tabPropType div:last button:eq(0)').prop('disabled', false);
                }

                $('#modalLoading').modal('hide');
            }
        }
    });

});

$('#btntabRequirements').click(function () {

    getLCIState();
    getLCICity('');
    getLCIZip('');

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetDataReq',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var regPropReq = d.data.regPropReq;

                if (regPropReq.length > 0) {
                    if (regPropReq[0].tag == 'PASSED') {
                        $('#tabPropReq div.tag').remove();
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);

                        var add_info = '';
                        if (regPropReq[0].addn_info != '') {
                            var split = regPropReq[0].addn_info.split(',');
                            $('#slctAddnInfo').multiselect('select', split);
                        }

                        $('#slctFirstTimeVacancyDate').val('' + regPropReq[0].first_time_vacancy_date + '');
                        $('#slctPresaleDefinition').val(regPropReq[0].presale_definition);
                        $('#slctSecuredRequired').val('' + regPropReq[0].secured_required + '');
                        $('#slctLocalContactRequired').val('' + regPropReq[0].local_contact_required + '');
                        $('#slctAddSignReq').val('' + regPropReq[0].additional_signage_required + '');
                        $('#slctPicReq').val('' + regPropReq[0].pictures_required + '');
                        $('#slctGseExclusion').val('' + regPropReq[0].gse_exclusion + '');
                        $('#slctMobileVINReq').val('' + regPropReq[0].mobile_vin_number_required + '');
                        $('#slctInsuranceReq').val('' + regPropReq[0].insurance_required + '');
                        $('#slctParcelReq').val('' + regPropReq[0].parcel_number_required + '');
                        $('#slctForeclosureActInfo').val('' + regPropReq[0].foreclosure_action_information_needed + '');
                        $('#slctLegalDescReq').val('' + regPropReq[0].legal_description_required + '');
                        $('#slctForeclosureCaseInfo').val('' + regPropReq[0].foreclosure_case_information_needed + '');
                        $('#slctBlockLotReq').val('' + regPropReq[0].block_and_lot_number_required + '');
                        $('#slctForeclosureDeedReq').val('' + regPropReq[0].foreclosure_deed_required + '');
                        $('#slctAttyInfoReq').val('' + regPropReq[0].attorney_information_required + '');
                        $('#slctBondReq').val('' + regPropReq[0].bond_required + '');
                        $('#slctBrkInfoReq').val('' + regPropReq[0].broker_information_required_if_reo + '');
                        $('#inBondAmount').val(regPropReq[0].bond_amount);
                        $('#slctMortContactNameReq').val('' + regPropReq[0].mortgage_contact_name_required + '');
                        $('#slctMaintePlanReq').val('' + regPropReq[0].maintenance_plan_required + '');
                        $('#slctClientTaxReq').val('' + regPropReq[0].client_tax_number_required + '');
                        $('#slctNoTrespassReq').val('' + regPropReq[0].no_trespass_form_required + '');
                        $('#slctSignReq').val('' + regPropReq[0].signature_required + '');
                        $('#slctUtilityInfoReq').val('' + regPropReq[0].utility_information_required + '');
                        $('#slctNotarizationReq').val('' + regPropReq[0].notarization_required + '');
                        $('#slctWinterReq').val('' + regPropReq[0].winterization_required + '');
                        $('#slctRecInsDate').val('' + regPropReq[0].recent_inspection_date + '');

                        $('#lciCompany').val(regPropReq[0].lcicompany_name);
                        $('#lciFirstName').val(regPropReq[0].lcifirst_name);
                        $('#lciLastName').val(regPropReq[0].lcilast_name);
                        $('#lciTitle').val(regPropReq[0].lcititle);
                        $('#lciBusinessLicenseNum').val(regPropReq[0].lcibusiness_license_num);
                        $('#lciPhoneNum1').val(regPropReq[0].lciphone_num1);
                        $('#lciPhoneNum2').val(regPropReq[0].lciphone_num2);
                        $('#lciBusinessPhoneNum1').val(regPropReq[0].lcibusiness_phone_num1);
                        $('#lciBusinessPhoneNum2').val(regPropReq[0].lcibusiness_phone_num2);
                        $('#lciEmrPhone1').val(regPropReq[0].lciemergency_phone_num1);
                        $('#lciEmrPhone2').val(regPropReq[0].lciemergency_phone_num2);
                        $('#lciFaxNum1').val(regPropReq[0].lcifax_num1);
                        $('#lciFaxNum2').val(regPropReq[0].lcifax_num2);
                        $('#lciCellNum1').val(regPropReq[0].lcicell_num1);
                        $('#lciCellNum2').val(regPropReq[0].lcicell_num2);
                        $('#lciEmail').val(regPropReq[0].lciemail);
                        $('#lciStreet').val(regPropReq[0].lcistreet);
                        $('#lciState').val(regPropReq[0].lcistate);
                        $('#lciCity').val(regPropReq[0].lcicity);
                        $('#lciZip').val(regPropReq[0].lcizip);
                        $('#lciHrsFrom').val(regPropReq[0].lcihours_from);
                        $('#lciHrsTo').val(regPropReq[0].lcihours_to);
                    } else {
                        $('#tabPropReq div.tag').remove();
                        $('#tabPropReq').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regPropReq[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                    }

                    $('#modalLoading').modal('hide');
                } else {
                    $('#tabPropReq div.tag').remove();

                    $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);
                }

                $('#modalLoading').modal('hide');
            }
        }
    });

});

$('#btntabCost').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetDataCost',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regCost = d.data.regCost;

                if (regCost.length > 0) {
                    if (regCost[0].tag == 'PASSED') {
                        $('#tabCost div.tag').remove();
                        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#slctRegCost').val('' + regCost[0].reg_cost + '');
                        $('#inRegCost').val(regCost[0].reg_cost_amt);
                        $('#slctRegCostCurr').val(regCost[0].reg_cost_curr);
                        $('#slctRegCostStandard').val('' + regCost[0].reg_cost_standard + '');
                        $('#slctRenewCostEscal').val('' + regCost[0].is_renewal_cost_escal1 + '');
                        $('#inRenewAmt').val(regCost[0].renewal_fee_amt);
                        $('#slctRenewAmtCurr1').val(regCost[0].renewal_fee_curr);
                        $('#inComRegFee').val(regCost[0].com_reg_fee);
                        $('#slctComRegFee').val(regCost[0].com_reg_curr);
                        $('#slctComFeeStandard').val('' + regCost[0].com_fee_standard + '');
                        $('#slctRenewCostEscal2').val('' + regCost[0].is_renew_cost_escal2 + '');
                        $('#inRenewAmt2').val(regCost[0].com_renew_cost_amt);
                        $('#slctRenewAmtCurr2').val(regCost[0].com_renew_cost_curr);
                        $('#slctRegCostStandard2').val('' + regCost[0].is_reg_cost_standard + '');

                        var splitServType1 = regCost[0].reg_escal_service_type.split(',');
                        var splitAmt1 = regCost[0].reg_escal_amount.split(',');
                        var splitCurr1 = regCost[0].reg_escal_curr.split(',');
                        var splitSucceeding1 = regCost[0].reg_escal_succeeding.split(',');
                        $('#divEscalRenewal div.escalInput').destroy;
                        $('#divEscalRenewal div.escalInput').empty();
                      //  $('#slcCost').empty();
                        $.each(splitServType1, function (idx, val) {
                            if (idx == 0) {
                                $('#divEscalRenewal div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm" id=slcCost>' +
                                                '<option value="renewal" selected="selected">' + splitServType1[0] + '</option>' +
                                                '<option value="renewal-2">' + splitServType1[1] + '</option>' +
                                                '<option value="renewal-3">' + splitServType1[2] + '</option>' +
                                                '<option value="renewal-4">' + splitServType1[3] + '</option>' +
                                                '<option value="renewal-5">' + splitServType1[4] + '</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '</div>'
                                );

                             //   $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            } else {
                                var checked = (splitSucceeding1 == 'true') ? 'checked' : '';
                                $('#divEscalRenewal div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="renewal">Renewal</option>' +
                                                '<option value="renewal-2">Renewal-2</option>' +
                                                '<option value="renewal-3">Renewal-3</option>' +
                                                '<option value="renewal-4">Renewal-4</option>' +
                                                '<option value="renewal-5">Renewal-5</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                                );

                             //   $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            }
                        });

                        var splitServType2 = regCost[0].com_escal_service_type.split(',');
                        var splitAmt2 = regCost[0].com_escal_amount.split(',');
                        var splitCurr2 = regCost[0].com_escal_curr.split(',');
                        var splitSucceeding2 = regCost[0].com_escal_succeeding.split(',');

                        $('#divEscalRenewal2 div.escalInput').empty();

                        $.each(splitServType2, function (idx, val) {
                            if (idx == 0) {
                                $('#divEscalRenewal2 div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="renewal" selected="selected">Renewal</option>' +
                                                '<option value="renewal-2">Renewal-2</option>' +
                                                '<option value="renewal-3">Renewal-3</option>' +
                                                '<option value="renewal-4">Renewal-4</option>' +
                                                '<option value="renewal-5">Renewal-5</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '</div>'
                                );

                             //   $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            } else {
                                var checked = (splitSucceeding2 == 'true') ? 'checked' : '';
                                $('#divEscalRenewal2 div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="renewal">Renewal</option>' +
                                                '<option value="renewal-2">Renewal-2</option>' +
                                                '<option value="renewal-3">Renewal-3</option>' +
                                                '<option value="renewal-4">Renewal-4</option>' +
                                                '<option value="renewal-5">Renewal-5</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                                );

                               // $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            }
                        });
                    } else {
                        $('#tabCost div.tag').remove();
                        $('#tabCost').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regCost[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', true);
                    }

                    $('#modalLoading').modal('hide');

                } else {
                    $('#tabCost div.tag').remove();

                    $('#tabCost div.col-xs-12').last().find('button').prop('disabled', false);
                }

                $('#modalLoading').modal('hide');
            }
        }
    });
});

function getDataPFC(state, city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetData',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                // PFC
                var regPFC = d.data.regPFC;

                if (regPFC.length > 0) {
                    if (regPFC[0].tag == 'PASSED') {
                        $('#tabPFC div.tag').remove();
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);



                        //PFC DEFAULT
                        $('#slctPfcDefault').val('' + regPFC[0].pfc_default + '');
                        $('#inDefRegTimeline1').val(regPFC[0].def_reg_timeline1);
                        $('#slctDefRegTimeline2').val(regPFC[0].def_reg_timeline2);
                        $('#slctDefRegTimeline3').val(regPFC[0].def_reg_timeline3);
                        $('#slctDefRegTimeline4').val(regPFC[0].def_reg_timeline4);
                        $('#slctDefRegTimeline5').val(regPFC[0].def_reg_timeline5);

                        //PFC VACANT
                        $('#slctPfcVacantTimeline').val('' + regPFC[0].pfc_vacant + '');
                        $('#inPfcVacantTimeline1').val('' + regPFC[0].pfc_vacant_timeline1);
                        $('#slctPfcVacantTimeline2').val('' + regPFC[0].pfc_vacant_timeline2);
                        $('#slctPfcVacantTimeline3').val('' + regPFC[0].pfc_vacant_timeline3);
                        $('#slctPfcVacantTimeline4').val('' + regPFC[0].pfc_vacant_timeline4);
                        $('#slctPfcVacantTimeline5').val('' + regPFC[0].pfc_vacant_timeline5);

                        //PFC FORECLOSURE
                        $('#slctPfcForeclosure').val('' + regPFC[0].pfc_foreclosure + '');
                        $('#inPfcDefForclosureTimeline1').val(regPFC[0].pfc_def_foreclosure_timeline1);
                        $('#slctPfcDefForclosureTimeline2').val(regPFC[0].pfc_def_foreclosure_timeline2);
                        $('#slctPfcDefForclosureTimeline3').val(regPFC[0].pfc_def_foreclosure_timeline3);
                        $('#slctPfcDefForclosureTimeline4').val(regPFC[0].pfc_def_foreclosure_timeline4);
                        $('#slctPfcDefForclosureTimeline5').val(regPFC[0].pfc_def_foreclosure_timeline5);

                        //FORECLOSURE AND VACANT
                        $('#slctPfcForeclosureVacant').val('' + regPFC[0].pfc_foreclosure_vacant + '');
                        $('#inPfcForeclosureVacantTimeline1').val(regPFC[0].pfc_foreclosure_vacant_timeline1);
                        $('#slctPfcForeclosureVacantTimeline2').val(regPFC[0].pfc_foreclosure_vacant_timeline2);
                        $('#slctPfcForeclosureVacantTimeline3').val(regPFC[0].pfc_foreclosure_vacant_timeline3);
                        $('#slctPfcForeclosureVacantTimeline4').val(regPFC[0].pfc_foreclosure_vacant_timeline4);
                        $('#slctPfcForeclosureVacantTimeline5').val(regPFC[0].pfc_foreclosure_vacant_timeline5);

                        //CITY NOTICE
                        $('#slctPfcCityNotice').val('' + regPFC[0].pfc_city_notice + '');
                        $('#inPfcCityNoticeTimeline1').val(regPFC[0].pfc_city_notice_timeline1);
                        $('#slctPfcCityNoticeTimeline2').val(regPFC[0].pfc_city_notice_timeline2);
                        $('#slctPfcCityNoticeTimeline3').val(regPFC[0].pfc_city_notice_timeline3);
                        $('#slctPfcCityNoticeTimeline4').val(regPFC[0].pfc_city_notice_timeline4);
                        $('#slctPfcCityNoticeTimeline5').val(regPFC[0].pfc_city_notice_timeline5);

                        //CODE VIOLATION
                        $('#slctPfcCodeViolation').val('' + regPFC[0].pfc_code_violation + '');
                        $('#inPfcCodeViolationTimeline1').val(regPFC[0].pfc_code_violation_timeline1);
                        $('#slctPfcCodeViolationTimeline2').val(regPFC[0].pfc_code_violation_timeline2);
                        $('#slctPfcCodeViolationTimeline3').val(regPFC[0].pfc_code_violation_timeline3);
                        $('#slctPfcCodeViolationTimeline4').val(regPFC[0].pfc_code_violation_timeline4);
                        $('#slctPfcCodeViolationTimeline5').val(regPFC[0].pfc_code_violation_timeline5);

                        //BOARDED
                        $('#slctPfcBoarded').val('' + regPFC[0].pfc_boarded + '');
                        $('#inPfcBoardedTimeline1').val(regPFC[0].pfc_boarded_timeline1);
                        $('#slctPfcBoardedTimeline2').val(regPFC[0].pfc_boarded_timeline2);
                        $('#slctPfcBoardedTimeline3').val(regPFC[0].pfc_boarded_timeline3);
                        $('#slctPfcBoardedTimeline4').val(regPFC[0].pfc_boarded_timeline4);
                        $('#slctPfcBoardedTimeline5').val(regPFC[0].pfc_boarded_timeline5);

                        //OTHERS
                        $('#slctPfcOther').val('' + regPFC[0].pfc_other + '');
                        $('#inPfcOtherTimeline1').val(regPFC[0].pfc_other_timeline1);
                        $('#slctPfcOtherTimeline2').val(regPFC[0].pfc_other_timeline2);
                        $('#slctPfcOtherTimeline3').val(regPFC[0].pfc_other_timeline3);
                        $('#slctPfcOtherTimeline4').val(regPFC[0].pfc_other_timeline4);
                        $('#slctPfcOtherTimeline5').val(regPFC[0].pfc_other_timeline5);


                        //$('#slctSpcReq').val(regPFC[0].special_requirements);
                        $('#slctPaymentType').val(regPFC[0].payment_type);
                        $('#slctTypeOfRegistration').val(regPFC[0].type_of_registration);
                        $('#slctVmsRenewal').val('' + regPFC[0].vms_renewal + '');


                        if (regPFC[0].vms_renewal == false || regPFC[0].vms_renewal == '') {

                            $('.renewYes').hide();
                        } else {

                            $('.renewYes').show();

                            if (regPFC[0].renew_every == 'false') {

                                $('#slctRenewOnMonths').prop('disabled', false);
                                $('#slctRenewOnNum').prop('disabled', false);

                                $('#RDON').prop('checked', true);
                                $('#RDEVERY').prop('checked', false);


                                $('#slctRenewOnMonths').val(regPFC[0].renew_on_months);
                                $('#slctRenewOnNum').val(regPFC[0].renew_on_num);
                                $('#slctRenewEveryNum').val('');
                                $('#slctRenewEveryYears').val('');

                                $('#slctRenewEveryNum').prop('disabled', true);
                                $('#slctRenewEveryYears').prop('disabled', true);
                            } else {
                                $('#slctRenewEveryNum').prop('disabled', false);
                                $('#slctRenewEveryYears').prop('disabled', false);

                                $('#RDEVERY').prop('checked', true);
                                $('#RDON').prop('checked', false);

                                $('#slctRenewOnMonths').val('');
                                $('#slctRenewOnNum').val('');
                                $('#slctRenewEveryNum').val(regPFC[0].renew_every_num);
                                $('#slctRenewEveryYears').val(regPFC[0].renew_every_years);


                                $('#slctRenewOnMonths').prop('disabled', true);
                                $('#slctRenewOnNum').prop('disabled', true);
                            }

                        }


                        if (regPFC[0].pfc_foreclosure_vacant == false) {
                            $('#inPfcForeclosureVacantTimeline1').val('');
                            $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline5').prop('disabled', true);
                        } else {
                            $('#inPfcForeclosureVacantTimeline1').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline2').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline3').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline4').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline5').prop('disabled', false);
                        }

                        if (regPFC[0].pfc_vacant == false) {
                            $('#inPfcVacantTimeline1').val('');
                            $('#inPfcVacantTimeline1').prop('disabled', true);
                            $('#slctPfcVacantTimeline2').prop('disabled', true);
                            $('#slctPfcVacantTimeline3').prop('disabled', true);
                            $('#slctPfcVacantTimeline4').prop('disabled', true);
                            $('#slctPfcVacantTimeline5').prop('disabled', true);
                        } else {
                            $('#inPfcVacantTimeline1').prop('disabled', false);
                            $('#slctPfcVacantTimeline2').prop('disabled', false);
                            $('#slctPfcVacantTimeline3').prop('disabled', false);
                            $('#slctPfcVacantTimeline4').prop('disabled', false);
                            $('#slctPfcVacantTimeline5').prop('disabled', false);
                        }
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if (regPFC[0].type_of_registration == 'PDF') {
                            var fn = regPFC[0].upload_path;
                            $('#linkPDF').text(fn);
                            //$('#linkButton').show();
                            $('#slctTypeOfRegistration').closest('div').next().show();
                            $('#btnSavePDF').show();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fn);
                            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctTypeOfRegistration').closest('div').next().hide();
                            $('#btnSavePDF').hide();
                            //$('#linkButton').hide();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                        }



                        if (regPFC[0].type_of_registration == 'PDF' && $('#slctVmsRenewal').val() == 'true' && regPFC[0].renew_upload_path != '') {
                            var fname = regPFC[0].renew_upload_path;

                            $('#linkPDFRenewal').text(fname);
                            $('#slctVmsRenewal').closest('div').next().show();
                            $('#btnSavePDFRenew').show();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fname);
                            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fname);
                        }
                        else {
                            $('#slctVmsRenewal').closest('div').next().hide();
                            $('#btnSavePDFRenew').hide();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                        }
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////

                        if (regPFC[0].no_pfc_reg == true) {
                            $('#pfcReg').prop('checked', true);
                            $('#tabPFC select').prop('disabled', true);
                            $('#tabPFC input[type=number]').prop('disabled', true);
                            $('#tabPFC input[type=text]').prop('disabled', true);
                            $('#tabPFC select').val('');
                            $('#tabPFC input[type=number]').val('');
                            $('#tabPFC input[type=text]').val('');
                        } else {
                            $('#pfcReg').prop('checked', false);
                            $('#tabPFC select').prop('disabled', false);
                            $('#tabPFC input[type=number]').prop('disabled', false);
                            $('#tabPFC input[type=text]').prop('disabled', false);
                        }

                        if (regPFC[0].statewide_reg == true) {
                            $('#pfcStatewideReg').prop('checked', true);
                        } else {
                            $('#pfcStatewideReg').prop('checked', false);
                        }

                    } else {
                        $('#tabPFC div.tag').remove();
                        $('#tabPFC').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regPFC[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                    }


                    if ($('#RDEVERY').is(':checked')) {
                        $('#slctRenewEveryNum').prop('disabled', false);
                        $('#slctRenewEveryYears').prop('disabled', false);
                        $('#slctRenewOnMonths').prop('disabled', true);
                        $('#slctRenewOnNum').prop('disabled', true);

                    } else if ($('#RDON').is(':checked')) {
                        $('#slctRenewOnMonths').prop('disabled', false);
                        $('#slctRenewOnNum').prop('disabled', false);
                        $('#slctRenewEveryNum').prop('disabled', true);
                        $('#slctRenewEveryYears').prop('disabled', true);
                    } else {
                        $('#slctRenewOnMonths').prop('disabled', true);
                        $('#slctRenewOnNum').prop('disabled', true);
                        $('#slctRenewEveryNum').prop('disabled', true);
                        $('#slctRenewEveryYears').prop('disabled', true);
                    }

                    $('#modalLoading').modal('hide');

                } else {
                    $('#tabPFC div.tag').remove();

                    $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
            $('#modalLoading').modal('hide');
        }
    });
}

function getData(state, city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetData',
        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                // PFC
                //var regPFC = d.data.regPFC;

                //if (regPFC.length > 0) {
                //    if (regPFC[0].tag == 'PASSED') {
                //        $('#tabPFC div.tag').remove();
                //        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);



                //        //PFC DEFAULT
                //        $('#slctPfcDefault').val('' + regPFC[0].pfc_default + '');
                //        $('#inDefRegTimeline1').val(regPFC[0].def_reg_timeline1);
                //        $('#slctDefRegTimeline2').val(regPFC[0].def_reg_timeline2);
                //        $('#slctDefRegTimeline3').val(regPFC[0].def_reg_timeline3);
                //        $('#slctDefRegTimeline4').val(regPFC[0].def_reg_timeline4);
                //        $('#slctDefRegTimeline5').val(regPFC[0].def_reg_timeline5);

                //        //PFC VACANT
                //        $('#slctPfcVacantTimeline').val('' + regPFC[0].pfc_vacant + '');
                //        $('#inPfcVacantTimeline1').val('' + regPFC[0].pfc_vacant_timeline1);
                //        $('#slctPfcVacantTimeline2').val('' + regPFC[0].pfc_vacant_timeline2);
                //        $('#slctPfcVacantTimeline3').val('' + regPFC[0].pfc_vacant_timeline3);
                //        $('#slctPfcVacantTimeline4').val('' + regPFC[0].pfc_vacant_timeline4);
                //        $('#slctPfcVacantTimeline5').val('' + regPFC[0].pfc_vacant_timeline5);

                //        //PFC FORECLOSURE
                //        $('#slctPfcForeclosure').val('' + regPFC[0].pfc_foreclosure + '');
                //        $('#inPfcDefForclosureTimeline1').val(regPFC[0].pfc_def_foreclosure_timeline1);
                //        $('#slctPfcDefForclosureTimeline2').val(regPFC[0].pfc_def_foreclosure_timeline2);
                //        $('#slctPfcDefForclosureTimeline3').val(regPFC[0].pfc_def_foreclosure_timeline3);
                //        $('#slctPfcDefForclosureTimeline4').val(regPFC[0].pfc_def_foreclosure_timeline4);
                //        $('#slctPfcDefForclosureTimeline5').val(regPFC[0].pfc_def_foreclosure_timeline5);

                //        //FORECLOSURE AND VACANT
                //        $('#slctPfcForeclosureVacant').val('' + regPFC[0].pfc_foreclosure_vacant + '');
                //        $('#inPfcForeclosureVacantTimeline1').val(regPFC[0].pfc_foreclosure_vacant_timeline1);
                //        $('#slctPfcForeclosureVacantTimeline2').val(regPFC[0].pfc_foreclosure_vacant_timeline2);
                //        $('#slctPfcForeclosureVacantTimeline3').val(regPFC[0].pfc_foreclosure_vacant_timeline3);
                //        $('#slctPfcForeclosureVacantTimeline4').val(regPFC[0].pfc_foreclosure_vacant_timeline4);
                //        $('#slctPfcForeclosureVacantTimeline5').val(regPFC[0].pfc_foreclosure_vacant_timeline5);

                //        //CITY NOTICE
                //        $('#slctPfcCityNotice').val('' + regPFC[0].pfc_city_notice + '');
                //        $('#inPfcCityNoticeTimeline1').val(regPFC[0].pfc_city_notice_timeline1);
                //        $('#slctPfcCityNoticeTimeline2').val(regPFC[0].pfc_city_notice_timeline2);
                //        $('#slctPfcCityNoticeTimeline3').val(regPFC[0].pfc_city_notice_timeline3);
                //        $('#slctPfcCityNoticeTimeline4').val(regPFC[0].pfc_city_notice_timeline4);
                //        $('#slctPfcCityNoticeTimeline5').val(regPFC[0].pfc_city_notice_timeline5);

                //        //CODE VIOLATION
                //        $('#slctPfcCodeViolation').val('' + regPFC[0].pfc_code_violation + '');
                //        $('#inPfcCodeViolationTimeline1').val(regPFC[0].pfc_code_violation_timeline1);
                //        $('#slctPfcCodeViolationTimeline2').val(regPFC[0].pfc_code_violation_timeline2);
                //        $('#slctPfcCodeViolationTimeline3').val(regPFC[0].pfc_code_violation_timeline3);
                //        $('#slctPfcCodeViolationTimeline4').val(regPFC[0].pfc_code_violation_timeline4);
                //        $('#slctPfcCodeViolationTimeline5').val(regPFC[0].pfc_code_violation_timeline5);

                //        //BOARDED
                //        $('#slctPfcBoarded').val('' + regPFC[0].pfc_boarded + '');
                //        $('#inPfcBoardedTimeline1').val(regPFC[0].pfc_boarded_timeline1);
                //        $('#slctPfcBoardedTimeline2').val(regPFC[0].pfc_boarded_timeline2);
                //        $('#slctPfcBoardedTimeline3').val(regPFC[0].pfc_boarded_timeline3);
                //        $('#slctPfcBoardedTimeline4').val(regPFC[0].pfc_boarded_timeline4);
                //        $('#slctPfcBoardedTimeline5').val(regPFC[0].pfc_boarded_timeline5);

                //        //OTHERS
                //        $('#slctPfcOther').val('' + regPFC[0].pfc_other + '');
                //        $('#inPfcOtherTimeline1').val(regPFC[0].pfc_other_timeline1);
                //        $('#slctPfcOtherTimeline2').val(regPFC[0].pfc_other_timeline2);
                //        $('#slctPfcOtherTimeline3').val(regPFC[0].pfc_other_timeline3);
                //        $('#slctPfcOtherTimeline4').val(regPFC[0].pfc_other_timeline4);
                //        $('#slctPfcOtherTimeline5').val(regPFC[0].pfc_other_timeline5);


                //        //$('#slctSpcReq').val(regPFC[0].special_requirements);
                //        $('#slctPaymentType').val(regPFC[0].payment_type);
                //        $('#slctTypeOfRegistration').val(regPFC[0].type_of_registration);
                //        $('#slctVmsRenewal').val('' + regPFC[0].vms_renewal + '');


                //        //if (regPFC[0].vms_renewal == false || regPFC[0].vms_renewal == '') {
                //        //    $('.renewYes').hide();
                //        //} else {
                //        //    $('.renewYes').show();

                //        //    if (regPFC[0].renew_every == 'false') {
                //        //        $('#slctRenewOnMonths').prop('disabled', false);
                //        //        $('#slctRenewOnNum').prop('disabled', false);
                //        //        $('#slctRenewEveryNum').prop('disabled', true);
                //        //        $('#slctRenewEveryYears').prop('disabled', true);

                //        //        $('#RDON').prop('checked', true);
                //        //        $('#RDEVERY').prop('checked', false);
                //        //    } else {
                //        //        $('#slctRenewEveryNum').prop('disabled', false);
                //        //        $('#slctRenewEveryYears').prop('disabled', false);
                //        //        $('#slctRenewOnMonths').prop('disabled', true);
                //        //        $('#slctRenewOnNum').prop('disabled', true);

                //        //        $('#RDEVERY').prop('checked', true);
                //        //        $('#RDON').prop('checked', false);
                //        //    }
                //        //}


                //        if (regPFC[0].vms_renewal == false || regPFC[0].vms_renewal == '') {

                //            $('.renewYes').hide();
                //        } else {

                //            $('.renewYes').show();

                //            if (regPFC[0].renew_every == 'false') {

                //                $('#slctRenewOnMonths').prop('disabled', false);
                //                $('#slctRenewOnNum').prop('disabled', false);

                //                $('#RDON').prop('checked', true);
                //                $('#RDEVERY').prop('checked', false);


                //                $('#slctRenewOnMonths').val(regPFC[0].renew_on_months);
                //                $('#slctRenewOnNum').val(regPFC[0].renew_on_num);
                //                $('#slctRenewEveryNum').val('');
                //                $('#slctRenewEveryYears').val('');

                //                $('#slctRenewEveryNum').prop('disabled', true);
                //                $('#slctRenewEveryYears').prop('disabled', true);
                //            } else {
                //                $('#slctRenewEveryNum').prop('disabled', false);
                //                $('#slctRenewEveryYears').prop('disabled', false);

                //                $('#RDEVERY').prop('checked', true);
                //                $('#RDON').prop('checked', false);

                //                $('#slctRenewOnMonths').val('');
                //                $('#slctRenewOnNum').val('');
                //                $('#slctRenewEveryNum').val(regPFC[0].renew_every_num);
                //                $('#slctRenewEveryYears').val(regPFC[0].renew_every_years);


                //                $('#slctRenewOnMonths').prop('disabled', true);
                //                $('#slctRenewOnNum').prop('disabled', true);
                //            }

                //        }


                //        if (regPFC[0].pfc_foreclosure_vacant == false) {
                //            $('#inPfcForeclosureVacantTimeline1').val('');
                //            $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
                //            $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
                //            $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
                //            $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
                //            $('#slctPfcForeclosureVacantTimeline5').prop('disabled', true);
                //        } else {
                //            $('#inPfcForeclosureVacantTimeline1').prop('disabled', false);
                //            $('#slctPfcForeclosureVacantTimeline2').prop('disabled', false);
                //            $('#slctPfcForeclosureVacantTimeline3').prop('disabled', false);
                //            $('#slctPfcForeclosureVacantTimeline4').prop('disabled', false);
                //            $('#slctPfcForeclosureVacantTimeline5').prop('disabled', false);
                //        }

                //        if (regPFC[0].pfc_vacant == false) {
                //            $('#inPfcVacantTimeline1').val('');
                //            $('#inPfcVacantTimeline1').prop('disabled', true);
                //            $('#slctPfcVacantTimeline2').prop('disabled', true);
                //            $('#slctPfcVacantTimeline3').prop('disabled', true);
                //            $('#slctPfcVacantTimeline4').prop('disabled', true);
                //            $('#slctPfcVacantTimeline5').prop('disabled', true);
                //        } else {
                //            $('#inPfcVacantTimeline1').prop('disabled', false);
                //            $('#slctPfcVacantTimeline2').prop('disabled', false);
                //            $('#slctPfcVacantTimeline3').prop('disabled', false);
                //            $('#slctPfcVacantTimeline4').prop('disabled', false);
                //            $('#slctPfcVacantTimeline5').prop('disabled', false);
                //        }
                //        /////////////////////////////////////////////////////////////////////////////////////////////////////////
                //        if (regPFC[0].type_of_registration == 'PDF') {
                //            var fn = regPFC[0].upload_path;
                //            $('#linkPDF').text(fn);
                //            //$('#linkButton').show();
                //            $('#slctTypeOfRegistration').closest('div').next().show();
                //            $('#btnSavePDF').show();
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fn);
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                //        } else {
                //            $('#slctTypeOfRegistration').closest('div').next().hide();
                //            $('#btnSavePDF').hide();
                //            //$('#linkButton').hide();
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                //        }



                //        if (regPFC[0].type_of_registration == 'PDF' && $('#slctVmsRenewal').val() == 'true' && regPFC[0].renew_upload_path != '') {
                //            var fname = regPFC[0].renew_upload_path;

                //            $('#linkPDFRenewal').text(fname);
                //            $('#slctVmsRenewal').closest('div').next().show();
                //            $('#btnSavePDFRenew').show();
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fname);
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fname);
                //        }
                //        else {
                //            $('#slctVmsRenewal').closest('div').next().hide();
                //            $('#btnSavePDFRenew').hide();
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                //        }
                //        /////////////////////////////////////////////////////////////////////////////////////////////////////////

                //        if (regPFC[0].no_pfc_reg == true) {
                //            $('#pfcReg').prop('checked', true);
                //            $('#tabPFC select').prop('disabled', true);
                //            $('#tabPFC input[type=number]').prop('disabled', true);
                //            $('#tabPFC input[type=text]').prop('disabled', true);
                //            $('#tabPFC select').val('');
                //            $('#tabPFC input[type=number]').val('');
                //            $('#tabPFC input[type=text]').val('');
                //        } else {
                //            $('#pfcReg').prop('checked', false);
                //            $('#tabPFC select').prop('disabled', false);
                //            $('#tabPFC input[type=number]').prop('disabled', false);
                //            $('#tabPFC input[type=text]').prop('disabled', false);
                //        }

                //        if (regPFC[0].statewide_reg == true) {
                //            $('#pfcStatewideReg').prop('checked', true);
                //        } else {
                //            $('#pfcStatewideReg').prop('checked', false);
                //        }

                //    } else {
                //        $('#tabPFC div.tag').remove();
                //        $('#tabPFC').prepend(
                //            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                //                '<label class="text-red">Pending ' + regPFC[0].tag + '!</label>' +
                //            '</div>'
                //        );
                //        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                //    }


                //    if ($('#RDEVERY').is(':checked')) {
                //        $('#slctRenewEveryNum').prop('disabled', false);
                //        $('#slctRenewEveryYears').prop('disabled', false);
                //        $('#slctRenewOnMonths').prop('disabled', true);
                //        $('#slctRenewOnNum').prop('disabled', true);

                //    } else if ($('#RDON').is(':checked')) {
                //        $('#slctRenewOnMonths').prop('disabled', false);
                //        $('#slctRenewOnNum').prop('disabled', false);
                //        $('#slctRenewEveryNum').prop('disabled', true);
                //        $('#slctRenewEveryYears').prop('disabled', true);
                //    } else {
                //        $('#slctRenewOnMonths').prop('disabled', true);
                //        $('#slctRenewOnNum').prop('disabled', true);
                //        $('#slctRenewEveryNum').prop('disabled', true);
                //        $('#slctRenewEveryYears').prop('disabled', true);
                //    }

                //} else {
                //    $('#tabPFC div.tag').remove();

                //    $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);
                //}

                // REO
                //var regREO = d.data.regREO;

                //if (regREO.length > 0) {
                //    if (regREO[0].tag == 'PASSED') {
                //        $('#tabREO div.tag').remove();
                //        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);

                //        //BANKED-OWED
                //        $('#slctReoBankOwed').val('' + regREO[0].reo_bank_owed + '');
                //        $('#Drpreobanktime').val(regREO[0].reo_bank_owed_timeline1);
                //        $('#inReoBankOwed1').val(regREO[0].reo_bank_owed_timeline2);
                //        $('#slctReoBankOwed2').val(regREO[0].reo_bank_owed_timeline3);
                //        $('#slctReoBankOwed3').val(regREO[0].reo_bank_owed_timeline4);
                //        $('#slctReoBankOwed4').val(regREO[0].reo_bank_owed_timeline5);

                //        //VACANT
                //        $('#slctReoVacant').val('' + regREO[0].reo_vacant + '');
                //        $('#slctReowith').val(regREO[0].reo_vacant_timeline1);
                //        $('#inReoVacantTimeline1').val(regREO[0].reo_vacant_timeline2);
                //        $('#slctReoVacantTimeline2').val(regREO[0].reo_vacant_timeline3);
                //        $('#slctReoVacantTimeline3').val(regREO[0].reo_vacant_timeline4);
                //        $('#slctReoVacantTimeline4').val(regREO[0].reo_vacant_timeline5);

                //        //CITY NOTICE
                //        $('#slctReoCityNotice').val('' + regREO[0].reo_city_notice + '');
                //        $('#slctReocitynoticewith').val(regREO[0].reo_city_notice_timeline1);
                //        $('#slctReocitytimeline1').val(regREO[0].reo_city_notice_timeline2);
                //        $('#slctReoBusinesstimeline2').val(regREO[0].reo_city_notice_timeline3);
                //        $('#slctReodaystimeline3').val(regREO[0].reo_city_notice_timeline4);
                //        $('#slctReovacancytimeline4').val(regREO[0].reo_city_notice_timeline5);

                //        //CODE VIOLATION
                //        $('#slctReoCodeViolation').val('' + regREO[0].reo_code_violation + '');
                //        $('#slctReoviolationwith').val(regREO[0].reo_code_violation_timeline1);
                //        $('#slctReoViolationtimeline1').val(regREO[0].reo_code_violation_timeline2);
                //        $('#slctReoviolationtimeline2').val(regREO[0].reo_code_violation_timeline3);
                //        $('#slctReoviolationdaystimeline3').val(regREO[0].reo_code_violation_timeline4);
                //        $('#slctReoviolationvacancytimeline4').val(regREO[0].reo_code_violation_timeline5);

                //        //BOARDED ONLY
                //        $('#slctReoBoardedOnly').val('' + regREO[0].reo_boarded_only + '');
                //        $('#slctReoBoardedwith').val(regREO[0].reo_boarded_only_timeline1);
                //        $('#slctReoBoardedtimeline1').val(regREO[0].reo_boarded_only_timeline2);
                //        $('#slctReoBoardedtimeline2').val(regREO[0].reo_boarded_only_timeline3);
                //        $('#slctReoBoardeddaystimeline3').val(regREO[0].reo_boarded_only_timeline4);
                //        $('#slctReoBoardedtimeline4').val(regREO[0].reo_boarded_only_timeline5);

                //        //DISTRESSED ABANDONED
                //        $('#slctReoDistressedAbandoned').val('' + regREO[0].reo_distressed_abandoned + '');
                //        $('#DrpReoAbandoned').val(regREO[0].reo_distressed_abandoned1);
                //        $('#inReoDistressedAbandoned1').val(regREO[0].reo_distressed_abandoned2);
                //        $('#slctReoDistressedAbandoned2').val(regREO[0].reo_distressed_abandoned3);
                //        $('#slctReoDistressedAbandoned3').val(regREO[0].reo_distressed_abandoned4);
                //        $('#slctReoDistressedAbandoned4').val(regREO[0].reo_distressed_abandoned5);

                //        //RENTAL REGISTRATION
                //        $('#slctRentalRegistration').val('' + regREO[0].rental_registration + '');
                //        $('#slctRentalFormwithin').val(regREO[0].rental_registration_timeline1);
                //        $('#slctReorentaltimeline1').val(regREO[0].rental_registration_timeline2);
                //        $('#slctReorentaltimeline2').val(regREO[0].rental_registration_timeline3);
                //        $('#slctReorentaltimeline3').val(regREO[0].rental_registration_timeline4);
                //        $('#slctReorentaltimeline4').val(regREO[0].rental_registration_timeline5);

                //        //OTHERS
                //        $('#slctReoOther').val(regREO[0].reo_other);
                //        $('#slctreootherwithin').val(regREO[0].reo_other_timeline1);
                //        $('#inReoOtherTimeline1').val(regREO[0].reo_other_timeline2);
                //        $('#slctReoOtherTimeline2').val(regREO[0].reo_other_timeline3);
                //        $('#slctReoOtherTimeline3').val(regREO[0].reo_other_timeline4);
                //        $('#slctReoOtherTimeline4').val(regREO[0].reo_other_timeline5);

                //        $('#slctREOPaymentType').val(regREO[0].payment_type);
                //        $('#slctREOTypeOfRegistration').val(regREO[0].type_of_registration);
                //        $('#slctREOVmsRenewal').val('' + regREO[0].vms_renewal + '');

                //        $('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                //        $('#slctREORenewOnNum').val(regREO[0].renew_on_num);
                //        $('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                //        $('#slctREORenewEveryYears').val(regREO[0].renew_every_years);


                //        if (regREO[0].vms_renewal == false || regREO[0].vms_renewal == '') {
                //            $('.renewREOYes').hide();
                //        } else {
                //            $('.renewREOYes').show();

                //            if (regREO[0].renew_every == 'false') {

                //                $('#slctREORenewOnMonths').prop('disabled', false);
                //                $('#slctREORenewOnNum').prop('disabled', false);

                //                $('#Reordon').prop('checked', true);
                //                $('#Reordvery').prop('checked', false);

                //                $('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                //                $('#slctREORenewOnNum').val(regREO[0].renew_on_num);
                //                $('#slctREORenewEveryNum').val('');
                //                $('#slctREORenewEveryYears').val('');

                //                $('#slctREORenewEveryNum').prop('disabled', true);
                //                $('#slctREORenewEveryYears').prop('disabled', true);

                //            } else {
                //                $('#slctREORenewEveryNum').prop('disabled', false);
                //                $('#slctREORenewEveryYears').prop('disabled', false);

                //                $('#Reordvery').prop('checked', true);
                //                $('#Reordon').prop('checked', false);

                //                $('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                //                $('#slctREORenewEveryYears').val(regREO[0].renew_every_years);
                //                $('#slctREORenewOnMonths').val('');
                //                $('#slctREORenewOnNum').val('');

                //                $('#slctREORenewOnMonths').prop('disabled', true);
                //                $('#slctREORenewOnNum').prop('disabled', true);
                //            }
                //        }

                //        //if (regREO[0].reo_bank_owed == false) {
                //        //    $('#inReoBankOwed1').val('');
                //        //    $('#inReoBankOwed1').prop('disabled', true);
                //        //    $('#slctReoBankOwed2').prop('disabled', true);
                //        //    $('#slctReoBankOwed3').prop('disabled', true);
                //        //    $('#slctReoBankOwed4').prop('disabled', true);
                //        //} else {
                //        //    $('#inReoBankOwed1').prop('disabled', false);
                //        //    $('#slctReoBankOwed2').prop('disabled', false);
                //        //    $('#slctReoBankOwed3').prop('disabled', false);
                //        //    $('#slctReoBankOwed4').prop('disabled', false);
                //        //}

                //        //if (regREO[0].reo_distressed_abandoned == false) {
                //        //    $('#inReoDistressedAbandoned1').val('');
                //        //    $('#inReoDistressedAbandoned1').prop('disabled', true);
                //        //    $('#slctReoDistressedAbandoned2').prop('disabled', true);
                //        //    $('#slctReoDistressedAbandoned3').prop('disabled', true);
                //        //    $('#slctReoDistressedAbandoned4').prop('disabled', true);
                //        //} else {
                //        //    $('#inReoDistressedAbandoned1').prop('disabled', false);
                //        //    $('#slctReoDistressedAbandoned2').prop('disabled', false);
                //        //    $('#slctReoDistressedAbandoned3').prop('disabled', false);
                //        //    $('#slctReoDistressedAbandoned4').prop('disabled', false);
                //        //}

                //        //if (regREO[0].reo_vacant == false) {
                //        //    $('#inReoVacantTimeline1').val('');
                //        //    $('#inReoVacantTimeline1').prop('disabled', true);
                //        //    $('#slctReoVacantTimeline2').prop('disabled', true);
                //        //    $('#slctReoVacantTimeline3').prop('disabled', true);
                //        //    $('#slctReoVacantTimeline4').prop('disabled', true);
                //        //} else {
                //        //    $('#inReoVacantTimeline1').prop('disabled', false);
                //        //    $('#slctReoVacantTimeline2').prop('disabled', false);
                //        //    $('#slctReoVacantTimeline3').prop('disabled', false);
                //        //    $('#slctReoVacantTimeline4').prop('disabled', false);
                //        //}

                //        if (regREO[0].type_of_registration == 'PDF') {
                //            var fn = regREO[0].upload_path;

                //            $('#slctREOTypeOfRegistration').closest('div').next().show();

                //            $('#linkPDFREO').text(fn);
                //            $('#btnSavePDF2').show();
                //            //$('#tabREO div.file-caption-main div.file-caption-name').attr('title', fn);
                //            //$('#tabREO div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                //        } else {
                //            $('#slctREOTypeOfRegistration').closest('div').next().hide();
                //            $('#btnSavePDF2').hide();
                //            $('#tabREO div.file-caption-main div.file-caption-name').removeAttr('title');
                //            $('#tabREO div.file-caption-main div.file-caption-name').html('');
                //        }

                //        if (regREO[0].type_of_registration == 'PDF' && regREO[0].vms_renewal == 'true' && regREO[0].reo_renew_uploadpath != '') {
                //            var fname = regREO[0].reo_renew_uploadpath;

                //            $('#linkPDFRenewalREO').text(fname);
                //            $('#slctREOVmsRenewal').closest('div').next().show();
                //            $('#btnSavePDFRenewREO').show();
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fname);
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fname);
                //        }
                //        else {
                //            $('#slctREOVmsRenewal').closest('div').next().hide();
                //            $('#btnSavePDFRenewREO').hide();
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                //            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                //        }

                //        if (regREO[0].no_reo_reg == true) {
                //            $('#reoReg').prop('checked', true);
                //            $('#tabREO select').prop('disabled', true);
                //            $('#tabREO input[type=number]').prop('disabled', true);
                //            $('#tabREO input[type=text]').prop('disabled', true);
                //            $('#tabREO select').val('');
                //            $('#tabREO input[type=number]').val('');
                //            $('#tabREO input[type=text]').val('');
                //        } else {
                //            $('#reoReg').prop('checked', false);
                //            $('#tabREO select').prop('disabled', false);
                //            $('#tabREO input[type=number]').prop('disabled', false);
                //            $('#tabREO input[type=text]').prop('disabled', false);
                //        }

                //        if (regREO[0].statewide_reg == true) {
                //            $('#reoStatewideReg').prop('checked', true);
                //        } else {
                //            $('#reoStatewideReg').prop('checked', false);
                //        }

                //    } else {
                //        $('#tabREO div.tag').remove();
                //        $('#tabREO').prepend(
                //            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                //                '<label class="text-red">Pending ' + regREO[0].tag + '!</label>' +
                //            '</div>'
                //        );
                //        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                //    }

                //    if ($('#Reordvery').is(':checked')) {
                //        $('#slctREORenewEveryNum').prop('disabled', false);
                //        $('#slctREORenewEveryYears').prop('disabled', false);
                //        $('#slctREORenewOnMonths').prop('disabled', true);
                //        $('#slctREORenewOnNum').prop('disabled', true);

                //    } else if ($('#Reordon').is(':checked')) {
                //        $('#slctREORenewOnMonths').prop('disabled', false);
                //        $('#slctREORenewOnNum').prop('disabled', false);
                //        $('#slctREORenewEveryNum').prop('disabled', true);
                //        $('#slctREORenewEveryYears').prop('disabled', true);
                //    } else {
                //        $('#slctREORenewOnMonths').prop('disabled', true);
                //        $('#slctREORenewOnNum').prop('disabled', true);
                //        $('#slctREORenewEveryNum').prop('disabled', true);
                //        $('#slctREORenewEveryYears').prop('disabled', true);
                //    }

                //} else {
                //    $('#tabREO div.tag').remove();

                //    $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                //}

                // Prop Type
                //var regPropType = d.data.regPropType;

                //if (regPropType.length > 0) {
                //    if (regPropType[0].tag == 'PASSED') {
                //        $('#tabPropType div.tag').remove();
                //        $('#tabPropType div:last button:eq(0)').prop('disabled', false);

                //        $('#slctPropResidential').val('' + regPropType[0].residential + '');
                //        $('#slctSingleFamily').val('' + regPropType[0].single_family + '');
                //        $('#slctMultiFamily').val('' + regPropType[0].multi_family + '');
                //        $('#slct2units').val('' + regPropType[0].unit2 + '');
                //        $('#slct3units').val('' + regPropType[0].unit3 + '');
                //        $('#slct4units').val('' + regPropType[0].unit4 + '');
                //        $('#slct5units').val('' + regPropType[0].unit5 + '');
                //        $('#slctPropRental').val('' + regPropType[0].rental + '');
                //        $('#slctPropCommercial').val('' + regPropType[0].commercial + '');
                //        $('#slctPropCondo').val('' + regPropType[0].condo + '');
                //        $('#slctPropTownhome').val('' + regPropType[0].townhome + '');
                //        $('#slctPropVacantLot').val('' + regPropType[0].vacant_lot + '');
                //        $('#slctPropMobilehome').val('' + regPropType[0].mobile_home + '');
                //    } else {
                //        $('#tabPropType div.tag').remove();
                //        $('#tabPropType').prepend(
                //            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                //                '<label class="text-red">Pending ' + regPropType[0].tag + '!</label>' +
                //            '</div>'
                //        );
                //        $('#tabPropType div:last button:eq(0)').prop('disabled', true);
                //    }
                //} else {
                //    $('#tabPropType div.tag').remove();

                //    $('#tabPropType div:last button:eq(0)').prop('disabled', false);
                //}

                // Prop Req
                //var regPropReq = d.data.regPropReq;

                //if (regPropReq.length > 0) {
                //    if (regPropReq[0].tag == 'PASSED') {
                //        $('#tabPropReq div.tag').remove();
                //        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);

                //        var add_info = '';
                //        if (regPropReq[0].addn_info != '') {
                //            var split = regPropReq[0].addn_info.split(',');
                //            $('#slctAddnInfo').multiselect('select', split);
                //        }

                //        $('#slctFirstTimeVacancyDate').val('' + regPropReq[0].first_time_vacancy_date + '');
                //        $('#slctPresaleDefinition').val(regPropReq[0].presale_definition);
                //        $('#slctSecuredRequired').val('' + regPropReq[0].secured_required + '');
                //        $('#slctLocalContactRequired').val('' + regPropReq[0].local_contact_required + '');
                //        $('#slctAddSignReq').val('' + regPropReq[0].additional_signage_required + '');
                //        $('#slctPicReq').val('' + regPropReq[0].pictures_required + '');
                //        $('#slctGseExclusion').val('' + regPropReq[0].gse_exclusion + '');
                //        $('#slctMobileVINReq').val('' + regPropReq[0].mobile_vin_number_required + '');
                //        $('#slctInsuranceReq').val('' + regPropReq[0].insurance_required + '');
                //        $('#slctParcelReq').val('' + regPropReq[0].parcel_number_required + '');
                //        $('#slctForeclosureActInfo').val('' + regPropReq[0].foreclosure_action_information_needed + '');
                //        $('#slctLegalDescReq').val('' + regPropReq[0].legal_description_required + '');
                //        $('#slctForeclosureCaseInfo').val('' + regPropReq[0].foreclosure_case_information_needed + '');
                //        $('#slctBlockLotReq').val('' + regPropReq[0].block_and_lot_number_required + '');
                //        $('#slctForeclosureDeedReq').val('' + regPropReq[0].foreclosure_deed_required + '');
                //        $('#slctAttyInfoReq').val('' + regPropReq[0].attorney_information_required + '');
                //        $('#slctBondReq').val('' + regPropReq[0].bond_required + '');
                //        $('#slctBrkInfoReq').val('' + regPropReq[0].broker_information_required_if_reo + '');
                //        $('#inBondAmount').val(regPropReq[0].bond_amount);
                //        $('#slctMortContactNameReq').val('' + regPropReq[0].mortgage_contact_name_required + '');
                //        $('#slctMaintePlanReq').val('' + regPropReq[0].maintenance_plan_required + '');
                //        $('#slctClientTaxReq').val('' + regPropReq[0].client_tax_number_required + '');
                //        $('#slctNoTrespassReq').val('' + regPropReq[0].no_trespass_form_required + '');
                //        $('#slctSignReq').val('' + regPropReq[0].signature_required + '');
                //        $('#slctUtilityInfoReq').val('' + regPropReq[0].utility_information_required + '');
                //        $('#slctNotarizationReq').val('' + regPropReq[0].notarization_required + '');
                //        $('#slctWinterReq').val('' + regPropReq[0].winterization_required + '');
                //        $('#slctRecInsDate').val('' + regPropReq[0].recent_inspection_date + '');

                //        $('#lciCompany').val(regPropReq[0].lcicompany_name);
                //        $('#lciFirstName').val(regPropReq[0].lcifirst_name);
                //        $('#lciLastName').val(regPropReq[0].lcilast_name);
                //        $('#lciTitle').val(regPropReq[0].lcititle);
                //        $('#lciBusinessLicenseNum').val(regPropReq[0].lcibusiness_license_num);
                //        $('#lciPhoneNum1').val(regPropReq[0].lciphone_num1);
                //        $('#lciPhoneNum2').val(regPropReq[0].lciphone_num2);
                //        $('#lciBusinessPhoneNum1').val(regPropReq[0].lcibusiness_phone_num1);
                //        $('#lciBusinessPhoneNum2').val(regPropReq[0].lcibusiness_phone_num2);
                //        $('#lciEmrPhone1').val(regPropReq[0].lciemergency_phone_num1);
                //        $('#lciEmrPhone2').val(regPropReq[0].lciemergency_phone_num2);
                //        $('#lciFaxNum1').val(regPropReq[0].lcifax_num1);
                //        $('#lciFaxNum2').val(regPropReq[0].lcifax_num2);
                //        $('#lciCellNum1').val(regPropReq[0].lcicell_num1);
                //        $('#lciCellNum2').val(regPropReq[0].lcicell_num2);
                //        $('#lciEmail').val(regPropReq[0].lciemail);
                //        $('#lciStreet').val(regPropReq[0].lcistreet);
                //        $('#lciState').val(regPropReq[0].lcistate);
                //        $('#lciCity').val(regPropReq[0].lcicity);
                //        $('#lciZip').val(regPropReq[0].lcizip);
                //        $('#lciHrsFrom').val(regPropReq[0].lcihours_from);
                //        $('#lciHrsTo').val(regPropReq[0].lcihours_to);
                //    } else {
                //        $('#tabPropReq div.tag').remove();
                //        $('#tabPropReq').prepend(
                //            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                //                '<label class="text-red">Pending ' + regPropReq[0].tag + '!</label>' +
                //            '</div>'
                //        );
                //        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                //    }
                //} else {
                //    $('#tabPropReq div.tag').remove();

                //    $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);
                //}

                // Cost
                //var regCost = d.data.regCost;

                //if (regCost.length > 0) {
                //    if (regCost[0].tag == 'PASSED') {
                //        $('#tabCost div.tag').remove();
                //        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', false);

                //        $('#slctRegCost').val('' + regCost[0].reg_cost + '');
                //        $('#inRegCost').val(regCost[0].reg_cost_amt);
                //        $('#slctRegCostCurr').val(regCost[0].reg_cost_curr);
                //        $('#slctRegCostStandard').val('' + regCost[0].reg_cost_standard + '');
                //        $('#slctRenewCostEscal').val('' + regCost[0].is_renewal_cost_escal1 + '');
                //        $('#inRenewAmt').val(regCost[0].renewal_fee_amt);
                //        $('#slctRenewAmtCurr1').val(regCost[0].renewal_fee_curr);
                //        $('#inComRegFee').val(regCost[0].com_reg_fee);
                //        $('#slctComRegFee').val(regCost[0].com_reg_curr);
                //        $('#slctComFeeStandard').val(regCost[0].com_fee_standard);
                //        $('#slctRenewCostEscal2').val('' + regCost[0].is_renew_cost_escal2 + '');
                //        $('#inRenewAmt2').val(regCost[0].com_renew_cost_amt);
                //        $('#slctRenewAmtCurr2').val(regCost[0].com_renew_cost_curr);
                //        $('#slctRegCostStandard2').val('' + regCost[0].is_reg_cost_standard + '');

                //        var splitServType1 = regCost[0].reg_escal_service_type.split(',');
                //        var splitAmt1 = regCost[0].reg_escal_amount.split(',');
                //        var splitCurr1 = regCost[0].reg_escal_curr.split(',');
                //        var splitSucceeding1 = regCost[0].reg_escal_succeeding.split(',');

                //        $('#divEscalRenewal div.escalInput').empty();

                //        $.each(splitServType1, function (idx, val) {
                //            if (idx == 0) {
                //                $('#divEscalRenewal div.escalInput').append(
                //                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                //                        '<div class="col-xs-3">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="renewal" selected="selected">Renewal</option>' +
                //                                '<option value="renewal-2">Renewal-2</option>' +
                //                                '<option value="renewal-3">Renewal-3</option>' +
                //                                '<option value="renewal-4">Renewal-4</option>' +
                //                                '<option value="renewal-5">Renewal-5</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="USD">USD</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                    '</div>'
                //                );

                //                $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                //            } else {
                //                var checked = (splitSucceeding1 == 'true') ? 'checked' : '';
                //                $('#divEscalRenewal div.escalInput').append(
                //                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                //                        '<div class="col-xs-3">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="renewal">Renewal</option>' +
                //                                '<option value="renewal-2">Renewal-2</option>' +
                //                                '<option value="renewal-3">Renewal-3</option>' +
                //                                '<option value="renewal-4">Renewal-4</option>' +
                //                                '<option value="renewal-5">Renewal-5</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="USD">USD</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                //                );

                //                $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                //            }
                //        });

                //        var splitServType2 = regCost[0].com_escal_service_type.split(',');
                //        var splitAmt2 = regCost[0].com_escal_amount.split(',');
                //        var splitCurr2 = regCost[0].com_escal_curr.split(',');
                //        var splitSucceeding2 = regCost[0].com_escal_succeeding.split(',');

                //        $('#divEscalRenewal2 div.escalInput').empty();

                //        $.each(splitServType2, function (idx, val) {
                //            if (idx == 0) {
                //                $('#divEscalRenewal2 div.escalInput').append(
                //                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                //                        '<div class="col-xs-3">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="renewal" selected="selected">Renewal</option>' +
                //                                '<option value="renewal-2">Renewal-2</option>' +
                //                                '<option value="renewal-3">Renewal-3</option>' +
                //                                '<option value="renewal-4">Renewal-4</option>' +
                //                                '<option value="renewal-5">Renewal-5</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="USD">USD</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                    '</div>'
                //                );

                //                $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                //            } else {
                //                var checked = (splitSucceeding2 == 'true') ? 'checked' : '';
                //                $('#divEscalRenewal2 div.escalInput').append(
                //                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                //                        '<div class="col-xs-3">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="renewal">Renewal</option>' +
                //                                '<option value="renewal-2">Renewal-2</option>' +
                //                                '<option value="renewal-3">Renewal-3</option>' +
                //                                '<option value="renewal-4">Renewal-4</option>' +
                //                                '<option value="renewal-5">Renewal-5</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                //                        '</div>' +
                //                        '<div class="col-xs-2">' +
                //                            '<select class="form-control input-sm">' +
                //                                '<option value="USD">USD</option>' +
                //                            '</select>' +
                //                        '</div>' +
                //                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                //                );

                //                $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                //            }
                //        });
                //    } else {
                //        $('#tabCost div.tag').remove();
                //        $('#tabCost').prepend(
                //            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                //                '<label class="text-red">Pending ' + regCost[0].tag + '!</label>' +
                //            '</div>'
                //        );
                //        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', true);
                //    }
                //} else {
                //    $('#tabCost div.tag').remove();

                //    $('#tabCost div.col-xs-12').last().find('button').prop('disabled', false);
                //}

                // Continuing Registration
                var regCont = d.data.regCont;

                //$('#slctContReg').val(regCont[0].cont_reg);
                //$('#slctUpdateReg').val(regCont[0].update_reg);

                if (regCont.length > 0) {
                    if (regCont[0].cont_reg_tag == 'PASSED') {
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#slctContReg').val(regCont[0].cont_reg);
                        $('#slctUpdateReg').val(regCont[0].update_reg);

                    } else {
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regCont[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabContReg div:last button:eq(0)').prop('disabled', true);
                    }
                } else {
                    $('#tabContReg div.tag').remove();

                    $('#tabContReg div:last button:eq(0)').prop('disabled', false);
                }

                // Inspection
                var inspect = d.data.inspect;

                if (inspect.length > 0) {
                    if (inspect[0].tag == 'PASSED') {
                        $('#tabInspection div.tag').remove();
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#slctInsUpReq').val('' + inspect[0].inspection_update_req + '');
                        $('#slctInsCriOcc').val('' + inspect[0].inspection_criteria_occ + '');

                        $('#slctInsCriVac').val(inspect[0].inspection_criteria_vac);
                        $('#slctInsFeePay').val('' + inspect[0].inspection_fee_payment_freq + '');
                        $('#slctInsFeeReq').val('' + inspect[0].inspection_fee_req + '');
                        $('#inInsFeeAmt').val('' + inspect[0].inspection_fee_amount + '');
                        $('#slctInsFeeAmt').val('' + inspect[0].inspection_fee_curr + '');
                        $('#slctInsRepFreq').val('' + inspect[0].inspection_reporting_freq + '');

                        //Criteria Occupied-------------------------------------------------------------------------------------------------------
                        if (inspect[0].inspection_criteria_occ == false) {
                            $('#btnViewInsCriteriaOcc').prop('disabled', true);
                        } else {
                            $('#btnViewInsCriteriaOcc').prop('disabled', false);

                            $('#slctInsCriteriaOccCycle1').val('' + inspect[0].criteria_occ_cycle_1 + '');
                            $('#slctInsCriteriaOccCycle2').val('' + inspect[0].criteria_occ_cycle_2 + '');

                            $('input[name="rdInsCriteriaOccFreq"][value="' + inspect[0].criteria_occ_freq + '"]').prop('checked', true);

                            var rbOccFreq = $("input[name='rdInsCriteriaOccFreq']:checked").val();

                            if (rbOccFreq == "daily") {

                                if (inspect[0].criteria_occ_cycle_day_1 != "") {
                                    $("#freqInsCriteriaOccDaily").show();
                                    $('input[name="rdFreq1"][value = "every1"]').prop('checked', true);
                                    var days = inspect[0].criteria_occ_cycle_day_1;
                                    var splitDays = days.split(',');
                                    for (i = 0; i < splitDays.length; i++) {
                                        if (splitDays[i] == "Monday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Tuesday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Wednesday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Thursday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Friday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                    }
                                }
                                if (inspect[0].criteria_occ_cycle_day_2 != "") {
                                    $('input[name="rdFreq1"][value = "every2"]').prop('checked', true);
                                    $("#inInsCriteriaOccDay").val('' + inspect[0].criteria_occ_cycle_day_2 + '');
                                }
                                if (inspect[0].criteria_occ_cycle_refresh_time != "") {
                                    $('input[name="cbRefresh"]').prop('checked', true);
                                    $("#inInsCriteriaOccTime").val('' + inspect[0].criteria_occ_cycle_refresh_time + '');
                                }

                            } else if (rbOccFreq == "weekly") {
                                $("#freqInsCriteriaOccWeekly").show();
                                $("#inInsCriteriaOccWeek").val('' + inspect[0].criteria_occ_cycle_week_1 + '');
                                $("#slctInsCriteriaOccWeek").val('' + inspect[0].criteria_occ_cycle_week_2 + '');

                            } else if (rbOccFreq == "monthly") {
                                $("#freqInsCriteriaOccMonthly").show();
                                $("#inInsCriteriaOccMonth").val('' + inspect[0].criteria_occ_cycle_month_1 + '');
                                $("#slctInsCriteriaMonth1").val('' + inspect[0].criteria_occ_cycle_month_2 + '');
                                $("#slctInsCriteriaMonth2").val('' + inspect[0].criteria_occ_cycle_month_3 + '');
                            } else if (rbOccFreq == "yearly") {
                                $("#freqInsCriteriaOccYearly").show();

                                if (inspect[0].criteria_occ_cycle_year_2 != "") {
                                    $('input[name="rdFreqYear1"][value="Anually"]').prop('checked', true);
                                }
                                if (inspect[0].criteria_occ_cycle_year_2 != "") {
                                    $("#inInsCriteriaOccYear").val('' + inspect[0].criteria_occ_cycle_year_2 + '');
                                }
                            }

                        }
                        //Criteria Occupied END-------------------------------------------------------------------------------------------------------


                        //Criteria Vacant-------------------------------------------------------------------------------------------------------
                        if (inspect[0].inspection_criteria_vac == false) {
                            $('#btnViewInsCriteriaVac').prop('disabled', true);
                        } else {
                            $('#btnViewInsCriteriaVac').prop('disabled', false);

                            $('#slctInsCriteriaVacCycle1').val('' + inspect[0].criteria_vac_cycle_1 + '');
                            $('#slctInsCriteriaVacCycle2').val('' + inspect[0].criteria_vac_cycle_2 + '');

                            $('input[name="rdInsCriteriaVacFreq"][value="' + inspect[0].criteria_vac_freq + '"]').prop('checked', true);

                            var rbVacFreq = $("input[name='rdInsCriteriaVacFreq']:checked").val();

                            if (rbVacFreq == "daily") {

                                if (inspect[0].criteria_vac_cycle_day_1 != "") {
                                    $("#freqInsCriteriaVacDaily").show();
                                    $('input[name="rdFreq2"][value = "every1"]').prop('checked', true);
                                    var days = inspect[0].criteria_vac_cycle_day_1;
                                    var splitDays = days.split(',');
                                    for (i = 0; i < splitDays.length; i++) {
                                        if (splitDays[i] == "Monday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Tuesday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Wednesday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Thursday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Friday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                    }
                                }
                                if (inspect[0].criteria_vac_cycle_day_2 != "") {
                                    $('input[name="rdFreq2"][value = "every2"]').prop('checked', true);
                                    $("#inInsCriteriaVacDay").val('' + inspect[0].criteria_vac_cycle_day_2 + '');
                                }

                            } else if (rbVacFreq == "weekly") {
                                $("#freqInsCriteriaVacWeekly").show();
                                $("#inInsCriteriaVacWeek").val('' + inspect[0].criteria_vac_cycle_week_1 + '');
                                $("#slctInsCriteriaVacWeek").val('' + inspect[0].criteria_vac_cycle_week_2 + '');

                            } else if (rbVacFreq == "monthly") {
                                $("#freqInsCriteriaVacMonthly").show();
                                $("#inInsCriteriaVacMonth").val('' + inspect[0].criteria_vac_cycle_month_1 + '');
                                $("#slctInsCriteriaVacMonth1").val('' + inspect[0].criteria_vac_cycle_month_2 + '');
                                $("#slctInsCriteriaVacMonth2").val('' + inspect[0].criteria_vac_cycle_month_3 + '');
                            } else if (rbVacFreq == "yearly") {
                                $("#freqInsCriteriaVacYearly").show();

                                if (inspect[0].criteria_vac_cycle_year_2 != "") {
                                    $('input[name="rdFreqVacYear1"][value="Anually"]').prop('checked', true);
                                }
                                if (inspect[0].criteria_vac_cycle_year_2 != "") {
                                    $("#inInsCriteriaVacYear").val('' + inspect[0].criteria_vac_cycle_year_2 + '');
                                }
                            }

                            if (inspect[0].criteria_vac_cycle_refresh_time != "") {
                                $('input[name="cbRefreshVac"]').prop('checked', true);
                                $("#inInsCriteriaVacTime").val('' + inspect[0].criteria_vac_cycle_refresh_time + '');
                            }
                        }
                        //Criteria Vacant END-------------------------------------------------------------------------------------------------------


                        //Fee Payment --------------------------------------------------------------------------------------
                        if (inspect[0].inspection_fee_payment_freq == false) {
                            $('#btnViewInsFeePaymentFreq').prop('disabled', true);
                        } else {
                            $('#btnViewInsFeePaymentFreq').prop('disabled', false);

                            $('#slctFeePayCycle1').val('' + inspect[0].fee_payment_cycle_1 + '');
                            $('#slctFeePayCycle2').val('' + inspect[0].fee_payment_cycle_2 + '');

                            $('input[name="rdInsFeePayFreq"][value="' + inspect[0].fee_payment_freq + '"]').prop('checked', true);

                            var rbFeeFreq = $("input[name='rdInsFeePayFreq']:checked").val();

                            if (rbFeeFreq == "daily") {

                                if (inspect[0].fee_payment_cycle_day_1 != "") {
                                    $("#freqInsFeePayFreqDaily").show();
                                    $('input[name="rdFreq3"][value = "daily1"]').prop('checked', true);
                                    var days = inspect[0].fee_payment_cycle_day_1;
                                    var splitDays = days.split(',');
                                    for (i = 0; i < splitDays.length; i++) {
                                        if (splitDays[i] == "Monday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Tuesday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Wednesday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Thursday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Friday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                    }
                                }
                                if (inspect[0].fee_payment_cycle_day_2 != "") {
                                    $('input[name="rdFreq3"][value = "daily2"]').prop('checked', true);
                                    $("#inFeePayFreqDay").val('' + inspect[0].fee_payment_cycle_day_2 + '');
                                }

                            } else if (rbFeeFreq == "weekly") {
                                $("#freqInsFeePayFreqWeekly").show();
                                $("#inFeePayFreqWeek").val('' + inspect[0].fee_payment_cycle_week_1 + '');
                                $("#slctFeePayFreqWeek").val('' + inspect[0].fee_payment_cycle_week_2 + '');

                            } else if (rbFeeFreq == "monthly") {
                                $("#freqInsFeePayFreqMonthly").show();

                                if (inspect[0].fee_payment_cycle_month_1 != "") {
                                    $('input[name="rdFreq3"][value = "monthly1"]').prop('checked', true);
                                    $("#inFeePayFreqMonth").val('' + inspect[0].fee_payment_cycle_month_1 + '');
                                }
                                if (inspect[0].fee_payment_cycle_month_2 != "") {
                                    $('input[name="rdFreq3"][value = "monthly2"]').prop('checked', true);
                                    $("#slctFeePayFreqMonth1").val('' + inspect[0].fee_payment_cycle_month_2 + '');
                                }
                                if (inspect[0].fee_payment_cycle_month_3 != "") {
                                    $('input[name="rdFreq3"][value = "monthly3"]').prop('checked', true);
                                    $("#slctFeePayFreqMonth2").val('' + inspect[0].fee_payment_cycle_month_3 + '');
                                }

                            } else if (rbFeeFreq == "yearly") {
                                $("#freqInsFeePayFreqYearly").show();

                                if (inspect[0].fee_payment_cycle_year_1 != "") {
                                    $('input[name="rdFreqFeePayYear1"][value="Anually"]').prop('checked', true);
                                }
                                if (inspect[0].fee_payment_cycle_year_2 != "") {
                                    $("#inFeePayFreqYear").val('' + inspect[0].fee_payment_cycle_year_2 + '');
                                }
                            }

                        }

                        //Fee Payment END--------------------------------------------------------------------------------------

                        if (inspect[0].inspection_fee_req == false) {
                            $('#inInsFeeAmt').val('');
                            $('#inInsFeeAmt').prop('disabled', true);
                            $('#slctInsFeeAmt').prop('disabled', true);
                        } else {
                            $('#inInsFeeAmt').prop('disabled', false);
                            $('#slctInsFeeAmt').prop('disabled', false);
                        }
                    } else {
                        $('#tabInspection div.tag').remove();
                        $('#tabInspection').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + inspect[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabInspection div.tag').remove();
                    $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                }


                // Municipality
                var municipal = d.data.municipal;

                if (municipal.length > 0) {
                    if (municipal[0].tag == 'PASSED') {
                        $('#tabMunicipality div.tag').remove();
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#inMuniDept').val(municipal[0].municipality_department);
                        $('#inMuniPhone').val(municipal[0].municipality_phone_num);
                        $('#inMuniEmail').val(municipal[0].municipality_email);
                        $('#inMuniMailStr').val(municipal[0].municipality_st);
                        $('#slctMuniMailCt').val(municipal[0].municipality_city);
                        $('#slctMuniMailSta').val(municipal[0].municipality_state);
                        $('#slctMuniMailZip').val(municipal[0].municipality_zip);
                        $('#inContact').val(municipal[0].contact_person);
                        $('#inTitle').val(municipal[0].title);
                        $('#inDept').val(municipal[0].department);
                        $('#inPhone').val(municipal[0].phone_num);
                        $('#inEmail').val(municipal[0].email);
                        $('#inMailing').val(municipal[0].address);
                        $('#inHrsFrom').val(municipal[0].ops_hrs_from);
                        $('#inHrsTo').val(municipal[0].ops_hrs_to);
                    } else {
                        $('#tabMunicipality div.tag').remove();
                        $('#tabMunicipality').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + municipal[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabMunicipality div.tag').remove();

                    $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', false);
                }

                // Deregistration
                var dereg = d.data.dereg;

                if (dereg.length > 0) {
                    if (dereg[0].tag == 'PASSED') {
                        $('#tabDeregistration div.tag').remove();
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#slctDeregRequired').val('' + dereg[0].dereg_req + '');
                        $('#slctConveyed').val('' + dereg[0].conveyed + '');
                        $('#slctOccupied').val('' + dereg[0].occupied + '');
                        $('#slctHowToDereg').val(dereg[0].how_to_dereg);
                        //$('#inUploadPathD').val(dereg[0].upload_file);
                        $('#slctNewOwnerInfoReq').val('' + dereg[0].new_owner_info_req + '');
                        $('#slctProofOfConveyReq').val('' + dereg[0].proof_of_conveyance_req + '');
                        $('#slctDateOfSaleReq').val('' + dereg[0].date_of_sale_req + '');

                        if (dereg[0].how_to_dereg == 'PDF') {
                            var fn = dereg[0].upload_file.replace('C:\\fakepath\\', '');

                            $('#slctHowToDereg').closest('div').next().show();

                            $('#tabDeregistratopm div.file-caption-main div.file-caption-name').attr('title', fn);
                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctHowToDereg').closest('div').next().hide();

                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').removeAttr('title');
                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').html('');
                        }
                    } else {
                        $('#tabDeregistration div.tag').remove();
                        $('#tabDeregistration').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + dereg[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabDeregistration div.tag').remove();

                    $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', false);
                }

                // Notification
                var notif = d.data.notif;

                if (notif.length > 0) {
                    if (notif[0].tag == 'PASSED') {
                        $('#tabNotif div.valid').remove();
                        $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#inRegSendRem').val(notif[0].reg_send_reminder_num);
                        $('#slctRegSendRem1').val(notif[0].reg_send_reminder_type);
                        $('#slctRegSendRem2').val(notif[0].reg_send_reminder_days);
                        $('#slctRegSendRem3').val(notif[0].reg_send_reminder_before);
                        $('#slctRegSendRem4').val(notif[0].reg_send_reminder_time_frame);
                        $('#inRenewSendRem').val(notif[0].ren_send_reminder_num);
                        $('#slctRenewSendRem1').val(notif[0].ren_send_reminder_type);
                        $('#slctRenewSendRem2').val(notif[0].ren_send_reminder_days);
                        $('#slctRenewSendRem3').val(notif[0].ren_send_reminder_before);
                        $('#slctRenewSendRem4').val(notif[0].ren_send_reminder_time_frame);
                        $('#inWebSendRem').val(notif[0].web_notif_url);
                    } else {
                        $('#tabNotif div.tag').remove();
                        $('#tabNotif').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + notif[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabNotif div.tag').remove();

                    $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }


            $('#modalLoading').modal('hide');

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getOrdinance(val) {
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetOrdinance',
        data: '{state: "' + state + '", city: "' + city + '", hist: "' + val + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //console.log(data);

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                if (val == "" || val == undefined) {
                    $('#tblOrdinance').bootstrapTable('destroy');

                    $('#tblOrdinance').bootstrapTable({
                        data: records
                    });

                    $('#tblOrdinance thead tr th:eq(2) div:eq(0)').attr('style', 'width: 500px');
                } else {
                    $('#tblOrdinanceHist').bootstrapTable('destroy');

                    $('#tblOrdinanceHist').bootstrapTable({
                        data: records
                    });

                    $('#tblOrdinanceHist thead tr th:eq(2) div:eq(0)').attr('style', 'width: 500px');
                }

            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function action(val) {
    return '<div class="col-xs-2"><button type="button" class="btn btn-success " data-id="' + val + '" onclick="editOrd(this);"><i class="fa fa-pencil-square"></i></button></div><div class="col-xs-2" style="margin-left: 10%"><button data-id="' + val + '" type="button" class="btn btn-danger " onclick="deleteOrd(this);"><i class="fa fa-trash-o"></i></button></div>';
}

$('#btnAddOrdinance').click(function () {
    $('#modalAddOrdinance').modal({ backdrop: 'static', keyboard: false });

    $('.datepicker').datepicker({
        autoclose: true
    });

    $('.file').fileinput({
        'showUpload': false
    });
});

function deleteOrd(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var ordID = $(val).attr('data-id');

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/DeleteOrdinance',
        data: '{id: "' + ordID + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            getOrdinance('');
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function editOrd(val) {
    $('#modalAddOrdinance').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var ordID = $(val).attr('data-id');
    //alert(ordID);
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetOrdinance',
        data: '{state: "' + state + '", city: "' + city + '", hist: "' + ordID + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //console.log(data);

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                $('#ordID').text(records[0].id)
                $('#tblAddOrd input:eq(0)').val(records[0].ordinance_num);
                $('#tblAddOrd input:eq(1)').val(records[0].ordinance_name);
                $('#tblAddOrd textarea:eq(0)').val(records[0].description);
                $('#tblAddOrd input:eq(2)').val(records[0].section);
                $('#tblAddOrd input:eq(3)').val(records[0].source);
                $('#tblAddOrd input:eq(4)').val(records[0].enacted_date);
                $('#tblAddOrd input:eq(5)').val(records[0].revision_date);

                $('.file').fileinput({
                    'showUpload': false
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });


}

$('#slctPfcCityNotice').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcCityNoticeTimeline1').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline2').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline3').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline4').prop('disabled', false);
        $('#slctPfcCityNoticeTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcCityNoticeTimeline1').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline2').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline3').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline4').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcCityNoticeTimeline1').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline2').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline3').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline4').prop('disabled', true);
        $('#slctPfcCityNoticeTimeline5').prop('disabled', true);
    }
});

$('#btnApplyOrdinance').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var ordNum = $('#tblAddOrd tbody tr td input:eq(0)').val();
    var ordName = $('#tblAddOrd tbody tr td input:eq(1)').val();
    var desc = $('#tblAddOrd tbody tr td textarea:eq(0)').val();
    var sect = $('#tblAddOrd tbody tr td input:eq(2)').val();
    var src = $('#tblAddOrd tbody tr td input:eq(3)').val();
    var enDate = $('#tblAddOrd tbody tr td input:eq(4)').val();
    var revDate = $('#tblAddOrd tbody tr td input:eq(5)').val();
    var file = $('#tblAddOrd tbody tr td input:eq(6)').val();
    var ordId = $('#ordID').text();

    var myData = [];

    if (ordId == '') {
        myData = ['ordinance', state, city, ordNum, ordName, desc, sect, src, enDate, revDate, file];
    } else {
        myData = ['ordinance2', state, city, ordNum, ordName, desc, sect, src, enDate, revDate, file, ordId];
    }
    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //console.log(data);

            getOrdinance('');
            $('#modalAddOrdinance').modal('hide');
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });

});

$('#slctPfcCodeViolation').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcCodeViolationTimeline1').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline2').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline3').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline4').prop('disabled', false);
        $('#slctPfcCodeViolationTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcCodeViolationTimeline1').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline2').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline3').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline4').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcCodeViolationTimeline1').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline2').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline3').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline4').prop('disabled', true);
        $('#slctPfcCodeViolationTimeline5').prop('disabled', true);
    }
});

$('#slctPfcBoarded').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcBoardedTimeline1').prop('disabled', false);
        $('#slctPfcBoardedTimeline2').prop('disabled', false);
        $('#slctPfcBoardedTimeline3').prop('disabled', false);
        $('#slctPfcBoardedTimeline4').prop('disabled', false);
        $('#slctPfcBoardedTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcBoardedTimeline1').prop('disabled', true);
        $('#slctPfcBoardedTimeline2').prop('disabled', true);
        $('#slctPfcBoardedTimeline3').prop('disabled', true);
        $('#slctPfcBoardedTimeline4').prop('disabled', true);
        $('#slctPfcBoardedTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcBoardedTimeline1').prop('disabled', true);
        $('#slctPfcBoardedTimeline2').prop('disabled', true);
        $('#slctPfcBoardedTimeline3').prop('disabled', true);
        $('#slctPfcBoardedTimeline4').prop('disabled', true);
        $('#slctPfcBoardedTimeline5').prop('disabled', true);
    }
});

$('#slctPfcOther').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcOtherTimeline1').prop('disabled', false);
        $('#slctPfcOtherTimeline2').prop('disabled', false);
        $('#slctPfcOtherTimeline3').prop('disabled', false);
        $('#slctPfcOtherTimeline4').prop('disabled', false);
        $('#slctPfcOtherTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcOtherTimeline1').prop('disabled', true);
        $('#slctPfcOtherTimeline2').prop('disabled', true);
        $('#slctPfcOtherTimeline3').prop('disabled', true);
        $('#slctPfcOtherTimeline4').prop('disabled', true);
        $('#slctPfcOtherTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcOtherTimeline1').prop('disabled', true);
        $('#slctPfcOtherTimeline2').prop('disabled', true);
        $('#slctPfcOtherTimeline3').prop('disabled', true);
        $('#slctPfcOtherTimeline4').prop('disabled', true);
        $('#slctPfcOtherTimeline5').prop('disabled', true);
    }
});

$('#btnHistory').click(function () {
    $('#modalOrdinanceHist').modal({ backdrop: 'static', keyboard: false });

    getOrdinance('hist');
});

$('#btnSaveRegPFC').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    //PFC DEFAULT
    var pfc_default = $('#slctPfcDefault').val();
    var pfc_default_timeline1 = $('#inDefRegTimeline1').val();
    var pfc_default_timeline2 = $('#slctDefRegTimeline2').val();
    var pfc_default_timeline3 = $('#slctDefRegTimeline3').val();
    var pfc_default_timeline4 = $('#slctDefRegTimeline4').val();
    var pfc_default_timeline5 = $('#slctDefRegTimeline5').val();

    //PFC VACANT
    var pfc_vacant = $('#slctPfcVacantTimeline').val();
    var pfc_vacant_timeline1 = $('#inPfcVacantTimeline1').val();
    var pfc_vacant_timeline2 = $('#slctPfcVacantTimeline2').val();
    var pfc_vacant_timeline3 = $('#slctPfcVacantTimeline3').val();
    var pfc_vacant_timeline4 = $('#slctPfcVacantTimeline4').val();
    var pfc_vacant_timeline5 = $('#slctPfcVacantTimeline5').val();

    //PFC FORECLOSURE
    var pfc_foreclosure = $('#slctPfcForeclosure').val();
    var pfc_foreclosure_timeline1 = $('#inPfcDefForclosureTimeline1').val();
    var pfc_foreclosure_timeline2 = $('#slctPfcDefForclosureTimeline2').val();
    var pfc_foreclosure_timeline3 = $('#slctPfcDefForclosureTimeline3').val();
    var pfc_foreclosure_timeline4 = $('#slctPfcDefForclosureTimeline4').val();
    var pfc_foreclosure_timeline5 = $('#slctPfcDefForclosureTimeline5').val();

    //PFC FORECLOSURE AND VACANT
    var pfc_foreclosure_vacant = $('#slctPfcForeclosureVacant').val();
    var pfc_foreclosure_vacant_timeline1 = $('#inPfcForeclosureVacantTimeline1').val();
    var pfc_foreclosure_vacant_timeline2 = $('#slctPfcForeclosureVacantTimeline2').val();
    var pfc_foreclosure_vacant_timeline3 = $('#slctPfcForeclosureVacantTimeline3').val();
    var pfc_foreclosure_vacant_timeline4 = $('#slctPfcForeclosureVacantTimeline4').val();
    var pfc_foreclosure_vacant_timeline5 = $('#slctPfcForeclosureVacantTimeline5').val();

    //PFC CITY NOTICE
    var pfc_city_notice = $('#slctPfcCityNotice').val();
    var pfc_city_notice_timeline1 = $('#inPfcCityNoticeTimeline1').val();
    var pfc_city_notice_timeline2 = $('#slctPfcCityNoticeTimeline2').val();
    var pfc_city_notice_timeline3 = $('#slctPfcCityNoticeTimeline3').val();
    var pfc_city_notice_timeline4 = $('#slctPfcCityNoticeTimeline4').val();
    var pfc_city_notice_timeline5 = $('#slctPfcCityNoticeTimeline5').val();

    //PFC CODE VIOLATION
    var pfc_code_violation = $('#slctPfcCodeViolation').val();
    var pfc_code_violation_timeline1 = $('#inPfcCodeViolationTimeline1').val();
    var pfc_code_violation_timeline2 = $('#slctPfcCodeViolationTimeline2').val();
    var pfc_code_violation_timeline3 = $('#slctPfcCodeViolationTimeline3').val();
    var pfc_code_violation_timeline4 = $('#slctPfcCodeViolationTimeline4').val();
    var pfc_code_violation_timeline5 = $('#slctPfcCodeViolationTimeline5').val();

    //PFC BOARDED
    var pfc_boarded = $('#slctPfcBoarded').val();
    var pfc_boarded_timeline1 = $('#inPfcBoardedTimeline1').val();
    var pfc_boarded_timeline2 = $('#slctPfcBoardedTimeline2').val();
    var pfc_boarded_timeline3 = $('#slctPfcBoardedTimeline3').val();
    var pfc_boarded_timeline4 = $('#slctPfcBoardedTimeline4').val();
    var pfc_boarded_timeline5 = $('#slctPfcBoardedTimeline5').val();

    //PFC OTHER
    var pfc_other = $('#slctPfcOther').val();
    var pfc_other_timeline1 = $('#inPfcOtherTimeline1').val();
    var pfc_other_timeline2 = $('#slctPfcOtherTimeline2').val();
    var pfc_other_timeline3 = $('#slctPfcOtherTimeline3').val();
    var pfc_other_timeline4 = $('#slctPfcOtherTimeline4').val();
    var pfc_other_timeline5 = $('#slctPfcOtherTimeline5').val();



    //var special_requirements = $('#slctSpcReq').val();
    var payment_type = $('#slctPaymentType').val();
    var type_of_registration = $('#slctTypeOfRegistration').val();
    var vms_renewal = $('#slctVmsRenewal').val();


    var upload_path = $('#linkPDF').text();
    //if ($('#tabPFC div.file-caption-name').attr('title') != undefined) {
    //    //upload_path = $('#tabPFC div.file-caption-name').attr('title');
    //    upload_path = $('#inUploadPath').val().replace('C:\\fakepath\\', '');
    //}
    var no_pfc_reg = '';
    if ($('#pfcReg').is(':checked')) {
        no_pfc_reg = 'true';
    } else {
        no_pfc_reg = 'false';
    }

    var statewide_reg = '';
    if ($('#pfcStatewideReg').is(':checked')) {
        statewide_reg = 'true';
    } else {
        statewide_reg = 'false';
    }

    var renew_every = '';
    var renew_on = '';
    //if ($('input[name=rdRenewal]').is(':checked')) {
    //    if ($(this).val() == 'Every') {
    //        renew_every = 'true';
    //        renew_on = 'false';
    //    } else {
    //        renew_every = 'false';
    //        renew_on = 'true';
    //    }

    //}

    if ($("#RDEVERY").prop("checked")) {
        renew_every = 'true';
        renew_on = 'false';
        // do something
    }
    else if ($("#RDON").prop("checked")) {
        renew_every = 'false';
        renew_on = 'true';
    }
    else {
        renew_every = '';
        renew_on = '';
    }




    var renew_every_num = $('#slctRenewEveryNum').val();
    var renew_every_years = $('#slctRenewEveryYears').val();

    var renew_on_months = $('#slctRenewOnMonths').val();
    var renew_on_num = $('#slctRenewOnNum').val();

    //var Renewupload_path = '';
    //if ($('#tabPFC div.file-caption-name').attr('title') != undefined) {
    //    //Renewupload_path = $('#tabPFC div.file-caption-name').attr('title');
    //    Renewupload_path = $('#inUploadPath1').val().replace('C:\\fakepath\\', '');
    //}
    var Renewupload_path = $('#linkPDFRenewal').text();


    var myData = [
        'regPFC',
        state,
        city,

        //PFC DEFAULT
        pfc_default,
        pfc_default_timeline1,
        pfc_default_timeline2,
        pfc_default_timeline3,
        pfc_default_timeline4,
        pfc_default_timeline5,

        //PFC VACANT
        pfc_vacant,
        pfc_vacant_timeline1,
        pfc_vacant_timeline2,
        pfc_vacant_timeline3,
        pfc_vacant_timeline4,
        pfc_vacant_timeline5,

        //PFC FORECLOSURE
        pfc_foreclosure,
        pfc_foreclosure_timeline1,
        pfc_foreclosure_timeline2,
        pfc_foreclosure_timeline3,
        pfc_foreclosure_timeline4,
        pfc_foreclosure_timeline5,

        //PFC FORECLOSURE AND VACANT
        pfc_foreclosure_vacant,
        pfc_foreclosure_vacant_timeline1,
        pfc_foreclosure_vacant_timeline2,
        pfc_foreclosure_vacant_timeline3,
        pfc_foreclosure_vacant_timeline4,
        pfc_foreclosure_vacant_timeline5,

        //PFC CITY NOTICE
        pfc_city_notice,
        pfc_city_notice_timeline1,
        pfc_city_notice_timeline2,
        pfc_city_notice_timeline3,
        pfc_city_notice_timeline4,
        pfc_city_notice_timeline5,

        //PFC CODE VIOLATION
        pfc_code_violation,
        pfc_code_violation_timeline1,
        pfc_code_violation_timeline2,
        pfc_code_violation_timeline3,
        pfc_code_violation_timeline4,
        pfc_code_violation_timeline5,

        //PFC BOARDED
        pfc_boarded,
        pfc_boarded_timeline1,
        pfc_boarded_timeline2,
        pfc_boarded_timeline3,
        pfc_boarded_timeline4,
        pfc_boarded_timeline5,

        //PFC OTHER
        pfc_other,
        pfc_other_timeline1,
        pfc_other_timeline2,
        pfc_other_timeline3,
        pfc_other_timeline4,
        pfc_other_timeline5,

        payment_type,
        type_of_registration,
        vms_renewal,
        upload_path,
        no_pfc_reg,
        statewide_reg,
        renew_every,
        renew_on,
        renew_every_num,
        renew_every_years,
        renew_on_months,
        renew_on_num,
        Renewupload_path
    ];
    console.log(myData);

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#slctReoCityNotice').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReocitynoticewith').prop('disabled', false);
        $('#slctReocitytimeline1').prop('disabled', false);
        $('#slctReoBusinesstimeline2').prop('disabled', false);
        $('#slctReodaystimeline3').prop('disabled', false);
        $('#slctReovacancytimeline4').prop('disabled', false);
    } else {
        $('#slctReocitynoticewith').prop('disabled', true);
        $('#slctReocitytimeline1').prop('disabled', true);
        $('#slctReoBusinesstimeline2').prop('disabled', true);
        $('#slctReodaystimeline3').prop('disabled', true);
        $('#slctReovacancytimeline4').prop('disabled', true);
    }
});

$('#btnSaveRegREO').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    //Banked-Owed
    var reo_bank_owed = $('#slctReoBankOwed').val();
    var reo_bank_owed_timeline1 = $('#Drpreobanktime').val();
    var reo_bank_owed_timeline2 = $('#inReoBankOwed1').val();
    var reo_bank_owed_timeline3 = $('#slctReoBankOwed2').val();
    var reo_bank_owed_timeline4 = $('#slctReoBankOwed3').val();
    var reo_bank_owed_timeline5 = $('#slctReoBankOwed4').val();

    //Vacant
    var reo_vacant = $('#slctReoVacant').val();
    var reo_vacant_timeline1 = $('#slctReowith').val();
    var reo_vacant_timeline2 = $('#inReoVacantTimeline1').val();
    var reo_vacant_timeline3 = $('#slctReoVacantTimeline2').val();
    var reo_vacant_timeline4 = $('#slctReoVacantTimeline3').val();
    var reo_vacant_timeline5 = $('#slctReoVacantTimeline4').val();

    //CITY NOTICE
    var reo_city_notice = $('#slctReoCityNotice').val();
    var reo_city_notice_timeline1 = $('#slctReocitynoticewith').val();
    var reo_city_notice_timeline2 = $('#slctReocitytimeline1').val();
    var reo_city_notice_timeline3 = $('#slctReoBusinesstimeline2').val();
    var reo_city_notice_timeline4 = $('#slctReodaystimeline3').val();
    var reo_city_notice_timeline5 = $('#slctReovacancytimeline4').val();

    //CODE VIOLATION
    var reo_code_violation = $('#slctReoCodeViolation').val();
    var reo_code_violation_timeline1 = $('#slctReoviolationwith').val();
    var reo_code_violation_timeline2 = $('#slctReoViolationtimeline1').val();
    var reo_code_violation_timeline3 = $('#slctReoviolationtimeline2').val();
    var reo_code_violation_timeline4 = $('#slctReoviolationdaystimeline3').val();
    var reo_code_violation_timeline5 = $('#slctReoviolationvacancytimeline4').val();

    //BOARDED
    var reo_boarded_only = $('#slctReoBoardedOnly').val();
    var reo_boarded_only_timeline1 = $('#slctReoBoardedwith').val();
    var reo_boarded_only_timeline2 = $('#slctReoBoardedtimeline1').val();
    var reo_boarded_only_timeline3 = $('#slctReoBoardedtimeline2').val();
    var reo_boarded_only_timeline4 = $('#slctReoBoardeddaystimeline3').val();
    var reo_boarded_only_timeline5 = $('#slctReoBoardedtimeline4').val();

    //DISTRESSED ABANDONED
    var reo_distressed_abandoned = $('#slctReoDistressedAbandoned').val();
    var reo_distressed_abandoned1 = $('#DrpReoAbandoned').val();
    var reo_distressed_abandoned2 = $('#inReoDistressedAbandoned1').val();
    var reo_distressed_abandoned3 = $('#slctReoDistressedAbandoned2').val();
    var reo_distressed_abandoned4 = $('#slctReoDistressedAbandoned3').val();
    var reo_distressed_abandoned5 = $('#slctReoDistressedAbandoned4').val();

    //RENTAL REGISTRATION
    var rental_registration = $('#slctRentalRegistration').val();
    var rental_registration_timeline1 = $('#slctRentalFormwithin').val();
    var rental_registration_timeline2 = $('#slctReorentaltimeline1').val();
    var rental_registration_timeline3 = $('#slctReorentaltimeline2').val();
    var rental_registration_timeline4 = $('#slctReorentaltimeline3').val();
    var rental_registration_timeline5 = $('#slctReorentaltimeline4').val();

    //OTHERS
    var reo_other = $('#slctReoOther').val();
    var reo_other_timeline1 = $('#slctreootherwithin').val();
    var reo_other_timeline2 = $('#inReoOtherTimeline1').val();
    var reo_other_timeline3 = $('#slctReoOtherTimeline2').val();
    var reo_other_timeline4 = $('#slctReoOtherTimeline3').val();
    var reo_other_timeline5 = $('#slctReoOtherTimeline4').val();


    //var rental_form = $('#slctRentalForm').val()

    //var special_requirements = $('#slctREOSpcReq').val();
    var payment_type = $('#slctREOPaymentType').val();
    var type_of_registration = $('#slctREOTypeOfRegistration').val();
    var vms_renewal = $('#slctREOVmsRenewal').val();
    var upload_path = $('#linkPDFREO').text();
    //if ($('#tabREO div.file-caption-name').attr('title') != undefined) {
    //    //upload_path = $('#tabREO div.file-caption-name').attr('title');
    //    upload_path = $('#inREOUploadPath').val().replace('C:\\fakepath\\', '');
    //}
    var no_reo_reg = '';
    if ($('#reoReg').is(':checked')) {
        no_reo_reg = 'true';
    } else {
        no_reo_reg = 'false';
    }

    var statewide_reg = '';
    if ($('#reoStatewideReg').is(':checked')) {
        statewide_reg = 'true';
    } else {
        statewide_reg = 'false';
    }

    var renew_every = '';
    var renew_on = '';
    //if ($('input[name=rdREORenewal]').is(':checked')) {
    //    if ($(this).val() == 'Every') {
    //        renew_every = 'true';
    //        renew_on = 'false';
    //    } else {
    //        renew_every = 'false';
    //        renew_on = 'true';
    //    }
    //}

    if ($("#Reordvery").prop("checked")) {
        renew_every = 'true';
        renew_on = 'false';
        // do something
    }
    else if ($("#Reordon").prop("checked")) {
        renew_every = 'false';
        renew_on = 'true';
    }
    else {
        renew_every = '';
        renew_on = '';
    }

    var renew_every_num = $('#slctREORenewEveryNum').val();
    var renew_every_years = $('#slctREORenewEveryYears').val();

    var renew_on_months = $('#slctREORenewOnMonths').val();
    var renew_on_num = $('#slctREORenewOnNum').val();

    var Renewupload_path = $('#linkPDFRenewalREO').text();
    //if ($('#tabPFC div.file-caption-name').attr('title') != undefined) {
    //    //Renewupload_path = $('#tabPFC div.file-caption-name').attr('title');
    //    Renewupload_path = $('#Reoupload1').val().replace('C:\\fakepath\\', '');
    //}


    var myData = [
        'regREO',
        state,
        city,

        //BANKED-OWED
        reo_bank_owed,
        reo_bank_owed_timeline1,
        reo_bank_owed_timeline2,
        reo_bank_owed_timeline3,
        reo_bank_owed_timeline4,
        reo_bank_owed_timeline5,

        //VACANT
        reo_vacant,
        reo_vacant_timeline1,
        reo_vacant_timeline2,
        reo_vacant_timeline3,
        reo_vacant_timeline4,
        reo_vacant_timeline5,

        //CITY NOTICE
        reo_city_notice,
        reo_city_notice_timeline1,
        reo_city_notice_timeline2,
        reo_city_notice_timeline3,
        reo_city_notice_timeline4,
        reo_city_notice_timeline5,

        //CODE VIOLATION
        reo_code_violation,
        reo_code_violation_timeline1,
        reo_code_violation_timeline2,
        reo_code_violation_timeline3,
        reo_code_violation_timeline4,
        reo_code_violation_timeline5,

        //BOARDED
        reo_boarded_only,
        reo_boarded_only_timeline1,
        reo_boarded_only_timeline2,
        reo_boarded_only_timeline3,
        reo_boarded_only_timeline4,
        reo_boarded_only_timeline5,

        //DISTRESSED ABANDONED
        reo_distressed_abandoned,
        reo_distressed_abandoned1,
        reo_distressed_abandoned2,
        reo_distressed_abandoned3,
        reo_distressed_abandoned4,
        reo_distressed_abandoned5,

        //RENTAL REGISTRATION
        rental_registration,
        rental_registration_timeline1,
        rental_registration_timeline2,
        rental_registration_timeline3,
        rental_registration_timeline4,
        rental_registration_timeline5,

        //OTHERS
        reo_other,
        reo_other_timeline1,
        reo_other_timeline2,
        reo_other_timeline3,
        reo_other_timeline4,
        reo_other_timeline5,

        //rental_form,
        //special_requirements,
        payment_type,
        type_of_registration,
        vms_renewal,
        upload_path,
        no_reo_reg,
        statewide_reg,
        renew_every,
        renew_on,
        renew_every_num,
        renew_every_years,
        renew_on_months,
        renew_on_num,
        Renewupload_path
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //console.log(data);

            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveRegProperty').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var residential = $('#tabPropType select:eq(0)').val();
    var single_family = $('#tabPropType select:eq(1)').val();
    var multi_family = $('#tabPropType select:eq(2)').val();
    var unit2 = $('#tabPropType select:eq(3)').val();
    var unit3 = $('#tabPropType select:eq(4)').val();
    var unit4 = $('#tabPropType select:eq(5)').val();
    var unit5 = $('#tabPropType select:eq(6)').val();
    var rental = $('#tabPropType select:eq(7)').val();
    var commercial = $('#tabPropType select:eq(8)').val();
    var condo = $('#tabPropType select:eq(9)').val();
    var townhome = $('#tabPropType select:eq(10)').val();
    var vacant_lot = $('#tabPropType select:eq(11)').val();
    var mobile_home = $('#tabPropType select:eq(12)').val();

    var myData = [
        'regProperty',
        state,
        city,
        residential,
        single_family,
        multi_family,
        unit2,
        unit3,
        unit4,
        unit5,
        rental,
        commercial,
        condo,
        townhome,
        vacant_lot,
        mobile_home
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveRegPropReq').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var addn_info = "";

    $.each($('#slctAddnInfo').val(), function (idx, val) {
        addn_info += val + ',';
    });

    addn_info = addn_info.slice(0, -1);

    var presale_definition = $('#slctPresaleDefinition').val();
    var first_time_vacancy_date = $('#slctFirstTimeVacancyDate').val();
    var secured_required = $('#slctSecuredRequired').val();
    var local_contact_required = $('#slctLocalContactRequired').val();
    var additional_signage_required = $('#slctAddSignReq').val();
    var pictures_required = $('#slctPicReq').val();
    var gse_exclusion = $('#slctGseExclusion').val();
    var mobile_vin_number_required = $('#slctMobileVINReq').val();
    var insurance_required = $('#slctInsuranceReq').val();
    var parcel_number_required = $('#slctParcelReq').val();
    var foreclosure_action_information_needed = $('#slctForeclosureActInfo').val();
    var legal_description_required = $('#slctLegalDescReq').val();
    var foreclosure_case_information_needed = $('#slctForeclosureCaseInfo').val();
    var block_and_lot_number_required = $('#slctBlockLotReq').val();
    var foreclosure_deed_required = $('#slctForeclosureDeedReq').val();
    var attorney_information_required = $('#slctAttyInfoReq').val();
    var bond_required = $('#slctBondReq').val();
    var broker_information_required_if_reo = $('#slctBrkInfoReq').val();
    var bond_amount = $('#inBondAmount').val();
    var mortgage_contact_name_required = $('#slctMortContactNameReq').val();
    var maintenance_plan_required = $('#slctMaintePlanReq').val();
    var client_tax_number_required = $('#slctClientTaxReq').val();
    var no_trespass_form_required = $('#slctNoTrespassReq').val();
    var signature_required = $('#slctSignReq').val();
    var utility_information_required = $('#slctUtilityInfoReq').val();
    var notarization_required = $('#slctNotarizationReq').val();
    var winterization_required = $('#slctWinterReq').val();
    var recent_inspection_date = $('#slctRecInsDate').val();

    var lcicompany_name = $('#lciCompany').val();
    var lcifirst_name = $('#lciFirstName').val();
    var lcilast_name = $('#lciLastName').val();
    var lcititle = $('#lciTitle').val();
    var lcibusiness_license_num = $('#lciBusinessLicenseNum').val();
    var lciphone_num1 = $('#lciPhoneNum1').val();
    var lciphone_num2 = $('#lciPhoneNum2').val();
    var lcibusiness_phone_num1 = $('#lciBusinessPhoneNum1').val();
    var lcibusiness_phone_num2 = $('#lciBusinessPhoneNum2').val();
    var lciemergency_phone_num1 = $('#lciEmrPhone1').val();
    var lciemergency_phone_num2 = $('#lciEmrPhone2').val();
    var lcifax_num1 = $('#lciFaxNum1').val();
    var lcifax_num2 = $('#lciFaxNum2').val();
    var lcicell_num1 = $('#lciCellNum1').val();
    var lcicell_num2 = $('#lciCellNum2').val();
    var lciemail = $('#lciEmail').val();
    var lcistreet = $('#lciStreet').val();
    var lcistate = '';
    if ($('#lciState').val() != undefined) {
        lcistate = $('#lciState').val();
    }
    var lcicity = '';
    if ($('#lciCity').val() != undefined) {
        lcicity = $('#lciCity').val();
    }
    var lcizip = '';
    if ($('#lciZip').val() != '') {
        lcizip = $('#lciZip').val();
    }
    var lcihours_from = $('#lciHrsFrom').val();
    var lcihours_to = $('#lciHrsTo').val();

    var myData = [
        'regPropReq',
        state,
        city,
        addn_info,
        presale_definition,
        first_time_vacancy_date,
        secured_required,
        local_contact_required,
        additional_signage_required,
        pictures_required,
        gse_exclusion,
        mobile_vin_number_required,
        insurance_required,
        parcel_number_required,
        foreclosure_action_information_needed,
        legal_description_required,
        foreclosure_case_information_needed,
        block_and_lot_number_required,
        foreclosure_deed_required,
        attorney_information_required,
        bond_required,
        broker_information_required_if_reo,
        bond_amount,
        mortgage_contact_name_required,
        maintenance_plan_required,
        client_tax_number_required,
        no_trespass_form_required,
        signature_required,
        utility_information_required,
        notarization_required,
        winterization_required,
        recent_inspection_date,

        lcicompany_name,
        lcifirst_name,
        lcilast_name,
        lcititle,
        lcibusiness_license_num,
        lciphone_num1,
        lciphone_num2,
        lcibusiness_phone_num1,
        lcibusiness_phone_num2,
        lciemergency_phone_num1,
        lciemergency_phone_num2,
        lcifax_num1,
        lcifax_num2,
        lcicell_num1,
        lcicell_num2,
        lciemail,
        lcistreet,
        lcistate,
        lcicity,
        lcizip,
        lcihours_from,
        lcihours_to
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveRegCost').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var reg_cost = $('#slctRegCost').val();
    var reg_cost_amt = $('#inRegCost').val();
    var reg_cost_curr = $('#slctRegCostCurr').val();
    var reg_cost_standard = $('#slctRegCostStandard').val();
    var is_renewal_cost_escal1 = $('#slctRenewCostEscal').val();
    var renewal_fee_amt = $('#inRenewAmt').val();
    var renewal_fee_curr = $('#slctRenewAmtCurr1').val();
    var reg_escal_service_type = $('#inComRegFee').val();
    var reg_escal_amount = $('#slctComRegFee').val();
    var reg_escal_curr = $('#slctComFeeStandard').val();
    var reg_escal_succeeding = $('#slctRenewCostEscal2').val();
    var com_reg_fee = $('#inRenewAmt2').val();
    var com_reg_curr = $('#slctRenewAmtCurr2').val();
    var com_fee_standard = $('#slctRegCostStandard2').val();

    var regEscalServType = "";
    var regEscalAmt = "";
    var regEscalCurr = "";
    var regEscalSucceeding = "";

    $.each($('#divEscalRenewal div.escalInput div.col-xs-12'), function (idx, val) {
        regEscalServType += $(this).find('div:eq(0) label').text() + ',';
        regEscalAmt += $(this).find('div:eq(1) input').val() + ',';
        regEscalCurr += $(this).find('div:eq(2) select').val() + ',';

        if ($(this).find('div:eq(3)').length != 0) {
            if ($(this).find('div:eq(3) input').is(':checked')) {
                regEscalSucceeding += 'true' + ',';
            } else {
                regEscalSucceeding += 'false' + ',';
            }
        } else {
            regEscalSucceeding += 'false' + ',';
        }
    });

    regEscalServType = regEscalServType.slice(0, -1);
    regEscalAmt = regEscalAmt.slice(0, -1);
    regEscalCurr = regEscalCurr.slice(0, -1);
    regEscalSucceeding = regEscalSucceeding.slice(0, -1);

    var comEscalServType = "";
    var comEscalAmt = "";
    var comEscalCurr = "";
    var comEscalSucceeding = "";
    $.each($('#divEscalRenewal2 div.escalInput div.col-xs-12'), function (idx, val) {
        comEscalServType += $(this).find('div:eq(0) label').text() + ',';
        comEscalAmt += $(this).find('div:eq(1) input').val() + ',';
        comEscalCurr += $(this).find('div:eq(2) select').val() + ',';

        if ($(this).find('div:eq(3)').length != 0) {
            if ($(this).find('div:eq(3) input').is(':checked')) {
                comEscalSucceeding += 'true' + ',';
            } else {
                comEscalSucceeding += 'false' + ',';
            }
        } else {
            comEscalSucceeding += 'false' + ',';
        }
    });

    comEscalServType = comEscalServType.slice(0, -1)
    comEscalAmt = comEscalAmt.slice(0, -1)
    comEscalCurr = comEscalCurr.slice(0, -1)
    comEscalSucceeding = comEscalSucceeding.slice(0, -1);

    var myData = [
        'regCost',
        state,
        city,
        reg_cost,
        reg_cost_amt,
        reg_cost_curr,
        reg_cost_standard,
        is_renewal_cost_escal1,
        renewal_fee_amt,
        renewal_fee_curr,
        reg_escal_service_type,
        reg_escal_amount,
        reg_escal_curr,
        reg_escal_succeeding,
        com_reg_fee,
        com_reg_curr,
        com_fee_standard,
        regEscalServType,
        regEscalAmt,
        regEscalCurr,
        regEscalSucceeding,
        comEscalServType,
        comEscalAmt,
        comEscalCurr,
        comEscalSucceeding
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabCost div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveRegCont').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var cont_reg = $('#slctContReg').val();
    var update_reg = $('#slctUpdateReg').val();

    var myData = [
        'regCont',
        state,
        city,
        cont_reg,
        update_reg
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveInspection').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var inspection_update_req = $('#slctInsUpReq').val();
    var inspection_criteria_occ = $('#slctInsCriOcc').val();
    var inspection_criteria_vac = $('#slctInsCriVac').val();
    var inspection_fee_payment_freq = $('#slctInsFeePay').val();
    var inspection_fee_req = $('#slctInsFeeReq').val();
    var inspection_fee_amount = $('#inInsFeeAmt').val();
    var inspection_fee_curr = $('#slctInsFeeAmt').val();
    var inspection_reporting_freq = $('#slctInsRepFreq').val();

    //-------------------------------------------------------------------------------------

    var criteria_occ_freq = "";
    var criteria_occ_cycle_1 = "";
    var criteria_occ_cycle_2 = "";
    var criteria_occ_cycle_refresh_time = "";

    var criteria_occ_cycle_day_1 = "";

    var criteria_occ_cycle_day_2 = "";
    var criteria_occ_cycle_week_1 = "";
    var criteria_occ_cycle_week_2 = "";
    var criteria_occ_cycle_month_1 = "";
    var criteria_occ_cycle_month_2 = "";
    var criteria_occ_cycle_month_3 = "";

    var criteria_occ_cycle_year_1 = "";

    var criteria_occ_cycle_year_2 = "";

    if ($('#slctInsCriOcc').val() != 'false') {

        if ($('input[name=rdInsCriteriaOccFreq]:checked').length > 0) {
            criteria_occ_freq = $('input[name=rdInsCriteriaOccFreq]:checked').val();
        }
        criteria_occ_cycle_1 = $('#slctInsCriteriaOccCycle1').val();
        criteria_occ_cycle_2 = $('#slctInsCriteriaOccCycle2').val();
        criteria_occ_cycle_refresh_time = $('#inInsCriteriaOccTime').val();

        if ($('input[name=cbFreq]:checked').length > 0) {
            $.each($('input[name=cbFreq]:checked'), function (idx, val) {
                criteria_occ_cycle_day_1 += $(this).val() + ',';
            });

            criteria_occ_cycle_day_1 = criteria_occ_cycle_day_1.slice(0, -1);
        }

        criteria_occ_cycle_day_2 = $('#inInsCriteriaOccDay').val();
        criteria_occ_cycle_week_1 = $('#inInsCriteriaOccWeek').val();
        criteria_occ_cycle_week_2 = $('#slctInsCriteriaOccWeek').val();
        criteria_occ_cycle_month_1 = $('#inInsCriteriaOccMonth').val();
        criteria_occ_cycle_month_2 = $('#slctInsCriteriaMonth1').val();
        criteria_occ_cycle_month_3 = $('#slctInsCriteriaMonth2').val();

        if ($('input[name=rdFreqYear1]:checked').val() != 'on') {
            criteria_occ_cycle_year_1 = 'true';
        } else {
            criteria_occ_cycle_year_1 = 'false';
        }

        criteria_occ_cycle_year_2 = $('#inInsCriteriaOccYear').val();
    }

    //-------------------------------------------------------------------------------------

    var criteria_vac_freq = "";
    var criteria_vac_cycle_1 = "";
    var criteria_vac_cycle_2 = "";
    var criteria_vac_cycle_refresh_time = "";

    var criteria_vac_cycle_day_1 = "";

    var criteria_vac_cycle_day_2 = "";
    var criteria_vac_cycle_week_1 = "";
    var criteria_vac_cycle_week_2 = "";
    var criteria_vac_cycle_month_1 = "";
    var criteria_vac_cycle_month_2 = "";
    var criteria_vac_cycle_month_3 = "";

    var criteria_vac_cycle_year_1 = "";

    var criteria_vac_cycle_year_2 = "";

    if ($('#slctInsCriVac').val() != 'false') {

        if ($('input[name=rdInsCriteriaVacFreq]:checked').length > 0) {
            criteria_vac_freq = $('input[name=rdInsCriteriaVacFreq]:checked').val();
        }
        criteria_vac_cycle_1 = $('#slctInsCriteriaVacCycle1').val();
        criteria_vac_cycle_2 = $('#slctInsCriteriaVacCycle2').val();
        criteria_vac_cycle_refresh_time = $('#inInsCriteriaVacTime').val();

        if ($('input[name=cbFreqVac]:checked').length > 0) {
            $.each($('input[name=cbFreqVac]:checked'), function (idx, val) {
                criteria_vac_cycle_day_1 += $(this).val() + ',';
            });

            criteria_vac_cycle_day_1 = criteria_vac_cycle_day_1.slice(0, -1);
        }

        criteria_vac_cycle_day_2 = $('#inInsCriteriaVacDay').val();
        criteria_vac_cycle_week_1 = $('#inInsCriteriaVacWeek').val();
        criteria_vac_cycle_week_2 = $('#slctInsCriteriaVacWeek').val();
        criteria_vac_cycle_month_1 = $('#inInsCriteriaVacMonth').val();
        criteria_vac_cycle_month_2 = $('#slctInsCriteriaVacMonth1').val();
        criteria_vac_cycle_month_3 = $('#slctInsCriteriaVacMonth2').val();

        if ($('input[name=rdFreqVacYear1]:checked').val() != 'on') {
            criteria_vac_cycle_year_1 = 'true';
        } else {
            criteria_vac_cycle_year_1 = 'false';
        }

        criteria_vac_cycle_year_2 = $('#inInsCriteriaVacYear').val();

    }

    //-------------------------------------------------------------------------------------

    var fee_payment_freq = "";
    var fee_payment_cycle_1 = "";
    var fee_payment_cycle_2 = "";
    var fee_payment_cycle_refresh_time = "";

    var fee_payment_cycle_day_1 = "";

    var fee_payment_cycle_day_2 = "";
    var fee_payment_cycle_week_1 = "";
    var fee_payment_cycle_week_2 = "";
    var fee_payment_cycle_month_1 = "";
    var fee_payment_cycle_month_2 = "";
    var fee_payment_cycle_month_3 = "";

    var fee_payment_cycle_year_1 = "";

    var fee_payment_cycle_year_2 = "";

    if ($('#slctInsFeePay').val() != 'false') {

        if ($('input[name=rdInsFeePayFreq]:checked').length > 0) {
            fee_payment_freq = $('input[name=rdInsFeePayFreq]:checked').val();
        }
        fee_payment_cycle_1 = $('#slctFeePayCycle1').val();
        fee_payment_cycle_2 = $('#slctFeePayCycle2').val();
        fee_payment_cycle_refresh_time = $('#inFeePayTime').val();

        if ($('input[name=cbFreqFeePay]:checked').length > 0) {
            $.each($('input[name=cbFreqFeePay]:checked'), function (idx, val) {
                fee_payment_cycle_day_1 += $(this).val() + ',';
            });

            fee_payment_cycle_day_1 = fee_payment_cycle_day_1.slice(0, -1);
        }

        fee_payment_cycle_day_2 = $('#inFeePayFreqDay').val();
        fee_payment_cycle_week_1 = $('#inFeePayFreqWeek').val();
        fee_payment_cycle_week_2 = $('#slctFeePayFreqWeek').val();
        fee_payment_cycle_month_1 = $('#inFeePayFreqMonth').val();
        fee_payment_cycle_month_2 = $('#slctFeePayFreqMonth1').val();
        fee_payment_cycle_month_3 = $('#slctFeePayFreqMonth2').val();

        if ($('input[name=rdFreqFeePayYear1]:checked').val() != 'on') {
            fee_payment_cycle_year_1 = 'true';
        } else {
            fee_payment_cycle_year_1 = 'false';
        }

        fee_payment_cycle_year_2 = $('#inFeePayFreqYear').val();

    }

    //-------------------------------------------------------------------------------------

    var myData = [
        'inspect',
        state,
        city,
        inspection_update_req,
        inspection_criteria_occ,
        inspection_criteria_vac,
        inspection_fee_payment_freq,
        inspection_fee_req,
        inspection_fee_amount,
        inspection_fee_curr,
        inspection_reporting_freq,
        criteria_occ_freq,
        criteria_occ_cycle_1,
        criteria_occ_cycle_2,
        criteria_occ_cycle_refresh_time,
        criteria_occ_cycle_day_1,
        criteria_occ_cycle_day_2,
        criteria_occ_cycle_week_1,
        criteria_occ_cycle_week_2,
        criteria_occ_cycle_month_1,
        criteria_occ_cycle_month_2,
        criteria_occ_cycle_month_3,
        criteria_occ_cycle_year_1,
        criteria_occ_cycle_year_2,
        criteria_vac_freq,
        criteria_vac_cycle_1,
        criteria_vac_cycle_2,
        criteria_vac_cycle_refresh_time,
        criteria_vac_cycle_day_1,
        criteria_vac_cycle_day_2,
        criteria_vac_cycle_week_1,
        criteria_vac_cycle_week_2,
        criteria_vac_cycle_month_1,
        criteria_vac_cycle_month_2,
        criteria_vac_cycle_month_3,
        criteria_vac_cycle_year_1,
        criteria_vac_cycle_year_2,
        fee_payment_freq,
        fee_payment_cycle_1,
        fee_payment_cycle_2,
        fee_payment_cycle_refresh_time,
        fee_payment_cycle_day_1,
        fee_payment_cycle_day_2,
        fee_payment_cycle_week_1,
        fee_payment_cycle_week_2,
        fee_payment_cycle_month_1,
        fee_payment_cycle_month_2,
        fee_payment_cycle_month_3,
        fee_payment_cycle_year_1,
        fee_payment_cycle_year_2
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveMunicipalInfo').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var municipality_department = $('#inMuniDept').val();
    var municipality_phone_num = $('#inMuniPhone').val();
    var municipality_email = $('#inMuniEmail').val();
    var municipality_st = $('#inMuniMailStr').val();
    var municipality_city = $('#slctMuniMailCt').val();
    var municipality_state = $('#slctMuniMailSta').val();
    var municipality_zip = $('#slctMuniMailZip').val();
    var contact_person = $('#inContact').val();
    var title = $('#inTitle').val();
    var department = $('#inDept').val();
    var phone_num = $('#inPhone').val();
    var email = $('#inEmail').val();
    var address = $('#inMailing').val();
    var ops_hrs_from = $('#inHrsFrom').val();
    var ops_hrs_to = $('#inHrsTo').val();

    var myData = [
        'municipalityInfo',
        state,
        city,
        municipality_department,
        municipality_phone_num,
        municipality_email,
        municipality_st,
        municipality_city,
        municipality_state,
        municipality_zip,
        contact_person,
        title,
        department,
        phone_num,
        email,
        address,
        ops_hrs_from,
        ops_hrs_to
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveDeregistration').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var dereg_req = $('#slctDeregRequired').val();
    var conveyed = $('#slctConveyed').val();
    var occupied = $('#slctOccupied').val();
    var how_to_dereg = $('#slctHowToDereg').val();
    var upload_file = $('#inUploadPathD').val();
    var new_owner_info_req = $('#slctNewOwnerInfoReq').val();
    var proof_of_conveyance_req = $('#slctProofOfConveyReq').val();
    var date_of_sale_req = $('#slctDateOfSaleReq').val();

    var myData = [
        'dereg',
        state,
        city,
        dereg_req,
        conveyed,
        occupied,
        how_to_dereg,
        upload_file,
        new_owner_info_req,
        proof_of_conveyance_req,
        date_of_sale_req
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('#btnSaveZip').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');

    var myData;

    var saved;

    $.each($('#tblZip tbody tr'), function (idx, val) {
        var zip = $(this).find('td:eq(1)').text();
        var isActive;

        if ($(this).find('td:eq(0) div').hasClass('off')) {
            isActive = '0';
        } else {
            isActive = '1';
        }

        myData = [
            'zip',
            state,
            city,
            zip,
            isActive
        ];

        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesEncode.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.d == '1') {
                    saved = 1;
                } else {
                    saved = 0;
                }
            }, error: function (response) {
                console.log(response.responseText);
            }
        });

        if (idx == $('#tblZip tbody tr').length - 1) {
            if (saved == 1) {
                alertify.alert('Record saved successfuly!');
                $('#modalLoading').modal('hide');
            } else if (saved == 0) {
                alertify.alert('Error Saving!');
                $('#modalLoading').modal('hide');
            }
        }
    });

});

$('#btnSaveNotif').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    //var state = $('#tblState tbody tr.activeRow td').html();
    var state = $('#tblState tbody tr.activeRow td').attr('data-id');
    //var city = $('#tblCity tbody tr.activeRow td').html();
    var city = $('#tblMunicipality tbody tr.activeRow td').attr('data-id');
    var reg_send_reminder_num = $('#inRegSendRem').val();
    var reg_send_reminder_type = $('#slctRegSendRem1').val();
    var reg_send_reminder_days = $('#slctRegSendRem2').val();
    var reg_send_reminder_before = $('#slctRegSendRem3').val();
    var reg_send_reminder_time_frame = $('#slctRegSendRem4').val();
    var ren_send_reminder_num = $('#inRenewSendRem').val();
    var ren_send_reminder_type = $('#slctRenewSendRem1').val();
    var ren_send_reminder_days = $('#slctRenewSendRem2').val();
    var ren_send_reminder_before = $('#slctRenewSendRem3').val();
    var ren_send_reminder_time_frame = $('#slctRenewSendRem4').val();
    var web_notif_url = $('#inWebSendRem').val();

    var myData = [
        'notif',
        state,
        city,
        reg_send_reminder_num,
        reg_send_reminder_type,
        reg_send_reminder_days,
        reg_send_reminder_before,
        reg_send_reminder_time_frame,
        ren_send_reminder_num,
        ren_send_reminder_type,
        ren_send_reminder_days,
        ren_send_reminder_before,
        ren_send_reminder_time_frame,
        web_notif_url
    ];

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.d == '1') {
                alertify.alert('Record saved successfuly!', function () {
                    $('#tabNotification div.col-xs-12').last().find('button').prop('disabled', true);
                });
            } else {
                alertify.alert('Error Saving!');
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
});

$('.btnCancel').click(function () {
    $('#divSettings input, #divSettings select').val('');
    $('#divSettings').hide();
    $('#tblCity tbody').empty();
    $('#tblMunicipality tbody').empty();
    $.each($('#tblState tbody tr'), function () {
        $(this).removeClass('activeRow');
    });
});

$('#slctPfcDefault').change(function () {
    if ($(this).val() == 'true') {
        $('#inDefRegTimeline1').prop('disabled', false);
        $('#slctDefRegTimeline2').prop('disabled', false);
        $('#slctDefRegTimeline3').prop('disabled', false);
        $('#slctDefRegTimeline4').prop('disabled', false);
        $('#slctDefRegTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inDefRegTimeline1').prop('disabled', true);
        $('#slctDefRegTimeline2').prop('disabled', true);
        $('#slctDefRegTimeline3').prop('disabled', true);
        $('#slctDefRegTimeline4').prop('disabled', true);
        $('#slctDefRegTimeline5').prop('disabled', true);
    }
    else {
        $('#inDefRegTimeline1').prop('disabled', true);
        $('#slctDefRegTimeline2').prop('disabled', true);
        $('#slctDefRegTimeline3').prop('disabled', true);
        $('#slctDefRegTimeline4').prop('disabled', true);
        $('#slctDefRegTimeline5').prop('disabled', true);
    }
});

$('#slctPfcForeclosure').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcDefForclosureTimeline1').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline2').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline3').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline4').prop('disabled', false);
        $('#slctPfcDefForclosureTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcDefForclosureTimeline1').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline2').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline3').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline4').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcDefForclosureTimeline1').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline2').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline3').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline4').prop('disabled', true);
        $('#slctPfcDefForclosureTimeline5').prop('disabled', true);
    }
});

$('#slctPfcForeclosureVacant').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline5').prop('disabled', true);
    }
});

$('#slctPfcVacantTimeline').change(function () {
    if ($(this).val() == 'true') {
        $('#inPfcVacantTimeline1').prop('disabled', false);
        $('#slctPfcVacantTimeline2').prop('disabled', false);
        $('#slctPfcVacantTimeline3').prop('disabled', false);
        $('#slctPfcVacantTimeline4').prop('disabled', false);
        $('#slctPfcVacantTimeline5').prop('disabled', false);
    }
    else if ($(this).val() == 'false') {
        $('#inPfcVacantTimeline1').prop('disabled', true);
        $('#slctPfcVacantTimeline2').prop('disabled', true);
        $('#slctPfcVacantTimeline3').prop('disabled', true);
        $('#slctPfcVacantTimeline4').prop('disabled', true);
        $('#slctPfcVacantTimeline5').prop('disabled', true);
    }
    else {
        $('#inPfcVacantTimeline1').prop('disabled', true);
        $('#slctPfcVacantTimeline2').prop('disabled', true);
        $('#slctPfcVacantTimeline3').prop('disabled', true);
        $('#slctPfcVacantTimeline4').prop('disabled', true);
        $('#slctPfcVacantTimeline5').prop('disabled', true);
    }
});

//$('#slctVmsRenewal').change(function () {
//    if ($(this).val() == 'false' || $(this).val() == '') {
//        $('.renewYes').hide();
//        $('#btnSavePDFRenew').hide();
//        $('#linkPDFRenewal').text('');
//    } else {
//        var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
//        var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
//        var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
//        var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

//        rdRenewal1.prop('checked', true);
//        divRenewOn1.prop('disabled', true);
//        divRenewOn2.prop('disabled', true);

//        $('.renewYes').show();
//        $('#btnSavePDFRenew').show();
//    }
//});
$('#slctTypeOfRegistration').change(function () {
    if ($(this).val() == 'PDF') {
        $('#btnSavePDF').show();
        $('#btnSavePDFRenew').show();
        $(this).closest('div').next().show();
        if ($('#slctVmsRenewal').val() == 'true') {
            $('.renewupload').show();
        }
    } else {
        $(this).closest('div').next().hide();
        $('.renewupload').hide();
        $('#btnSavePDF').hide();
        $('#btnSavePDFRenew').hide();
        $('#linkPDF').text('');
        $('#linkPDFRenewal').text('');
    }
});
$('#slctREOTypeOfRegistration').change(function () {
    if ($(this).val() == 'PDF') {
        $('#btnSavePDF2').show();
        $('#btnSavePDFRenewREO').show();
        $(this).closest('div').next().show();
        if ($('#slctREOVmsRenewal').val() == 'true') {
            $('.renewUpload').show();
            $('#btnSavePDF2').show();
            $('#btnSavePDFRenewREO').show();
        }
    } else {
        $(this).closest('div').next().hide();
        $('#btnSavePDF2').hide();
        $('#btnSavePDFRenewREO').hide();
        $('.renewUpload').hide();
        $('#linkPDFREO').text('');
        $('#linkPDFRenewalREO').text('');
    }
});


$('#slctVmsRenewal').change(function () {
    if ($(this).val() == 'false' || $(this).val() == '') {
        $('.renewYes').hide();
        $('#btnSavePDFRenew').hide();

        $('#slctRenewEveryNum').val('');
        $('#slctRenewEveryYears').val('');
        $('#slctRenewOnMonths').val('');
        $('#slctRenewOnNum').val('');

    } else {
        $('#linkPDFRenewal').text('');
        if ($('#slctTypeOfRegistration').val() == 'PDF') {
            var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', true);
            divRenewOn2.prop('disabled', true);
            $('#btnSavePDFRenew').show();

            $('.renewYes').show();
        }
        else {
            var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', true);
            divRenewOn2.prop('disabled', true);

            $('.renewYes').show();
            $('.renewupload').hide();
            $('#btnSavePDFRenew').hide();

        }
    }
});

$('#slctREOVmsRenewal').change(function () {
    if ($(this).val() == 'false' || $(this).val() == '') {
        $('.renewREOYes').hide();
        $('#btnSavePDFRenewREO').hide();

        $('#slctREORenewEveryNum').val('');
        $('#slctREORenewEveryYears').val('');
        $('#slctREORenewEveryNum').val('');
        $('#slctREORenewEveryNum').val('');
    } else {
        $('#linkPDFRenewalREO').text('');
        if ($('#slctREOTypeOfRegistration').val() == 'PDF') {
            var rdRenewal1 = $('input[name=rdREORenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdREORenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', true);
            divRenewOn2.prop('disabled', true);
            $('#btnSavePDFRenewREO').show();
            $('.renewREOYes').show();
        }
        else {
            var rdRenewal1 = $('input[name=rdREORenewal]:eq(0)');
            var rdRenewal2 = $('input[name=rdREORenewal]:eq(1)');
            var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
            var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');


            rdRenewal1.prop('checked', true);
            divRenewOn1.prop('disabled', false);
            divRenewOn2.prop('disabled', false);

            $('#btnSavePDFRenewREO').hide();
            $('.renewREOYes').show();
            $('.renewUpload').hide();

        }
    }
});

$('input[name=rdRenewal]').click(function () {
    var rdRenewal1 = $('input[name=rdRenewal]:eq(0)');
    var rdRenewal2 = $('input[name=rdRenewal]:eq(1)');
    var divRenewEvery1 = rdRenewal1.closest('div').next().find('select');
    var divRenewEvery2 = rdRenewal1.closest('div').next().next().find('select');
    var divRenewOn1 = rdRenewal2.closest('div').next().find('select');
    var divRenewOn2 = rdRenewal2.closest('div').next().next().find('select');

    if ($(this).val() == 'Every') {
        divRenewEvery1.prop('disabled', false);
        divRenewEvery2.prop('disabled', false);
        divRenewOn1.prop('disabled', true);
        divRenewOn2.prop('disabled', true);
    } else {
        divRenewEvery1.prop('disabled', true);
        divRenewEvery2.prop('disabled', true);
        divRenewOn1.prop('disabled', false);
        divRenewOn2.prop('disabled', false);
    }
});

$('#slctReoBankOwed').change(function () {
    if ($(this).val() == 'true') {
        $('#Drpreobanktime').prop('disabled', false);
        $('#inReoBankOwed1').prop('disabled', false);
        $('#slctReoBankOwed2').prop('disabled', false);
        $('#slctReoBankOwed3').prop('disabled', false);
        $('#slctReoBankOwed4').prop('disabled', false);

    } else {
        $('#Drpreobanktime').prop('disabled', true);
        $('#inReoBankOwed1').prop('disabled', true);
        $('#slctReoBankOwed2').prop('disabled', true);
        $('#slctReoBankOwed3').prop('disabled', true);
        $('#slctReoBankOwed4').prop('disabled', true);
    }
});

$('#slctReoDistressedAbandoned').change(function () {
    if ($(this).val() == 'true') {
        $('#DrpReoAbandoned').prop('disabled', false);
        $('#inReoDistressedAbandoned1').prop('disabled', false);
        $('#slctReoDistressedAbandoned2').prop('disabled', false);
        $('#slctReoDistressedAbandoned3').prop('disabled', false);
        $('#slctReoDistressedAbandoned4').prop('disabled', false);
    } else {
        $('#DrpReoAbandoned').prop('disabled', true);
        $('#inReoDistressedAbandoned1').prop('disabled', true);
        $('#slctReoDistressedAbandoned2').prop('disabled', true);
        $('#slctReoDistressedAbandoned3').prop('disabled', true);
        $('#slctReoDistressedAbandoned4').prop('disabled', true);
    }
});

$('#slctReoVacant').change(function () {
    if ($(this).val() == 'true') {
        $('#slctReowith').prop('disabled', false);
        $('#inReoVacantTimeline1').prop('disabled', false);
        $('#slctReoVacantTimeline2').prop('disabled', false);
        $('#slctReoVacantTimeline3').prop('disabled', false);
        $('#slctReoVacantTimeline4').prop('disabled', false);
    } else {
        $('#slctReowith').prop('disabled', true);
        $('#inReoVacantTimeline1').prop('disabled', true);
        $('#slctReoVacantTimeline2').prop('disabled', true);
        $('#slctReoVacantTimeline3').prop('disabled', true);
        $('#slctReoVacantTimeline4').prop('disabled', true);
    }
});

$('#slctBondReq').change(function () {
    if ($(this).val() == 'false') {
        $('#inBondAmount').prop('disabled', true);
    } else {
        $('#inBondAmount').prop('disabled', false);
    }
});

$('#slctRegCost').change(function () {
    if ($(this).val() == 'false') {
        $('#tabCost div.col-xs-12:lt(27) input').prop('disabled', true);
        $('#tabCost div.col-xs-12:lt(27) select').prop('disabled', true);
        $('#tabCost div.col-xs-12:lt(27) button').prop('disabled', true);
    } else {
        $('#tabCost div.col-xs-12:lt(27) input').prop('disabled', false);
        $('#tabCost div.col-xs-12:lt(27) select').prop('disabled', false);
        $('#tabCost div.col-xs-12:lt(27) button').prop('disabled', false);
    }

    $(this).prop('disabled', false);
});

$('#inRegCost').keyup(function () {
    if ($('#slctRegCostStandard').val() == 'true') {
        $('#inRenewAmt').val($(this).val());
    }
});

$('#slctRegCostStandard').change(function () {
    if ($(this).val() == 'true') {
        $('#inRenewAmt').val($('#inRegCost').val());
        $('#slctRenewAmtCurr1').val($('#slctRegCostCurr').val());
        $('#slctRenewCostEscal').prop('disabled', true);
    } else {
        $('#slctRenewCostEscal').prop('disabled', false);
    }
});

$('#slctRenewCostEscal').change(function () {
    if ($(this).val() == 'false') {
        $('#inRenewAmt').prop('disabled', false);
        $('#slctRenewAmtCurr1').prop('disabled', false);
        $('#btnViewEscalRenewAmt').prop('disabled', true);
    } else {
        $('#inRenewAmt').prop('disabled', true);
        $('#slctRenewAmtCurr1').prop('disabled', true);
        $('#inRenewAmt').val('');
        $('#btnViewEscalRenewAmt').prop('disabled', false);
    }
});

$('#btnAddNew1').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);

        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#inComRegFee').keyup(function () {
    if ($('#slctComFeeStandard').val() == 'true') {
        $('#inRenewAmt2').val($(this).val());
    }
});

$('#slctComFeeStandard').change(function () {
    if ($(this).val() == 'true') {
        $('#inRenewAmt2').val($('#inComRegFee').val());
        $('#slctRenewAmtCurr2').val($('#slctComRegFee').val());
        $('#slctRenewCostEscal2').prop('disabled', true);
    } else {
        $('#slctRenewCostEscal2').prop('disabled', false);
    }
});

$('#slctRenewCostEscal2').change(function () {
    if ($(this).val() == 'false') {
        $('#inRenewAmt2').prop('disabled', false);
        $('#slctRenewAmtCurr2').prop('disabled', false);
        $('#btnViewEscalRenewAmt2').prop('disabled', true);
    } else {
        $('#inRenewAmt2').prop('disabled', true);
        $('#slctRenewAmtCurr2').prop('disabled', true);
        $('#inRenewAmt2').val('');
        $('#btnViewEscalRenewAmt2').prop('disabled', false);
    }
});

$('#btnAddNew2').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);

        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#slctInsCriOcc').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsCriteriaOcc').prop('disabled', false);
    } else {
        $('#btnViewInsCriteriaOcc').prop('disabled', true);
    }
});

$('#slctInsCriVac').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsCriteriaVac').prop('disabled', false);
    } else {
        $('#btnViewInsCriteriaVac').prop('disabled', true);
    }
});

$('#slctInsFeePay').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsFeePaymentFreq').prop('disabled', false);
    } else {
        $('#btnViewInsFeePaymentFreq').prop('disabled', true);
    }
});

$('#slctInsFeeReq').change(function () {
    if ($(this).val() == 'true') {
        $('#inInsFeeAmt').prop('disabled', false);
        $('#slctInsFeeAmt').prop('disabled', false);
    } else {
        $('#inInsFeeAmt').val('');
        $('#inInsFeeAmt').prop('disabled', true);
        $('#slctInsFeeAmt').prop('disabled', true);
    }
});

$('input[name=rdInsCriteriaOccFreq]:radio').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsCriteriaOccDaily').show();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').show();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').show();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').show();
    }
});

$('input[name=rdInsCriteriaVacFreq]:radio').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsCriteriaVacDaily').show();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').show();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').show();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').show();
    }
});

$('input[name=rdInsFeePayFreq]:radio').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsFeePayFreqDaily').show();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').show();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').show();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').show();
    }
});

$('input[name=rdFreq1]:radio').change(function () {
    if ($(this).val() == 'every1') {
        $('#inInsCriteriaOccDay').val('');
    } else if ($(this).val() == 'every2') {
        $("input[name = 'cbFreq']").prop('checked', false);
    }
})
$('#slctHowToDereg').change(function () {
    if ($(this).val() == 'PDF') {
        $(this).closest('div').next().show();
    } else {
        $(this).closest('div').next().hide();
    }
});

$('#pfcReg').change(function () {
    if (this.checked) {
        $('#tabPFC select').prop('disabled', true);
        $('#tabPFC input[type=number]').prop('disabled', true);
        $('#tabPFC input[type=text]').prop('disabled', true);
        $('#tabPFC select').val('');
        $('#tabPFC input[type=number]').val('');
        $('#tabPFC input[type=text]').val('');
    } else {
        $('#tabPFC select').prop('disabled', false);
        $('#tabPFC input[type=number]').prop('disabled', false);
        $('#tabPFC input[type=text]').prop('disabled', false);
        $('#tabPFC select').val('');
        $('#tabPFC input[type=number]').val('');
        $('#tabPFC input[type=text]').val('');
    }
});

$('#reoReg').change(function () {
    if (this.checked) {
        $('#tabREO select').prop('disabled', true);
        $('#tabREO input[type=number]').prop('disabled', true);
        $('#tabREO input[type=text]').prop('disabled', true);
        $('#tabREO select').val('');
        $('#tabREO input[type=number]').val('');
        $('#tabREO input[type=text]').val('');
    } else {
        $('#tabREO select').prop('disabled', false);
        $('#tabREO input[type=number]').prop('disabled', false);
        $('#tabREO input[type=text]').prop('disabled', false);
        $('#tabREO select').val('');
        $('#tabREO input[type=number]').val('');
        $('#tabREO input[type=text]').val('');
    }
});

$('#navReg li button').click(function () {
    $.each($('#navReg li'), function () {
        if ($(this).find('button').hasClass('btn-primary')) {
            $(this).find('button').removeClass('btn-primary');
            $(this).find('button').addClass('btn-default');
        }
    });

    $(this).addClass('btn-primary');
    $(this).focus();
});

$('.file').fileinput({
    'showUpload': false
});

function cellStyle(value, row, index, field) {
    return {
        classes: 'text-justify'
    };
}

$('.criteriaEdit').click(function () {
    $('#modalEdit').modal({ backdrop: 'static', keyboard: false });

    $(".timepicker").timepicker({
        showInputs: false
    });

    var fieldText = $(this).parent().parent().find('div:eq(0)').text();
    var btnData = $(this).attr('data-id');
    $('#lblEdit').text('Enter ' + fieldText + ' ');

    $.each($('#modalEdit div.panel-body'), function () {
        $(this).css('display', 'none');
    });

    $('#' + btnData).removeAttr('style');


});

$('#lciState').change(function () {
    var state = $(this).val();

    getLCICity(state);
});

$('#lciCity').change(function () {
    var city = $(this).val();

    getLCIZip(city);
});

$('#slctMuniMailSta').change(function () {
    var state = $(this).val();

    getMuniCity(state);
});

$('#slctMuniMailCt').change(function () {
    var city = $(this).val();

    getMuniZip(city);
});

function getLCIState() {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var states = d.data.record;

                $('#lciState').empty();
                $('#lciState').append('<option value="" selected="selected">--Select State--</option>');
                $.each(states, function (idx, val) {
                    $('#lciState').append('<option value="' + val.State + '">' + val.State + '</option>');
                });

                //$('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCICity(state) {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetCity',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var cities = d.data.record;

                $('#lciCity').empty();
                $('#lciCity').append('<option value="" selected="selected">--Select City--</option>');
                $.each(cities, function (idx, val) {
                    $('#lciCity').append('<option value="' + val.City + '">' + val.City + '</option>');
                });

                //$('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCIZip(city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetZip',
        data: '{city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var zips = d.data.record;

                $('#lciZip').empty();
                $('#lciZip').append('<option value="" selected="selected">--Select Zip--</option>');
                $.each(zips, function (idx, val) {
                    $('#lciZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

//Muni State
function getMuniState() {
    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetState',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var states = d.data.record;

                $('#slctMuniMailSta').empty();
                $('#slctMuniMailSta').append('<option value="" selected="selected">--Select State--</option>');
                $.each(states, function (idx, val) {
                    $('#slctMuniMailSta').append('<option value="' + val.State + '">' + val.State + '</option>');
                });

                //$('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
//Muni State

//Muni City
function getMuniCity(state) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetCity',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var cities = d.data.record;

                $('#slctMuniMailCt').empty();
                $('#slctMuniMailCt').append('<option value="" selected="selected">--Select City--</option>');
                $.each(cities, function (idx, val) {
                    $('#slctMuniMailCt').append('<option value="' + val.City + '">' + val.City + '</option>');
                });

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}
//Muni City

//Muni Zip
//function getMuniZip(city) {
//    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
//    $.ajax({
//        type: 'POST',
//        url: 'CompRulesEncode.aspx/GetZip',
//        data: '{city: "' + city + '"}',
//        contentType: 'application/json; charset=utf-8',
//        success: function (data) {
//            var d = $.parseJSON(data.d);

//            if (d.Success) {
//                var zips = d.data.record;

//                $('#slctMuniMailZip').empty();
//                $('#slctMuniMailZip').append('<option value="" selected="selected">--Select Zip--</option>');
//                $.each(zips, function (idx, val) {
//                    $('#slctMuniMailZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
//                });

//                $('#modalLoading').modal('hide');
//            }
//        },
//        error: function (response) {
//            console.log(response.responseText);
//        }
//    });
//}
//<Muni Zip

$('#populateRegistration').click(function () {

    var municipalityCode = ($('.slctCopyMuni').val());

    //if ($('#liRegistration').hasClass('active')) {
    if ($('#btntabPFC').hasClass('btn-primary')) {
        console.log('PFC');
        getCopySettingsPFC(municipalityCode, $('#btntabPFC').text());
    }
    else if ($('#btntabREO').hasClass('btn-primary')) {
        console.log('REO');
        getCopySettingsREO(municipalityCode, $('#btntabREO').text());
    }
    else if ($('#btntabProperty').hasClass('btn-primary')) {
        console.log('Property Type');
        getCopySettingsPropType(municipalityCode, $('#btntabProperty').text())
    }
    else if ($('#btntabRequirements').hasClass('btn-primary')) {
        console.log('Requirements');
        getCopySettingsRequirements(municipalityCode, $('#btntabRequirements').text());
    }
    else if ($('#btntabCost').hasClass('btn-primary')) {
        console.log('Cost');
        getCopySettingsCost(municipalityCode, $('#btntabCost').text());
    }
    else if ($('#ContReg').hasClass('btn-primary')) {
        console.log('Continuing Registration');
        getCopySettingsContReg(municipalityCode, $('#ContReg').text());
    }
    else {
        console.log('Not Clicked');
    }
    //}
    //else if ($('#liInspection').hasClass('active')) {
    //    console.log('Inspection Tab');

    //}
    //else if ($('#liMunicipality').hasClass('active')) {
    //    console.log('Municipality Tab');
    //}
    //else if ($('#liDeRegistration').hasClass('active')) {
    //    console.log('DeRegistration Tab');
    //}
    //else if ($('#liOrdinance').hasClass('active')) {
    //    console.log('Ordinance Tab');
    //}
    //else if ($('#liZip').hasClass('active')) {
    //    console.log('Zip Codes Tab');
    //}
    //else if ($('#liNotif').hasClass('active')) {
    //    console.log('Notification Tab');
    //}
    //else {
    //    console.log('Nothing to show');
    //}
});

function clickIns() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetSettings',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                console.log(records);
                $('.slctCopyInpection').empty();
                $('.slctCopyInpection').append('<option value="">--Select One--</option>');
                //$('.slctCopyInpection').empty();
                //$('.slctCopyInpection').append('<option value="">--Select One--</option>');
                $.each(records, function (idx, val) {
                    $('.slctCopyInpection').append('<option value="' + val.municipality_code + '">' + val.municipality_name + '</option>');
                    //$('.slctCopyInpection').append('<option value="' + val.municipality_code + '">' + val.municipality_name + '</option>');
                });
            }
            $('#modalLoading').modal('hide');
        }
    });
}

function clickDereg() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetSettings',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                console.log(records);
                $('.slctCopyDeregistration').empty();
                $('.slctCopyDeregistration').append('<option value="">--Select One--</option>');
                $.each(records, function (idx, val) {
                    $('.slctCopyDeregistration').append('<option value="' + val.municipality_code + '">' + val.municipality_name + '</option>');
                });
            }
            $('#modalLoading').modal('hide');
        }
    });
}

function clickNotif() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetSettings',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                console.log(records);
                $('.slctCopyNotif').empty();
                $('.slctCopyNotif').append('<option value="">--Select One--</option>');
                $.each(records, function (idx, val) {
                    $('.slctCopyNotif').append('<option value="' + val.municipality_code + '">' + val.municipality_name + '</option>');
                });
            }
            $('#modalLoading').modal('hide');
        }
    });
}

function clickMunicipality() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/GetSettings',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                console.log(records);
                $('.slctCopyMunicipality').empty();
                $('.slctCopyMunicipality').append('<option value="">--Select One--</option>');
                $.each(records, function (idx, val) {
                    $('.slctCopyMunicipality').append('<option value="' + val.municipality_code + '">' + val.municipality_name + '</option>');
                });
            }
            $('#modalLoading').modal('hide');
        }
    });
}

$('#populateInspection').click(function () {
    var municipalityCode = ($('.slctCopyInpection').val());

    getCopySettingsInspection(municipalityCode);
});

$('#populateDereg').click(function () {
    var municipalityCode = ($('.slctCopyDeregistration').val());

    getCopySettingsDeregistration(municipalityCode);
});

$('#populateNotif').click(function () {
    var municipalityCode = ($('.slctCopyNotif').val());

    getCopySettingsNotification(municipalityCode);
});

$('#populateMunicipality').click(function () {
    var municipalityCode = ($('.slctCopyMunicipality').val());

    getCopySettingsMunicipality(municipalityCode);
});

function getCopySettingsMunicipality(municipalityCode) {
    var s = ($('.slctCopyMunicipality').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateMunicipality',
        data: '{municipalityCode: "' + s + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var municipal = d.data.municipal;

                if (municipal.length > 0) {
                    if (municipal[0].tag == 'PASSED') {
                        $('#tabMunicipality div.tag').remove();
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#inMuniDept').val(municipal[0].municipality_department);
                        $('#inMuniPhone').val(municipal[0].municipality_phone_num);
                        $('#inMuniEmail').val(municipal[0].municipality_email);
                        $('#inMuniMailStr').val(municipal[0].municipality_st);
                        $('#slctMuniMailCt').val(municipal[0].municipality_city);
                        $('#slctMuniMailSta').val(municipal[0].municipality_state);
                        $('#slctMuniMailZip').val(municipal[0].municipality_zip);
                        $('#inContact').val(municipal[0].contact_person);
                        $('#inTitle').val(municipal[0].title);
                        $('#inDept').val(municipal[0].department);
                        $('#inPhone').val(municipal[0].phone_num);
                        $('#inEmail').val(municipal[0].email);
                        $('#inMailing').val(municipal[0].address);
                        $('#inHrsFrom').val(municipal[0].ops_hrs_from);
                        $('#inHrsTo').val(municipal[0].ops_hrs_to);
                    } else {
                        $('#tabMunicipality div.tag').remove();
                        $('#tabMunicipality').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + municipal[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabMunicipality div.tag').remove();

                    $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
        }
    });
}

function getCopySettingsNotification(municipalityCode) {
    var s = ($('.slctCopyNotif').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateNotif',
        data: '{municipalityCode: "' + s + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var notif = d.data.notif;

                if (notif.length > 0) {
                    if (notif[0].tag == 'PASSED') {
                        $('#tabNotif div.valid').remove();
                        $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#inRegSendRem').val(notif[0].reg_send_reminder_num);
                        $('#slctRegSendRem1').val(notif[0].reg_send_reminder_type);
                        $('#slctRegSendRem2').val(notif[0].reg_send_reminder_days);
                        $('#slctRegSendRem3').val(notif[0].reg_send_reminder_before);
                        $('#slctRegSendRem4').val(notif[0].reg_send_reminder_time_frame);
                        $('#inRenewSendRem').val(notif[0].ren_send_reminder_num);
                        $('#slctRenewSendRem1').val(notif[0].ren_send_reminder_type);
                        $('#slctRenewSendRem2').val(notif[0].ren_send_reminder_days);
                        $('#slctRenewSendRem3').val(notif[0].ren_send_reminder_before);
                        $('#slctRenewSendRem4').val(notif[0].ren_send_reminder_time_frame);
                        $('#inWebSendRem').val(notif[0].web_notif_url);
                    } else {
                        $('#tabNotif div.tag').remove();
                        $('#tabNotif').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + notif[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabNotif div.tag').remove();

                    $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getCopySettingsDeregistration(municipalityCode) {
    var s = ($('.slctCopyDeregistration').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateDereg',
        data: '{municipalityCode: "' + s + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var dereg = d.data.dereg;

                if (dereg.length > 0) {
                    if (dereg[0].tag == 'PASSED') {
                        $('#tabDeregistration div.tag').remove();
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#slctDeregRequired').val('' + dereg[0].dereg_req + '');
                        $('#slctConveyed').val('' + dereg[0].conveyed + '');
                        $('#slctOccupied').val('' + dereg[0].occupied + '');
                        $('#slctHowToDereg').val(dereg[0].how_to_dereg);
                        //$('#inUploadPathD').val(dereg[0].upload_file);
                        $('#slctNewOwnerInfoReq').val('' + dereg[0].new_owner_info_req + '');
                        $('#slctProofOfConveyReq').val('' + dereg[0].proof_of_conveyance_req + '');
                        $('#slctDateOfSaleReq').val('' + dereg[0].date_of_sale_req + '');

                        if (dereg[0].how_to_dereg == 'PDF') {
                            var fn = dereg[0].upload_file.replace('C:\\fakepath\\', '');

                            $('#slctHowToDereg').closest('div').next().show();

                            $('#tabDeregistratopm div.file-caption-main div.file-caption-name').attr('title', fn);
                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctHowToDereg').closest('div').next().hide();

                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').removeAttr('title');
                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').html('');
                        }
                    } else {
                        $('#tabDeregistration div.tag').remove();
                        $('#tabDeregistration').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + dereg[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabDeregistration div.tag').remove();

                    $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
        }
    });
}

function getCopySettingsInspection(municipalityCode) {
    var s = ($('.slctCopyInpection').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateInspection',
        data: '{municipalityCode: "' + s + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var inspect = d.data.inspect;

                if (inspect.length > 0) {
                    if (inspect[0].tag == 'PASSED') {
                        $('#tabInspection div.tag').remove();
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#slctInsUpReq').val('' + inspect[0].inspection_update_req + '');
                        $('#slctInsCriOcc').val('' + inspect[0].inspection_criteria_occ + '');

                        $('#slctInsCriVac').val(inspect[0].inspection_criteria_vac);
                        $('#slctInsFeePay').val('' + inspect[0].inspection_fee_payment_freq + '');
                        $('#slctInsFeeReq').val('' + inspect[0].inspection_fee_req + '');
                        $('#inInsFeeAmt').val('' + inspect[0].inspection_fee_amount + '');
                        $('#slctInsFeeAmt').val('' + inspect[0].inspection_fee_curr + '');
                        $('#slctInsRepFreq').val('' + inspect[0].inspection_reporting_freq + '');

                        //Criteria Occupied-------------------------------------------------------------------------------------------------------
                        if (inspect[0].inspection_criteria_occ == false) {
                            $('#btnViewInsCriteriaOcc').prop('disabled', true);
                        } else {
                            $('#btnViewInsCriteriaOcc').prop('disabled', false);

                            $('#slctInsCriteriaOccCycle1').val('' + inspect[0].criteria_occ_cycle_1 + '');
                            $('#slctInsCriteriaOccCycle2').val('' + inspect[0].criteria_occ_cycle_2 + '');

                            $('input[name="rdInsCriteriaOccFreq"][value="' + inspect[0].criteria_occ_freq + '"]').prop('checked', true);

                            var rbOccFreq = $("input[name='rdInsCriteriaOccFreq']:checked").val();

                            if (rbOccFreq == "daily") {

                                if (inspect[0].criteria_occ_cycle_day_1 != "") {
                                    $("#freqInsCriteriaOccDaily").show();
                                    $('input[name="rdFreq1"][value = "every1"]').prop('checked', true);
                                    var days = inspect[0].criteria_occ_cycle_day_1;
                                    var splitDays = days.split(',');
                                    for (i = 0; i < splitDays.length; i++) {
                                        if (splitDays[i] == "Monday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Tuesday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Wednesday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Thursday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Friday") {
                                            $('input[name="cbFreq"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                    }
                                }
                                if (inspect[0].criteria_occ_cycle_day_2 != "") {
                                    $('input[name="rdFreq1"][value = "every2"]').prop('checked', true);
                                    $("#inInsCriteriaOccDay").val('' + inspect[0].criteria_occ_cycle_day_2 + '');
                                }
                                if (inspect[0].criteria_occ_cycle_refresh_time != "") {
                                    $('input[name="cbRefresh"]').prop('checked', true);
                                    $("#inInsCriteriaOccTime").val('' + inspect[0].criteria_occ_cycle_refresh_time + '');
                                }

                            } else if (rbOccFreq == "weekly") {
                                $("#freqInsCriteriaOccWeekly").show();
                                $("#inInsCriteriaOccWeek").val('' + inspect[0].criteria_occ_cycle_week_1 + '');
                                $("#slctInsCriteriaOccWeek").val('' + inspect[0].criteria_occ_cycle_week_2 + '');

                            } else if (rbOccFreq == "monthly") {
                                $("#freqInsCriteriaOccMonthly").show();
                                $("#inInsCriteriaOccMonth").val('' + inspect[0].criteria_occ_cycle_month_1 + '');
                                $("#slctInsCriteriaMonth1").val('' + inspect[0].criteria_occ_cycle_month_2 + '');
                                $("#slctInsCriteriaMonth2").val('' + inspect[0].criteria_occ_cycle_month_3 + '');
                            } else if (rbOccFreq == "yearly") {
                                $("#freqInsCriteriaOccYearly").show();

                                if (inspect[0].criteria_occ_cycle_year_2 != "") {
                                    $('input[name="rdFreqYear1"][value="Anually"]').prop('checked', true);
                                }
                                if (inspect[0].criteria_occ_cycle_year_2 != "") {
                                    $("#inInsCriteriaOccYear").val('' + inspect[0].criteria_occ_cycle_year_2 + '');
                                }
                            }

                        }
                        //Criteria Occupied END-------------------------------------------------------------------------------------------------------


                        //Criteria Vacant-------------------------------------------------------------------------------------------------------
                        if (inspect[0].inspection_criteria_vac == false) {
                            $('#btnViewInsCriteriaVac').prop('disabled', true);
                        } else {
                            $('#btnViewInsCriteriaVac').prop('disabled', false);

                            $('#slctInsCriteriaVacCycle1').val('' + inspect[0].criteria_vac_cycle_1 + '');
                            $('#slctInsCriteriaVacCycle2').val('' + inspect[0].criteria_vac_cycle_2 + '');

                            $('input[name="rdInsCriteriaVacFreq"][value="' + inspect[0].criteria_vac_freq + '"]').prop('checked', true);

                            var rbVacFreq = $("input[name='rdInsCriteriaVacFreq']:checked").val();

                            if (rbVacFreq == "daily") {

                                if (inspect[0].criteria_vac_cycle_day_1 != "") {
                                    $("#freqInsCriteriaVacDaily").show();
                                    $('input[name="rdFreq2"][value = "every1"]').prop('checked', true);
                                    var days = inspect[0].criteria_vac_cycle_day_1;
                                    var splitDays = days.split(',');
                                    for (i = 0; i < splitDays.length; i++) {
                                        if (splitDays[i] == "Monday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Tuesday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Wednesday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Thursday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Friday") {
                                            $('input[name="cbFreqVac"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                    }
                                }
                                if (inspect[0].criteria_vac_cycle_day_2 != "") {
                                    $('input[name="rdFreq2"][value = "every2"]').prop('checked', true);
                                    $("#inInsCriteriaVacDay").val('' + inspect[0].criteria_vac_cycle_day_2 + '');
                                }

                            } else if (rbVacFreq == "weekly") {
                                $("#freqInsCriteriaVacWeekly").show();
                                $("#inInsCriteriaVacWeek").val('' + inspect[0].criteria_vac_cycle_week_1 + '');
                                $("#slctInsCriteriaVacWeek").val('' + inspect[0].criteria_vac_cycle_week_2 + '');

                            } else if (rbVacFreq == "monthly") {
                                $("#freqInsCriteriaVacMonthly").show();
                                $("#inInsCriteriaVacMonth").val('' + inspect[0].criteria_vac_cycle_month_1 + '');
                                $("#slctInsCriteriaVacMonth1").val('' + inspect[0].criteria_vac_cycle_month_2 + '');
                                $("#slctInsCriteriaVacMonth2").val('' + inspect[0].criteria_vac_cycle_month_3 + '');
                            } else if (rbVacFreq == "yearly") {
                                $("#freqInsCriteriaVacYearly").show();

                                if (inspect[0].criteria_vac_cycle_year_2 != "") {
                                    $('input[name="rdFreqVacYear1"][value="Anually"]').prop('checked', true);
                                }
                                if (inspect[0].criteria_vac_cycle_year_2 != "") {
                                    $("#inInsCriteriaVacYear").val('' + inspect[0].criteria_vac_cycle_year_2 + '');
                                }
                            }

                            if (inspect[0].criteria_vac_cycle_refresh_time != "") {
                                $('input[name="cbRefreshVac"]').prop('checked', true);
                                $("#inInsCriteriaVacTime").val('' + inspect[0].criteria_vac_cycle_refresh_time + '');
                            }
                        }
                        //Criteria Vacant END-------------------------------------------------------------------------------------------------------


                        //Fee Payment --------------------------------------------------------------------------------------
                        if (inspect[0].inspection_fee_payment_freq == false) {
                            $('#btnViewInsFeePaymentFreq').prop('disabled', true);
                        } else {
                            $('#btnViewInsFeePaymentFreq').prop('disabled', false);

                            $('#slctFeePayCycle1').val('' + inspect[0].fee_payment_cycle_1 + '');
                            $('#slctFeePayCycle2').val('' + inspect[0].fee_payment_cycle_2 + '');

                            $('input[name="rdInsFeePayFreq"][value="' + inspect[0].fee_payment_freq + '"]').prop('checked', true);

                            var rbFeeFreq = $("input[name='rdInsFeePayFreq']:checked").val();

                            if (rbFeeFreq == "daily") {

                                if (inspect[0].fee_payment_cycle_day_1 != "") {
                                    $("#freqInsFeePayFreqDaily").show();
                                    $('input[name="rdFreq3"][value = "daily1"]').prop('checked', true);
                                    var days = inspect[0].fee_payment_cycle_day_1;
                                    var splitDays = days.split(',');
                                    for (i = 0; i < splitDays.length; i++) {
                                        if (splitDays[i] == "Monday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Tuesday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Wednesday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Thursday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                        else if (splitDays[i] == "Friday") {
                                            $('input[name="cbFreqFeePay"][value="' + splitDays[i] + '"]').prop('checked', true);
                                        }
                                    }
                                }
                                if (inspect[0].fee_payment_cycle_day_2 != "") {
                                    $('input[name="rdFreq3"][value = "daily2"]').prop('checked', true);
                                    $("#inFeePayFreqDay").val('' + inspect[0].fee_payment_cycle_day_2 + '');
                                }

                            } else if (rbFeeFreq == "weekly") {
                                $("#freqInsFeePayFreqWeekly").show();
                                $("#inFeePayFreqWeek").val('' + inspect[0].fee_payment_cycle_week_1 + '');
                                $("#slctFeePayFreqWeek").val('' + inspect[0].fee_payment_cycle_week_2 + '');

                            } else if (rbFeeFreq == "monthly") {
                                $("#freqInsFeePayFreqMonthly").show();

                                if (inspect[0].fee_payment_cycle_month_1 != "") {
                                    $('input[name="rdFreq3"][value = "monthly1"]').prop('checked', true);
                                    $("#inFeePayFreqMonth").val('' + inspect[0].fee_payment_cycle_month_1 + '');
                                }
                                if (inspect[0].fee_payment_cycle_month_2 != "") {
                                    $('input[name="rdFreq3"][value = "monthly2"]').prop('checked', true);
                                    $("#slctFeePayFreqMonth1").val('' + inspect[0].fee_payment_cycle_month_2 + '');
                                }
                                if (inspect[0].fee_payment_cycle_month_3 != "") {
                                    $('input[name="rdFreq3"][value = "monthly3"]').prop('checked', true);
                                    $("#slctFeePayFreqMonth2").val('' + inspect[0].fee_payment_cycle_month_3 + '');
                                }

                            } else if (rbFeeFreq == "yearly") {
                                $("#freqInsFeePayFreqYearly").show();

                                if (inspect[0].fee_payment_cycle_year_1 != "") {
                                    $('input[name="rdFreqFeePayYear1"][value="Anually"]').prop('checked', true);
                                }
                                if (inspect[0].fee_payment_cycle_year_2 != "") {
                                    $("#inFeePayFreqYear").val('' + inspect[0].fee_payment_cycle_year_2 + '');
                                }
                            }

                        }

                        //Fee Payment END--------------------------------------------------------------------------------------

                        if (inspect[0].inspection_fee_req == false) {
                            $('#inInsFeeAmt').val('');
                            $('#inInsFeeAmt').prop('disabled', true);
                            $('#slctInsFeeAmt').prop('disabled', true);
                        } else {
                            $('#inInsFeeAmt').prop('disabled', false);
                            $('#slctInsFeeAmt').prop('disabled', false);
                        }
                    } else {
                        $('#tabInspection div.tag').remove();
                        $('#tabInspection').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + inspect[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabInspection div.tag').remove();
                    $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
        }
    });
}

function getCopySettingsContReg(municipalityCode, Tab) {
    var s = ($('.slctCopyMuni').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateData',
        data: '{municipalityCode: "' + s + '", Tab: "Continuing Registration"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regCont = d.data.regCont;

                //$('#slctContReg').val(regCont[0].cont_reg);
                //$('#slctUpdateReg').val(regCont[0].update_reg);

                if (regCont.length > 0) {
                    if (regCont[0].cont_reg_tag == 'PASSED') {
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#slctContReg').val(regCont[0].cont_reg);
                        $('#slctUpdateReg').val(regCont[0].update_reg);

                    } else {
                        $('#tabContReg div.tag').remove();
                        $('#tabContReg').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regCont[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabContReg div:last button:eq(0)').prop('disabled', true);
                    }
                } else {
                    $('#tabContReg div.tag').remove();

                    $('#tabContReg div:last button:eq(0)').prop('disabled', false);
                }
            }
        }
    });
}

function getCopySettingsCost(municipalityCode, Tab) {
    var s = ($('.slctCopyMuni').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateData',
        data: '{municipalityCode: "' + s + '", Tab: "Cost"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regCost = d.data.regCost;

                if (regCost.length > 0) {
                    if (regCost[0].tag == 'PASSED') {
                        $('#tabCost div.tag').remove();
                        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', false);

                        $('#slctRegCost').val('' + regCost[0].reg_cost + '');
                        $('#inRegCost').val(regCost[0].reg_cost_amt);
                        $('#slctRegCostCurr').val(regCost[0].reg_cost_curr);
                        $('#slctRegCostStandard').val('' + regCost[0].reg_cost_standard + '');
                        $('#slctRenewCostEscal').val('' + regCost[0].is_renewal_cost_escal1 + '');
                        $('#inRenewAmt').val(regCost[0].renewal_fee_amt);
                        $('#slctRenewAmtCurr1').val(regCost[0].renewal_fee_curr);
                        $('#inComRegFee').val(regCost[0].com_reg_fee);
                        $('#slctComRegFee').val(regCost[0].com_reg_curr);
                        $('#slctComFeeStandard').val(regCost[0].com_fee_standard);
                        $('#slctRenewCostEscal2').val('' + regCost[0].is_renew_cost_escal2 + '');
                        $('#inRenewAmt2').val(regCost[0].com_renew_cost_amt);
                        $('#slctRenewAmtCurr2').val(regCost[0].com_renew_cost_curr);
                        $('#slctRegCostStandard2').val('' + regCost[0].is_reg_cost_standard + '');

                        var splitServType1 = regCost[0].reg_escal_service_type.split(',');
                        var splitAmt1 = regCost[0].reg_escal_amount.split(',');
                        var splitCurr1 = regCost[0].reg_escal_curr.split(',');
                        var splitSucceeding1 = regCost[0].reg_escal_succeeding.split(',');

                        $('#divEscalRenewal div.escalInput').empty();

                        $.each(splitServType1, function (idx, val) {
                            if (idx == 0) {
                                $('#divEscalRenewal div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="renewal" selected="selected">Renewal</option>' +
                                                '<option value="renewal-2">Renewal-2</option>' +
                                                '<option value="renewal-3">Renewal-3</option>' +
                                                '<option value="renewal-4">Renewal-4</option>' +
                                                '<option value="renewal-5">Renewal-5</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '</div>'
                                );

                                $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            } else {
                                var checked = (splitSucceeding1 == 'true') ? 'checked' : '';
                                $('#divEscalRenewal div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="renewal">Renewal</option>' +
                                                '<option value="renewal-2">Renewal-2</option>' +
                                                '<option value="renewal-3">Renewal-3</option>' +
                                                '<option value="renewal-4">Renewal-4</option>' +
                                                '<option value="renewal-5">Renewal-5</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                                );

                                $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            }
                        });

                        var splitServType2 = regCost[0].com_escal_service_type.split(',');
                        var splitAmt2 = regCost[0].com_escal_amount.split(',');
                        var splitCurr2 = regCost[0].com_escal_curr.split(',');
                        var splitSucceeding2 = regCost[0].com_escal_succeeding.split(',');

                        $('#divEscalRenewal2 div.escalInput').empty();

                        $.each(splitServType2, function (idx, val) {
                            if (idx == 0) {
                                $('#divEscalRenewal2 div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="renewal" selected="selected">Renewal</option>' +
                                                '<option value="renewal-2">Renewal-2</option>' +
                                                '<option value="renewal-3">Renewal-3</option>' +
                                                '<option value="renewal-4">Renewal-4</option>' +
                                                '<option value="renewal-5">Renewal-5</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '</div>'
                                );

                                $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            } else {
                                var checked = (splitSucceeding2 == 'true') ? 'checked' : '';
                                $('#divEscalRenewal2 div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="renewal">Renewal</option>' +
                                                '<option value="renewal-2">Renewal-2</option>' +
                                                '<option value="renewal-3">Renewal-3</option>' +
                                                '<option value="renewal-4">Renewal-4</option>' +
                                                '<option value="renewal-5">Renewal-5</option>' +
                                            '</select>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                                );

                                $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            }
                        });
                    } else {
                        $('#tabCost div.tag').remove();
                        $('#tabCost').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regCost[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabCost div.tag').remove();

                    $('#tabCost div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
        }
    });
}

function getCopySettingsRequirements(municipalityCode, Tab) {
    var s = ($('.slctCopyMuni').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateData',
        data: '{municipalityCode: "' + s + '", Tab: "Requirements"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regPropReq = d.data.regPropReq;

                if (regPropReq.length > 0) {
                    if (regPropReq[0].tag == 'PASSED') {
                        $('#tabPropReq div.tag').remove();
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);

                        var add_info = '';
                        if (regPropReq[0].addn_info != '') {
                            var split = regPropReq[0].addn_info.split(',');
                            $('#slctAddnInfo').multiselect('select', split);
                        }

                        $('#slctFirstTimeVacancyDate').val('' + regPropReq[0].first_time_vacancy_date + '');
                        $('#slctPresaleDefinition').val(regPropReq[0].presale_definition);
                        $('#slctSecuredRequired').val('' + regPropReq[0].secured_required + '');
                        $('#slctLocalContactRequired').val('' + regPropReq[0].local_contact_required + '');
                        $('#slctAddSignReq').val('' + regPropReq[0].additional_signage_required + '');
                        $('#slctPicReq').val('' + regPropReq[0].pictures_required + '');
                        $('#slctGseExclusion').val('' + regPropReq[0].gse_exclusion + '');
                        $('#slctMobileVINReq').val('' + regPropReq[0].mobile_vin_number_required + '');
                        $('#slctInsuranceReq').val('' + regPropReq[0].insurance_required + '');
                        $('#slctParcelReq').val('' + regPropReq[0].parcel_number_required + '');
                        $('#slctForeclosureActInfo').val('' + regPropReq[0].foreclosure_action_information_needed + '');
                        $('#slctLegalDescReq').val('' + regPropReq[0].legal_description_required + '');
                        $('#slctForeclosureCaseInfo').val('' + regPropReq[0].foreclosure_case_information_needed + '');
                        $('#slctBlockLotReq').val('' + regPropReq[0].block_and_lot_number_required + '');
                        $('#slctForeclosureDeedReq').val('' + regPropReq[0].foreclosure_deed_required + '');
                        $('#slctAttyInfoReq').val('' + regPropReq[0].attorney_information_required + '');
                        $('#slctBondReq').val('' + regPropReq[0].bond_required + '');
                        $('#slctBrkInfoReq').val('' + regPropReq[0].broker_information_required_if_reo + '');
                        $('#inBondAmount').val(regPropReq[0].bond_amount);
                        $('#slctMortContactNameReq').val('' + regPropReq[0].mortgage_contact_name_required + '');
                        $('#slctMaintePlanReq').val('' + regPropReq[0].maintenance_plan_required + '');
                        $('#slctClientTaxReq').val('' + regPropReq[0].client_tax_number_required + '');
                        $('#slctNoTrespassReq').val('' + regPropReq[0].no_trespass_form_required + '');
                        $('#slctSignReq').val('' + regPropReq[0].signature_required + '');
                        $('#slctUtilityInfoReq').val('' + regPropReq[0].utility_information_required + '');
                        $('#slctNotarizationReq').val('' + regPropReq[0].notarization_required + '');
                        $('#slctWinterReq').val('' + regPropReq[0].winterization_required + '');
                        $('#slctRecInsDate').val('' + regPropReq[0].recent_inspection_date + '');

                        $('#lciCompany').val(regPropReq[0].lcicompany_name);
                        $('#lciFirstName').val(regPropReq[0].lcifirst_name);
                        $('#lciLastName').val(regPropReq[0].lcilast_name);
                        $('#lciTitle').val(regPropReq[0].lcititle);
                        $('#lciBusinessLicenseNum').val(regPropReq[0].lcibusiness_license_num);
                        $('#lciPhoneNum1').val(regPropReq[0].lciphone_num1);
                        $('#lciPhoneNum2').val(regPropReq[0].lciphone_num2);
                        $('#lciBusinessPhoneNum1').val(regPropReq[0].lcibusiness_phone_num1);
                        $('#lciBusinessPhoneNum2').val(regPropReq[0].lcibusiness_phone_num2);
                        $('#lciEmrPhone1').val(regPropReq[0].lciemergency_phone_num1);
                        $('#lciEmrPhone2').val(regPropReq[0].lciemergency_phone_num2);
                        $('#lciFaxNum1').val(regPropReq[0].lcifax_num1);
                        $('#lciFaxNum2').val(regPropReq[0].lcifax_num2);
                        $('#lciCellNum1').val(regPropReq[0].lcicell_num1);
                        $('#lciCellNum2').val(regPropReq[0].lcicell_num2);
                        $('#lciEmail').val(regPropReq[0].lciemail);
                        $('#lciStreet').val(regPropReq[0].lcistreet);
                        $('#lciState').val(regPropReq[0].lcistate);
                        $('#lciCity').val(regPropReq[0].lcicity);
                        $('#lciZip').val(regPropReq[0].lcizip);
                        $('#lciHrsFrom').val(regPropReq[0].lcihours_from);
                        $('#lciHrsTo').val(regPropReq[0].lcihours_to);
                    } else {
                        $('#tabPropReq div.tag').remove();
                        $('#tabPropReq').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regPropReq[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                    }
                } else {
                    $('#tabPropReq div.tag').remove();

                    $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
        }
    });
}

function getCopySettingsPropType(municipalityCode, Tab) {
    var s = ($('.slctCopyMuni').val());

    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateData',
        data: '{municipalityCode: "' + s + '", Tab: "Property Type"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regPropType = d.data.regPropType;

                if (regPropType.length > 0) {
                    if (regPropType[0].tag == 'PASSED') {
                        $('#tabPropType div.tag').remove();
                        $('#tabPropType div:last button:eq(0)').prop('disabled', false);

                        $('#slctPropResidential').val('' + regPropType[0].residential + '');
                        $('#slctSingleFamily').val('' + regPropType[0].single_family + '');
                        $('#slctMultiFamily').val('' + regPropType[0].multi_family + '');
                        $('#slct2units').val('' + regPropType[0].unit2 + '');
                        $('#slct3units').val('' + regPropType[0].unit3 + '');
                        $('#slct4units').val('' + regPropType[0].unit4 + '');
                        $('#slct5units').val('' + regPropType[0].unit5 + '');
                        $('#slctPropRental').val('' + regPropType[0].rental + '');
                        $('#slctPropCommercial').val('' + regPropType[0].commercial + '');
                        $('#slctPropCondo').val('' + regPropType[0].condo + '');
                        $('#slctPropTownhome').val('' + regPropType[0].townhome + '');
                        $('#slctPropVacantLot').val('' + regPropType[0].vacant_lot + '');
                        $('#slctPropMobilehome').val('' + regPropType[0].mobile_home + '');
                    } else {
                        $('#tabPropType div.tag').remove();
                        $('#tabPropType').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regPropType[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabPropType div:last button:eq(0)').prop('disabled', true);
                    }
                } else {
                    $('#tabPropType div.tag').remove();

                    $('#tabPropType div:last button:eq(0)').prop('disabled', false);
                }
            }
        }
    });
}

function getCopySettingsPFC(municipalityCode, Tab) {
    var s = ($('.slctCopyMuni').val());


    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateData',
        data: '{municipalityCode: "' + s + '", Tab: "PFC"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                // PFC
                var regPFC = d.data.regPFC;

                if (regPFC.length > 0) {
                    if (regPFC[0].tag == 'PASSED') {
                        $('#tabPFC div.tag').remove();
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);



                        //PFC DEFAULT
                        $('#slctPfcDefault').val('' + regPFC[0].pfc_default + '');
                        $('#inDefRegTimeline1').val(regPFC[0].def_reg_timeline1);
                        $('#slctDefRegTimeline2').val(regPFC[0].def_reg_timeline2);
                        $('#slctDefRegTimeline3').val(regPFC[0].def_reg_timeline3);
                        $('#slctDefRegTimeline4').val(regPFC[0].def_reg_timeline4);
                        $('#slctDefRegTimeline5').val(regPFC[0].def_reg_timeline5);

                        //PFC VACANT
                        $('#slctPfcVacantTimeline').val('' + regPFC[0].pfc_vacant + '');
                        $('#inPfcVacantTimeline1').val('' + regPFC[0].pfc_vacant_timeline1);
                        $('#slctPfcVacantTimeline2').val('' + regPFC[0].pfc_vacant_timeline2);
                        $('#slctPfcVacantTimeline3').val('' + regPFC[0].pfc_vacant_timeline3);
                        $('#slctPfcVacantTimeline4').val('' + regPFC[0].pfc_vacant_timeline4);
                        $('#slctPfcVacantTimeline5').val('' + regPFC[0].pfc_vacant_timeline5);

                        //PFC FORECLOSURE
                        $('#slctPfcForeclosure').val('' + regPFC[0].pfc_foreclosure + '');
                        $('#inPfcDefForclosureTimeline1').val(regPFC[0].pfc_def_foreclosure_timeline1);
                        $('#slctPfcDefForclosureTimeline2').val(regPFC[0].pfc_def_foreclosure_timeline2);
                        $('#slctPfcDefForclosureTimeline3').val(regPFC[0].pfc_def_foreclosure_timeline3);
                        $('#slctPfcDefForclosureTimeline4').val(regPFC[0].pfc_def_foreclosure_timeline4);
                        $('#slctPfcDefForclosureTimeline5').val(regPFC[0].pfc_def_foreclosure_timeline5);

                        //FORECLOSURE AND VACANT
                        $('#slctPfcForeclosureVacant').val('' + regPFC[0].pfc_foreclosure_vacant + '');
                        $('#inPfcForeclosureVacantTimeline1').val(regPFC[0].pfc_foreclosure_vacant_timeline1);
                        $('#slctPfcForeclosureVacantTimeline2').val(regPFC[0].pfc_foreclosure_vacant_timeline2);
                        $('#slctPfcForeclosureVacantTimeline3').val(regPFC[0].pfc_foreclosure_vacant_timeline3);
                        $('#slctPfcForeclosureVacantTimeline4').val(regPFC[0].pfc_foreclosure_vacant_timeline4);
                        $('#slctPfcForeclosureVacantTimeline5').val(regPFC[0].pfc_foreclosure_vacant_timeline5);

                        //CITY NOTICE
                        $('#slctPfcCityNotice').val('' + regPFC[0].pfc_city_notice + '');
                        $('#inPfcCityNoticeTimeline1').val(regPFC[0].pfc_city_notice_timeline1);
                        $('#slctPfcCityNoticeTimeline2').val(regPFC[0].pfc_city_notice_timeline2);
                        $('#slctPfcCityNoticeTimeline3').val(regPFC[0].pfc_city_notice_timeline3);
                        $('#slctPfcCityNoticeTimeline4').val(regPFC[0].pfc_city_notice_timeline4);
                        $('#slctPfcCityNoticeTimeline5').val(regPFC[0].pfc_city_notice_timeline5);

                        //CODE VIOLATION
                        $('#slctPfcCodeViolation').val('' + regPFC[0].pfc_code_violation + '');
                        $('#inPfcCodeViolationTimeline1').val(regPFC[0].pfc_code_violation_timeline1);
                        $('#slctPfcCodeViolationTimeline2').val(regPFC[0].pfc_code_violation_timeline2);
                        $('#slctPfcCodeViolationTimeline3').val(regPFC[0].pfc_code_violation_timeline3);
                        $('#slctPfcCodeViolationTimeline4').val(regPFC[0].pfc_code_violation_timeline4);
                        $('#slctPfcCodeViolationTimeline5').val(regPFC[0].pfc_code_violation_timeline5);

                        //BOARDED
                        $('#slctPfcBoarded').val('' + regPFC[0].pfc_boarded + '');
                        $('#inPfcBoardedTimeline1').val(regPFC[0].pfc_boarded_timeline1);
                        $('#slctPfcBoardedTimeline2').val(regPFC[0].pfc_boarded_timeline2);
                        $('#slctPfcBoardedTimeline3').val(regPFC[0].pfc_boarded_timeline3);
                        $('#slctPfcBoardedTimeline4').val(regPFC[0].pfc_boarded_timeline4);
                        $('#slctPfcBoardedTimeline5').val(regPFC[0].pfc_boarded_timeline5);

                        //OTHERS
                        $('#slctPfcOther').val('' + regPFC[0].pfc_other + '');
                        $('#inPfcOtherTimeline1').val(regPFC[0].pfc_other_timeline1);
                        $('#slctPfcOtherTimeline2').val(regPFC[0].pfc_other_timeline2);
                        $('#slctPfcOtherTimeline3').val(regPFC[0].pfc_other_timeline3);
                        $('#slctPfcOtherTimeline4').val(regPFC[0].pfc_other_timeline4);
                        $('#slctPfcOtherTimeline5').val(regPFC[0].pfc_other_timeline5);


                        //$('#slctSpcReq').val(regPFC[0].special_requirements);
                        $('#slctPaymentType').val(regPFC[0].payment_type);
                        $('#slctTypeOfRegistration').val(regPFC[0].type_of_registration);
                        $('#slctVmsRenewal').val('' + regPFC[0].vms_renewal + '');


                        //if (regPFC[0].vms_renewal == false || regPFC[0].vms_renewal == '') {
                        //    $('.renewYes').hide();
                        //} else {
                        //    $('.renewYes').show();

                        //    if (regPFC[0].renew_every == 'false') {
                        //        $('#slctRenewOnMonths').prop('disabled', false);
                        //        $('#slctRenewOnNum').prop('disabled', false);
                        //        $('#slctRenewEveryNum').prop('disabled', true);
                        //        $('#slctRenewEveryYears').prop('disabled', true);

                        //        $('#RDON').prop('checked', true);
                        //        $('#RDEVERY').prop('checked', false);
                        //    } else {
                        //        $('#slctRenewEveryNum').prop('disabled', false);
                        //        $('#slctRenewEveryYears').prop('disabled', false);
                        //        $('#slctRenewOnMonths').prop('disabled', true);
                        //        $('#slctRenewOnNum').prop('disabled', true);

                        //        $('#RDEVERY').prop('checked', true);
                        //        $('#RDON').prop('checked', false);
                        //    }
                        //}


                        if (regPFC[0].vms_renewal == false || regPFC[0].vms_renewal == '') {

                            $('.renewYes').hide();
                        } else {

                            $('.renewYes').show();

                            if (regPFC[0].renew_every == 'false') {

                                $('#slctRenewOnMonths').prop('disabled', false);
                                $('#slctRenewOnNum').prop('disabled', false);

                                $('#RDON').prop('checked', true);
                                $('#RDEVERY').prop('checked', false);


                                $('#slctRenewOnMonths').val(regPFC[0].renew_on_months);
                                $('#slctRenewOnNum').val(regPFC[0].renew_on_num);


                                $('#slctRenewEveryNum').prop('disabled', true);
                                $('#slctRenewEveryYears').prop('disabled', true);
                            } else {
                                $('#slctRenewEveryNum').prop('disabled', false);
                                $('#slctRenewEveryYears').prop('disabled', false);

                                $('#RDEVERY').prop('checked', true);
                                $('#RDON').prop('checked', false);

                                $('#slctRenewEveryNum').val(regPFC[0].renew_every_num);
                                $('#slctRenewEveryYears').val(regPFC[0].renew_every_years);


                                $('#slctRenewOnMonths').prop('disabled', true);
                                $('#slctRenewOnNum').prop('disabled', true);
                            }

                        }


                        if (regPFC[0].pfc_foreclosure_vacant == false) {
                            $('#inPfcForeclosureVacantTimeline1').val('');
                            $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
                            $('#slctPfcForeclosureVacantTimeline5').prop('disabled', true);
                        } else {
                            $('#inPfcForeclosureVacantTimeline1').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline2').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline3').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline4').prop('disabled', false);
                            $('#slctPfcForeclosureVacantTimeline5').prop('disabled', false);
                        }

                        if (regPFC[0].pfc_vacant == false) {
                            $('#inPfcVacantTimeline1').val('');
                            $('#inPfcVacantTimeline1').prop('disabled', true);
                            $('#slctPfcVacantTimeline2').prop('disabled', true);
                            $('#slctPfcVacantTimeline3').prop('disabled', true);
                            $('#slctPfcVacantTimeline4').prop('disabled', true);
                            $('#slctPfcVacantTimeline5').prop('disabled', true);
                        } else {
                            $('#inPfcVacantTimeline1').prop('disabled', false);
                            $('#slctPfcVacantTimeline2').prop('disabled', false);
                            $('#slctPfcVacantTimeline3').prop('disabled', false);
                            $('#slctPfcVacantTimeline4').prop('disabled', false);
                            $('#slctPfcVacantTimeline5').prop('disabled', false);
                        }
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////
                        if (regPFC[0].type_of_registration == 'PDF') {
                            var fn = regPFC[0].upload_path;
                            $('#linkPDF').text(fn);
                            //$('#linkButton').show();
                            $('#slctTypeOfRegistration').closest('div').next().show();
                            $('#btnSavePDF').show();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fn);
                            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctTypeOfRegistration').closest('div').next().hide();
                            $('#btnSavePDF').hide();
                            //$('#linkButton').hide();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                        }



                        if (regPFC[0].type_of_registration == 'PDF' && $('#slctVmsRenewal').val() == 'true' && regPFC[0].renew_upload_path != '') {
                            var fname = regPFC[0].renew_upload_path;

                            $('#linkPDFRenewal').text(fname);
                            $('#slctVmsRenewal').closest('div').next().show();
                            $('#btnSavePDFRenew').show();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fname);
                            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fname);
                        }
                        else {
                            $('#slctVmsRenewal').closest('div').next().hide();
                            $('#btnSavePDFRenew').hide();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                        }
                        /////////////////////////////////////////////////////////////////////////////////////////////////////////

                        if (regPFC[0].no_pfc_reg == true) {
                            $('#pfcReg').prop('checked', true);
                            $('#tabPFC select').prop('disabled', true);
                            $('#tabPFC input[type=number]').prop('disabled', true);
                            $('#tabPFC input[type=text]').prop('disabled', true);
                            $('#tabPFC select').val('');
                            $('#tabPFC input[type=number]').val('');
                            $('#tabPFC input[type=text]').val('');
                        } else {
                            $('#pfcReg').prop('checked', false);
                            $('#tabPFC select').prop('disabled', false);
                            $('#tabPFC input[type=number]').prop('disabled', false);
                            $('#tabPFC input[type=text]').prop('disabled', false);
                        }

                        if (regPFC[0].statewide_reg == true) {
                            $('#pfcStatewideReg').prop('checked', true);
                        } else {
                            $('#pfcStatewideReg').prop('checked', false);
                        }

                    } else {
                        $('#tabPFC div.tag').remove();
                        $('#tabPFC').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regPFC[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                    }


                    if ($('#RDEVERY').is(':checked')) {
                        $('#slctRenewEveryNum').prop('disabled', false);
                        $('#slctRenewEveryYears').prop('disabled', false);
                        $('#slctRenewOnMonths').prop('disabled', true);
                        $('#slctRenewOnNum').prop('disabled', true);

                    } else if ($('#RDON').is(':checked')) {
                        $('#slctRenewOnMonths').prop('disabled', false);
                        $('#slctRenewOnNum').prop('disabled', false);
                        $('#slctRenewEveryNum').prop('disabled', true);
                        $('#slctRenewEveryYears').prop('disabled', true);
                    } else {
                        $('#slctRenewOnMonths').prop('disabled', true);
                        $('#slctRenewOnNum').prop('disabled', true);
                        $('#slctRenewEveryNum').prop('disabled', true);
                        $('#slctRenewEveryYears').prop('disabled', true);
                    }

                    $('#modalLoading').modal('hide');

                } else {
                    $('#tabPFC div.tag').remove();

                    $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
            $('#modalLoading').modal('hide');
        }
    });
}

function getCopySettingsREO(municipalityCode, Tab) {
    var s = ($('.slctCopyMuni').val());


    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesEncode.aspx/populateData',
        data: '{municipalityCode: "' + s + '", Tab: "REO"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {

                var regREO = d.data.regREO;

                if (regREO.length > 0) {
                    if (regREO[0].tag == 'PASSED') {
                        $('#tabREO div.tag').remove();
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);

                        //BANKED-OWED
                        $('#slctReoBankOwed').val('' + regREO[0].reo_bank_owed + '');
                        $('#Drpreobanktime').val(regREO[0].reo_bank_owed_timeline1);
                        $('#inReoBankOwed1').val(regREO[0].reo_bank_owed_timeline2);
                        $('#slctReoBankOwed2').val(regREO[0].reo_bank_owed_timeline3);
                        $('#slctReoBankOwed3').val(regREO[0].reo_bank_owed_timeline4);
                        $('#slctReoBankOwed4').val(regREO[0].reo_bank_owed_timeline5);

                        //VACANT
                        $('#slctReoVacant').val('' + regREO[0].reo_vacant + '');
                        $('#slctReowith').val(regREO[0].reo_vacant_timeline1);
                        $('#inReoVacantTimeline1').val(regREO[0].reo_vacant_timeline2);
                        $('#slctReoVacantTimeline2').val(regREO[0].reo_vacant_timeline3);
                        $('#slctReoVacantTimeline3').val(regREO[0].reo_vacant_timeline4);
                        $('#slctReoVacantTimeline4').val(regREO[0].reo_vacant_timeline5);

                        //CITY NOTICE
                        $('#slctReoCityNotice').val('' + regREO[0].reo_city_notice + '');
                        $('#slctReocitynoticewith').val(regREO[0].reo_city_notice_timeline1);
                        $('#slctReocitytimeline1').val(regREO[0].reo_city_notice_timeline2);
                        $('#slctReoBusinesstimeline2').val(regREO[0].reo_city_notice_timeline3);
                        $('#slctReodaystimeline3').val(regREO[0].reo_city_notice_timeline4);
                        $('#slctReovacancytimeline4').val(regREO[0].reo_city_notice_timeline5);

                        //CODE VIOLATION
                        $('#slctReoCodeViolation').val('' + regREO[0].reo_code_violation + '');
                        $('#slctReoviolationwith').val(regREO[0].reo_code_violation_timeline1);
                        $('#slctReoViolationtimeline1').val(regREO[0].reo_code_violation_timeline2);
                        $('#slctReoviolationtimeline2').val(regREO[0].reo_code_violation_timeline3);
                        $('#slctReoviolationdaystimeline3').val(regREO[0].reo_code_violation_timeline4);
                        $('#slctReoviolationvacancytimeline4').val(regREO[0].reo_code_violation_timeline5);

                        //BOARDED ONLY
                        $('#slctReoBoardedOnly').val('' + regREO[0].reo_boarded_only + '');
                        $('#slctReoBoardedwith').val(regREO[0].reo_boarded_only_timeline1);
                        $('#slctReoBoardedtimeline1').val(regREO[0].reo_boarded_only_timeline2);
                        $('#slctReoBoardedtimeline2').val(regREO[0].reo_boarded_only_timeline3);
                        $('#slctReoBoardeddaystimeline3').val(regREO[0].reo_boarded_only_timeline4);
                        $('#slctReoBoardedtimeline4').val(regREO[0].reo_boarded_only_timeline5);

                        //DISTRESSED ABANDONED
                        $('#slctReoDistressedAbandoned').val('' + regREO[0].reo_distressed_abandoned + '');
                        $('#DrpReoAbandoned').val(regREO[0].reo_distressed_abandoned1);
                        $('#inReoDistressedAbandoned1').val(regREO[0].reo_distressed_abandoned2);
                        $('#slctReoDistressedAbandoned2').val(regREO[0].reo_distressed_abandoned3);
                        $('#slctReoDistressedAbandoned3').val(regREO[0].reo_distressed_abandoned4);
                        $('#slctReoDistressedAbandoned4').val(regREO[0].reo_distressed_abandoned5);

                        //RENTAL REGISTRATION
                        $('#slctRentalRegistration').val('' + regREO[0].rental_registration + '');
                        $('#slctRentalFormwithin').val(regREO[0].rental_registration_timeline1);
                        $('#slctReorentaltimeline1').val(regREO[0].rental_registration_timeline2);
                        $('#slctReorentaltimeline2').val(regREO[0].rental_registration_timeline3);
                        $('#slctReorentaltimeline3').val(regREO[0].rental_registration_timeline4);
                        $('#slctReorentaltimeline4').val(regREO[0].rental_registration_timeline5);

                        //OTHERS
                        $('#slctReoOther').val(regREO[0].reo_other);
                        $('#slctreootherwithin').val(regREO[0].reo_other_timeline1);
                        $('#inReoOtherTimeline1').val(regREO[0].reo_other_timeline2);
                        $('#slctReoOtherTimeline2').val(regREO[0].reo_other_timeline3);
                        $('#slctReoOtherTimeline3').val(regREO[0].reo_other_timeline4);
                        $('#slctReoOtherTimeline4').val(regREO[0].reo_other_timeline5);

                        //$('#slctRentalForm').val('' + regREO[0].rental_form + '');
                        //$('#slctREOSpcReq').val(regREO[0].special_requirements);
                        $('#slctREOPaymentType').val(regREO[0].payment_type);
                        $('#slctREOTypeOfRegistration').val(regREO[0].type_of_registration);
                        $('#slctREOVmsRenewal').val('' + regREO[0].vms_renewal + '');

                        $('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                        $('#slctREORenewOnNum').val(regREO[0].renew_on_num);
                        $('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                        $('#slctREORenewEveryYears').val(regREO[0].renew_every_years);

                        //}

                        if (regREO[0].vms_renewal == false || regREO[0].vms_renewal == '') {
                            $('.renewREOYes').hide();
                        } else {
                            $('.renewREOYes').show();

                            if (regREO[0].renew_every == 'false') {
                                $('#slctREORenewEveryNum').prop('disabled', true);
                                $('#slctREORenewEveryYears').prop('disabled', true);

                                $('#slctREORenewOnMonths').prop('disabled', false);
                                $('#slctREORenewOnNum').prop('disabled', false);

                                $('#Reordon').prop('checked', true);
                                $('#Reordvery').prop('checked', false);

                                //$('#slctREORenewOnMonths').val(regREO[0].renew_on_months);
                                //$('#slctREORenewOnNum').val(regREO[0].renew_on_num);

                            } else {
                                $('#slctREORenewEveryNum').prop('disabled', false);
                                $('#slctREORenewEveryYears').prop('disabled', false);

                                $('#slctREORenewOnMonths').prop('disabled', true);
                                $('#slctREORenewOnNum').prop('disabled', true);

                                $('#Reordvery').prop('checked', true);
                                $('#Reordon').prop('checked', false);

                                //$('#slctREORenewEveryNum').val(regREO[0].renew_every_num);
                                //$('#slctREORenewEveryYears').val(regREO[0].renew_every_years);
                            }
                        }

                        //if (regREO[0].reo_bank_owed == false) {
                        //    $('#inReoBankOwed1').val('');
                        //    $('#inReoBankOwed1').prop('disabled', true);
                        //    $('#slctReoBankOwed2').prop('disabled', true);
                        //    $('#slctReoBankOwed3').prop('disabled', true);
                        //    $('#slctReoBankOwed4').prop('disabled', true);
                        //} else {
                        //    $('#inReoBankOwed1').prop('disabled', false);
                        //    $('#slctReoBankOwed2').prop('disabled', false);
                        //    $('#slctReoBankOwed3').prop('disabled', false);
                        //    $('#slctReoBankOwed4').prop('disabled', false);
                        //}

                        //if (regREO[0].reo_distressed_abandoned == false) {
                        //    $('#inReoDistressedAbandoned1').val('');
                        //    $('#inReoDistressedAbandoned1').prop('disabled', true);
                        //    $('#slctReoDistressedAbandoned2').prop('disabled', true);
                        //    $('#slctReoDistressedAbandoned3').prop('disabled', true);
                        //    $('#slctReoDistressedAbandoned4').prop('disabled', true);
                        //} else {
                        //    $('#inReoDistressedAbandoned1').prop('disabled', false);
                        //    $('#slctReoDistressedAbandoned2').prop('disabled', false);
                        //    $('#slctReoDistressedAbandoned3').prop('disabled', false);
                        //    $('#slctReoDistressedAbandoned4').prop('disabled', false);
                        //}

                        //if (regREO[0].reo_vacant == false) {
                        //    $('#inReoVacantTimeline1').val('');
                        //    $('#inReoVacantTimeline1').prop('disabled', true);
                        //    $('#slctReoVacantTimeline2').prop('disabled', true);
                        //    $('#slctReoVacantTimeline3').prop('disabled', true);
                        //    $('#slctReoVacantTimeline4').prop('disabled', true);
                        //} else {
                        //    $('#inReoVacantTimeline1').prop('disabled', false);
                        //    $('#slctReoVacantTimeline2').prop('disabled', false);
                        //    $('#slctReoVacantTimeline3').prop('disabled', false);
                        //    $('#slctReoVacantTimeline4').prop('disabled', false);
                        //}

                        if (regREO[0].type_of_registration == 'PDF') {
                            var fn = regREO[0].upload_path;

                            $('#slctREOTypeOfRegistration').closest('div').next().show();

                            $('#linkPDFREO').text(fn);
                            $('#btnSavePDF2').show();
                            //$('#tabREO div.file-caption-main div.file-caption-name').attr('title', fn);
                            //$('#tabREO div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctREOTypeOfRegistration').closest('div').next().hide();
                            $('#btnSavePDF2').hide();
                            $('#tabREO div.file-caption-main div.file-caption-name').removeAttr('title');
                            $('#tabREO div.file-caption-main div.file-caption-name').html('');
                        }

                        if (regREO[0].type_of_registration == 'PDF' && regREO[0].vms_renewal == 'true' && regREO[0].reo_renew_uploadpath != '') {
                            var fname = regREO[0].reo_renew_uploadpath;

                            $('#linkPDFRenewalREO').text(fname);
                            $('#slctREOVmsRenewal').closest('div').next().show();
                            $('#btnSavePDFRenewREO').show();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fname);
                            //$('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fname);
                        }
                        else {
                            $('#slctREOVmsRenewal').closest('div').next().hide();
                            $('#btnSavePDFRenewREO').hide();
                            //$('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                            //$('#tabPFC div.file-caption-main div.file-caption-name').html('');
                        }

                        if (regREO[0].no_reo_reg == true) {
                            $('#reoReg').prop('checked', true);
                            $('#tabREO select').prop('disabled', true);
                            $('#tabREO input[type=number]').prop('disabled', true);
                            $('#tabREO input[type=text]').prop('disabled', true);
                            $('#tabREO select').val('');
                            $('#tabREO input[type=number]').val('');
                            $('#tabREO input[type=text]').val('');
                        } else {
                            $('#reoReg').prop('checked', false);
                            $('#tabREO select').prop('disabled', false);
                            $('#tabREO input[type=number]').prop('disabled', false);
                            $('#tabREO input[type=text]').prop('disabled', false);
                        }

                        if (regREO[0].statewide_reg == true) {
                            $('#reoStatewideReg').prop('checked', true);
                        } else {
                            $('#reoStatewideReg').prop('checked', false);
                        }

                    } else {
                        $('#tabREO div.tag').remove();
                        $('#tabREO').prepend(
                            '<div class="col-xs-12 text-center tag" style="margin-top: 1%;">' +
                                '<label class="text-red">Pending ' + regREO[0].tag + '!</label>' +
                            '</div>'
                        );
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                    }

                    if ($('#Reordvery').is(':checked')) {
                        $('#slctREORenewEveryNum').prop('disabled', false);
                        $('#slctREORenewEveryYears').prop('disabled', false);
                        $('#slctREORenewOnMonths').prop('disabled', true);
                        $('#slctREORenewOnNum').prop('disabled', true);

                    } else if ($('#Reordon').is(':checked')) {
                        $('#slctREORenewOnMonths').prop('disabled', false);
                        $('#slctREORenewOnNum').prop('disabled', false);
                        $('#slctREORenewEveryNum').prop('disabled', true);
                        $('#slctREORenewEveryYears').prop('disabled', true);
                    } else {
                        $('#slctREORenewOnMonths').prop('disabled', true);
                        $('#slctREORenewOnNum').prop('disabled', true);
                        $('#slctREORenewEveryNum').prop('disabled', true);
                        $('#slctREORenewEveryYears').prop('disabled', true);
                    }

                    $('#modalLoading').modal('hide');

                } else {
                    $('#tabREO div.tag').remove();

                    $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                }
            }
            $('#modalLoading').modal('hide');
        }
    });
}

$('#slctVacLotRegCostStan').change(function () {
    if ($(this).val == 'true') {
        $('#slctVacRenCostEsc').prop('disable', true);
    }
    else {
        $('#slctVacRenCostEsc').prop('disable', false);
    }
});

$('#slctVacRenCostEsc').change(function () {
    if ($(this).val == 'false') {
        $('#btnViewEscalRenewAmt3').prop('disable', true);
    }
    else {
        $('#btnViewEscalRenewAmt3').prop('disable', false);
    }
});
