﻿$(document).ready(function () {

    //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	//auditTable();

	$('.datepicker').datepicker({
		autoclose: true
	});
});

$('#btnApply').click(function () {
	auditTable();
});

function auditTable()
{
	var time = $('#slctTime').val();
	var dtFrom = $('#inDateFrom').val();
	var dtTo = $('#inDateTo').val();

    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'Audit.aspx/auditList',
        data: '{time: "' + time + '", dtFrom: "' + dtFrom + '", dtTo: "' + dtTo + '"}',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            $('#tblAudit').bootstrapTable('destroy');

            $('#tblAudit').bootstrapTable({
                data: data.d.tblAuditList,
                height: 500,
                width: 1000,
                pagination: true
            });

            $('#tblAudit').show();

            $('#modalLoading').modal('hide');
        },
        error: function (response) {
            console.log(response.responseText);
        },
        failure: function (response) {
            console.log(response.responseText);
        }
    });
}

$('#slctTime').on('change', function () {
	if ($(this).val() != '') {
		$('#inDateFrom, #inDateTo').val('');
		$('#inDateFrom, #inDateTo').prop('disabled', true);
	}
});