﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class Audit : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

        [WebMethod]
        public static AuditList auditList(string time, string dtFrom, string dtTo)
        {
            clsConnection cls = new clsConnection();
            AuditList al = new AuditList();
            List<AuditClass> acl = new List<AuditClass>();
            DataTable dt = new DataTable();

			string param = "";
			DateTime now = DateTime.Now;

			if (time == "year")
			{
				param = "where YEAR(action_date) = '" + now.Year.ToString() + "'";
			}
			else if (time == "month")
			{
				param = "where MONTH(action_date) = '" + now.Month.ToString() + "'";
			}
			else if (time == "week")
			{
				//DateTime today = DateTime.Today;
				int currentDayOfWeek = (int)now.DayOfWeek + 1;
				DateTime sunday = now.AddDays(-currentDayOfWeek);
				DateTime monday = sunday.AddDays(1);
				// If we started on Sunday, we should actually have gone *back*
				// 6 days instead of forward 1...
				if (currentDayOfWeek == 0)
				{
					monday = monday.AddDays(-7);
				}
				var dates = Enumerable.Range(0, 7).Select(days => monday.AddDays(days)).ToList();

				param = "where action_date between '" + dates[0].ToShortDateString() + "' and '" + dates[6].ToShortDateString() + "'";
			}
			else if (time == "yesterday")
			{
				param = "where action_date = '" + now.AddDays(-1).ToShortDateString() + "'";
			}
			else if (time == "today")
			{
				param = "where action_date = '" + now.ToShortDateString() + "'";
			}
			else
			{
				param = "where action_date between '" + dtFrom + "' and '" + dtTo + "'";
			}


			dt = cls.GetData("select id,page,case when sub_page is null then '-' else sub_page end, case when sub_sub_page is null then '-' else sub_sub_page end, " +
							"action,action_by,action_date,Convert(time(0),action_time) from tbl_VPR_Audit " + param + " order by action_date;");
			for (int i = 0; i < dt.Rows.Count; i++)
			{
				AuditClass ac = new AuditClass();
				ac.id = dt.Rows[i][0].ToString();
				ac.page = dt.Rows[i][1].ToString();
				ac.sub_page = dt.Rows[i][2].ToString();
				ac.sub_sub_page = dt.Rows[i][3].ToString();
				ac.action = dt.Rows[i][4].ToString();
				ac.action_by = dt.Rows[i][5].ToString();
				ac.action_date = Convert.ToDateTime(dt.Rows[i][6].ToString()).ToShortDateString();
				ac.action_time = dt.Rows[i][7].ToString();
				acl.Add(ac);
			}

			al.tblAuditList = acl;

            return al;
            
        }
	}
}