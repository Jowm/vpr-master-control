﻿$(document).ready(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });

	loadrole_accesstype("Role");

	$('#roleBtn').click(function () {
		$('#roleDiv').hide();
		$('#roleDiv').fadeIn();
		$('#tison').empty();
		$('#headType').text("Role");
		loadrole_accesstype("Role");
	});

	$('#skillBtn').click(function () {
		$('#roleDiv').hide();
		$('#roleDiv').fadeIn();
		$('#tison').empty();
		$('#headType').text("Skill Level");
		loadrole_accesstype("Skill Level");
	});
});

$('#btnNew_Type').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	var type = $('#headType').text();
	$.ajax({
		type: 'POST',
		url: 'User.aspx/addAccess',
		data: '{"name" : "' + $('#accessTypeName').val() + '","desc" : "' + $('#accessTypeDesc').val() + '","type" : "' + type + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			$('#accessTypeName').val("");
			$('#accessTypeDesc').val("")

			loadrole_accesstype(type);
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});

});

$('#btnEdit_Type').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	var type = $('#headType').text();
	$.ajax({
		type: 'POST',
		url: 'User.aspx/editAccess',
		data: '{"id" : "' + $('#lineId').val() + '","name" : "' + $('#eaccessTypeName').val() + '","desc" : "' + $('#eaccessTypeDesc').val() + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			$('#eaccessTypeName').val("");
			$('#eaccessTypeDesc').val("")
			loadrole_accesstype(data.d);
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});

});

function editLine(id) {
	$.ajax({
		type: 'POST',
		url: 'User.aspx/loadLineAccess',
		data: '{"id" : "' + id + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var obj = data.d;
			console.log(obj);
			$('#eaccessTypeName').val(obj.name);
			$('#eaccessTypeDesc').val(obj.desc);
			$('#lineId').val(obj.id);

		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}

function deleteLine(id) {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$.ajax({
		type: 'POST',
		url: 'User.aspx/deleteLine',
		data: '{"id" : "' + id + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			loadrole_accesstype(data.d);
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}

function loadrole_accesstype(type) {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$('#tbdy').empty();
	$.ajax({
		type: 'POST',
		url: 'User.aspx/loadAccess',
		data: '{"type":"' + type + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var obj = data.d;
			console.log(obj);
			for (var i = 0; i < obj.length; i++) {
				var ta = " <tr>";
				ta += "<td style='width:70%'><a type='button' style='cursor: pointer;' id='role_" + obj[i].id + "' onclick='loadrole_accessfunc(" + obj[i].id + ")'>" + obj[i].name + "</a></td>";
				ta += "<td style='width:15%'><button type='button' class='btn btn-success btn-sm' onclick='editLine(" + obj[i].id + ")' style='width:32px;'data-toggle='modal' data-target='#modalEditAccess'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></button></td>"
				ta += "<td style='width:15%'><button type='button' class='btn btn-danger btn-sm' onclick='deleteLine(" + obj[i].id + ")' style='width:32px;'><i class='fa fa-trash-o' style='color:white' aria-hidden='true'></i></button></td>"
				$('#tbdy').append(ta);
			}
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}


function loadrole_accessfunc(id) {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$('#tFunc').empty();
	$('#tison').empty();
	var type = $('#headType').text();
	$.ajax({
		type: 'POST',
		url: 'User.aspx/loadFunction',
		data: '{"id" : "' + id + '","type" : "' + type + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var obj = data.d;
			console.log(obj);
			$('#conId').val(id);
			for (var i = 0; i < obj.length; i++) {
				if (obj[i].isOn == "True") {
					var ta = " <tr>";
					ta += "<td style='width:15%'><input type='checkbox' id='" + obj[i].id + "' class='fncChkA'></td>";
					ta += "<td style='width:85%' >" + obj[i].name + "</td><tr>";
					$('#tison').append(ta);
				}
				else {
					var ta = " <tr>";
					ta += "<td style='width:15%'><input type='checkbox' id='" + obj[i].id + "' class='fncChkR'></td>";
					ta += "<td style='width:85%'>" + obj[i].name + "</td><tr>";
					$('#tFunc').append(ta);
				}
			}
			$('#modalLoading').modal('hide');

		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}

$('#appFunc').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	var sList = "";
	$('.fncChkR').each(function () {
		var sThisVal = (this.checked ? $(this).attr('id') : "0");
		sList += (sList == "" ? sThisVal : "," + sThisVal);
	});
	console.log(sList);
	$('.fncChkA').each(function () {
		var sThisVal = $(this).attr('id');
		sList += (sList == "" ? sThisVal : "," + sThisVal);
	});
	console.log(sList);
	var id = $('#conId').val();
	$.ajax({
		type: 'POST',
		url: 'User.aspx/updateFunction',
		data: '{"id" : "' + id + '","newStr" : "' + sList + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {

			loadrole_accessfunc(id);
			$('#modalLoading').modal('hide');

		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
});


$('#remFunc').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	var sList = "";
	$('.fncChkA').each(function () {
		var sThisVal = (this.checked ? "0" : $(this).attr('id'));
		sList += (sList == "" ? sThisVal : "," + sThisVal);
	});

	console.log(sList);
	var id = $('#conId').val();
	$.ajax({
		type: 'POST',
		url: 'User.aspx/updateFunction',
		data: '{"id" : "' + id + '","newStr" : "' + sList + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			loadrole_accessfunc(id);
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
});

$('#userTab').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	loadUserTable('', '', '', '');
	popActions();
	empCatFilter();
	empIMFilter('');
});

function loadUserTable(empCat, im, user, name) {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$.ajax({
		type: 'POST',
		url: 'User.aspx/userList',
		data: '{cat : "' + empCat + '", im : "' + im + '", user : "' + user + '", name : "' + name + '"}',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {

			if (data.d.tblUserList != null) {
				$('#tblUser').bootstrapTable('destroy');

				$('#tblUser').bootstrapTable({
					data: data.d.tblUserList,
					height: 663,
					width: 1000,

					pagination: true
				});
			}

			//$('.iCheck').iCheck({
			//	checkboxClass: 'icheckbox_flat-blue',
			//	radioClass: 'iradio_flat-blue'
			//});

			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}

function idFormatter1(value) {
	return "<input type='checkbox' id='chkRow1' class='chkRow1 iCheck' data-id='" + value + "' />";
}

function popActions() {
	$.ajax({
		type: 'POST',
		url: 'User.aspx/popRS',
		data: '{}',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var obj = data.d;
			console.log(obj);
			$('#roleSlct').empty();
			$('#skillSlct').empty();
			var ta1 = "";
			var ta2 = "";
			for (var i = 0; i < obj.length; i++) {
				if (obj[i].rstype == "Role") {
					ta1 = "<option value='" + obj[i].id + "'>" + obj[i].rsname + "</option>";

					$('#roleSlct').append(ta1);
				}
				else {
					ta2 = "<option value='" + obj[i].id + "'>" + obj[i].rsname + "</option>";
					$('#skillSlct').append(ta2);
				}
			}

		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});
}

$('#applyBtn').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	var role_id = $('#roleSlct option:selected').val();
	var skill_id = $('#skillSlct option:selected').val();
	var emp_id = "";
	$('.chkRow1').each(function () {
		var sThisVal = (this.checked ? $(this).attr('data-id') : "0");
		emp_id += (emp_id == "" ? sThisVal : "," + sThisVal);
	});
	emp_id = emp_id.replace(/\,0/g, "");;
	$.ajax({
		type: 'POST',
		url: 'User.aspx/applyAction',
		data: '{"role_id" : "' + role_id + '","skill_id" : "' + skill_id + '","emp_id" : "' + emp_id + '"}',
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			loadUserTable('', '', '', '');
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		},
		failure: function (response) {
			console.log(response.responseText);
		}
	});

});

function empCatFilter() {
	$.ajax({
		type: 'POST',
		url: 'User.aspx/empCatFilter',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			//console.log(data);
			$('#roleFSlct').empty();
			$('#roleFSlct').append(
				'<option value="">ALL</option>'
			);
			$.each(data.d, function(idx, val) {
				$('#roleFSlct').append(
					'<option> ' + val +  ' </option>'
				);
			});
		}, error: function (response) {
			console.log(response.responseText);
		}
	});
}

function empIMFilter(cat) {
	$.ajax({
		type: 'POST',
		url: 'User.aspx/empIMFilter',
		data: '{cat: "' + cat + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			console.log(data);
			$('#immSlct').empty();
			$('#immSlct').append(
				'<option value="">ALL</option>'
			);
			$.each(data.d, function (idx, val) {
				$('#immSlct').append(
					'<option> ' + val + ' </option>'
				);
			});
		}, error: function (response) {
			console.log(response.responseText);
		}
	});
}

function empUserFilter(im) {
	$.ajax({
		type: 'POST',
		url: 'User.aspx/empUserFilter',
		data: '{im: "' + im + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			console.log(data);
			$('#userSelect').empty();
			$('#userSelect').append(
				'<option value="">ALL</option>'
			);
			$.each(data.d, function (idx, val) {
				$('#userSelect').append(
					'<option> ' + val + ' </option>'
				);
			});
		}, error: function (response) {
			console.log(response.responseText);
		}
	});
}

$('#roleFSlct').change(function () {
	if ($(this).val() == "") {
		$('#userSelect').empty();
		$('#userSelect').append(
			'<option value="">ALL</option>'
		);
		empIMFilter($(this).val());
	} else {
		empIMFilter($(this).val());
	}
});

$('#immSlct').change(function () {
	empUserFilter($(this).val());
});

$('#searchFBtn').click(function () {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });

	var empCat = $("#roleFSlct").val();
	var im = $("#immSlct").val();
	var user = $("#userSelect").val();
	var name = $('#userFltr').val();

	loadUserTable(empCat, im, user, name);
});