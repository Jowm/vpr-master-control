﻿$('.item').draggable({
	revert: true,
	onStartDrag: function () {
		//console.log("onStartDrag");
	},
	onStopDrag: function () {
		$(this).draggable('options').cursor = 'move';
	},
	onDragEnter: function (e, source) {
		//console.log("onDragEnter");
	}

});

$('.tableDrop').droppable({
	accept: ".item",
	onDragEnter: function (e, source) {
		$(source).draggable('options').cursor = 'auto';
	},
	onDragLeave: function (e, source) {
		$(source).draggable('options').cursor = 'not-allowed';
	},
	onDrop: function (e, source) {
		//console.log(source);
	},
	drop: function (event, ui) {

		var id = $(ui.draggable).attr("id");

		var text = $(ui.draggable).text();

		var v = $(ui.draggable).attr("id");
		var t = $(ui.draggable).html();
		console.log(v);
		console.log(t);

		AppendToTableColumn(v, t);

		$(this).removeClass("box-success").addClass("box-default");

		updateFields();

		//Run_report();

	},
	over: function (event, elem) {
		$(this).removeClass("box-default").addClass("box-success");
	}, out: function (event, elem) {
		$(this).removeClass("box-success").addClass("box-default");

	}
});

function removeColumn(sender) {
	$(sender).closest("div").parent().remove();
	//Run_report();
	updateFields();

}

function AppendToTableColumn(v, t) {

	var val = v;
	var text = t;

	var sel = $("#sortable li[data-id='" + val + "']");
	if (sel.length > 0) {
		sel.find("div").effect("bounce", "slow");
	} else {

		$("#sortable").append("<li data-id='" + val + "'><div class='dropdown'>" +
			"<a class='btn btn-primary btn-xs dropdown-toggle' data-toggle='dropdown'>" + text + "&nbsp;<span class='caret'></span></a>" +
				"<ul class='dropdown-menu' role='menu' tabindex='-1' style='border: black solid 1px;box-shadow: black 1px 1px 10px;'> " +
					"<li role='presentation' style='width: 100%; padding: 0px 5px;'>" +
						"<a role='menuitem' tabindex='-1' onclick='removeColumn(this);'><i class='fa fa-remove'></i>Remove</a>" +
					"</li>" +
					"<li role='presentation' style='width: 100%; padding: 0px 5px;'>" +
						"<a role='menuitem' tabindex='-1' onclick='openSettings(this);'><i class='fa fa-sliders'></i>Properties</a>" +
					"</li>" +
				"</ul>" +
			"</div></li>");

		$("#sortable").sortable({
			placeholder: "ui-state-highlight",
			revert: true,
			axis: "x",
			update: function (event, ui) {
				//Run_report();
			}
		});
		$("#sortable").disableSelection();
	}
}

function updateFields() {
	$("#Fields, #FieldList, #addAfter, #fieldList").empty();
	$.each($("#sortable > li"), function (index, value) {
		$("#addAfter").append("<option value='" + index + "' data-id='" + $(value).attr("data-id") + "'>" + $(value).find("a:eq(0)").text().trim() + "</option>");
		$("#Fields, #FieldList, #fieldList").append("<option value='" + $(value).attr("data-id") + "'>" + $(value).find("a:eq(0)").text().trim() + "</option>");
	});
}

function Run_report() {
	$("#modalLoading").modal({ backdrop: 'static', keyboard: false });

	var objFields = [];
	$.each($("#sortable > li"), function (index, value) {

		var myObjFields = {
			name: $(value).attr("data-id"),
			alias: $(value).find("a:eq(0)").text().trim()
		}

		objFields.push(myObjFields);
	});

	var myFilter = [];
	$.each($("#tblFilters tbody tr"), function (index, value) {

		var field = $(value).find("td span:eq(0)").attr("data-id");
		var operator = $(value).find("td label").attr("data-id");
		var value = $(value).find("td span:eq(1)").html();

		var objFilter = {
			field: field,
			field_operator: operator,
			field_values: value
		}

		myFilter.push(objFilter);

	});

	var myObj = {
		fields: objFields,
		where: myFilter
	};

	$("#tblReportData").bootstrapTable('showLoading');

	$.ajax({
		type: "POST",
		url: "Reporting.aspx/runReport",
		data: "{data: " + JSON.stringify(myObj) + "}",
		contentType: "application/json",
		success: function (data) {

			var d = $.parseJSON(data.d);
			console.log(d);

			if (d.Success) {

				var toAppend = "";
				var objField = d.data.columns;
				$.each(objField, function (index, val) {
					var v = val;

					var t;
					if (d.data.dtFields) {
						t = d.data.dtFields[index].alias;
					} else {
						t = v;
					}

					if (t == undefined) { t = v; }

					toAppend += "<th data-field='" + v + "' data-sortable='true'>" + t + "</th>";
				});

				$("#tblReportData").bootstrapTable('destroy');

				$("#tblReportData thead tr").empty();
				$("#tblReportData thead tr").append(toAppend);

				var obj = d.data.record;
				toAppend = "";
				$.each(obj, function (index, value) {
					toAppend += "<tr>";
					$.each(objField, function (index, val) {
						var v = value[val];
						if (value[val] != null) {
							v = value[val];
						} else {
							v = "&nbsp;";
						}
						toAppend += "<td>" + v + "</td>";
					});
					toAppend += "</tr>";
				});
				$("#tblReportData tbody").empty();
				$("#tblReportData tbody").append(toAppend);

				$("#tblReportData").bootstrapTable({
					pagination: true,
					exportDataType: "all"
				});

			} else {
				alertify.error("No record found..");
			}
			$("#tblReportData").bootstrapTable('hideLoading');
			$("#modalLoading").modal("hide");
		},
		failure: function (response) {
			console.log(response.d);
			$("#modalLoading").modal("hide");
		},
		error: function (response) {
			console.log(response.responseText);
			$("#modalLoading").modal("hide");
		}
	});
}

$('#slctFilter').on('change', function () {

	if ($(this).val() == '') {
		$('#divFilter').fadeOut();
	} else {
		$('#divFilter').fadeIn();
		if ($(this).val() == 'Field') {

			updateFields();

			$('#divField').fadeIn();
			$('#divLogic').hide();
			$('#divCross').hide();
		} else if ($(this).val() == 'Logic') {
			$('#divField').hide();
			$('#divLogic').fadeIn();
			$('#divCross').hide();



		} else if ($(this).val() == 'Cross') {
			$('#divField').hide();
			$('#divLogic').hide();
			$('#divCross').fadeIn();
		}
	}
});

$('#btnDone').click(function () {

	var val1, val2, val3, id1, id2, id3;

	if ($('#divField').is(':visible') == true) {

		id1 = $('#divField').find('select:eq(0) option:selected').val();
		val1 = $('#divField').find('select:eq(0) option:selected').text().trim();

		id2 = $('#divField').find('select:eq(1) option:selected').val();
		val2 = $('#divField').find('select:eq(1) option:selected').text().trim();

		val3 = $('#divField').find('#FieldValue').val().trim();

	} else if ($('#divLogic').is(':visible') == true) {

		id1 = $('#divLogic').find('select:eq(0) option:selected').val();
		val1 = $('#divLogic').find('select:eq(0) option:selected').text().trim();

		id2 = $('#divLogic').find('select:eq(1) option:selected').val();
		val2 = $('#divLogic').find('select:eq(1) option:selected').text().trim();

		id3 = $('#divLogic').find('select:eq(2) option:selected').val();
		val3 = $('#divLogic').find('select:eq(2) option:selected').text().trim();

	} else if ($('#divCross').is(':visible') == true) {
		val1 = $('#divCross').find('select:eq(0) option:selected').text();
		val2 = $('#divCross').find('select:eq(1) option:selected').text();
		val3 = $('#divCross').find('select:eq(2) option:selected').text();
	}

	$('#tblFilters').append(
		  '<tr onmouseover="mouseover(this);" onmouseout="mouseout(this);">' +
		  '<td style="padding: 0px 8px"><span data-id="' + id1 + '">' + val1 + '</span> <label data-id="' + id2 + '">' + val2 + '</label> <span data-id="' + id3 + '">' + val3 + '</span></td>' +
		  '<td style="padding: 0px 8px"><a style="cursor: pointer;" class="filterHover" onclick="editFiler(this);">Edit</a></td>' +
		  '<td style="padding: 0px 8px"><a style="cursor: pointer;" class="filterHover" onclick="removeFilter(this);">Remove</a></td>' +
		  '</tr>'
	);

	$('#slctFilter').val('');
	$('#divFilter').hide();

	//Run_report();

});

$('#btnCancelFilter').click(function () {
	$('#slctFilter').val('');
	$('#divFilter').hide();
});

function mouseover(val) {
	$(val).find('td:eq(1) a').removeClass('filterHover');
	$(val).find('td:eq(2) a').removeClass('filterHover');
}

function mouseout(val) {
	$(val).find('td:eq(1) a').addClass('filterHover');
	$(val).find('td:eq(2) a').addClass('filterHover');
}

function removeFilter(val) {
	$(val).closest('tr').remove();

	//Run_report();
}

function editFiler(val) {
	var select1 = $(val).closest('tr').find('td:eq(0) span:eq(0)').text();
	var select2 = $(val).closest('tr').find('td:eq(0) label').text();
	var select3 = $(val).closest('tr').find('td:eq(0) span:eq(1)').text();

	$('#divFilter').show();

	if (select2 == 'Equal' || select2 == 'Not Equal') {
		$('#slctFilter').val('Field');
		$('#divField').show();
		$('#divLogic').hide();
		$('#divCross').hide();

		$('#divField').find('select:eq(0) option:selected').text(select1);
		$('#divField').find('select:eq(1) option:selected').text(select2);
		$('#divField').find('#FieldValue').val(select3);

	} else if (select2 == 'AND' || select2 == 'OR') {
		$('#slctFilter').val('Field');
		$('#divField').hide();
		$('#divLogic').show();
		$('#divCross').hide();

		$('#divLogic').find('select:eq(0) option:selected').text(select1);
		$('#divLogic').find('select:eq(1) option:selected').text(select2);
		$('#divLogic').find('select:eq(2) option:selected').text(select3);

	} else if (select2 == 'With' || select2 == 'Without') {
		$('#slctFilter').val('Field');
		$('#divField').hide();
		$('#divLogic').hide();
		$('#divCross').show();

		$('#divCross').find('select:eq(0) option:selected').text(select1);
		$('#divCross').find('select:eq(1) option:selected').text(select2);
		$('#divCross').find('select:eq(2) option:selected').text(select3);

	}

	$(val).closest('tr').remove();

}

function openSettings(sender) {

	var desc = $(sender).closest("div").find("a:eq(0)").text().trim();
	var value = $(sender).closest("div").find("a:eq(0)").closest("li").attr("data-id");

	$("#prop_desc").text(desc).attr("data-id", value);

	$("#modalProperties").modal("show");
}

function save_prop() {

	var id = $("#prop_desc").attr("data-id");

	var name = $("#prop_rename").val().trim();

	if (name != '') {

		$("#sortable > li[data-id='" + id + "'] a:eq(0)").html(name + "&nbsp;<span class='caret'></span>");

	}

	$('#modalProperties').modal('hide');

	//Run_report();
}