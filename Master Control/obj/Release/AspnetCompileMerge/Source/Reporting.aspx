﻿<%@ Page Title="Reports" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Reporting.aspx.cs" Inherits="Master_Control.Reporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		#tblFilters tr:hover {
			background-color: #fff8dc;
		}

		.filterHover {
			display: none;
		}

		#sortable {
			list-style-type: none;
			margin: 0;
			padding: 0;
			width: 100%;
		}

			#sortable li {
				margin-left: 2px;
				display: inline-block;
			}

		.dropdown-menu > li > a:hover {
			width: 100%;
			cursor: pointer;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<section class="content-header">
		<h1>Reporting</h1>
		<ol class="breadcrumb">
			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">
				<span><i class="fa fa-bar-chart"></i>&nbsp;Reports</span>
			</li>
		</ol>
	</section>

	<section class="content">
		<div class="box box-default">
			<div class="box-body">
				<div class="col-xs-3 noPadMar" style="z-index: 1000;">
					<div class="panel panel-default">
						<div class="panel-heading padTab">
							<h4 class="box-title"><i class="fa fa-navicon"></i>&nbsp;Fields</h4>
						</div>
						<div class="panel-body noPadMar" style="min-height: 500px">
							<div class="col-xs-12 no-padding" style="background-color: #ffffda; border: 2px solid #ffc000; text-align: center;">
								<small>Drag and drop to add fields to the report</small>
							</div>
							<div class="col-xs-12 noPadMar">
								<div class="panel-group" id="accordion">
									<div class="panel-group" id="accordionFields">
										<%--<div class="panel panel-success">
											<div class="panel-heading padTab">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionFields" href="#acDate" class="CustomFont">Date</a>
												</h4>
											</div>
											<div id="acDate" class="panel-collapse collapse fieldPad" style="">
												<table>
													<tr>
														<td>
															<a class="item label label-primary" id="year">Year</a>
														</td>
													</tr>
													<tr>
														<td>
															<a class="item label label-primary" id="month">Month</a>
														</td>
													</tr>
													<tr>
														<td>
															<a class="item label label-primary" id="day">Day</a>
														</td>
													</tr>
													<tr>
														<td>
															<a class="item label label-primary" id="date">Date</a>
														</td>
													</tr>
													<tr>
														<td>
															<a class="item label label-primary" id="start_time">Start Time</a>
														</td>
													</tr>
													<tr>
														<td>
															<a class="item label label-primary" id="end_time">End Time</a>
														</td>
													</tr>
												</table>
											</div>
										</div>--%>
										<div class="panel panel-success" style="display: none;">
											<div class="panel-heading padTab">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionFields" href="#acEmpProfile" class="CustomFont">Employee Profile</a>
												</h4>
											</div>
											<div id="acEmpProfile" class="panel-collapse collapse fieldPad" style="">
												<table>
													<tr>
														<td><a class="item label label-primary" id="empname">Employee Name</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="ntid">NT Login</a></td>
													</tr>
												</table>
											</div>
										</div>

										<%--<div class="panel panel-success">
											<div class="panel-heading padTab">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionFields" href="#acLocation" class="CustomFont">Location</a>
												</h4>
											</div>
											<div id="acLocation" class="panel-collapse collapse fieldPad" style="">
												<table>
													<tr>
														<td><a class="item label label-primary" id="state">State</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="city">City</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="zip">Zip Code</a></td>
													</tr>
												</table>
											</div>
										</div>--%>
										<div class="panel panel-success">
											<div class="panel-heading padTab">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionFields" href="#acMetrics" class="CustomFont">Metrics</a>
												</h4>
											</div>
											<div id="acMetrics" class="panel-collapse collapse fieldPad" style="">
												<table>
													<tr>
														<td><a class="item label label-primary" id="handle_time">Handle Time</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="productivity">Productivity</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="registration_timeline">Registration Timeline</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="registered_reo">Registered REO</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="registered_pfc">Registered PFC</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="prochamp">Prochamp</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="renewal">Renewal</a></td>
													</tr>
												</table>
											</div>
										</div>

										<div class="panel panel-success">
											<div class="panel-heading padTab">
												<h4 class="panel-title">
													<a data-toggle="collapse" data-parent="#accordionFields" href="#acControlRept" class="CustomFont">Control Report</a>
												</h4>
											</div>
											<div id="acControlRept" class="panel-collapse collapse fieldPad" style="">
												<table>
													<tr>
														<td><a class="item label label-primary" id="prop_id">Property ID</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="prop_num">Property #</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="prod">Product</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="vpr_serv_type">VPR Service Type</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="prop_stat">Property  Status</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="vpr_stat">VPR Status</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="crt_date">Created Date</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="sla">SLA</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="vpr_stat_age">VPR Status Age</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="total_age">Total Aging</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="client_stat">Client Status</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="occ_stat">Occupancy Status</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="assigned_assoc">Assigned Associate</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="prop_add">Property Address</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="city">City</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="state">State</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="zip">Zip code</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="vendor">Vendor</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="municipality_name">Municipality Name</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="pod">POD</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="followup_date">Follow up Date</a></td>
													</tr>
													<tr>
														<td><a class="item label label-primary" id="last_inspect_date">Last Inspection Date</a></td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-9 noPadMar">
					<div class="col-xs-8 noPadMar">
						<div class="panel panel-default noPadMar">
							<div class="panel-body noPadMar" style="min-height: 177px;">
								<div class="col-xs-12 noPadMar">
									<div class="col-xs-12 noPadMar">
										<label>Date</label>
									</div>
									<div class="col-xs-3">
										<select class="form-control input-sm noPadMar " id="dataField">
											<option value="0">-Select-</option>
											<%--<option value="datejoined">Joining Date</option>--%>
										</select>
									</div>
									<div class="col-xs-1 col-xs-offset-2 noPadMar">
										<label>Range:</label>
									</div>

									<div class="col-xs-2 noPadMar">
										<select class="form-control input-sm noPadMar">
											<option value="0">-Select-</option>
											<option value="TM">This Month</option>
										</select>
									</div>
									<div class="col-xs-1 noPadMar text-center">
										<label style="margin-top: 5%;">or</label>
									</div>
									<div class="col-xs-2 noPadMar">
										<input type="text" class="form-control input-sm datepicker" id="from" />
									</div>
									<div class="col-xs-1 noPadMar text-center">
										<label style="margin-top: 5%;">to</label>
									</div>
									<div class="col-xs-2 noPadMar">
										<input type="text" class="form-control input-sm datepicker" id="to" />
									</div>

								</div>
								<div class="col-xs-12 noPadMar" style="margin-top: 1%;">
									<div class="col-xs-1 noPadMar">
										<label style="margin-top: 5%;">Filter</label>
									</div>
									<div class="col-xs-3 noPadMar">
										<select id="slctFilter" class="form-control input-sm noPadMar">
											<option value="">--Select--</option>
											<option value="Field">Field Filters</option>
											<option value="Logic">Filter Logic</option>
											<option value="Cross">Cross Filters</option>
										</select>
									</div>
									<div class="col-xs-8 noPadMar text-right">
										<button id="btnCalculations" type="button" class="btn btn-primary btn-sm"><i class="fa fa-calculator"></i>&nbsp;Calculation</button>
									</div>
								</div>
								<div id="divFilter" class="col-xs-12 noPadMar" style="display: none;">
									<div id="divField" class="col-xs-12 noPadMar" style="margin-top: 1%; display: none;">
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm" id="Fields">
											</select>
										</div>
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm slctFilter" id="FieldOperator">
												<option value="=">Equal</option>
												<option value="<>">Not Equal</option>
											</select>
										</div>
										<div class="col-xs-4 noPadMar">
											<input type="text" class="form-control input-sm" id="FieldValue" />
										</div>
									</div>
									<div id="divLogic" class="col-xs-12 noPadMar" style="margin-top: 1%; display: none;">
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm" id="LogicFilter1">
											</select>
										</div>
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm slctFilter" id="LogicOperator">
												<option value="AND">AND</option>
												<option value="OR">OR</option>
											</select>
										</div>
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm" id="LogicFilter2">
											</select>
										</div>
									</div>
									<div id="divCross" class="col-xs-12 noPadMar" style="margin-top: 1%; display: none;">
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm">
												<option></option>
											</select>
											<div class="col-xs-2" style="margin-top: 5px;">
												<span>
													<i class="fa fa-chevron-right"></i>
												</span>
											</div>
											<div class="col-xs-10 noPadMar">
												<input type="text" class="form-control input-sm" />
											</div>
										</div>
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm slctFilter" id="CrossOperator">
												<option>With</option>
												<option>Without</option>
											</select>
										</div>
										<div class="col-xs-4 noPadMar">
											<select class="form-control input-sm">
												<option></option>
											</select>
											<div class="col-xs-2" style="margin-top: 5px;">
												<span>
													<i class="fa fa-chevron-right"></i>
												</span>
											</div>
											<div class="col-xs-10 noPadMar">
												<input type="text" class="form-control input-sm" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 noPadMar text-right" style="margin-top: 1%;">
										<button type="button" class="btn btn-primary btn-sm" id="btnDone">Done</button>
										<button type="button" class="btn btn-primary btn-sm" id="btnCancelFilter" onclick="btnCancel">Cancel</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xs-4 noPadMar">
						<div class="panel panel-default noPadMar">
							<div class="panel-body noPadMar" style="min-height: 145px;">
								<div class="col-xs-12 noPadMar">
									<label>Applied Filter List</label>
								</div>
								<table class="table table-bordered" id="tblFilters" style="font-size: x-small;">
									<tbody></tbody>
								</table>
							</div>
							<div class="panel-footer noPadMar text-right no-border" style="background-color: #fff; margin-right: 1px; margin-bottom: 1px;">
								<button type="button" class="btn btn-primary btn-sm" onclick="Run_report();" id="runReport"><i class="fa fa-download"></i>&nbsp;Run Report</button>
								<button id="btnSave" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
								<%--<button type="button" class="btn btn-primary btn-sm" onclick="close_click(this);"><i class="fa fa-close"></i>&nbsp;Close</button>--%>
							</div>
						</div>
					</div>
					<div class="col-xs-12 noPadMar">
						<div class="panel noPadMar">
							<div class="panel-body noPadMar" style="min-height: 317px; max-height: 317px;">
								<div class="panel panel-default tableDrop">
									<div class="panel-body" style="min-height: 371px">
										<div class="col-xs-12 no-padding">
											<h3 class="panel-title"><i class="fa fa-database"></i>&nbsp;Drop zone</h3>
										</div>
										<div class="col-xs-12 no-padding">
											<ul id="sortable">
											</ul>
											<table id="tblReport" class="table-bordered" style="font-size: smaller">
												<thead>
													<tr></tr>
												</thead>
												<tbody></tbody>
											</table>
										</div>
										<div class="col-xs-12 no-padding" style="overflow: auto;">
											<table id="tblReportData" class="table table-bordered table-striped" style="font-size: smaller"
												data-show-export="true"
												data-click-to-select="true"
												data-show-columns="true"
												data-search="true"
												data-show-refresh="true"
												data-show-toggle="true"
												data-page-list="[10, 25, 50, 100, ALL]">
												<thead>
													<tr></tr>
												</thead>
												<tbody></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div id="modalProperties" class="modal fade">
		<div class="modal-dialog" role="document">
			<div class="panel panel-default">
				<div class="panel-heading">
					<label id="prop_desc"></label>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				</div>
				<div class="panel-body" style="min-height: 320px">
					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Rename</label>
							<div class="col-md-6">
								<input type="text" class="form-control input-sm" id="prop_rename" />
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
					<br />

					<div class="col-md-12">
						<div class="form-group">
							<label class="control-label col-md-3">Value Settings</label>
							<div class="col-md-6">
								<select class="form-control input-sm">
									<option value="COUNT">count</option>
									<option value="AVERAGE">average</option>
									<option value="SUM">sum</option>
								</select>
							</div>
						</div>
					</div>

					<div class="clearfix"></div>
					<br />

					<div class="col-md-12">
						<div class="col-md-3 noPadMar">
							<label><u>Category</u></label>
							<select class="form-control input-sm" multiple="multiple" style="min-height: 155px;" onchange="cat_change(this)">
								<option selected="selected" value="number">Number</option>
								<option value="percentage">Percentage</option>
								<option value="currency">Currency</option>
								<option value="time">Time</option>
								<option value="date">Date</option>
							</select>
						</div>
						<div class="col-md-8 noPadMar" style="margin-left: 16px">
							<label class="control-label"><u>Settings</u></label><br />
							<div class="col-md-12 noPadMar">
								<div class="number">

									<label class="control-label col-md-4 noPadMar">Decimal Places</label>
									<div class="col-md-6">
										<input type="text" class="form-control input-sm text-center" id="prop_decimal_places" value="0" />
									</div>
								</div>
								<div class="percentage hidden">

									<label class="control-label col-md-4 noPadMar">Decimal Places</label>
									<div class="col-md-6">
										<input type="text" class="form-control input-sm text-center" id="prop_percentage_places" value="0" />
									</div>
								</div>
								<div class="currency hidden noPadMar">
									<label class="control-label col-md-4 noPadMar">Decimal Places</label>
									<div class="col-md-2 noPadMar">
										<input type="text" class="form-control input-sm text-center" id="prop_currency_places" value="0" />
									</div>
									<label class="control-label col-md-2 noPadMar">Symbol</label>
									<div class="col-md-4 noPadMar">
										<select class="form-control input-sm">
											<option value="Php">Php</option>
											<option value="$">$</option>
										</select>
									</div>
								</div>
								<div class="time hidden">
									<label class="control-label col-md-3 noPadMar">Format</label>
									<div class="col-md-6">
										<select class="form-control input-sm">
											<option value="seconds">Seconds only</option>
											<option value="minutes">Minutes only</option>
											<option value="hours">Hours only</option>
											<option value="hoursMinutes">Hours and Minutes</option>
											<option value="minutesSeconds">Minutes and Seconds</option>
											<option value="HMS">Hours, Minutes, Seconds</option>
										</select>
									</div>
								</div>
								<div class="date hidden">
									<label class="checkbox-inline">
										<input type="checkbox" />&nbsp;With Year</label>
								</div>
							</div>
							<div class="col-md-12">
								<hr />
							</div>
							<div class="col-md-12">
								<div class="number">
									<select class="form-control input-sm" multiple="multiple" style="min-height: 89px;">
										<option value="1">1234.12</option>
										<option value="2">-1234.12</option>
										<option value="3">(1234.12)</option>
									</select>
								</div>
								<div class="percentage hidden">
									<select class="form-control input-sm" multiple="multiple" style="min-height: 89px;">
										<option value="1">1234.12%</option>
										<option value="2">-1234.12%</option>
									</select>
								</div>
								<div class="currency hidden">
									<select class="form-control input-sm" multiple="multiple" style="min-height: 89px;">
										<option value="1">$1234.12</option>
										<option value="2">-$1234.12</option>
										<option value="3">($1234.12)</option>
									</select>
								</div>
								<div class="time hidden">
									<select class="form-control input-sm" multiple="multiple" style="min-height: 89px;">
										<option value="1">13:30:05</option>
										<option value="2">1:30:05 PM</option>
										<option value="3">30.55.5</option>
									</select>
								</div>
								<div class="date hidden">
									<select class="form-control input-sm" multiple="multiple" style="min-height: 89px;">
										<option value="1">12-13-2000</option>
										<option value="2">12/13/2000</option>
										<option value="3">13-12-2000</option>
										<option value="4">13/12/2000</option>
										<option value="5">Day</option>
										<option value="6">Month</option>
										<option value="7">Dec-13</option>
										<option value="8">December 13, 2000</option>
										<option value="9">13-Dec</option>
									</select>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class="panel-footer text-right no-border">
					<button type="button" class="btn btn-primary btn-xs" onclick="save_prop();">Save</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
	<script src="js/Reporting.js"></script>
</asp:Content>
