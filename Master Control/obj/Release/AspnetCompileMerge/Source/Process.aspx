﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Process.aspx.cs" Inherits="Master_Control.Process" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		.activeList {
			background-color: #4d6578;
		}

		.activeList button, .activeList button:hover {
			color: #fff;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <div class="col-xs-3" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>States</label>
                        </div>
                        <div class="panel-body" style="height: 575px; overflow-y: scroll">
                            <ul id="lstState">
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xs-9" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>City</label>
                        </div>
                        <div class="panel-body" style="height: 534px; overflow-y:scroll">
                            <table class="table no-border">
                                <thead>
                                    <tr>
                                        <th style='width:5%'></th>
                                        <th style='width:55%'>Condition</th>
                                        <th style='width:40%'>Process</th>
                                    </tr>
                                </thead>
                                <tbody id="cityBody">
                                </tbody>
                            </table>

                        </div>
                        <div class="text-right" style="margin-top: 1%;">
                            <button type="button" class="btn btn-primary btn-sm" id="saveBtn"><i class="fa fa-save"></i>Save</button>
                            <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-times"></i>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
    <script src="js/Process.js"></script>
    <style>
        ul#lstState {
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
    </style>
</asp:Content>
