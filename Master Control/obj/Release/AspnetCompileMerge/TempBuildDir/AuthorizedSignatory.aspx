﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="AuthorizedSignatory.aspx.cs" Inherits="Master_Control.AuthorizedSignatory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Authorized Signatory</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-tasks"></i>&nbsp;Authorized Signatory</span>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default" style="margin-top: 1%;">
            <div class="box-header">
                <label>Signatures</label>
            </div>
            <div class="box-body">
                <div class="col-xs-12" style="padding-left: 0px;">
                    <div class="col-xs-12">
                        <div class="col-xs-3">
                            <label>Authorized Signatory</label>
                        </div>
                        <div class="col-xs-3">
                            <select id="slctAuthSig" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-3">
                            <label>Alternate</label>
                        </div>
                        <div class="col-xs-3">
                            <select id="slctAlter" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12 text-right">
                        <button type="button" class="btn btn-primary btn-sm">&nbsp;Save</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/AuthorizedSignatory.js"></script>
</asp:Content>
