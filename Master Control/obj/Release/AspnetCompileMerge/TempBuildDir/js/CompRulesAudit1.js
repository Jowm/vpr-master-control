﻿$(document).ready(function () {
    getState();

    $('.datepicker').datepicker({
        autoclose: true
    });

    $(".timepicker").timepicker({
        showInputs: false
    });

    $('#tabPFC, #tabREO, #tabPropType, #tabCost, #tabContReg, #tabPropReq, #lci, ' +
        '#tabInspection, #tabMunicipality, #tabDeregistration, #tabNotif').find('div.audit input').prop('disabled', true);
    $('#tabPFC, #tabREO, #tabPropType, #tabCost, #tabContReg, #tabPropReq, #lci, ' +
        '#tabInspection, #tabMunicipality, #tabDeregistration, #tabNotif').find('div.audit select').prop('disabled', true);
    $('#tabPFC .file-input, #tabDeregistration .file-input').prop('disabled', true);

    $("input[type=number]").keypress(function (event) {
        if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
            return false;
        }
    });
});

function check(val) {
    $(val).hide();
    $(val).closest('div.col-xs-12').find('div.audit input').prop('disabled', true);
    $(val).closest('div.col-xs-12').find('div.audit select').prop('disabled', true);

    $.each($('div.audit .file-input *'), function (idx, val) {
        $(this).prop('disabled', true);
        $(this).attr('disabled', true);
    });

    $.each($('div.audit .btn-group button'), function (idx, val) {
        $(this).prop('disabled', true);
        $(this).attr('disabled', true);
    });
}

function cross(val) {
    $(val).prev().show();
    $(val).closest('div.col-xs-12').find('div.audit input').prop('disabled', false);
    $(val).closest('div.col-xs-12').find('div.audit select').prop('disabled', false);

    if ($(val).closest('div').prev().find('div.file-input').hasClass('file-input')) {
        $.each($('div.audit .file-input *'), function (idx, val) {
            $(this).prop('disabled', false);
            $(this).attr('disabled', false);
            if ($(this).hasClass('disable')) {
                $(this).removeClass('disable');
            }
            if ($(this).hasClass('file-caption-disabled')) {
                $(this).removeClass('file-caption-disabled');
            }
        });
    }

    if ($(val).closest('div').prev().find('div.btn-group').hasClass('btn-group')) {
        $.each($('div.audit button'), function (idx, val) {
            $(this).prop('disabled', false);
            $(this).attr('disabled', false);
            if ($(this).hasClass('disable')) {
                $(this).removeClass('disable');
            }
            if ($(this).hasClass('dropdown-toggle')) {
                $(this).removeClass('disabled');
            }
        });
    }
}

function checkInspect(val) {
    $(val).hide();
    $(val).closest('div.grp').find('div.audit input').prop('disabled', true);
    $(val).closest('div.grp').find('div.audit select').prop('disabled', true);
}

function crossInspect(val) {
    $(val).prev().show();
    $(val).closest('div.grp').find('div.audit input').prop('disabled', false);
    $(val).closest('div.grp').find('div.audit select').prop('disabled', false);
}

function getState() {
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetState',

        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblState tbody').empty();
                $('#slctMuniMailSta').empty();
                $.each(records, function (idx, val) {
                    $('#tblState tbody').append(
						'<tr class="rowHover" onclick="getCity(this);">' +
							'<td>' + val.State + '</td>' +
						'</tr>'
					);

                    $('#slctMuniMailSta').append('<option value="' + val.State + '">' + val.State + '</option>');
                });
            }
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function getCity(val) {
    var state = $(val).find('td').text();
    $.each($('#tblState tbody tr'), function () {
        $(this).removeClass('activeRow');
    })
    $(val).addClass('activeRow');

    $('input[type=text], select').val('');

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetCity',

        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblCity tbody').empty();
                $('#tblZip tbody').empty();
                $('#slctMuniMailCt').empty();

                $.each(records, function (idx, val) {
                    $('#tblCity tbody').append(
						'<tr class="rowHover" onclick="getZip(this);">' +
							'<td>' + val.City + '</td>' +
						'</tr>'
					);

                    $('#slctMuniMailCt').append('<option value="' + val.City + '">' + val.City + '</option>');
                });

                getLCIState();
                getLCICity('');
                getLCIZip('');
            }


            $('#divSettings').hide();

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function getZip(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow').text();
    var city = $(val).find('td').text();
    $.each($('#tblCity tbody tr'), function () {
        $(this).removeClass('activeRow');
    })
    $(val).addClass('activeRow');

    $('#divSettings input[type=text]').val('');
    $('#divSettings input[type=number]').val('');
    $('#modalEdit input[type=text]').val('');
    $('#divSettings select').each(function () {
        $(this).find('option:first-child').prop('selected', true);
    });

    $('#slctAddnInfo').multiselect({
        numberDisplayed: 1,
        includeSelectAllOption: true,
        buttonWidth: '100%'
    });

    getData(state, city);

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetZip',

        data: '{city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            //$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                //console.log(records);
                $('#tblZip tbody').empty();
                $('#slctMuniMailZip').empty();

                $.each(records, function (idx, val) {
                    var checked = '';
                    if (val.isZipActive) {
                        checked = 'checked';
                    }

                    $('#tblZip tbody').append(
						'<tr>' +
							'<td style="width: 10%;"><input data-id="' + val.ZipCode + '" type="checkbox" class="toggle" ' + checked + ' /></td>' +
							'<td><label>' + val.ZipCode + '</label></td>' +
						'</tr>'
					);

                    $('#slctMuniMailZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });


            }

            getOrdinance();

            $('.toggle').bootstrapToggle({
                on: 'Active',
                off: 'Inactive'
            });

            $('#divSettings').show();

            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }, failure: function (response) {
            console.log(response.responseText);
        }
    });
}

function getData(state, city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetData',

        data: '{state: "' + state + '", city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {

            var d = $.parseJSON(data.d);

            if (d.Success) {
                // PFC
                var regPFC = d.data.regPFC;

                if (regPFC.length > 0) {
                    if (regPFC[0].tag == 'for audit') {
                        $('#tabPFC div.valid').remove();
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#slctPfcCityNotice').val('' + regPFC[0].pfc_city_notice + '');
                        $('#slctPfcDefault').val('' + regPFC[0].pfc_default + '');
                        $('#inDefRegTimeline1').val(regPFC[0].def_reg_timeline1);
                        $('#slctDefRegTimeline2').val(regPFC[0].def_reg_timeline2);
                        $('#slctDefRegTimeline3').val(regPFC[0].def_reg_timeline3);
                        $('#slctDefRegTimeline4').val(regPFC[0].def_reg_timeline4);
                        $('#inPfcDefForclosureTimeline1').val(regPFC[0].pfc_def_foreclosure_timeline1);
                        $('#slctPfcDefForclosureTimeline2').val(regPFC[0].pfc_def_foreclosure_timeline2);
                        $('#slctPfcDefForclosureTimeline3').val(regPFC[0].pfc_def_foreclosure_timeline3);
                        $('#slctPfcDefForclosureTimeline4').val(regPFC[0].pfc_def_foreclosure_timeline4);
                        $('#slctPfcForeclosureVacant').val('' + regPFC[0].pfc_foreclosure_vacant + '');
                        $('#inPfcForeclosureVacantTimeline1').val(regPFC[0].pfc_foreclosure_vacant_timeline1);
                        $('#slctPfcForeclosureVacantTimeline2').val(regPFC[0].pfc_foreclosure_vacant_timeline2);
                        $('#slctPfcForeclosureVacantTimeline3').val(regPFC[0].pfc_foreclosure_vacant_timeline3);
                        $('#slctPfcForeclosureVacantTimeline4').val(regPFC[0].pfc_foreclosure_vacant_timeline4);
                        $('#slctPfcBoarded').val('' + regPFC[0].pfc_boarded + '');
                        $('#slctPfcForeclosure').val('' + regPFC[0].pfc_foreclosure + '');
                        $('#inPfcOtherTimeline1').val(regPFC[0].pfc_other_timeline1);
                        $('#slctPfcOtherTimeline2').val(regPFC[0].pfc_other_timeline2);
                        $('#slctPfcOtherTimeline3').val(regPFC[0].pfc_other_timeline3);
                        $('#slctPfcOtherTimeline4').val(regPFC[0].pfc_other_timeline4);
                        $('#slctPfcVacantTimeline').val('' + regPFC[0].pfc_vacant + '');
                        $('#inPfcVacantTimeline1').val(regPFC[0].pfc_vacant_timeline1);
                        $('#slctPfcVacantTimeline2').val(regPFC[0].pfc_vacant_timeline2);
                        $('#slctPfcVacantTimeline3').val(regPFC[0].pfc_vacant_timeline3);
                        $('#slctPfcVacantTimeline4').val(regPFC[0].pfc_vacant_timeline4);
                        $('#slctPfcCodeViolation').val('' + regPFC[0].pfc_code_violation + '');
                        $('#slctSpcReq').val(regPFC[0].special_requirements);
                        $('#slctPaymentType').val(regPFC[0].payment_type);
                        $('#slctTypeOfRegistration').val(regPFC[0].type_of_registration);
                        $('#slctVmsRenewal').val(regPFC[0].vms_renewal);

                        if (regPFC[0].type_of_registration == 'pdf') {
                            var fn = regPFC[0].upload_path;

                            $('#slctTypeOfRegistration').closest('div').next().show();

                            $('#tabPFC div.file-caption-main div.file-caption-name').attr('title', fn);
                            $('#tabPFC div.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctTypeOfRegistration').closest('div').next().hide();

                            $('#tabPFC div.file-caption-main div.file-caption-name').removeAttr('title');
                            $('#tabPFC div.file-caption-main div.file-caption-name').html('');
                        }

                    } else {
                        $('#tabPFC div.valid').remove();
                        $('#tabPFC').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabPFC div.valid').remove();
                    $('#tabPFC').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabPFC div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                // REO
                var regREO = d.data.regREO;

                if (regREO.length > 0) {
                    if (regREO[0].tag == 'for audit') {
                        $('#tabREO div.valid').remove();
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#slctReoBankOwed').val('' + regREO[0].reo_bank_owed + '');
                        $('#inReoBankOwed1').val('' + regREO[0].reo_bank_owed_timeline1 + '');
                        $('#slctReoBankOwed2').val('' + regREO[0].reo_bank_owed_timeline2 + '');
                        $('#slctReoBankOwed3').val('' + regREO[0].reo_bank_owed_timeline3 + '');
                        $('#slctReoBankOwed4').val('' + regREO[0].reo_bank_owed_timeline4 + '');
                        $('#slctReoBoardedOnly').val('' + regREO[0].reo_boarded_only + '');
                        $('#slctReoCityNotice').val('' + regREO[0].reo_city_notice + '');
                        $('#slctReoCodeViolation').val('' + regREO[0].reo_code_violation + '');
                        $('#slctReoDistressedAbandoned').val('' + regREO[0].reo_distressed_abandoned + '');
                        $('#inReoDistressedAbandoned1').val('' + regREO[0].reo_distressed_abandoned1 + '');
                        $('#slctReoDistressedAbandoned2').val('' + regREO[0].reo_distressed_abandoned2 + '');
                        $('#slctReoDistressedAbandoned3').val('' + regREO[0].reo_distressed_abandoned3 + '');
                        $('#slctReoDistressedAbandoned4').val('' + regREO[0].reo_distressed_abandoned4 + '');
                        $('#inReoOtherTimeline1').val('' + regREO[0].reo_other_timeline1 + '');
                        $('#slctReoOtherTimeline2').val('' + regREO[0].reo_other_timeline2 + '');
                        $('#slctReoOtherTimeline3').val('' + regREO[0].reo_other_timeline3 + '');
                        $('#slctReoOtherTimeline4').val('' + regREO[0].reo_other_timeline4 + '');
                        $('#slctReoVacant').val('' + regREO[0].reo_vacant + '');
                        $('#inReoVacantTimeline1').val('' + regREO[0].reo_vacant_timeline1 + '');
                        $('#slctReoVacantTimeline2').val('' + regREO[0].reo_vacant_timeline2 + '');
                        $('#slctReoVacantTimeline3').val('' + regREO[0].reo_vacant_timeline3 + '');
                        $('#slctReoVacantTimeline4').val('' + regREO[0].reo_vacant_timeline4 + '');
                        $('#slctRentalRegistration').val('' + regREO[0].rental_registration + '');
                        $('#slctRentalForm').val('' + regREO[0].rental_form + '');

                    } else {
                        $('#tabREO div.valid').remove();
                        $('#tabREO').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabREO div.valid').remove();
                    $('#tabREO').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabREO div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                // Prop Type
                var regPropType = d.data.regPropType;

                if (regPropType.length > 0) {
                    if (regPropType[0].tag == 'for audit') {
                        $('#tabPropType div.valid').remove();
                        $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#slctPropResidential').val('' + regPropType[0].residential + '');
                        $('#slctPropRental').val('' + regPropType[0].rental + '');
                        $('#slctPropCommercial').val('' + regPropType[0].commercial + '');
                        $('#slctPropCondo').val('' + regPropType[0].condo + '');
                        $('#slctPropTownhome').val('' + regPropType[0].townhome + '');
                        $('#slctPropVacantLot').val('' + regPropType[0].vacant_lot + '');
                        $('#slctPropMobilehome').val('' + regPropType[0].mobile_home + '');
                    } else {
                        $('#tabPropType div.valid').remove();
                        $('#tabPropType').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabPropType div.valid').remove();
                    $('#tabPropType').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabPropType div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                var regPropReq = d.data.regPropReq;

                if (regPropReq.length > 0) {
                    if (regPropReq[0].tag == 'for audit') {
                        $('#tabPropReq div.valid').remove();
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        var add_info = '';
                        if (regPropReq[0].addn_info != '') {
                            var split = regPropReq[0].addn_info.split(',');
                            $('#slctAddnInfo').multiselect('select', split);
                        }

                        $('#slctFirstTimeVacancyDate').val('' + regPropReq[0].first_time_vacancy_date + '');
                        $('#slctPresaleDefinition').val(regPropReq[0].presale_definition);
                        $('#slctSecuredRequired').val('' + regPropReq[0].secured_required + '');
                        $('#slctLocalContactRequired').val('' + regPropReq[0].local_contact_required + '');
                        $('#slctAddSignReq').val('' + regPropReq[0].additional_signage_required + '');
                        $('#slctPicReq').val('' + regPropReq[0].pictures_required + '');
                        $('#slctGseExclusion').val('' + regPropReq[0].gse_exclusion + '');
                        $('#slctMobileVINReq').val('' + regPropReq[0].mobile_vin_number_required + '');
                        $('#slctInsuranceReq').val('' + regPropReq[0].insurance_required + '');
                        $('#slctParcelReq').val('' + regPropReq[0].parcel_number_required + '');
                        $('#slctForeclosureActInfo').val('' + regPropReq[0].foreclosure_action_information_needed + '');
                        $('#slctLegalDescReq').val('' + regPropReq[0].legal_description_required + '');
                        $('#slctForeclosureCaseInfo').val('' + regPropReq[0].foreclosure_case_information_needed + '');
                        $('#slctBlockLotReq').val('' + regPropReq[0].block_and_lot_number_required + '');
                        $('#slctForeclosureDeedReq').val('' + regPropReq[0].foreclosure_deed_required + '');
                        $('#slctAttyInfoReq').val('' + regPropReq[0].attorney_information_required + '');
                        $('#slctBondReq').val('' + regPropReq[0].bond_required + '');
                        $('#slctBrkInfoReq').val('' + regPropReq[0].broker_information_required_if_reo + '');
                        $('#inBondAmount').val(regPropReq[0].bond_amount);
                        $('#slctMortContactNameReq').val('' + regPropReq[0].mortgage_contact_name_required + '');
                        $('#slctMaintePlanReq').val('' + regPropReq[0].maintenance_plan_required + '');
                        $('#slctClientTaxReq').val('' + regPropReq[0].client_tax_number_required + '');
                        $('#slctNoTrespassReq').val('' + regPropReq[0].no_trespass_form_required + '');
                        $('#slctSignReq').val('' + regPropReq[0].signature_required + '');
                        $('#slctUtilityInfoReq').val('' + regPropReq[0].utility_information_required + '');
                        $('#slctNotarizationReq').val('' + regPropReq[0].notarization_required + '');
                        $('#slctWinterReq').val('' + regPropReq[0].winterization_required + '');
                        $('#slctRecInsDate').val('' + regPropReq[0].recent_inspection_date + '');

                        $('#lciCompany').val(regPropReq[0].lcicompany_name);
                        $('#lciFirstName').val(regPropReq[0].lcifirst_name);
                        $('#lciLastName').val(regPropReq[0].lcilast_name);
                        $('#lciTitle').val(regPropReq[0].lcititle);
                        $('#lciBusinessLicenseNum').val(regPropReq[0].lcibusiness_license_num);
                        $('#lciPhoneNum1').val(regPropReq[0].lciphone_num1);
                        $('#lciPhoneNum2').val(regPropReq[0].lciphone_num2);
                        $('#lciBusinessPhoneNum1').val(regPropReq[0].lcibusiness_phone_num1);
                        $('#lciBusinessPhoneNum2').val(regPropReq[0].lcibusiness_phone_num2);
                        $('#lciEmrPhone1').val(regPropReq[0].lciemergency_phone_num1);
                        $('#lciEmrPhone2').val(regPropReq[0].lciemergency_phone_num2);
                        $('#lciFaxNum1').val(regPropReq[0].lcifax_num1);
                        $('#lciFaxNum2').val(regPropReq[0].lcifax_num2);
                        $('#lciCellNum1').val(regPropReq[0].lcicell_num1);
                        $('#lciCellNum2').val(regPropReq[0].lcicell_num2);
                        $('#lciEmail').val(regPropReq[0].lciemail);
                        $('#lciStreet').val(regPropReq[0].lcistreet);
                        $('#lciState').val(regPropReq[0].lcistate);
                        $('#lciCity').val(regPropReq[0].lcicity);
                        $('#lciZip').val(regPropReq[0].lcizip);
                        $('#lciHrsFrom').val(regPropReq[0].lcihours_from);
                        $('#lciHrsTo').val(regPropReq[0].lcihours_to);
                    } else {
                        $('#tabPropReq div.valid').remove();
                        $('#tabPropReq').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabPropReq div.valid').remove();
                    $('#tabPropReq').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabPropReq div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                var regCost = d.data.regCost;

                if (regCost.length > 0) {
                    if (regCost[0].tag == 'for audit') {
                        $('#tabCost div.valid').remove();
                        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabCost div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabCost div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#slctRegCost').val('' + regCost[0].reg_cost + '');
                        $('#inRegCost').val(regCost[0].reg_cost_amt);
                        $('#slctRegCostCurr').val(regCost[0].reg_cost_curr);
                        $('#slctRegCostStandard').val('' + regCost[0].reg_cost_standard + '');
                        $('#slctRenewCostEscal').val('' + regCost[0].is_renewal_cost_escal1 + '');
                        $('#inRenewAmt').val(regCost[0].renewal_fee_amt);
                        $('#slctRenewAmtCurr1').val(regCost[0].renewal_fee_curr);
                        $('#inComRegFee').val(regCost[0].com_reg_fee);
                        $('#slctComRegFee').val(regCost[0].com_reg_curr);
                        $('#slctComFeeStandard').val(regCost[0].com_fee_standard);
                        $('#slctRenewCostEscal2').val('' + regCost[0].is_renew_cost_escal2 + '');
                        $('#inRenewAmt2').val(regCost[0].com_renew_cost_amt);
                        $('#slctRenewAmtCurr2').val(regCost[0].com_renew_cost_curr);
                        $('#slctRegCostStandard2').val('' + regCost[0].is_reg_cost_standard + '');

                        var splitServType1 = regCost[0].reg_escal_service_type.split(',');
                        var splitAmt1 = regCost[0].reg_escal_amount.split(',');
                        var splitCurr1 = regCost[0].reg_escal_curr.split(',');
                        var splitSucceeding1 = regCost[0].reg_escal_succeeding.split(',');

                        $('#divEscalRenewal div.escalInput').empty();

                        $.each(splitServType1, function (idx, val) {
                            if (idx == 0) {
                                $('#divEscalRenewal div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<label>Renewal</label>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '</div>'
                                );

                                $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            } else {
                                var checked = (splitSucceeding1 == 'true') ? 'checked' : '';
                                $('#divEscalRenewal div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<label>Renewal</label>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt1[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                                );

                                $('#divEscalRenewal div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            }
                        });

                        var splitServType2 = regCost[0].com_escal_service_type.split(',');
                        var splitAmt2 = regCost[0].com_escal_amount.split(',');
                        var splitCurr2 = regCost[0].com_escal_curr.split(',');
                        var splitSucceeding2 = regCost[0].com_escal_succeeding.split(',');

                        $('#divEscalRenewal2 div.escalInput').empty();

                        $.each(splitServType2, function (idx, val) {
                            if (idx == 0) {
                                $('#divEscalRenewal2 div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<label>Renewal</label>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '</div>'
                                );

                                $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            } else {
                                var checked = (splitSucceeding2 == 'true') ? 'checked' : '';
                                $('#divEscalRenewal2 div.escalInput').append(
                                    '<div class="col-xs-12" style="margin-top: 1%;">' +
                                        '<div class="col-xs-3">' +
                                            '<label>Renewal</label>' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<input type="number" class="form-control input-sm" value="' + splitAmt2[idx] + '">' +
                                        '</div>' +
                                        '<div class="col-xs-2">' +
                                            '<select class="form-control input-sm">' +
                                                '<option value="USD">USD</option>' +
                                            '</select>' +
                                        '</div>' +
                                    '<div class="col-xs-5"><input type="checkbox" ' + checked + '>&nbsp;Check if succeeding renewals use this amount</div></div>'
                                );

                                $('#divEscalRenewal2 div.escalInput div.col-xs-12:last-child select:eq(0)').val(val);
                            }
                        });

                    } else {
                        $('#tabCost div.valid').remove();
                        $('#tabCost').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabCost div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabCost div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabCost div.valid').remove();
                    $('#tabCost').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-green">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabCost div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabCost div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabCost div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                var regCont = d.data.regCont;

                if (regCont.length > 0) {
                    if (regCont[0].tag == 'for audit') {
                        $('#tabContReg div.valid').remove();
                        $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#slctContReg').val(regCont[0].cont_reg);
                    } else {
                        $('#tabContReg div.valid').remove();
                        $('#tabContReg').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                }

                var inspect = d.data.inspect;

                if (inspect.length > 0) {
                    if (inspect[0].tag == 'for audit') {
                        $('#tabInspection div.valid').remove();
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#slctInsUpReq').val('' + inspect[0].inspection_update_req + '');
                        $('#slctInsCriOcc').val('' + inspect[0].inspection_criteria_occ + '');
                        $('#slctInsCriVac').val(inspect[0].inspection_criteria_vac);
                        $('#slctInsFeePay').val(inspect[0].inspection_fee_payment_freq);
                        $('#slctInsFeeReq').val('' + inspect[0].inspection_fee_req + '');
                        $('#inInsFeeAmt').val(inspect[0].inspection_fee_amount);
                        $('#slctInsFeeAmt').val(inspect[0].inspection_fee_curr);
                        $('#slctInsRepFreq').val('' + inspect[0].inspection_reporting_freq + '');

                        $("#inInsCriteriaOccTime").val(inspect[0].criteria_occ_cycle_refresh_time);
                        document.getElementById("setRefresh").checked = true;

                        $("#slctInsCriteriaOccCycle1").val(inspect[0].criteria_occ_cycle_1);
                        $("#slctInsCriteriaOccCycle2").val(inspect[0].criteria_occ_cycle_2);

                        //Criteria Occupied
                        //daily 
                        if (inspect[0].criteria_occ_freq == 'daily') {

                            document.getElementById("daily").checked = true
                            $("#freqInsCriteriaOccDaily").show();

                            if (inspect[0].criteria_occ_cycle_day_1 != "") {
                                document.getElementById("dailyevery1").checked = true;

                                var days = inspect[0].criteria_occ_cycle_day_1;

                                var splitDays = days.split(',');

                                for (i = 0; i < splitDays.length; i++) {
                                    if (splitDays[i] == "Monday") {
                                        document.getElementById("mon").checked = true
                                    }
                                    else if (splitDays[i] == "Tuesday") {
                                        document.getElementById("tues").checked = true
                                    }
                                    else if (splitDays[i] == "Wednesday") {
                                        document.getElementById("wed").checked = true
                                    }
                                    else if (splitDays[i] == "Thursday") {
                                        document.getElementById("thurs").checked = true
                                    }
                                    else if (splitDays[i] == "Friday") {
                                        document.getElementById("fri").checked = true
                                    }
                                }
                            }
                            else if (inspect.criteria_occ_cycle_day_2 != "") {
                                inInsCriteriaOccDay
                                $("#inInsCriteriaOccDay").val(inspect[0].criteria_occ_cycle_day_2);
                                document.getElementById("dailyevery2").checked = true;
                            } else {
                                document.getElementById("dailyevery1").checked = false;
                                document.getElementById("dailyevery2").checked = false;
                            }

                        }
                            //weekly
                        else if (inspect[0].criteria_occ_freq == 'weekly') {
                            document.getElementById("weekly").checked = true
                            $("#freqInsCriteriaOccWeekly").show();

                            if (inspect[0].criteria_occ_cycle_week_1 != "") {
                                document.getElementById("rbEvery1").checked = true;
                                $("#inInsCriteriaOccWeek").val(inspect[0].criteria_occ_cycle_week_1);
                            } else if (inspect[0].criteria_occ_cycle_week_2 != "") {
                                document.getElementById("rbEvery2").checked = true;
                                $("#slctInsCriteriaOccWeek").val(inspect[0].criteria_occ_cycle_week_2);
                            } else {
                                document.getElementById("rbEvery1").checked = false;
                                document.getElementById("rbEvery2").checked = false;
                            }
                        }
                            //monthly
                        else if (inspect[0].criteria_occ_freq == 'monthly') {

                            document.getElementById("monthly").checked = true
                            $("#freqInsCriteriaOccMonthly").show();

                            if (inspect[0].criteria_occ_cycle_month_1 != "") {
                                document.getElementById("everyMonth1").checked = true;
                                $("#inInsCriteriaOccMonth").val(inspect[0].criteria_occ_cycle_month_1);
                            }
                            else if (inspect[0].criteria_occ_cycle_month_2 != "") {
                                document.getElementById("everyMonth2").checked = true;
                                $("#slctInsCriteriaMonth1").val(inspect[0].criteria_occ_cycle_month_2);
                                $("#slctInsCriteriaMonth2").val(inspect[0].criteria_occ_cycle_month_3);
                            } else {
                                document.getElementById("everyMonth1").checked = false;
                                document.getElementById("everyMonth2").checked = false;
                            }
                        }
                            //yearly
                        else if (inspect[0].criteria_occ_freq == 'yearly') {

                            document.getElementById("yearly").checked = true
                            $("#freqInsCriteriaOccYearly").show();

                            if (inspect[0].criteria_occ_cycle_year_1 != "") {
                                document.getElementById("anually").checked = true;

                            }
                            else if (inspect[0].criteria_occ_cycle_year_2 != "") {
                                document.getElementById("everyYear").checked = true;
                                $("#inInsCriteriaOccYear").val(inspect[0].criteria_occ_cycle_year_2);
                            } else {
                                document.getElementById("anually").checked = false;
                                document.getElementById("everyYear").checked = false;
                            }
                        }

                        //Criteria Vacant
                        $("#inInsCriteriaVacTime").val(inspect[0].criteria_vac_cycle_refresh_time);
                        document.getElementById("setRefreshVac").checked = true;
                        $("#slctInsCriteriaVacCycle1").val(inspect[0].criteria_vac_cycle_1);
                        $("#slctInsCriteriaVacCycle2").val(inspect[0].criteria_vac_cycle_2);

                        //dailyVac
                        if (inspect[0].criteria_vac_freq == "daily") {
                            document.getElementById("dailyVac").checked = true
                            $("#freqInsCriteriaVacDaily").show();
                            if (inspect[0].criteria_vac_cycle_day_1 != "") {
                                document.getElementById("rbDailyVac1").checked = true;

                                var daysVac = inspect[0].criteria_vac_cycle_day_1;

                                var splitDaysVac = daysVac.split(',');

                                for (i = 0; i < splitDaysVac.length; i++) {
                                    if (splitDaysVac[i] == "Monday") {
                                        document.getElementById("vacMon").checked = true
                                    }
                                    else if (splitDaysVac[i] == "Tuesday") {
                                        document.getElementById("vacTues").checked = true
                                    }
                                    else if (splitDaysVac[i] == "Wednesday") {
                                        document.getElementById("vacWed").checked = true
                                    }
                                    else if (splitDaysVac[i] == "Thursday") {
                                        document.getElementById("vacThurs").checked = true
                                    }
                                    else if (splitDaysVac[i] == "Friday") {
                                        document.getElementById("vacFri").checked = true
                                    }
                                }
                            }
                            else if (inspect[0].criteria_vac_cycle_day_2 != "") {
                                document.getElementById("rbDailyVac2").checked = true;
                                $("#inInsCriteriaVacDay").val(inspect[0].criteria_vac_cycle_day_2);
                            }
                            else {
                                document.getElementById("rbDailyVac1").checked = false;
                                document.getElementById("rbDailyVac2").checked = false;
                            }
                        }
                            //weeklyVac
                        else if (inspect[0].criteria_vac_freq == "weekly") {
                            document.getElementById("weeklyVac").checked = true
                            $("#freqInsCriteriaVacWeekly").show();

                            if (inspect[0].criteria_vacc_cycle_week_1 != "") {
                                document.getElementById("rbWeeklyVac1").checked = true;
                                $("#inInsCriteriaVacWeek").val(inspect[0].criteria_vac_cycle_week_1);
                            }
                            else if (inspect[0].criteria_vac_cycle_week_2 != "") {
                                document.getElementById("rbWeeklyVac2").checked = true;
                                $("#slctInsCriteriaVacWeek").val(inspect[0].criteria_vac_cycle_week_2);
                            }
                            else {
                                document.getElementById("rbWeeklyVac2").checked = false;
                                document.getElementById("rbWeeklyVac1").checked = false;
                            }
                        }
                            //monthlyVac
                        else if (inspect[0].criteria_vac_freq == "monthly") {
                            document.getElementById("monthlyVac").checked = true
                            $("#freqInsCriteriaVacMonthly").show();

                            if (inspect[0].criteria_vac_cycle_month_1 != "") {
                                document.getElementById("rbMonthlyVac1").checked = true;
                                $("#inInsCriteriaVacMonth").val(inspect[0].criteria_vac_cycle_month_1);
                            }
                            else if (inspect[0].criteria_vac_cycle_month_2 != "") {
                                document.getElementById("rbMonthlyVac2").checked = true;
                                $("#slctInsCriteriaVacMonth1").val(inspect[0].criteria_vac_cycle_month_2);
                                $("#slctInsCriteriaVacMonth2").val(inspect[0].criteria_vac_cycle_month_3);
                            } else {
                                document.getElementById("rbMonthlyVac1").checked = false;
                                document.getElementById("rbMonthlyVac2").checked = false;
                            }

                        }
                            //yearlyVac
                        else if (inspect[0].criteria_vac_freq == "yearly") {
                            document.getElementById("yearlyVac").checked = true
                            $("#freqInsCriteriaVacYearly").show();

                            if (inspect[0].criteria_vac_cycle_year_1 != "") {
                                document.getElementById("rbYearlyVac1").checked = true;

                            }
                            else if (inspect[0].criteria_vac_cycle_year_2 != "") {
                                document.getElementById("everyYear").checked = true;
                                $("#inInsCriteriaVacYear").val(inspect[0].criteria_vac_cycle_year_2);
                            } else {
                                document.getElementById("rbYearlyVac1").checked = false;
                                document.getElementById("rbYearlyVac2").checked = false;
                            }
                        }

                        //Payment Frequency
                        $("#inFeePayTime").val(inspect[0].fee_payment_cycle_refresh_time);
                        document.getElementById("cbFreqTime").checked = true;
                        $("#slctFeePayCycle1").val(inspect[0].fee_payment_cycle_1);
                        $("#slctFeePayCycle2").val(inspect[0].fee_payment_cycle_2);

                        //dailyFrequency
                        if (inspect[0].fee_payment_freq == "daily") {
                            document.getElementById("dailyFreq").checked = true
                            $('#freqInsFeePayFreqDaily').show();

                            if (inspect[0].fee_payment_cycle_day_1 != "") {
                                document.getElementById("rbFreq1").checked = true;

                                var daysFreq = inspect[0].fee_payment_cycle_day_1;

                                var splitDaysFreq = daysFreq.split(',');

                                for (i = 0; i < splitDaysFreq.length; i++) {
                                    if (splitDaysFreq[i] == "Monday") {
                                        document.getElementById("freqMon").checked = true
                                    }
                                    else if (splitDaysFreq[i] == "Tuesday") {
                                        document.getElementById("freqTues").checked = true
                                    }
                                    else if (splitDaysFreq[i] == "Wednesday") {
                                        document.getElementById("freqWed").checked = true
                                    }
                                    else if (splitDaysFreq[i] == "Thursday") {
                                        document.getElementById("freqThurs").checked = true
                                    }
                                    else if (splitDaysFreq[i] == "Friday") {
                                        document.getElementById("freqFri").checked = true
                                    }
                                }
                            }
                            else if (inspect[0].fee_payment_cycle_day_2 != "") {
                                document.getElementById("rbFreq2").checked = true;
                                $("#inFeePayFreqDay").val(inspect[0].fee_payment_cycle_day_2);
                            }
                            else {
                                document.getElementById("rbFreq1").checked = false;
                                document.getElementById("rbFreq2").checked = false;
                            }
                        }
                            //weeklyFrequency
                        else if (inspect[0].fee_payment_freq == "weekly") {
                            document.getElementById("weeklyFreq").checked = true
                            $('#freqInsFeePayFreqWeekly').show();

                            if (inspect[0].fee_payment_cycle_week_1 != "") {
                                document.getElementById("rbWeeklyFreq1").checked = true;
                                $("#inFeePayFreqWeek").val(inspect[0].fee_payment_cycle_week_1);
                            }
                            else if (inspect[0].fee_payment_cycle_week_2 != "") {
                                document.getElementById("rbWeeklyFreq2").checked = true;
                                $("#slctFeePayFreqWeek").val(inspect[0].fee_payment_cycle_week_2);
                            }
                            else {
                                document.getElementById("rbWeeklyFreq1").checked = false;
                                document.getElementById("rbWeeklyFreq2").checked = false;
                            }

                        }
                            //monthlyFrequency
                        else if (inspect[0].fee_payment_freq == "monthly") {
                            document.getElementById("monthlyFreq").checked = true
                            $('#freqInsFeePayFreqMonthly').show();

                            if (inspect[0].fee_payment_cycle_month_1 != "") {
                                document.getElementById("rbMonthlyFreq1").checked = true;
                                $("#inFeePayFreqMonth").val(inspect[0].fee_payment_cycle_month_1);
                            }
                            else if (inspect[0].fee_payment_cycle_month_2 != "") {
                                document.getElementById("rbMonthlyFreq2").checked = true;
                                $("#slctFeePayFreqMonth1").val(inspect[0].fee_payment_cycle_month_2);
                                $("#slctFeePayFreqMonth2").val(inspect[0].fee_payment_cycle_month_3);
                            } else {
                                document.getElementById("rbMonthlyFreq1").checked = false;
                                document.getElementById("rbMonthlyFreq2").checked = false;
                            }

                        }
                            //yearlyFrequency
                        else if (inspect[0].fee_payment_freq == "yearly") {
                            document.getElementById("yearlyFreq").checked = true
                            $('#freqInsFeePayFreqYearly').show();

                            if (inspect[0].fee_payment_cycle_year_1 != "") {
                                document.getElementById("rbYearlyFreq1").checked = true;

                            }
                            else if (inspect[0].fee_payment_cycle_year_2 != "") {
                                document.getElementById("rbYearlyFreq2").checked = true;
                                $("#inFeePayFreqYear").val(inspect[0].fee_payment_cycle_year_2);
                            } else {
                                document.getElementById("rbYearlyFreq1").checked = false;
                                document.getElementById("rbYearlyFreq2").checked = false;
                            }
                        }

                    } else {
                        $('#tabInspection div.valid').remove();
                        $('#tabInspection').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabInspection div.valid').remove();
                    $('#tabInspection').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-red">NO DATA TO VALIDATED!</label>' +
                        '</div>'
                    );
                    $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabInspection div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                var municipal = d.data.municipal;

                if (municipal.length > 0) {
                    if (municipal[0].tag == 'for audit') {
                        $('#tabMunicipality div.valid').remove();
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#inMuniDept').val(municipal[0].municipality_department);
                        $('#inMuniPhone').val(municipal[0].municipality_phone_num);
                        $('#inMuniEmail').val(municipal[0].municipality_email);
                        $('#inMuniMailStr').val(municipal[0].municipality_st);
                        $('#inMuniMailCt').val(municipal[0].municipality_city);
                        $('#inMuniMailSta').val(municipal[0].municipality_state);
                        $('#inMuniMailZip').val(municipal[0].municipality_zip);
                        $('#inContact').val(municipal[0].contact_person);
                        $('#inTitle').val(municipal[0].title);
                        $('#inDept').val(municipal[0].department);
                        $('#inPhone').val(municipal[0].phone_num);
                        $('#inEmail').val(municipal[0].email);
                        $('#inMailing').val(municipal[0].address);
                        $('#inHrsFrom').val(municipal[0].ops_hrs_from);
                        $('#inHrsTo').val(municipal[0].ops_hrs_to);
                    } else {
                        $('#tabMunicipality div.valid').remove();
                        $('#tabMunicipality').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabMunicipality div.valid').remove();
                    $('#tabMunicipality').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-red">NO DATA TO VALIDATED!</label>' +
                        '</div>'
                    );
                    $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                var dereg = d.data.dereg;

                if (dereg.length > 0) {
                    if (dereg[0].tag == 'for audit') {
                        $('#tabDeregistration div.valid').remove();
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-check').closest('button').show();
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').show();

                        $('#slctDeregRequired').val('' + dereg[0].dereg_req + '');
                        $('#slctConveyed').val('' + dereg[0].conveyed + '');
                        $('#slctOccupied').val('' + dereg[0].occupied + '');
                        $('#slctHowToDereg').val(dereg[0].how_to_dereg);
                        $('#inUploadPathD').val(dereg[0].upload_file);
                        $('#slctNewOwnerInfoReq').val('' + dereg[0].new_owner_info_req + '');
                        $('#slctProofOfConveyReq').val('' + dereg[0].proof_of_conveyance_req + '');
                        $('#slctDateOfSaleReq').val('' + dereg[0].date_of_sale_req + '');

                        if (dereg[0].how_to_dereg == 'pdf') {
                            var fn = dereg[0].upload_file.replace('C:\\fakepath\\', '');

                            $('#slctHowToDereg').closest('div').next().show();

                            $('#tabDeregistratopm div.file-caption-main div.file-caption-name').attr('title', fn);
                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').append('<i class="glyphicon glyphicon-file kv-caption-icon"></i>' + fn);
                        } else {
                            $('#slctHowToDereg').closest('div').next().hide();

                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').removeAttr('title');
                            $('#tabDeregistratopmdiv.file-caption-main div.file-caption-name').html('');
                        }
                    } else {
                        $('#tabDeregistration div.valid').remove();
                        $('#tabDeregistration').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabDeregistration div.valid').remove();
                    $('#tabDeregistration').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-red">NO DATA TO VALIDATED!</label>' +
                        '</div>'
                    );
                    $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-check').closest('button').hide();
                    $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                }

                // Notification
                var notif = d.data.notif;

                if (notif.length > 0) {
                    if (notif[0].tag == 'for audit') {
                        $('#tabNotif div.valid').remove();
                        $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', false);
                        $('#tabNotif fieldset div.col-xs-12 .fa-check').closest('button').show();
                        $('#tabNotif fieldset div.col-xs-12 .fa-times').closest('button').show();

                        $('#inRegSendRem').val(notif[0].reg_send_reminder_num);
                        $('#slctRegSendRem1').val(notif[0].reg_send_reminder_type);
                        $('#slctRegSendRem2').val(notif[0].reg_send_reminder_days);
                        $('#slctRegSendRem3').val(notif[0].reg_send_reminder_before);
                        $('#slctRegSendRem4').val(notif[0].reg_send_reminder_time_frame);
                        $('#inRenewSendRem').val(notif[0].ren_send_reminder_num);
                        $('#slctRenewSendRem1').val(notif[0].ren_send_reminder_type);
                        $('#slctRenewSendRem2').val(notif[0].ren_send_reminder_days);
                        $('#slctRenewSendRem3').val(notif[0].ren_send_reminder_before);
                        $('#slctRenewSendRem4').val(notif[0].ren_send_reminder_time_frame);
                        $('#inWebSendRem').val(notif[0].web_notif_url);
                    } else {
                        $('#tabNotif div.valid').remove();
                        $('#tabNotif').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );
                        $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabNotif fieldset div.col-xs-12 .fa-check').closest('button').hide();
                        $('#tabNotif fieldset div.col-xs-12 .fa-times').closest('button').hide();
                    }
                } else {
                    $('#tabNotif div.valid').remove();
                    $('#tabNotif').prepend(
                        '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                            '<label class="text-red">NO DATA TO VALIDATE!</label>' +
                        '</div>'
                    );
                    $('#tabNotif div.col-xs-12').last().find('button').prop('disabled', true);
                    $('#tabNotif fieldset div.col-xs-12 .fa-check').closest('button').hide();
                    $('#tabNotif fieldset div.col-xs-12 .fa-times').closest('button').hide();
                }
            }

            $('#modalLoading').modal('hide');

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

$('input[name=rdInsCriteriaOccFreq]').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsCriteriaOccDaily').show();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').show();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').show();
        $('#freqInsCriteriaOccYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsCriteriaOccDaily').hide();
        $('#freqInsCriteriaOccWeekly').hide();
        $('#freqInsCriteriaOccMonthly').hide();
        $('#freqInsCriteriaOccYearly').show();
    }
});

$('input[name=rdInsCriteriaVacFreq]').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsCriteriaVacDaily').show();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').show();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').show();
        $('#freqInsCriteriaVacYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsCriteriaVacDaily').hide();
        $('#freqInsCriteriaVacWeekly').hide();
        $('#freqInsCriteriaVacMonthly').hide();
        $('#freqInsCriteriaVacYearly').show();
    }
});

$('input[name=rdInsFeePayFreq]').change(function () {
    if ($(this).val() == 'daily') {
        $('#freqInsFeePayFreqDaily').show();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'weekly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').show();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'monthly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').show();
        $('#freqInsFeePayFreqYearly').hide();
    } else if ($(this).val() == 'yearly') {
        $('#freqInsFeePayFreqDaily').hide();
        $('#freqInsFeePayFreqWeekly').hide();
        $('#freqInsFeePayFreqMonthly').hide();
        $('#freqInsFeePayFreqYearly').show();
    }
});

function getOrdinance(val) {
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetOrdinance',

        data: '{state: "' + state + '", city: "' + city + '", hist: "' + val + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                if (val == "" || val == undefined) {
                    $('#tblOrdinance').bootstrapTable('destroy');

                    $('#tblOrdinance').bootstrapTable({
                        data: records
                    });

                    $('#tblOrdinance thead tr th:eq(2) div:eq(0)').attr('style', 'width: 500px');
                } else {
                    $('#tblOrdinanceHist').bootstrapTable('destroy');

                    $('#tblOrdinanceHist').bootstrapTable({
                        data: records
                    });

                    $('#tblOrdinanceHist thead tr th:eq(2) div:eq(0)').attr('style', 'width: 500px');
                }

            }

        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function action(val) {
    return '<div class="col-xs-2"><button type="button" class="btn btn-success " data-id="' + val + '" onclick="editOrd(this);"><i class="fa fa-pencil-square"></i></button></div><div class="col-xs-2" style="margin-left: 10%"><button data-id="' + val + '" type="button" class="btn btn-danger " onclick="deleteOrd(this);"><i class="fa fa-trash-o"></i></button></div>';
}

$('#btnAddOrdinance').click(function () {
    $('#modalAddOrdinance').modal({ backdrop: 'static', keyboard: false });

    $('.datepicker').datepicker({
        autoclose: true
    });

    $('.file').fileinput({
        'showUpload': false
    });
});

function deleteOrd(val) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var ordID = $(val).attr('data-id');

    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/DeleteOrdinance',

        data: '{id: "' + ordID + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            getOrdinance('');
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });
}

function editOrd(val) {
    $('#modalAddOrdinance').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var ordID = $(val).attr('data-id');
    //alert(ordID);
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetOrdinance',

        data: '{state: "' + state + '", city: "' + city + '", hist: "' + ordID + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;
                $('#ordID').text(records[0].id)
                $('#tblAddOrd input:eq(0)').val(records[0].ordinance_num);
                $('#tblAddOrd input:eq(1)').val(records[0].ordinance_name);
                $('#tblAddOrd textarea:eq(0)').val(records[0].description);
                $('#tblAddOrd input:eq(2)').val(records[0].section);
                $('#tblAddOrd input:eq(3)').val(records[0].source);
                $('#tblAddOrd input:eq(4)').val(records[0].enacted_date);
                $('#tblAddOrd input:eq(5)').val(records[0].revision_date);

                $('.file').fileinput({
                    'showUpload': false
                });
            }
        }, error: function (response) {
            console.log(response.responseText);
        }
    });


}

$('#btnApplyOrdinance').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var ordNum = $('#tblAddOrd tbody tr td input:eq(0)').val();
    var ordName = $('#tblAddOrd tbody tr td input:eq(1)').val();
    var desc = $('#tblAddOrd tbody tr td textarea:eq(0)').val();
    var sect = $('#tblAddOrd tbody tr td input:eq(2)').val();
    var src = $('#tblAddOrd tbody tr td input:eq(3)').val();
    var enDate = $('#tblAddOrd tbody tr td input:eq(4)').val();
    var revDate = $('#tblAddOrd tbody tr td input:eq(5)').val();
    var file = $('#tblAddOrd tbody tr td input:eq(6)').val();
    var ordId = $('#ordID').text();

    var myData = [];

    if (ordId == '') {
        myData = ['ordinance', state, city, ordNum, ordName, desc, sect, src, enDate, revDate, file];
    } else {
        myData = ['ordinance2', state, city, ordNum, ordName, desc, sect, src, enDate, revDate, file, ordId];
    }

    var user = $('#usr').text();

    $.ajax({
        type: 'POST',
        url: 'CompRulesApprover.aspx/SaveWorkable',
        data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);

            getOrdinance('');
            $('#modalAddOrdinance').modal('hide');
            $('#modalLoading').modal('hide');
        }, error: function (response) {
            console.log(response.responseText);
        }
    });

});

$('#btnHistory').click(function () {
    $('#modalOrdinanceHist').modal({ backdrop: 'static', keyboard: false });

    getOrdinance('hist');
});

function isHidden(el) {
    var bool = true;
    if ($(el).attr('style') == 'display: none;') {
        bool = false;
    }
    return bool;
}

$('#btnSaveRegPFC').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var pfc_city_notice = $('#slctPfcCityNotice').val();
    var pfc_default = $('#slctPfcDefault').val();
    var def_reg_timeline1 = $('#inDefRegTimeline1').val();
    var def_reg_timeline2 = $('#slctDefRegTimeline2').val();
    var def_reg_timeline3 = $('#slctDefRegTimeline3').val();
    var def_reg_timeline4 = $('#slctDefRegTimeline4').val();
    var pfc_def_foreclosure_timeline1 = $('#inPfcDefForclosureTimeline1').val();
    var pfc_def_foreclosure_timeline2 = $('#slctPfcDefForclosureTimeline2').val();
    var pfc_def_foreclosure_timeline3 = $('#slctPfcDefForclosureTimeline3').val();
    var pfc_def_foreclosure_timeline4 = $('#slctPfcDefForclosureTimeline4').val();
    var pfc_foreclosure_vacant = $('#slctPfcForeclosureVacant').val();
    var pfc_foreclosure_vacant_timeline1 = $('#inPfcForeclosureVacantTimeline1').val();
    var pfc_foreclosure_vacant_timeline2 = $('#slctPfcForeclosureVacantTimeline2').val();
    var pfc_foreclosure_vacant_timeline3 = $('#slctPfcForeclosureVacantTimeline3').val();
    var pfc_foreclosure_vacant_timeline4 = $('#slctPfcForeclosureVacantTimeline4').val();
    var pfc_boarded = $('#slctPfcBoarded').val();
    var pfc_foreclosure = $('#slctPfcForeclosure').val();
    var pfc_other_timeline1 = $('#inPfcOtherTimeline1').val();
    var pfc_other_timeline2 = $('#slctPfcOtherTimeline2').val();
    var pfc_other_timeline3 = $('#slctPfcOtherTimeline3').val();
    var pfc_other_timeline4 = $('#slctPfcOtherTimeline4').val();
    var pfc_vacant = $('#slctPfcVacantTimeline').val();
    var pfc_vacant_timeline1 = $('#inPfcVacantTimeline1').val();
    var pfc_vacant_timeline2 = $('#slctPfcVacantTimeline2').val();
    var pfc_vacant_timeline3 = $('#slctPfcVacantTimeline3').val();
    var pfc_vacant_timeline4 = $('#slctPfcVacantTimeline4').val();
    var pfc_code_violation = $('#slctPfcCodeViolation').val();
    var special_requirements = $('#slctSpcReq').val();
    var payment_type = $('#slctPaymentType').val();
    var type_of_registration = $('#slctTypeOfRegistration').val();
    var vms_renewal = $('#slctVmsRenewal').val();
    var upload_path = '';
    if ($('#tabPFC div.file-caption-name').attr('title') != undefined) {
        upload_path = $('#tabPFC div.file-caption-name').attr('title');
    }

    var myData = [
    	'regPFC',
    	state,
    	city,
        pfc_city_notice,
        pfc_default,
        def_reg_timeline1,
        def_reg_timeline2,
        def_reg_timeline3,
        def_reg_timeline4,
        pfc_def_foreclosure_timeline1,
        pfc_def_foreclosure_timeline2,
        pfc_def_foreclosure_timeline3,
        pfc_def_foreclosure_timeline4,
        pfc_foreclosure_vacant,
        pfc_foreclosure_vacant_timeline1,
        pfc_foreclosure_vacant_timeline2,
        pfc_foreclosure_vacant_timeline3,
        pfc_foreclosure_vacant_timeline4,
        pfc_boarded,
        pfc_foreclosure,
        pfc_other_timeline1,
        pfc_other_timeline2,
        pfc_other_timeline3,
        pfc_other_timeline4,
        pfc_vacant,
        pfc_vacant_timeline1,
        pfc_vacant_timeline2,
        pfc_vacant_timeline3,
        pfc_vacant_timeline4,
        pfc_code_violation,
        special_requirements,
        payment_type,
        type_of_registration,
        vms_renewal,
        upload_path
    ];

    var btns = $('#tabPFC .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabPFC').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabPFC div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabPFC div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegREO').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var reo_bank_owed = $('#slctReoBankOwed').val()
    var reo_bank_owed_timeline1 = $('#inReoBankOwed1').val()
    var reo_bank_owed_timeline2 = $('#slctReoBankOwed2').val()
    var reo_bank_owed_timeline3 = $('#slctReoBankOwed3').val()
    var reo_bank_owed_timeline4 = $('#slctReoBankOwed4').val()
    var reo_boarded_only = $('#slctReoBoardedOnly').val()
    var reo_city_notice = $('#slctReoCityNotice').val()
    var reo_code_violation = $('#slctReoCodeViolation').val()
    var reo_distressed_abandoned = $('#slctReoDistressedAbandoned').val()
    var reo_distressed_abandoned1 = $('#inReoDistressedAbandoned1').val()
    var reo_distressed_abandoned2 = $('#slctReoDistressedAbandoned2').val()
    var reo_distressed_abandoned3 = $('#slctReoDistressedAbandoned3').val()
    var reo_distressed_abandoned4 = $('#slctReoDistressedAbandoned4').val()
    var reo_other_timeline1 = $('#inReoOtherTimeline1').val()
    var reo_other_timeline2 = $('#slctReoOtherTimeline2').val()
    var reo_other_timeline3 = $('#slctReoOtherTimeline3').val()
    var reo_other_timeline4 = $('#slctReoOtherTimeline4').val()
    var reo_vacant = $('#slctReoVacant').val()
    var reo_vacant_timeline1 = $('#inReoVacantTimeline1').val()
    var reo_vacant_timeline2 = $('#slctReoVacantTimeline2').val()
    var reo_vacant_timeline3 = $('#slctReoVacantTimeline3').val()
    var reo_vacant_timeline4 = $('#slctReoVacantTimeline4').val()
    var rental_registration = $('#slctRentalRegistration').val()
    var rental_form = $('#slctRentalForm').val()

    var myData = [
    	'regREO',
    	state,
    	city,
        reo_bank_owed,
        reo_bank_owed_timeline1,
        reo_bank_owed_timeline2,
        reo_bank_owed_timeline3,
        reo_bank_owed_timeline4,
        reo_boarded_only,
        reo_city_notice,
        reo_code_violation,
        reo_distressed_abandoned,
        reo_distressed_abandoned1,
        reo_distressed_abandoned2,
        reo_distressed_abandoned3,
        reo_distressed_abandoned4,
        reo_other_timeline1,
        reo_other_timeline2,
        reo_other_timeline3,
        reo_other_timeline4,
        reo_vacant,
        reo_vacant_timeline1,
        reo_vacant_timeline2,
        reo_vacant_timeline3,
        reo_vacant_timeline4,
        rental_registration,
        rental_form
    ];

    var btns = $('#tabREO .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabREO').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabREO div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabREO div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegProperty').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var residential = $('#tabPropType select:eq(0)').val();
    var rental = $('#tabPropType select:eq(1)').val();
    var commercial = $('#tabPropType select:eq(2)').val();
    var condo = $('#tabPropType select:eq(3)').val();
    var townhome = $('#tabPropType select:eq(4)').val();
    var vacant_lot = $('#tabPropType select:eq(5)').val();
    var mobile_home = $('#tabPropType select:eq(6)').val();

    var myData = [
		'regProperty',
		state,
		city,
        residential,
		rental,
		commercial,
		condo,
		townhome,
		vacant_lot,
		mobile_home
    ];

    var btns = $('#tabPropType .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabPropType').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabPropType div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabPropType div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegPropReq').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var addn_info = "";

    $.each($('#slctAddnInfo').val(), function (idx, val) {
        addn_info += val + ',';
    });

    addn_info = addn_info.slice(0, -1);

    var presale_definition = $('#slctPresaleDefinition').val();
    var first_time_vacancy_date = $('#slctFirstTimeVacancyDate').val();
    var secured_required = $('#slctSecuredRequired').val();
    var local_contact_required = $('#slctLocalContactRequired').val();
    var additional_signage_required = $('#slctAddSignReq').val();
    var pictures_required = $('#slctPicReq').val();
    var gse_exclusion = $('#slctGseExclusion').val();
    var mobile_vin_number_required = $('#slctMobileVINReq').val();
    var insurance_required = $('#slctInsuranceReq').val();
    var parcel_number_required = $('#slctParcelReq').val();
    var foreclosure_action_information_needed = $('#slctForeclosureActInfo').val();
    var legal_description_required = $('#slctLegalDescReq').val();
    var foreclosure_case_information_needed = $('#slctForeclosureCaseInfo').val();
    var block_and_lot_number_required = $('#slctBlockLotReq').val();
    var foreclosure_deed_required = $('#slctForeclosureDeedReq').val();
    var attorney_information_required = $('#slctAttyInfoReq').val();
    var bond_required = $('#slctBondReq').val();
    var broker_information_required_if_reo = $('#slctBrkInfoReq').val();
    var bond_amount = $('#inBondAmount').val();
    var mortgage_contact_name_required = $('#slctMortContactNameReq').val();
    var maintenance_plan_required = $('#slctMaintePlanReq').val();
    var client_tax_number_required = $('#slctClientTaxReq').val();
    var no_trespass_form_required = $('#slctNoTrespassReq').val();
    var signature_required = $('#slctSignReq').val();
    var utility_information_required = $('#slctUtilityInfoReq').val();
    var notarization_required = $('#slctNotarizationReq').val();
    var winterization_required = $('#slctWinterReq').val();
    var recent_inspection_date = $('#slctRecInsDate').val();

    var lcicompany_name = $('#lciCompany').val();
    var lcifirst_name = $('#lciFirstName').val();
    var lcilast_name = $('#lciLastName').val();
    var lcititle = $('#lciTitle').val();
    var lcibusiness_license_num = $('#lciBusinessLicenseNum').val();
    var lciphone_num1 = $('#lciPhoneNum1').val();
    var lciphone_num2 = $('#lciPhoneNum2').val();
    var lcibusiness_phone_num1 = $('#lciBusinessPhoneNum1').val();
    var lcibusiness_phone_num2 = $('#lciBusinessPhoneNum2').val();
    var lciemergency_phone_num1 = $('#lciEmrPhone1').val();
    var lciemergency_phone_num2 = $('#lciEmrPhone2').val();
    var lcifax_num1 = $('#lciFaxNum1').val();
    var lcifax_num2 = $('#lciFaxNum2').val();
    var lcicell_num1 = $('#lciCellNum1').val();
    var lcicell_num2 = $('#lciCellNum2').val();
    var lciemail = $('#lciEmail').val();
    var lcistreet = $('#lciStreet').val();
    var lcistate = $('#lciState').val();
    var lcicity = $('#lciCity').val();
    var lcizip = $('#lciZip').val();
    var lcihours_from = $('#lciHrsFrom').val();
    var lcihours_to = $('#lciHrsTo').val();

    var myData = [
		'regPropReq',
		state,
		city,
        addn_info,
        presale_definition,
        first_time_vacancy_date,
        secured_required,
        local_contact_required,
        additional_signage_required,
        pictures_required,
        gse_exclusion,
        mobile_vin_number_required,
        insurance_required,
        parcel_number_required,
        foreclosure_action_information_needed,
        legal_description_required,
        foreclosure_case_information_needed,
        block_and_lot_number_required,
        foreclosure_deed_required,
        attorney_information_required,
        bond_required,
        broker_information_required_if_reo,
        bond_amount,
        mortgage_contact_name_required,
        maintenance_plan_required,
        client_tax_number_required,
        no_trespass_form_required,
        signature_required,
        utility_information_required,
        notarization_required,
        winterization_required,
        recent_inspection_date,
        lcicompany_name,
        lcifirst_name,
        lcilast_name,
        lcititle,
        lcibusiness_license_num,
        lciphone_num1,
        lciphone_num2,
        lcibusiness_phone_num1,
        lcibusiness_phone_num2,
        lciemergency_phone_num1,
        lciemergency_phone_num2,
        lcifax_num1,
        lcifax_num2,
        lcicell_num1,
        lcicell_num2,
        lciemail,
        lcistreet,
        lcistate,
        lcicity,
        lcizip,
        lcihours_from,
        lcihours_to
    ];

    var btns = $('#tabPropReq .fa-check').closest('button');
    var lcibtns = $('#lci .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    $.each(lcibtns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabPropReq').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabPropReq div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabPropReq div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegCost').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var reg_cost = $('#slctRegCost').val();
    var reg_cost_amt = $('#inRegCost').val();
    var reg_cost_curr = $('#slctRegCostCurr').val();
    var reg_cost_standard = $('#slctRegCostStandard').val();
    var is_renewal_cost_escal1 = $('#slctRenewCostEscal').val();
    var renewal_fee_amt = $('#inRenewAmt').val();
    var renewal_fee_curr = $('#slctRenewAmtCurr1').val();
    var reg_escal_service_type = $('#inComRegFee').val();
    var reg_escal_amount = $('#slctComRegFee').val();
    var reg_escal_curr = $('#slctComFeeStandard').val();
    var reg_escal_succeeding = $('#slctRenewCostEscal2').val();
    var com_reg_fee = $('#inRenewAmt2').val();
    var com_reg_curr = $('#slctRenewAmtCurr2').val();
    var com_fee_standard = $('#slctRegCostStandard2').val();

    var regEscalServType = "";
    var regEscalAmt = "";
    var regEscalCurr = "";
    var regEscalSucceeding = "";
    $.each($('#divEscalRenewal div.escalInput div.col-xs-12'), function (idx, val) {
        regEscalServType += $(this).find('div:eq(0) label').text() + ',';
        regEscalAmt += $(this).find('div:eq(1) input').val() + ',';
        regEscalCurr += $(this).find('div:eq(2) select').val() + ',';

        if ($(this).find('div:eq(3)').length != 0) {
            if ($(this).find('div:eq(3) input').is(':checked')) {
                regEscalSucceeding += 'true' + ',';
            } else {
                regEscalSucceeding += 'false' + ',';
            }
        }
    });

    regEscalServType = regEscalServType.slice(0, -1);
    regEscalAmt = regEscalAmt.slice(0, -1);
    regEscalCurr = regEscalCurr.slice(0, -1);
    regEscalSucceeding = regEscalSucceeding.slice(0, -1);

    var comEscalServType = "";
    var comEscalAmt = "";
    var comEscalCurr = "";
    var comEscalSucceeding = "";
    $.each($('#divEscalRenewal2 div.escalInput div.col-xs-12'), function (idx, val) {
        comEscalServType += $(this).find('div:eq(0) label').text() + ',';
        comEscalAmt += $(this).find('div:eq(1) input').val() + ',';
        comEscalCurr += $(this).find('div:eq(2) select').val() + ',';

        if ($(this).find('div:eq(3)').length != 0) {
            if ($(this).find('div:eq(3) input').is(':checked')) {
                comEscalSucceeding += 'true' + ',';
            } else {
                comEscalSucceeding += 'false' + ',';
            }
        }
    });

    comEscalServType = comEscalServType.slice(0, -1)
    comEscalAmt = comEscalAmt.slice(0, -1)
    comEscalCurr = comEscalCurr.slice(0, -1)
    comEscalSucceeding = comEscalSucceeding.slice(0, -1);

    var myData = [
		'regCost',
		state,
		city,
        reg_cost,
        reg_cost_amt,
        reg_cost_curr,
        reg_cost_standard,
        is_renewal_cost_escal1,
        renewal_fee_amt,
        renewal_fee_curr,
        reg_escal_service_type,
        reg_escal_amount,
        reg_escal_curr,
        reg_escal_succeeding,
        com_reg_fee,
        com_reg_curr,
        com_fee_standard,
        regEscalServType,
        regEscalAmt,
        regEscalCurr,
        regEscalSucceeding,
        comEscalServType,
        comEscalAmt,
        comEscalCurr,
        comEscalSucceeding
    ];

    var btns = $('#tabCost .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabCost').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabCost div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabCost div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveRegCont').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var cont_reg = $('#slctContReg').val();

    var myData = [
        'regCont',
        state,
        city,
        cont_reg
    ];

    var btns = $('#tabContReg .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabContReg').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabContReg div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabContReg div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveInspection').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var inspection_update_req = $('#slctInsUpReq').val();
    var inspection_criteria_occ = $('#slctInsCriOcc').val();
    var inspection_criteria_vac = $('#slctInsCriVac').val();
    var inspection_fee_payment_freq = $('#slctInsFeePay').val();
    var inspection_fee_req = $('#slctInsFeeReq').val();
    var inspection_fee_amount = $('#inInsFeeAmt').val();
    var inspection_fee_curr = $('#slctInsFeeAmt').val();
    var inspection_reporting_freq = $('#slctInsRepFreq').val();

    //-------------------------------------------------------------------------------------

    var criteria_occ_freq = "";
    var criteria_occ_cycle_1 = "";
    var criteria_occ_cycle_2 = "";
    var criteria_occ_cycle_refresh_time = "";

    var criteria_occ_cycle_day_1 = "";

    var criteria_occ_cycle_day_2 = "";
    var criteria_occ_cycle_week_1 = "";
    var criteria_occ_cycle_week_2 = "";
    var criteria_occ_cycle_month_1 = "";
    var criteria_occ_cycle_month_2 = "";
    var criteria_occ_cycle_month_3 = "";

    var criteria_occ_cycle_year_1 = "";

    var criteria_occ_cycle_year_2 = "";

    if ($('#slctInsCriOcc').val() != 'false') {

        criteria_occ_freq = $('input[name=rdInsCriteriaOccFreq]:checked').val();
        criteria_occ_cycle_1 = $('#slctInsCriteriaOccCycle1').val();
        criteria_occ_cycle_2 = $('#slctInsCriteriaOccCycle2').val();
        criteria_occ_cycle_refresh_time = $('#inInsCriteriaOccTime').val();

        if ($('input[name=cbFreq]:checked').length > 0) {
            $.each($('input[name=cbFreq]:checked'), function (idx, val) {
                criteria_occ_cycle_day_1 += $(this).val() + ',';
            });

            criteria_occ_cycle_day_1 = criteria_occ_cycle_day_1.slice(0, -1);
        }

        criteria_occ_cycle_day_2 = $('#inInsCriteriaOccDay').val();
        criteria_occ_cycle_week_1 = $('#inInsCriteriaOccWeek').val();
        criteria_occ_cycle_week_2 = $('#slctInsCriteriaOccWeek').val();
        criteria_occ_cycle_month_1 = $('#inInsCriteriaOccMonth').val();
        criteria_occ_cycle_month_2 = $('#slctInsCriteriaMonth1').val();
        criteria_occ_cycle_month_3 = $('#slctInsCriteriaMonth2').val();

        if ($('input[name=rdFreqYear1]:checked').val() != 'on') {
            criteria_occ_cycle_year_1 = 'true';
        } else {
            criteria_occ_cycle_year_1 = 'false';
        }

        criteria_occ_cycle_year_2 = $('#inInsCriteriaOccYear').val();
    }

    //-------------------------------------------------------------------------------------

    var criteria_vac_freq = "";
    var criteria_vac_cycle_1 = "";
    var criteria_vac_cycle_2 = "";
    var criteria_vac_cycle_refresh_time = "";

    var criteria_vac_cycle_day_1 = "";

    var criteria_vac_cycle_day_2 = "";
    var criteria_vac_cycle_week_1 = "";
    var criteria_vac_cycle_week_2 = "";
    var criteria_vac_cycle_month_1 = "";
    var criteria_vac_cycle_month_2 = "";
    var criteria_vac_cycle_month_3 = "";

    var criteria_vac_cycle_year_1 = "";

    var criteria_vac_cycle_year_2 = "";

    if ($('#slctInsCriVac').val() != 'false') {

        criteria_vac_freq = $('input[name=rdInsCriteriaVacFreq]:checked').val();
        criteria_vac_cycle_1 = $('#slctInsCriteriaVacCycle1').val();
        criteria_vac_cycle_2 = $('#slctInsCriteriaVacCycle2').val();
        criteria_vac_cycle_refresh_time = $('#inInsCriteriaVacTime').val();

        if ($('input[name=cbFreqVac]:checked').length > 0) {
            $.each($('input[name=cbFreqVac]:checked'), function (idx, val) {
                criteria_vac_cycle_day_1 += $(this).val() + ',';
            });

            criteria_vac_cycle_day_1 = criteria_vac_cycle_day_1.slice(0, -1);
        }

        criteria_vac_cycle_day_2 = $('#inInsCriteriaVacDay').val();
        criteria_vac_cycle_week_1 = $('#inInsCriteriaVacWeek').val();
        criteria_vac_cycle_week_2 = $('#slctInsCriteriaVacWeek').val();
        criteria_vac_cycle_month_1 = $('#inInsCriteriaVacMonth').val();
        criteria_vac_cycle_month_2 = $('#slctInsCriteriaVacMonth1').val();
        criteria_vac_cycle_month_3 = $('#slctInsCriteriaVacMonth2').val();

        if ($('input[name=rdFreqVacYear1]:checked').val() != 'on') {
            criteria_vac_cycle_year_1 = 'true';
        } else {
            criteria_vac_cycle_year_1 = 'false';
        }

        criteria_vac_cycle_year_2 = $('#inInsCriteriaVacYear').val();

    }

    //-------------------------------------------------------------------------------------

    var fee_payment_freq = "";
    var fee_payment_cycle_1 = "";
    var fee_payment_cycle_2 = "";
    var fee_payment_cycle_refresh_time = "";

    var fee_payment_cycle_day_1 = "";

    var fee_payment_cycle_day_2 = "";
    var fee_payment_cycle_week_1 = "";
    var fee_payment_cycle_week_2 = "";
    var fee_payment_cycle_month_1 = "";
    var fee_payment_cycle_month_2 = "";
    var fee_payment_cycle_month_3 = "";

    var fee_payment_cycle_year_1 = "";

    var fee_payment_cycle_year_2 = "";

    if ($('#slctInsFeePay').val() != 'false') {

        fee_payment_freq = $('input[name=rdInsFeePayFreq]:checked').val();
        fee_payment_cycle_1 = $('#slctFeePayCycle1').val();
        fee_payment_cycle_2 = $('#slctFeePayCycle2').val();
        fee_payment_cycle_refresh_time = $('#inInsCriteriaOccTime').val();

        if ($('input[name=cbFreqFeePay]:checked').length > 0) {
            $.each($('input[name=cbFreqFeePay]:checked'), function (idx, val) {
                fee_payment_cycle_day_1 += $(this).val() + ',';
            });

            fee_payment_cycle_day_1 = fee_payment_cycle_day_1.slice(0, -1);
        }

        fee_payment_cycle_day_2 = $('#inFeePayFreqDay').val();
        fee_payment_cycle_week_1 = $('#inFeePayFreqWeek').val();
        fee_payment_cycle_week_2 = $('#slctFeePayFreqWeek').val();
        fee_payment_cycle_month_1 = $('#inFeePayFreqMonth').val();
        fee_payment_cycle_month_2 = $('#slctFeePayFreqMonth1').val();
        fee_payment_cycle_month_3 = $('#slctFeePayFreqMonth2').val();

        if ($('input[name=rdFreqFeePayYear1]:checked').val() != 'on') {
            fee_payment_cycle_year_1 = 'true';
        } else {
            fee_payment_cycle_year_1 = 'false';
        }

        fee_payment_cycle_year_2 = $('#inFeePayFreqYear').val();

    }

    //-------------------------------------------------------------------------------------

    var myData = [
		'inspect',
		state,
		city,
		inspection_update_req,
        inspection_criteria_occ,
        inspection_criteria_vac,
        inspection_fee_payment_freq,
        inspection_fee_req,
        inspection_fee_amount,
        inspection_fee_curr,
        inspection_reporting_freq,
        criteria_occ_freq,
        criteria_occ_cycle_1,
        criteria_occ_cycle_2,
        criteria_occ_cycle_refresh_time,
        criteria_occ_cycle_day_1,
        criteria_occ_cycle_day_2,
        criteria_occ_cycle_week_1,
        criteria_occ_cycle_week_2,
        criteria_occ_cycle_month_1,
        criteria_occ_cycle_month_2,
        criteria_occ_cycle_month_3,
        criteria_occ_cycle_year_1,
        criteria_occ_cycle_year_2,
        criteria_vac_freq,
        criteria_vac_cycle_1,
        criteria_vac_cycle_2,
        criteria_vac_cycle_refresh_time,
        criteria_vac_cycle_day_1,
        criteria_vac_cycle_day_2,
        criteria_vac_cycle_week_1,
        criteria_vac_cycle_week_2,
        criteria_vac_cycle_month_1,
        criteria_vac_cycle_month_2,
        criteria_vac_cycle_month_3,
        criteria_vac_cycle_year_1,
        criteria_vac_cycle_year_2,
        fee_payment_freq,
        fee_payment_cycle_1,
        fee_payment_cycle_2,
        fee_payment_cycle_refresh_time,
        fee_payment_cycle_day_1,
        fee_payment_cycle_day_2,
        fee_payment_cycle_week_1,
        fee_payment_cycle_week_2,
        fee_payment_cycle_month_1,
        fee_payment_cycle_month_2,
        fee_payment_cycle_month_3,
        fee_payment_cycle_year_1,
        fee_payment_cycle_year_2
    ];

    var btns = $('.fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabInspection').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabInspection div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabInspection div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveMunicipalInfo').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var municipality_department = $('#inMuniDept').val();
    var municipality_phone_num = $('#inMuniPhone').val();
    var municipality_email = $('#inMuniEmail').val();
    var municipality_st = $('#inMuniMailStr').val();
    var municipality_city = $('#inMuniMailCt').val();
    var municipality_state = $('#inMuniMailSta').val();
    var municipality_zip = $('#inMuniMailZip').val();
    var contact_person = $('#inContact').val();
    var title = $('#inTitle').val();
    var department = $('#inDept').val();
    var phone_num = $('#inPhone').val();
    var email = $('#inEmail').val();
    var address = $('#inMailing').val();
    var ops_hrs_from = $('#inHrsFrom').val();
    var ops_hrs_to = $('#inHrsTo').val();

    var myData = [
		'municipalityInfo',
		state,
		city,
		municipality_department,
        municipality_phone_num,
        municipality_email,
        municipality_st,
        municipality_city,
        municipality_state,
        municipality_zip,
        contact_person,
        title,
        department,
        phone_num,
        email,
        address,
        ops_hrs_from,
        ops_hrs_to
    ];

    var btns = $('.fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabMunicipality').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabMunicipality div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabMunicipality div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveDeregistration').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var dereg_req = $('#slctDeregRequired').val();
    var conveyed = $('#slctConveyed').val();
    var occupied = $('#slctOccupied').val();
    var how_to_dereg = $('#slctHowToDereg').val();
    var upload_file = $('#inUploadPathD').val();
    var new_owner_info_req = $('#slctNewOwnerInfoReq').val();
    var proof_of_conveyance_req = $('#slctProofOfConveyReq').val();
    var date_of_sale_req = $('#slctDateOfSaleReq').val();

    var myData = [
		'dereg',
		state,
		city,
		dereg_req,
        conveyed,
        occupied,
        how_to_dereg,
        upload_file,
        new_owner_info_req,
        proof_of_conveyance_req,
        date_of_sale_req
    ];

    var btns = $('#tabDeregistration .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',

            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                console.log(data);

                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabDeregistration').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabDeregistration div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabDeregistration div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('#btnSaveZip').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();

    var myData;

    $.each($('#tblZip tbody tr'), function () {
        var zip = $(this).find('td:eq(1)').text();
        var isActive;

        if ($('#tblZip tbody tr td:eq(0) div').hasClass('off')) {
            isActive = '0';
        } else {
            isActive = '1';
        }

        myData = [
			'zip',
			state,
			city,
			zip,
			isActive
        ];

        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!');
                } else {
                    alertify.alert('Error Saving!');
                }
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    });
    $('#modalLoading').modal('hide');
});

$('#btnSaveNotif').click(function () {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    var state = $('#tblState tbody tr.activeRow td').html();
    var city = $('#tblCity tbody tr.activeRow td').html();
    var reg_send_reminder_num = $('#inRegSendRem').val();
    var reg_send_reminder_type = $('#slctRegSendRem1').val();
    var reg_send_reminder_days = $('#slctRegSendRem2').val();
    var reg_send_reminder_before = $('#slctRegSendRem3').val();
    var reg_send_reminder_time_frame = $('#slctRegSendRem4').val();
    var ren_send_reminder_num = $('#inRenewSendRem').val();
    var ren_send_reminder_type = $('#slctRenewSendRem1').val();
    var ren_send_reminder_days = $('#slctRenewSendRem2').val();
    var ren_send_reminder_before = $('#slctRenewSendRem3').val();
    var ren_send_reminder_time_frame = $('#slctRenewSendRem4').val();
    var web_notif_url = $('#inWebSendRem').val();

    var myData = [
		'notif',
		state,
		city,
		reg_send_reminder_num,
        reg_send_reminder_type,
        reg_send_reminder_days,
        reg_send_reminder_before,
        reg_send_reminder_time_frame,
        ren_send_reminder_num,
        ren_send_reminder_type,
        ren_send_reminder_days,
        ren_send_reminder_before,
        ren_send_reminder_time_frame,
        web_notif_url
    ];

    var btns = $('#tabNotification .fa-check').closest('button');
    var count = 0;
    $.each(btns, function (idx, val) {
        if (isHidden($(this))) {
            count += 1;
        }
    });

    if (count == 0) {
        var user = $('#usr').text();

        $.ajax({
            type: 'POST',
            url: 'CompRulesAudit1.aspx/SaveWorkable',
            data: '{data: ' + JSON.stringify(myData) + ', user: "' + user + '"}',
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                if (data.d == '1') {
                    alertify.alert('Record saved successfuly!', function () {
                        $('#tabNotification').prepend(
                            '<div class="col-xs-12 text-center valid" style="margin-top: 1%;">' +
                                '<label class="text-green">VALIDATED!</label>' +
                            '</div>'
                        );

                        $('#tabNotification div.col-xs-12').last().find('button').prop('disabled', true);
                        $('#tabNotification div.col-xs-12:not(:last-child) .fa-times').closest('button').hide();
                    });
                } else {
                    alertify.alert('Error Saving!');
                }

                $('#modalLoading').modal('hide');
            }, error: function (response) {
                console.log(response.responseText);
            }
        });
    } else {
        alertify.alert('Please check all fields before saving!');
        $('#modalLoading').modal('hide');
    }
});

$('.btnCancel').click(function () {
    $('#divSettings input, #divSettings select').val('');
    $('#divSettings').hide();
    $('#tblCity tbody').empty();
    $.each($('#tblState tbody tr'), function () {
        $(this).removeClass('activeRow');
    });
});

$('#slctPfcDefault').change(function () {
    if ($(this).val() == 'false') {
        $('#inDefRegTimeline1').prop('disabled', true);
        $('#slctDefRegTimeline2').prop('disabled', true);
        $('#slctDefRegTimeline3').prop('disabled', true);
        $('#slctDefRegTimeline4').prop('disabled', true);
    } else {
        $('#inDefRegTimeline1').prop('disabled', false);
        $('#slctDefRegTimeline2').prop('disabled', false);
        $('#slctDefRegTimeline3').prop('disabled', false);
        $('#slctDefRegTimeline4').prop('disabled', false);
    }
});

$('#slctPfcForeclosureVacant').change(function () {
    if ($(this).val() == 'false') {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', true);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', true);
    } else {
        $('#inPfcForeclosureVacantTimeline1').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline2').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline3').prop('disabled', false);
        $('#slctPfcForeclosureVacantTimeline4').prop('disabled', false);
    }
});

$('#slctPfcVacantTimeline').change(function () {
    if ($(this).val() == 'false') {
        $('#inPfcVacantTimeline1').prop('disabled', true);
        $('#slctPfcVacantTimeline2').prop('disabled', true);
        $('#slctPfcVacantTimeline3').prop('disabled', true);
        $('#slctPfcVacantTimeline4').prop('disabled', true);
    } else {
        $('#inPfcVacantTimeline1').prop('disabled', false);
        $('#slctPfcVacantTimeline2').prop('disabled', false);
        $('#slctPfcVacantTimeline3').prop('disabled', false);
        $('#slctPfcVacantTimeline4').prop('disabled', false);
    }
});

$('#slctReoBankOwed').change(function () {
    if ($(this).val() == 'false') {
        $('#inReoBankOwed1').prop('disabled', true);
        $('#slctReoBankOwed2').prop('disabled', true);
        $('#slctReoBankOwed3').prop('disabled', true);
        $('#slctReoBankOwed4').prop('disabled', true);
    } else {
        $('#inReoBankOwed1').prop('disabled', false);
        $('#slctReoBankOwed2').prop('disabled', false);
        $('#slctReoBankOwed3').prop('disabled', false);
        $('#slctReoBankOwed4').prop('disabled', false);
    }
});

$('#slctReoDistressedAbandoned').change(function () {
    if ($(this).val() == 'false') {
        $('#inReoDistressedAbandoned1').prop('disabled', true);
        $('#slctReoDistressedAbandoned2').prop('disabled', true);
        $('#slctReoDistressedAbandoned3').prop('disabled', true);
        $('#slctReoDistressedAbandoned4').prop('disabled', true);
    } else {
        $('#inReoDistressedAbandoned1').prop('disabled', false);
        $('#slctReoDistressedAbandoned2').prop('disabled', false);
        $('#slctReoDistressedAbandoned3').prop('disabled', false);
        $('#slctReoDistressedAbandoned4').prop('disabled', false);
    }
});

$('#slctReoVacant').change(function () {
    if ($(this).val() == 'false') {
        $('#inReoVacantTimeline1').prop('disabled', true);
        $('#slctReoVacantTimeline2').prop('disabled', true);
        $('#slctReoVacantTimeline3').prop('disabled', true);
        $('#slctReoVacantTimeline4').prop('disabled', true);
    } else {
        $('#inReoVacantTimeline1').prop('disabled', false);
        $('#slctReoVacantTimeline2').prop('disabled', false);
        $('#slctReoVacantTimeline3').prop('disabled', false);
        $('#slctReoVacantTimeline4').prop('disabled', false);
    }
});

$('#slctBondReq').change(function () {
    if ($(this).val() == 'false') {
        $('#inBondAmount').prop('disabled', true);
    } else {
        $('#inBondAmount').prop('disabled', false);
    }
});

$('#slctRegCost').change(function () {
    if ($(this).val() == 'false') {
        $('#tabCost div.col-xs-12:lt(27) input').prop('disabled', true);
        $('#tabCost div.col-xs-12:lt(27) select').prop('disabled', true);
        $('#tabCost div.col-xs-12:lt(27) button').prop('disabled', true);
    } else {
        $('#tabCost div.col-xs-12:lt(27) input').prop('disabled', false);
        $('#tabCost div.col-xs-12:lt(27) select').prop('disabled', false);
        $('#tabCost div.col-xs-12:lt(27) button').prop('disabled', false);
    }

    $(this).prop('disabled', false);
});

$('#inRegCost').keyup(function () {
    if ($('#slctRegCostStandard').val() == 'true') {
        $('#inRenewAmt').val($(this).val());
    }
});

$('#slctRegCostStandard').change(function () {
    if ($(this).val() == 'true') {
        $('#inRenewAmt').val($('#inRegCost').val());
        $('#slctRenewAmtCurr1').val($('#slctRegCostCurr').val());
        $('#slctRenewCostEscal').prop('disabled', true);
    } else {
        $('#slctRenewCostEscal').prop('disabled', false);
    }
});

$('#slctRenewCostEscal').change(function () {
    if ($(this).val() == 'false') {
        $('#inRenewAmt').prop('disabled', false);
        $('#slctRenewAmtCurr1').prop('disabled', false);
        $('#btnViewEscalRenewAmt').prop('disabled', true);
    } else {
        $('#inRenewAmt').prop('disabled', true);
        $('#slctRenewAmtCurr1').prop('disabled', true);
        $('#inRenewAmt').val('');
        $('#btnViewEscalRenewAmt').prop('disabled', false);
    }
});

$('#btnAddNew1').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);
        
        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#inComRegFee').keyup(function () {
    if ($('#slctComFeeStandard').val() == 'true') {
        $('#inRenewAmt2').val($(this).val());
    }
});

$('#slctComFeeStandard').change(function () {
    if ($(this).val() == 'true') {
        $('#inRenewAmt2').val($('#inComRegFee').val());
        $('#slctRenewAmtCurr2').val($('#slctComRegFee').val());
        $('#slctRenewCostEscal2').prop('disabled', true);
    } else {
        $('#slctRenewCostEscal2').prop('disabled', false);
    }
});

$('#slctRenewCostEscal2').change(function () {
    if ($(this).val() == 'false') {
        $('#inRenewAmt2').prop('disabled', false);
        $('#slctRenewAmtCurr2').prop('disabled', false);
        $('#btnViewEscalRenewAmt2').prop('disabled', true);
    } else {
        $('#inRenewAmt2').prop('disabled', true);
        $('#slctRenewAmtCurr2').prop('disabled', true);
        $('#inRenewAmt2').val('');
        $('#btnViewEscalRenewAmt2').prop('disabled', false);
    }
});

$('#btnAddNew2').click(function () {
    var escalDiv = $(this).closest('div.collapse').find('.escalInput');
    var lastDiv = escalDiv.find('div.col-xs-12').last();
    var count = escalDiv.find('div.col-xs-12').length;

    if (count < 5) {
        lastDiv.clone().appendTo(escalDiv);
        
        if (count == 1) {
            lastDiv.next().find('div:eq(0) label').text('2nd Renewal');
        } else if (count == 2) {
            lastDiv.next().find('div:eq(0) label').text('3rd Renewal');
        } else {
            lastDiv.next().find('div:eq(0) label').text((count + 1) + 'th Renewal');
        }

        if (count <= 1) {
            lastDiv.next().append('<div class="col-xs-5"><input type="checkbox" />&nbsp;Check if succeeding renewals use this amount</div>');
        }
    }
});

$('#slctInsCriOcc').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsCriteriaOcc').prop('disabled', false);
    } else {
        $('#btnViewInsCriteriaOcc').prop('disabled', true);
    }
});

$('#slctInsCriVac').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsCriteriaVac').prop('disabled', false);
    } else {
        $('#btnViewInsCriteriaVac').prop('disabled', true);
    }
});

$('#slctInsFeePay').change(function () {
    if ($(this).val() == 'true') {
        $('#btnViewInsFeePaymentFreq').prop('disabled', false);
    } else {
        $('#btnViewInsFeePaymentFreq').prop('disabled', true);
    }
});

$('#slctInsFeeReq').change(function () {
    if ($(this).val() == 'true') {
        $('#inInsFeeAmt').prop('disabled', false);
        $('#slctInsFeeAmt').prop('disabled', false);
    } else {
        $('#inInsFeeAmt').val('');
        $('#inInsFeeAmt').prop('disabled', true);
        $('#slctInsFeeAmt').prop('disabled', true);
    }
});

$('#slctTypeOfRegistration').change(function () {
    if ($(this).val() == 'pdf') {
        $(this).closest('div').next().show();
    } else {
        $(this).closest('div').next().hide();
    }
});

$('#slctHowToDereg').change(function () {
    if ($(this).val() == 'pdf') {
        $(this).closest('div').next().show();
    } else {
        $(this).closest('div').next().hide();
    }
});

$('#navReg li button').click(function () {
    $.each($('#navReg li'), function () {
        if ($(this).find('button').hasClass('btn-primary')) {
            $(this).find('button').removeClass('btn-primary');
            $(this).find('button').addClass('btn-default');
        }
    });

    $(this).addClass('btn-primary');
    $(this).focus();
});

$('.file').fileinput({
    'showUpload': false
});

function cellStyle(value, row, index, field) {
    return {
        classes: 'text-justify'
    };
}

$('.criteriaEdit').click(function () {
    $('#modalEdit').modal({ backdrop: 'static', keyboard: false });

    $(".timepicker").timepicker({
        showInputs: false
    });

    var fieldText = $(this).parent().parent().find('div:eq(0)').text();
    var btnData = $(this).attr('data-id');
    $('#lblEdit').text('Enter ' + fieldText + ' ');

    $.each($('#modalEdit div.panel-body'), function () {
        $(this).css('display', 'none');
    });

    $('#' + btnData).removeAttr('style');

    getLCIState();
    getLCICity('');
    getLCIZip('');


    //$.when(getLCICity('')).then();

    //$('#lciState').change(function () {
    //    var state = $(this).val();

    //    getLCICity(state);
    //});

    //$('#lciCity').change(function () {
    //    var city = $(this).val();

    //    getLCIZip(city);
    //});
});

function getLCIState() {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetState',

        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var states = d.data.record;

                $('#lciState').empty();
                $.each(states, function (idx, val) {
                    $('#lciState').append('<option value="' + val.State + '">' + val.State + '</option>');
                });

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCICity(state) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetCity',
        data: '{state: "' + state + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var cities = d.data.record;

                $('#lciCity').empty();
                $.each(cities, function (idx, val) {
                    $('#lciCity').append('<option value="' + val.City + '">' + val.City + '</option>');
                });

                $('#modalLoading').modal('hide');
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function getLCIZip(city) {
    $('#modalLoading').modal({ backdrop: 'static', keyboard: false });
    $.ajax({
        type: 'POST',
        url: 'CompRulesAudit1.aspx/GetZip',

        data: '{city: "' + city + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var zips = d.data.record;

                $('#lciZip').empty();
                $.each(zips, function (idx, val) {
                    $('#lciZip').append('<option value="' + val.ZipCode + '">' + val.ZipCode + '</option>');
                });

                $('#modalLoading').modal('hide');

                var state = $('#tblState tbody tr.activeRow td').html();
                var city = $('#tblCity tbody tr.activeRow td').html();
                getData(state, city);
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}