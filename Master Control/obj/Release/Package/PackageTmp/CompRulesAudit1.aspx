﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CompRulesAudit1.aspx.cs" Inherits="Master_Control.CompRulesAudit1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tblState tbody td, #tblCity tbody td {
            padding: 0px;
            margin: 0px;
        }

        .rowHover:hover {
            cursor: pointer;
            color: #000;
            background-color: #eff8b3;
        }

        .activeRow {
            color: #fff;
            background-color: #4d6578;
        }

        #navReg {
            list-style: none;
            display: inline-flex;
        }

            #navReg li {
                padding: 5px;
            }

                #navReg li.active {
                    border-top-color: #fff !important;
                }

                    #navReg li.active button {
                        color: #fff !important;
                    }

        #ulWorkSettings li.active a {
            color: #1e3d99;
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Compliance Rules</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-tasks"></i>&nbsp;Compliance Rules</span>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default" style="margin-top: 1%;">
            <div class="box-body">
                <div class="col-xs-2" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>States</label>
                        </div>
                        <div class="panel-body" style="min-height: 760px; max-height: 760px; overflow-y: auto;">
                            <table id="tblState" class="table no-border">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>Workable Settings</label>
                        </div>
                        <div class="panel-body" style="padding: 5px;">
                            <div class="col-xs-2 noPadMar">
                                <div class="panel panel-default noPadMar">
                                    <div class="panel-heading text-center">
                                        <label>City</label>
                                    </div>
                                    <div class="panel-body" style="min-height: 710px; max-height: 710px; overflow-y: auto;">
                                        <table id="tblCity" class="table no-border">
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="divSettings" class="col-xs-10 noPadMar" style="display: none;">
                                <div class="panel panel-default noPadMar">
                                    <div class="panel-body noPadMar">
                                        <div class="nav-tabs-custom noPadMar">
                                            <ul id="ulWorkSettings" class="nav nav-tabs text-nowrap" style="display: flex;">
                                                <li class="active">
                                                    <a href="#tabRegistration" data-toggle="tab">Registration Criteria</a>
                                                </li>
                                                <li>
                                                    <a href="#tabInspection" data-toggle="tab">Inspection</a>
                                                </li>
                                                <li>
                                                    <a href="#tabMunicipality" data-toggle="tab">Municipality Contact Information</a>
                                                </li>
                                                <li>
                                                    <a href="#tabDeregistration" data-toggle="tab">Deregistration</a>
                                                </li>
                                                <li>
                                                    <a href="#tabOrdinance" data-toggle="tab">Ordinance Settings</a>
                                                </li>
                                                <li>
                                                    <a href="#tabZip" data-toggle="tab">Zip Codes</a>
                                                </li>
                                                <li>
                                                    <a href="#tabNotif" data-toggle="tab">Notification Settings</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <%-- Registration --%>
                                            <div id="tabRegistration" class="tab-pane active">
                                                <div class="panel panel-default noPadMar">
                                                    <div class="panel-body noPadMar">
                                                        <div class="nav-tabs-custom noPadMar">
                                                            <ul class="nav nav-tabs" id="navReg">
                                                                <li class="active">
                                                                    <button type="button" class="btn btn-primary" href="#tabPFC" data-toggle="tab">PFC</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabREO" data-toggle="tab">REO</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabPropType" data-toggle="tab">Property Type</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabPropReq" data-toggle="tab">Requirements</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabCost" data-toggle="tab">Cost</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabContReg" data-toggle="tab">Continuing Registration</button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="tab-content">
                                                            <%-- PFC --%>
                                                            <div id="tabPFC" class="tab-pane active">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC City Notice
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcCityNotice" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Default Y/N
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcDefault" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        Default Registration Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inDefRegTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctDefRegTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctDefRegTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctDefRegTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Default / Foreclosure Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inPfcDefForclosureTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcDefForclosureTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcDefForclosureTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcDefForclosureTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Foreclosure and Vacant
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcForeclosureVacant" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Foreclosure and Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inPfcForeclosureVacantTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcForeclosureVacantTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcForeclosureVacantTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcForeclosureVacantTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Boarded Y/N
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcBoarded" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Foreclosure
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcForeclosure" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Vacant
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcVacantTimeline" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inPfcVacantTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcVacantTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcVacantTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcVacantTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC Code Violation
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcCodeViolation" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        Special Requirements
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctSpcReq" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="A">A</option>
                                                                            <option value="B">B</option>
                                                                            <option value="C">C</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        Payment Type
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPaymentType" class="form-control input-sm">
                                                                            <option value="" selected="selected">None</option>
                                                                            <option value="check">Check</option>
                                                                            <option value="credit card">Credit Card</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        Type of Registration
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctTypeOfRegistration" style="width: 100%" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="pdf">City Registration Form</option>
                                                                            <option value="website">City Website</option>
                                                                            <option value="prochamps">ProChamps Online</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-5 audit" style="display: none;">
                                                                        <input id="inUploadPath" type="file" class="file form-control input-sm" data-show-preview="false" disabled="disabled" />
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        Renewal
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctVmsRenewal" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="Annual">Annual</option>
                                                                            <option value="3 Months">3 Months</option>
                                                                            <option value="6 Months">6 Months</option>
                                                                            <option value="0">0</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        PFC OTHER Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inPfcOtherTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcOtherTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcOtherTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPfcOtherTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegPFC" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- REO --%>
                                                            <div id="tabREO" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Banked-owed Y/N
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoBankOwed" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 audit noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Banked-owed Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inReoBankOwed1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoBankOwed2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoBankOwed3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoBankOwed4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Boarded Only
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoBoardedOnly" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO City Notice
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoCityNotice" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Code Violation
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoCodeViolation" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Distressed/Abandoned Y/N
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoDistressedAbandoned" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Distressed/Abandoned
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inReoDistressedAbandoned1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoDistressedAbandoned2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoDistressedAbandoned3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoDistressedAbandoned4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Vacant
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoVacant" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inReoVacantTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoVacantTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoVacantTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoVacantTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        Rental Registration
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRentalRegistration" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        Rental Form
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRentalForm" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2">
                                                                        REO OTHER Timeline
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inReoOtherTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoOtherTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoOtherTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctReoOtherTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy" selected="selected">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegREO" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- Property Type --%>
                                                            <div id="tabPropType" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Residential
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropResidential" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Rental
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropRental" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Commercial
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropCommercial" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Condo
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropCondo" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Townhome
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropTownhome" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Vacant Lot
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropVacantLot" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Mobile Home
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctPropMobilehome" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegProperty" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- Property Requirements --%>
                                                            <div id="tabPropReq" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            ADDN. FORM/INFO
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctAddnInfo" class="form-control input-sm" multiple="multiple">
                                                                                <option value="2016 Occupancy Registration form">2016 Occupancy Registration form</option>
                                                                                <option value="Bond form for Foreclosure + Vacant">Bond form for Foreclosure + Vacant</option>
                                                                                <option value="Insurance Certificate">Insurance Certificate</option>
                                                                                <option value="Insurance Plan">Insurance Plan</option>
                                                                                <option value="Maintenance Plan">Maintenance Plan</option>
                                                                                <option value="Maintenance Plan and Timetable">Maintenance Plan and Timetable</option>
                                                                                <option value="No Trespass Form">No Trespass Form</option>
                                                                                <option value="Non-Owner-Occupied License Application">Non-Owner-Occupied License Application</option>
                                                                                <option value="Out-of-County form and fee ">Out-of-County form and fee </option>
                                                                                <option value="Proof of insurance">Proof of insurance</option>
                                                                                <option value="Proof of utilities connection/deconnection">Proof of utilities connection/deconnection</option>
                                                                                <option value="Property Maintenance form">Property Maintenance form</option>
                                                                                <option value="Signage Form">Signage Form</option>
                                                                                <option value="Statement of Intent">Statement of Intent</option>
                                                                                <option value="Structure Checklist">Structure Checklist</option>
                                                                                <option value="Trespass Affidavit Form">Trespass Affidavit Form</option>
                                                                                <option value="Vacant Building Plan Form">Vacant Building Plan Form</option>
                                                                                <option value="Vacant Property Plan">Vacant Property Plan</option>
                                                                                <option value="Trespass form Foreclosure and Vacant">Trespass form Foreclosure and Vacant</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            First Time Vacancy Date
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctFirstTimeVacancyDate" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Presale "Owner" Definition
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctPresaleDefinition" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="Current Owner on Recorded Deed">Current Owner on Recorded Deed</option>
                                                                                <option value="Trustee/Mortgagee">Trustee/Mortgagee</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Secured Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctSecuredRequired" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Local Contact Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctLocalContactRequired" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Additional Signage Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctAddSignReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Local Contact Information
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="lci"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Pictures Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctPicReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            GSE Exclusion
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctGseExclusion" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Mobile VIN Number Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctMobileVINReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Insurance Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctInsuranceReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Parcel Number Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctParcelReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Foreclosure Action Information Needed
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctForeclosureActInfo" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Legal Description Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctLegalDescReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Foreclosure Case Information Needed
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctForeclosureCaseInfo" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Block and Lot Number Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctBlockLotReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Foreclosure Deed Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctForeclosureDeedReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Attorney Information Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctAttyInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Bond Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctBondReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Broker Information Required If REO
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctBrkInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Bond Amount
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <input id="inBondAmount" type="number" class="form-control input-sm" />
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Mortgage Contact Name Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctMortContactNameReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Maintenance Plan Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctMaintePlanReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Client Tax Number Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctClientTaxReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            No Trespass Form Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctNoTrespassReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Signature Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctSignReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Utility Information Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctUtilityInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Notarization Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctNotarizationReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Winterization Required
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctWinterReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-4">
                                                                            Recent Inspection Date
                                                                        </div>
                                                                        <div class="col-xs-4 audit">
                                                                            <select id="slctRecInsDate" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-xs-2 noPadMar">
                                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegPropReq" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- Cost --%>
                                                            <div id="tabCost" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Registration Cost Y/N
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRegCost" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Registration Cost
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inRegCost" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRegCostCurr" class="form-control input-sm">
                                                                            <option value="USD" selected="selected">USD</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Registration Cost Standard
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRegCostStandard" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Is Renewal Cost Escalating
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRenewCostEscal" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inRenewAmt" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRenewAmtCurr1" class="form-control input-sm">
                                                                            <option value="USD" selected="selected">USD</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Escalating Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <button id="btnViewEscalRenewAmt" data-toggle="collapse" data-target="#divEscalRenewal" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                                    </div>
                                                                </div>
                                                                <div id="divEscalRenewal" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                        <button type="button" data-toggle="collapse" data-target="#divEscalRenewal" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-12">
                                                                            <div class="col-xs-3">
                                                                                <label>Service Type</label>
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <label>Amount</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-3 audit">
                                                                                <label>Renewal</label>
                                                                            </div>
                                                                            <div class="col-xs-2 audit">
                                                                                <input type="number" class="form-control input-sm" />
                                                                            </div>
                                                                            <div class="col-xs-2 audit">
                                                                                <select class="form-control input-sm">
                                                                                    <option value="USD">USD</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-xs-1 noPadMar">
                                                                                <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                                <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <button id="btnAddNew1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add New</button>
                                                                    </div>
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%; margin-bottom: 1%;">
                                                                        <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Commercial Registration Fee
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inComRegFee" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctComRegFee" class="form-control input-sm">
                                                                            <option value="USD" selected="selected">USD</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Commercial Fee Standard?
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctComFeeStandard" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Is Renewal Cost Escalating
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRenewCostEscal2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <input id="inRenewAmt2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRenewAmtCurr2" class="form-control input-sm">
                                                                            <option value="USD" selected="selected">USD</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Escalating Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <button id="btnViewEscalRenewAmt2" data-toggle="collapse" data-target="#divEscalRenewal2" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                                    </div>
                                                                </div>
                                                                <div id="divEscalRenewal2" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                        <button type="button" data-toggle="collapse" data-target="#divEscalRenewal2" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-3">
                                                                            <label>Service Type</label>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <label>Amount</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-3 audit">
                                                                                <label>Renewal</label>
                                                                            </div>
                                                                            <div class="col-xs-2 audit">
                                                                                <input type="number" class="form-control input-sm" />
                                                                            </div>
                                                                            <div class="col-xs-2 audit">
                                                                                <select class="form-control input-sm">
                                                                                    <option value="USD">USD</option>
                                                                                </select>
                                                                            </div>
                                                                            <div class="col-xs-1 noPadMar">
                                                                                <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                                <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <button id="btnAddNew2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add New</button>
                                                                    </div>
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%; margin-bottom: 1%;">
                                                                        <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Is Registration Cost Standard? Y/N
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctRegCostStandard2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegCost" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- Continuing Registration --%>
                                                            <div id="tabContReg" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Continue Registration from PFC to REO?
                                                                    </div>
                                                                    <div class="col-xs-2 audit">
                                                                        <select id="slctContReg" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar">
                                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegCont" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <%-- Inspection --%>
                                            <div id="tabInspection" class="tab-pane">
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Update Required
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsUpReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Criteria Occupied
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsCriOcc" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button id="btnViewInsCriteriaOcc" data-toggle="collapse" data-target="#divInsCriteriaOcc" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div id="divInsCriteriaOcc" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                    <div class="col-xs-11 grp" style="margin-top: 1%">
                                                        <div class="col-xs-2">
                                                            Select Frequency:
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" id="daily" value="daily" />Daily
                                                        </div>
                                                        <div id="divWeekly" class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" id="weekly" value="weekly" />&nbsp;Weekly
                                                        </div>
                                                        <div id="divMonthly" class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" id="monthly" value="monthly" />&nbsp;Monthly
                                                        </div>
                                                        <div id="divYearly" class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" id="yearly" value="yearly" />&nbsp;Yearly
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1 text-right">
                                                        <button type="button" data-toggle="collapse" data-target="#divInsCriteriaOcc" class="btn btn-link btn-lg text-red noPadMar"><i class="fa fa-times"></i></button>
                                                    </div>
                                                    <div class="col-xs-12 grp" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            What is the reference for this cycle?
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctInsCriteriaOccCycle1" class="form-control input-sm">
                                                                <option value="Before">Before</option>
                                                                <option value="After">After</option>
                                                                <option value="Upon">Upon</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctInsCriteriaOccCycle2" class="form-control input-sm">
                                                                <option value="Vacancy">Vacancy</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccDaily" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12" style="margin-top: 1%">
                                                            <div class="col-xs-3">
                                                                Set frequency of the cycle:
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-1 audit">
                                                                    <input type="radio" name="rdFreq1" id="dailyevery1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="mon" name="cbFreq" value="Monday" />&nbsp;Monday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="tues" name="cbFreq" value="Tuesday" />&nbsp;Tuesday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="wed" name="cbFreq" value="Wednesday" />&nbsp;Wednesday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="thurs" name="cbFreq" value="Thursday" />&nbsp;Thursday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="fri" name="cbFreq" value="Friday" />&nbsp;Friday
                                                                </div>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreq1" id="dailyevery2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inInsCriteriaOccDay" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">Day/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccWeekly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbEvery1" name="rdFreq1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inInsCriteriaOccWeek" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">week/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbEvery2" name="rdFreq1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctInsCriteriaOccWeek" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the week</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccMonthly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreq1" id="everyMonth1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inInsCriteriaOccMonth" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">month/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreq1" id="everyMonth2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctInsCriteriaMonth1" class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctInsCriteriaMonth2" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" id="everyMonth3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                        <option value="6th">6th</option>
                                                                        <option value="7th">7th</option>
                                                                        <option value="8th">8th</option>
                                                                        <option value="9th">9th</option>
                                                                        <option value="10th">10th</option>
                                                                        <option value="11th">11th</option>
                                                                        <option value="12th">12th</option>
                                                                        <option value="13th">13th</option>
                                                                        <option value="14th">14th</option>
                                                                        <option value="15th">15th</option>
                                                                        <option value="16th">16th</option>
                                                                        <option value="17th">17th</option>
                                                                        <option value="18th">18th</option>
                                                                        <option value="19th">19th</option>
                                                                        <option value="20th">20th</option>
                                                                        <option value="21st">21st</option>
                                                                        <option value="22nd">22nd</option>
                                                                        <option value="23rd">23rd</option>
                                                                        <option value="24th">24th</option>
                                                                        <option value="25th">25th</option>
                                                                        <option value="26th">26th</option>
                                                                        <option value="27th">27th</option>
                                                                        <option value="28th">28th</option>
                                                                        <option value="29th">29th</option>
                                                                        <option value="30th">30th</option>
                                                                        <option value="31st">31st</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccYearly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreqYear1" id="anually" value="Anually" />&nbsp;Anually
                                                                </div>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreqYear1" id="everyYear" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inInsCriteriaOccYear" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">years</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 grp" style="margin-top: 1%">
                                                        <div class="col-xs-3 audit">
                                                            <input type="checkbox" id="setRefresh" />&nbsp;Set refresh time of cycle:
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input id="inInsCriteriaOccTime" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 text-right" style="margin-bottom: 1%;">
                                                        <button id="btnSaveInsCriteriaOcc" data-toggle="collapse" data-target="#divInsCriteriaOcc" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 grp" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Criteria Vacant
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsCriVac" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button id="btnViewInsCriteriaVac" data-toggle="collapse" data-target="#divInsCriteriaVac" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div id="divInsCriteriaVac" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                    <div class="col-xs-11 grp" style="margin-top: 1%">
                                                        <div class="col-xs-2">
                                                            Select Frequency:
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" id="dailyVac" value="daily" />&nbsp;Daily
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" id="weeklyVac" value="weekly" />&nbsp;Weekly
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" id="monthlyVac" value="monthly" />&nbsp;Monthly
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" id="yearlyVac" value="yearly" />&nbsp;Yearly
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1 text-right">
                                                        <button type="button" data-toggle="collapse" data-target="#divInsCriteriaVac" class="btn btn-link btn-lg text-red noPadMar"><i class="fa fa-times"></i></button>
                                                    </div>
                                                    <div class="col-xs-12 grp" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            What is the reference for this cycle?
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctInsCriteriaVacCycle1" class="form-control input-sm">
                                                                <option value="Before">Before</option>
                                                                <option value="After">After</option>
                                                                <option value="Upon">Upon</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctInsCriteriaVacCycle2" class="form-control input-sm">
                                                                <option value="Vacancy">Vacancy</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacDaily" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12" style="margin-top: 1%">
                                                            <div class="col-xs-3">
                                                                Set frequency of the cycle:
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbDailyVac1" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="vacMon" name="cbFreqVac" value="Monday" />&nbsp;Monday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="vacTues" name="cbFreqVac" value="Tuesday" />&nbsp;Tuesday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="vacWed" name="cbFreqVac" value="Wednesday" />&nbsp;Wednesday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="vacThurs" name="cbFreqVac" value="Thursday" />&nbsp;Thursday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="vacFri" name="cbFreqVac" value="Friday" />&nbsp;Friday
                                                                </div>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreq2" id="rbDailyVac2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inInsCriteriaVacDay" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">Day/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacWeekly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbWeeklyVac1" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inInsCriteriaVacWeek" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">week/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbWeeklyVac2" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctInsCriteriaVacWeek" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the week</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacMonthly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreq2" id="rbMonthlyVac1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inInsCriteriaVacMonth" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">month/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" name="rdFreq2" id="rbMonthlyVac2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctInsCriteriaVacMonth1" class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctInsCriteriaVacMonth2" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                        <option value="6th">6th</option>
                                                                        <option value="7th">7th</option>
                                                                        <option value="8th">8th</option>
                                                                        <option value="9th">9th</option>
                                                                        <option value="10th">10th</option>
                                                                        <option value="11th">11th</option>
                                                                        <option value="12th">12th</option>
                                                                        <option value="13th">13th</option>
                                                                        <option value="14th">14th</option>
                                                                        <option value="15th">15th</option>
                                                                        <option value="16th">16th</option>
                                                                        <option value="17th">17th</option>
                                                                        <option value="18th">18th</option>
                                                                        <option value="19th">19th</option>
                                                                        <option value="20th">20th</option>
                                                                        <option value="21st">21st</option>
                                                                        <option value="22nd">22nd</option>
                                                                        <option value="23rd">23rd</option>
                                                                        <option value="24th">24th</option>
                                                                        <option value="25th">25th</option>
                                                                        <option value="26th">26th</option>
                                                                        <option value="27th">27th</option>
                                                                        <option value="28th">28th</option>
                                                                        <option value="29th">29th</option>
                                                                        <option value="30th">30th</option>
                                                                        <option value="31st">31st</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacYearly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbYearlyVac1" name="rdFreqVacYear1" value="Anually" />&nbsp;Anually
                                                                </div>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input id="rbYearlyVac2" type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="text" id="inInsCriteriaVacYear" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">years</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 grp" style="margin-top: 1%">
                                                        <div class="col-xs-3 audit">
                                                            <input type="checkbox" id="setRefreshVac" />&nbsp;Set refresh time of cycle:
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input id="inInsCriteriaVacTime" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 text-right" style="margin-bottom: 1%;">
                                                        <button id="btnSaveInsCriteriaVac" data-toggle="collapse" data-target="#divInsCriteriaVac" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection/Monitoring Fee Payment Frequency
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsFeePay" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button id="btnViewInsFeePaymentFreq" data-toggle="collapse" data-target="#divInsFeePayFreq" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div id="divInsFeePayFreq" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                    <div class="col-xs-11 grp" style="margin-top: 1%">
                                                        <div class="col-xs-2">
                                                            Select Frequency:
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" id="dailyFreq" name="rdInsFeePayFreq" value="daily" />&nbsp;Daily
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" id="weeklyFreq" name="rdInsFeePayFreq" value="weekly" />&nbsp;Weekly
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" id="monthlyFreq" name="rdInsFeePayFreq" value="monthly" />&nbsp;Monthly
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input type="radio" id="yearlyFreq" name="rdInsFeePayFreq" value="yearly" />&nbsp;Yearly
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1 text-right">
                                                        <button type="button" data-toggle="collapse" data-target="#divInsFeePayFreq" class="btn btn-link btn-lg text-red noPadMar"><i class="fa fa-times"></i></button>
                                                    </div>
                                                    <div class="col-xs-12 grp" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            What is the reference for this cycle?
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctFeePayCycle1" class="form-control input-sm">
                                                                <option value="Before">Before</option>
                                                                <option value="After">After</option>
                                                                <option value="Upon">Upon</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctFeePayCycle2" class="form-control input-sm">
                                                                <option value="Vacancy">Vacancy</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqDaily" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12" style="margin-top: 1%">
                                                            <div class="col-xs-3">
                                                                Set frequency of the cycle:
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbFreq1" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="freqMon" name="cbFreqFeePay" value="Monday" />&nbsp;Monday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="freqTues" name="cbFreqFeePay" value="Tuesday" />&nbsp;Tuesday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="freqWed" name="cbFreqFeePay" value="Wednesday" />&nbsp;Wednesday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="freqThurs" name="cbFreqFeePay" value="Thursday" />&nbsp;Thursday
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input type="checkbox" id="freqFri" name="cbFreqFeePay" value="Friday" />&nbsp;Friday
                                                                </div>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbFreq2" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inFeePayFreqDay" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">Day/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqWeekly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbWeeklyFreq1" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inFeePayFreqWeek" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">week/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbWeeklyFreq2" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctFeePayFreqWeek" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the week</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqMonthly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbMonthlyFreq1" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inFeePayFreqMonth" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">month/s</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbMonthlyFreq2" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctFeePayFreqMonth1" class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <select id="slctFeePayFreqMonth2" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                        <option value="6th">6th</option>
                                                                        <option value="7th">7th</option>
                                                                        <option value="8th">8th</option>
                                                                        <option value="9th">9th</option>
                                                                        <option value="10th">10th</option>
                                                                        <option value="11th">11th</option>
                                                                        <option value="12th">12th</option>
                                                                        <option value="13th">13th</option>
                                                                        <option value="14th">14th</option>
                                                                        <option value="15th">15th</option>
                                                                        <option value="16th">16th</option>
                                                                        <option value="17th">17th</option>
                                                                        <option value="18th">18th</option>
                                                                        <option value="19th">19th</option>
                                                                        <option value="20th">20th</option>
                                                                        <option value="21st">21st</option>
                                                                        <option value="22nd">22nd</option>
                                                                        <option value="23rd">23rd</option>
                                                                        <option value="24th">24th</option>
                                                                        <option value="25th">25th</option>
                                                                        <option value="26th">26th</option>
                                                                        <option value="27th">27th</option>
                                                                        <option value="28th">28th</option>
                                                                        <option value="29th">29th</option>
                                                                        <option value="30th">30th</option>
                                                                        <option value="31st">31st</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqYearly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbYearlyFreq1" name="rdFreqFeePayYear1" value="Anually" />&nbsp;Anually
                                                                </div>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12 grp" style="margin-top: 1%">
                                                                <div class="col-xs-2 audit">
                                                                    <input type="radio" id="rbYearlyFreq2" name="rdFreqFeePayYear1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2 audit">
                                                                    <input id="inFeePayFreqYear" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">years</span>
                                                                <div class="col-xs-1 noPadMar">
                                                                    <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                                    <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 grp" style="margin-top: 1%">
                                                        <div class="col-xs-3 audit">
                                                            <input type="checkbox" id="cbFreqTime" />&nbsp;Set refresh time of cycle:
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <input id="inFeePayTime" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="checkInspect(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="crossInspect(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 text-right" style="margin-bottom: 1%;">
                                                        <button id="btnSaveInsFeePayFreq" data-toggle="collapse" data-target="#divInsFeePayFreq" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection/Monitoring Fee Required Y/N
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsFeeReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection/Monitoring Fee Amount
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inInsFeeAmt" type="number" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsFeeAmt" class="form-control input-sm">
                                                            <option value="USD" selected="selected">USD</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Reporting Frequency
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctInsRepFreq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveInspection" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Municipality --%>
                                            <div id="tabMunicipality" class="tab-pane">
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Department
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inMuniDept" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Phone Number
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inMuniPhone" type="number" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Email Address
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inMuniEmail" type="email" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Mailing Address
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inMuniMailStr" type="text" class="form-control input-sm" placeholder="Street Name" />
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctMuniMailSta" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctMuniMailCt" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctMuniMailZip" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Contact Person
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inContact" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Title
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inTitle" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Department
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inDept" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Phone Number
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inPhone" type="number" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Email Address
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inEmail" type="email" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Mailing Address
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inMailing" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Hours of Operation
                                                    </div>
                                                    <div class="col-xs-1">from:</div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inHrsFrom" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1">to:</div>
                                                    <div class="col-xs-2 audit">
                                                        <input id="inHrsTo" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveMunicipalInfo" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Deregistration --%>
                                            <div id="tabDeregistration" class="tab-pane">
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Deregistration Required
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctDeregRequired" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Only if Conveyed
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctConveyed" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Only if Occupied
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctOccupied" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        How to Deregister
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctHowToDereg" style="width: 100%" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="pdf">PDF</option>
                                                            <option value="website">Website</option>
                                                            <option value="prochamp">Prochamp</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-7 audit" style="display: none;">
                                                        <input id="inUploadPathD" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        New Owner Information Required
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctNewOwnerInfoReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Proof of Conveyance Required
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctProofOfConveyReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Date of Sale Required
                                                    </div>
                                                    <div class="col-xs-2 audit">
                                                        <select id="slctDateOfSaleReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-1 noPadMar">
                                                        <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                        <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveDeregistration" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Ordinance Settings --%>
                                            <div id="tabOrdinance" class="tab-pane">
                                                <table id="tblOrdinance" class="table no-border table-responsive" data-pagination="true">
                                                    <thead>
                                                        <tr>
                                                            <th data-field="ordinance_num">Ordinance Number</th>
                                                            <th data-field="ordinance_name">Ordinance Name</th>
                                                            <th data-field="description" data-cell-style="cellStyle">Description</th>
                                                            <th data-field="section">Section</th>
                                                            <th data-field="source">Source</th>
                                                            <th data-field="enacted_date">Enacted Date</th>
                                                            <th data-field="revision_date">Revision Date</th>
                                                            <th data-field="id" data-formatter="action"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <div class="text-left" style="margin-top: 1%;">
                                                    <button id="btnAddOrdinance" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add New</button>
                                                    <button id="btnHistory" type="button" class="btn btn-primary btn-sm"><i class="fa fa-history"></i>&nbsp;History</button>
                                                </div>
                                                <div class="text-right" style="margin-top: 1%;">
                                                    <%--<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>--%>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Zip Code --%>
                                            <div id="tabZip" class="tab-pane">
                                                <table id="tblZip" class="table no-border">
                                                    <tbody></tbody>
                                                </table>
                                                <div class="text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveZip" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Notification Setting --%>
                                            <div class="tab-pane" id="tabNotif">
                                                <fieldset>
                                                    <legend>
                                                        <label>Registration Notification</label></legend>
                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                        <div class="col-xs-2">
                                                            Send Reminder
                                                        </div>
                                                        <div class="col-xs-1 audit">
                                                            <input id="inRegSendRem" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRegSendRem1" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Business">Business</option>
                                                                <option value="Calendar">Calendar</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRegSendRem2" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Days">Days</option>
                                                                <option value="Months">Months</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRegSendRem3" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Before">Before</option>
                                                                <option value="After">After</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRegSendRem4" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Vacant Registration Time Frame">Vacant Registration Time Frame</option>
                                                                <option value="Renewal Time Frame">Renewal Time Frame</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <legend>
                                                        <label>Renewal Notification</label></legend>
                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                        <div class="col-xs-2">
                                                            Send Reminder
                                                        </div>
                                                        <div class="col-xs-1 audit">
                                                            <input id="inRenewSendRem" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRenewSendRem1" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Business">Business</option>
                                                                <option value="Calendar">Calendar</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRenewSendRem2" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Days">Days</option>
                                                                <option value="Months">Months</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRenewSendRem3" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Before">Before</option>
                                                                <option value="After">After</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2 audit">
                                                            <select id="slctRenewSendRem4" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Vacant Registration Time Frame">Vacant Registration Time Frame</option>
                                                                <option value="Renewal Time Frame">Renewal Time Frame</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <legend>
                                                        <label>Web Notification</label></legend>
                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                        <div class="col-xs-2">
                                                            Send Reminder
                                                        </div>
                                                        <div class="col-xs-9 audit">
                                                            <input id="inWebSendRem" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-1 noPadMar">
                                                            <button type="button" class="btn btn-link text-green noPadMar" onclick="check(this);"><i class="fa fa-check fa-2x"></i></button>
                                                            <button type="button" class="btn btn-link text-red noPadMar" onclick="cross(this);"><i class="fa fa-times fa-2x"></i></button>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveNotif" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalEdit">
        <div class="modal-dialog" role="document">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <label id="lblEdit"></label>
                </div>
                <div id="lci" class="panel-body" style="display: none;">
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Company</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciCompany" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Contact First Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciFirstName" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Contact Last Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciLastName" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Title</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciTitle" class="form-control input-sm">
                                <option value="">N/A</option>
                                <option value="Broker">Broker</option>
                                <option value="SPI Vendor">SPI Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Business License Number</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciBusinessLicenseNum" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Phone Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciPhoneNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciPhoneNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Business Phone Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciBusinessPhoneNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciBusinessPhoneNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>24-Hour Emergency Phone</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciEmrPhone1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciEmrPhone2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Fax Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciFaxNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciFaxNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Cell</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciCellNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciCellNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Email Address</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciEmail" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Street Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciStreet" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>State</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciState" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>City</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciCity" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Zip</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciZip" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Hours of Operation</label>
                        </div>
                        <div class="col-xs-1">
                            from:
                        </div>
                        <div class="col-xs-3">
                            <div class="bootstrap-timepicker">
                                <input id="lciHrsFrom" type="text" class="form-control input-sm timepicker" />
                            </div>
                        </div>
                        <div class="col-xs-1">
                            to:
                        </div>
                        <div class="col-xs-3">
                            <div class="bootstrap-timepicker">
                                <input id="lciHrsTo" type="text" class="form-control input-sm timepicker" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="rr" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="ico" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="icv" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="dr" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/CompRulesAudit1.js"></script>
</asp:Content>
