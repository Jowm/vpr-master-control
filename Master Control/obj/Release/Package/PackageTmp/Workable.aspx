﻿<%@ Page Title="Business Rules" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Workable.aspx.cs" Inherits="Master_Control.Workable" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style type="text/css">
		#tblState tbody td, #tblCity tbody td {
			padding: 0px;
			margin: 0px;
		}

		.rowHover:hover {
			cursor: pointer;
			color: #000;
			background-color: #eff8b3;
		}

		.activeRow {
			color: #fff;
			background-color: #4d6578;
		}

		#navReg {
			list-style: none;
			display: inline-flex;
		}

			#navReg li {
				padding: 5px;
			}

				#navReg li.active {
					border-top-color: #fff !important;
				}

					#navReg li.active button {
						color: #fff !important;
					}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<section class="content-header">
		<h1>Business Rules</h1>
		<ol class="breadcrumb">
			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
			<li class="active">
				<span><i class="fa fa-tasks"></i>&nbsp;Business Rules</span>
			</li>
		</ol>
	</section>

	<section class="content">

		<%--<div class="input-group" style="width: 600px;">
			<input type="text" name="q" class="form-control" placeholder="Search By: Zip Code, City, State, Property Type, Property Status, and Website…" />
			<span class="input-group-btn">
				<button type="submit" name="search" id="search-btn" class="btn btn-flat">
					<i class="fa fa-search"></i>
				</button>
			</span>
		</div>--%>

		<div class="box box-default" style="margin-top: 1%;">
			<div class="box-body">
				<div class="col-xs-2" style="padding-left: 0px;">
					<div class="panel panel-primary">
						<div class="panel-heading text-center">
							<label>States</label>
						</div>
						<div class="panel-body" style="max-height: 680px; overflow-y: auto;">
							<table id="tblState" class="table no-border">
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-xs-10" style="padding-left: 0px;">
					<div class="panel panel-primary">
						<div class="panel-heading text-center">
							<label>Workable Settings</label>
						</div>
						<div class="panel-body" style="padding: 5px;">
							<div class="col-xs-2 noPadMar">
								<div class="panel panel-default noPadMar">
									<div class="panel-heading text-center">
										<label>City</label>
									</div>
									<div class="panel-body" style="max-height: 630px; overflow-y: auto;">
										<table id="tblCity" class="table no-border">
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div id="divSettings" class="col-xs-10 noPadMar" style="display: none;">
								<div class="panel panel-default noPadMar">
									<div class="panel-body noPadMar">
										<div class="nav-tabs-custom noPadMar">
											<ul class="nav nav-tabs text-nowrap" style="display: flex;">
												<li class="active">
													<a href="#tabRegistration" data-toggle="tab">Registration Criteria</a>
												</li>
												<li>
													<a href="#tabInspection" data-toggle="tab">Inspection</a>
												</li>
												<li>
													<a href="#tabMunicipality" data-toggle="tab">Municipality Contact Information</a>
												</li>

												<li>
													<a href="#tabDeregistration" data-toggle="tab">Deregistration</a>
												</li>
												<li>
													<a href="#tabOrdinance" data-toggle="tab">Ordinance Settings</a>
												</li>

												<li>
													<a href="#tabZip" data-toggle="tab">Zip Codes</a>
												</li>
												<li>
													<a href="#tabNotif" data-toggle="tab">Notification Settings</a>
												</li>
											</ul>

										</div>
										<div class="tab-content">
											<%-- Registration --%>
											<div id="tabRegistration" class="tab-pane active">
												<div class="panel panel-default noPadMar">
													<div class="panel-body noPadMar">
														<div class="nav-tabs-custom noPadMar">
															<ul class="nav nav-tabs" id="navReg">
																<li class="active">
																	<button type="button" class="btn btn-primary" href="#tabCriteria" data-toggle="tab">Condition</button>
																</li>
																<li>
																	<button type="button" class="btn btn-default" href="#tabPropType" data-toggle="tab">Property Type</button>
																</li>
																<li>
																	<button type="button" class="btn btn-default" href="#tabPropReq" data-toggle="tab">Requirements</button>
																</li>
																<li>
																	<button type="button" class="btn btn-default" href="#tabCost" data-toggle="tab">Cost</button>
																</li>
															</ul>
														</div>
														<div class="tab-content">
															<div id="tabCriteria" class="tab-pane active">
																<table id="tblReg" class="table no-border">
																	<%--<tr>
																		<td style="width: 10%;">Vendor Code</td>
																		<td style="width: 10%;">
																			<input type="text" class="form-control input-sm" />
																		</td>
																	</tr>--%>
																	<tr>
																		<td>Foreclosure Registration </td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td style="width: 25%;">Foreclosure Registration Time Frame</td>
																		<td style="width: 7%;">
																			<input type="text" class="form-control input-sm" /></td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Calendar">Calendar</option>
																				<option value="Working">Working</option>
																			</select></td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Days">Days</option>
																				<option value="Months">Months</option>
																			</select></td>
																		<td class="noPadMar">from</td>
																		<td style="width: 18%;">
																			<select class="form-control input-sm">
																				<option value="foreclosure sale">Foreclosure Sale</option>
																				<option value="filing of foreclosure">Filing of Foreclosure</option>
																				<option value="transfer of foreclosed property">Transfer of Foreclosed property</option>
																				<option value="initiation of foreclosure">Initiation of Foreclosure</option>
																				<option value="upon municipal notice">Upon Municipal Notice</option>
																				<option value="inspection">Inspection</option>
																				<option value="recording of deed">Recording of Deed</option>
																			</select>
																		</td>
																	</tr>
																	<%--<tr>
																
															</tr>--%>
																	<tr>
																		<td>Vacant Registration </td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Vacant Registration Time Frame</td>
																		<td>
																			<input type="text" class="form-control input-sm" /></td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Calendar">Calendar</option>
																				<option value="Working">Working</option>
																			</select>
																		</td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Days">Days</option>
																				<option value="Months">Months</option>
																			</select>
																		</td>
																		<td class="noPadMar">from
																		</td>
																		<td>
																			<select class="form-control input-sm">
																				<option value="vacancy">Vacancy</option>
																				<option value="assumption of ownership">Assumption of Ownership</option>
																				<option value="recording of deed">Recording of Deed</option>
																				<option value="upon municipal notice">Upon Municipal Notice</option>
																				<option value="inspection">Inspection</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Foreclosure and Vacant Registration </td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Foreclosure and Vacant Registration Time Frame</td>
																		<td>
																			<input type="text" class="form-control input-sm" /></td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Calendar">Calendar</option>
																				<option value="Working">Working</option>
																			</select>
																		</td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Days">Days</option>
																				<option value="Months">Months</option>
																			</select>
																		</td>
																		<td class="noPadMar">from
																		</td>
																		<td>
																			<select class="form-control input-sm">
																				<option value="foreclosure sale">Foreclosure Sale</option>
																				<option value="filing of foreclosure">Filing of Foreclosure</option>
																				<option value="transfer of foreclosed property">Transfer of Foreclosed property</option>
																				<option value="initiation of foreclosure">Initiation of Foreclosure</option>
																				<option value="upon municipal notice">Upon Municipal Notice</option>
																				<option value="inspection">Inspection</option>
																				<option value="vacancy">Vacancy</option>
																				<option value="assumption of ownership">Assumption of Ownership</option>
																				<option value="recording of deed">Recording of Deed</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Pre-Sale Registration</td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Past Filed Exclusions </td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td class="tdPastDate">Past Filed Exclusions Date</td>
																		<td class="tdPastDate" colspan="2">
																			<input type="text" class="form-control input-sm datepicker" />
																		</td>
																	</tr>
																	<tr>
																	</tr>
																	<tr>
																		<td>REO/FHA Post-sale Registration </td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>City notice Only </td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Boarded Only </td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Special Requirements</td>
																		<td>
																			<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="sr"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
																		</td>
																	</tr>
																	<tr>
																		<td>How to Register</td>
																		<td>
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="pdf">PDF</option>
																				<option value="website">Website</option>
																				<option value="prochamp">Prochamp</option>
																			</select>
																		</td>
																		<td id="tdUpload">
																			<input id="inUploadFile" type="file" class="file input-sm" data-show-preview="false" />
																		</td>
																	</tr>
																</table>
																<div class="text-right" style="margin-top: 1%;">
																	<button id="btnSaveRegCriteria" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
																	<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
																</div>
															</div>
															<div id="tabPropType" class="tab-pane">
																<table id="tblProp" class="table no-border">
																	<tr>
																		<td style="width: 25%">Residential Y/N</td>
																		<td>
																			<select style="width: 10%" class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select></td>
																	</tr>
																	<tr>
																		<td>Rental Y/N</td>
																		<td>
																			<select style="width: 10%" class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select></td>
																	</tr>
																	<tr>
																		<td>Commercial Y/N</td>
																		<td>
																			<select style="width: 10%" class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select></td>
																	</tr>
																	<tr>
																		<td>Condo Y/N</td>
																		<td>
																			<select style="width: 10%" class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select></td>
																	</tr>
																	<tr>
																		<td>Townhome Y/N</td>
																		<td>
																			<select style="width: 10%" class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select></td>
																	</tr>
																	<tr>
																		<td>Vacant Lot Y/N</td>
																		<td>
																			<select style="width: 10%" class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select></td>
																	</tr>
																	<tr>
																		<td>Mobile Home Y/N</td>
																		<td>
																			<select style="width: 10%" class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select></td>
																	</tr>

																</table>
																<div class="text-right" style="margin-top: 1%;">
																	<button id="btnSaveRegProperty" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
																	<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
																</div>
															</div>
															<div id="tabPropReq" class="tab-pane">
																<table class="table no-border">
																	<tr>
																		<td style="width: 20%;">ADDN. FORM/INFO</td>
																		<td style="width: 10%;">
																			<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="ai"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
																		</td>
																		<td style="width: 20%;">First Time Vacancy Date</td>
																		<td style="width: 10%;">
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Presale "Owner" Definition</td>
																		<td>
																			<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="pd"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
																		</td>
																		<td>Secured Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Local Contact Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Additional Signage Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Local Contact Information</td>
																		<td>
																			<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="lci"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
																		</td>
																		<td>Pictures Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>GSE Exclusion </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Mobile VIN Number Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Insurance Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Parcel Number Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Foreclosure Action Information Needed </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Legal Description Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Foreclosure Case Information Needed </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Block and Lot Number Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Foreclosure Deed Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Attorney Information Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Bond Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Broker Information Required If REO </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Bond Amount</td>
																		<td>
																			<input type="text" class="form-control input-sm" />
																		</td>
																		<td>Mortgagee Contact Name Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Maintenance Plan Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Client Tax Number Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>No Trespass Form Required</td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Signature Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Utility Information Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Notarization Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Winterization Required </td>
																		<td>
																			<select  class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Recent Inspection Date</td>
																		<td>
																			<select  class="form-control input-sm">

																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																</table>
																<div class="text-right" style="margin-top: 1%;">
																	<button id="btnSaveRegPropReq" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
																	<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
																</div>
															</div>
															<div id="tabCost" class="tab-pane">
																<table class="table no-border">
																	<tr>
																		<td>Registration Fee Required</td>
																		<td>
																			<select style="width: 40%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																		<td>Registration Fee</td>
																		<td>
																			<input type="text" class="form-control input-sm" />
																		</td>
																	</tr>
																	<tr>
																		<td>Renewal Fee Required</td>
																		<td>
																			<select style="width: 40%;" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																	<tr>
																		<td>Renewal Timeframe</td>
																		<td>
																			<select style="width: 40%;" class="form-control input-sm">
																				<option value="Annual">Annual</option>
																			</select>
																		</td>
																		<%--<td>
																			<select class="form-control input-sm">
																				<option value="Date">Date</option>
																				<option value="Days">Days</option>
																			</select>
																		</td>
																		<td style="width: 8%;">
																			<input type="text" class="form-control input-sm" />
																		</td>
																		<td class="tdDays">
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Calendar">Calendar</option>
																				<option value="Working">Working</option>
																			</select></td>
																		<td class="tdDays">
																			<select style="width: 100%" class="form-control input-sm">
																				<option value="Days">Days</option>
																				<option value="Months">Months</option>
																			</select>
																		</td>--%>
																	</tr>
																	<tr>
																		<td>Renewal Requirements</td>
																		<td>
																			<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="rr"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
																		</td>
																	</tr>
																	<tr>
																		<td>Renewal Fee Amounts</td>
																		<td>
																			<input type="text" class="form-control input-sm" />
																		</td>
																	</tr>
																	<tr>
																		<td>Commercial Registration Fee</td>
																		<td>
																			<input type="text" class="form-control input-sm" />
																		</td>
																	</tr>
																	<tr>
																		<td>Monthly Monitoring Fee Required</td>
																		<td>
																			<select style="width: 40%" class="form-control input-sm">
																				<option value="true" selected="selected">Yes</option>
																				<option value="false">No</option>
																			</select>
																		</td>
																	</tr>
																</table>
																<div class="text-right" style="margin-top: 1%;">
																	<button id="btnSaveRegCost" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
																	<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<%-- Inspection --%>
											<div id="tabInspection" class="tab-pane">
												<table class="table no-border noPadMar">
													<tr>
														<td style="width: 25%;">Inspection Update Required</td>
														<td>
															<select style="width: 10%" class="form-control input-sm">
																<option value="true" selected="selected">Yes</option>
																<option value="false">No</option>
															</select>
														</td>
													</tr>
													<tr>
														<td>Inspection Criteria Occupied</td>
														<td>
															<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="ico"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
														</td>
													</tr>
													<tr>
														<td>Inspection Criteria Vacant</td>
														<td>
															<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="icv"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
														</td>
													</tr>
												</table>
												<div class="text-right" style="margin-top: 1%;">
													<button id="btnSaveInspection" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
													<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
												</div>
											</div>

											<%-- Municipality --%>
											<div id="tabMunicipality" class="tab-pane">
												<table class="table no-border noPadMar">
													<tr class="noPadMar">
														<td>Municipality Department</td>
														<td>
															<input type="text" class="form-control input-sm" /></td>
													</tr>
													<tr>
														<td>Municipality Phone Number</td>
														<td>
															<input type="text" class="form-control input-sm" /></td>
													</tr>
													<tr>
														<td>Municipality Email Address</td>
														<td>
															<input type="text" class="form-control input-sm" /></td>
													</tr>
													<tr>
														<td style="width: 20%">Municipality Mailing Address</td>
														<td style="width: 20%">
															<input type="text" class="form-control input-sm" placeholder="Street" /></td>
														<td style="width: 20%">
															<input type="text" class="form-control input-sm" placeholder="City" /></td>
														<td style="width: 20%">
															<input type="text" class="form-control input-sm" placeholder="State" /></td>
														<td style="width: 20%">
															<input type="text" class="form-control input-sm" placeholder="Zip Code" /></td>
													</tr>

													<tr>
													</tr>
													<tr>
														<td>Contact Person</td>
														<td>
															<input type="text" class="form-control input-sm" /></td>
													</tr>
													<tr>
														<td>Title</td>
														<td>
															<input type="text" class="form-control input-sm" /></td>
													</tr>
													<tr>
														<td>Department</td>
														<td>
															<input type="text" class="form-control input-sm" /></td>
													</tr>
													<tr>
														<td>Phone Number</td>
														<td>
															<input type="text" class="form-control input-sm" />
														</td>
													</tr>
													<tr>
														<td>Email Address</td>
														<td>
															<input type="text" class="form-control input-sm" />
														</td>
													</tr>
													<tr>
														<td>Mailing Address</td>
														<td>
															<input type="text" class="form-control input-sm" />
														</td>
													</tr>
													<tr>
														<td>Hours of Operation</td>
														<td colspan="4">
															<div class="col-xs-1 noPadMar">from:</div>
															<div class="col-xs-2 noPadMar">
																<div class="bootstrap-timepicker">
																	<input type="text" class="form-control input-sm timepicker" />
																</div>
															</div>
															<div class="col-xs-1">to:</div>
															<div class="col-xs-2 noPadMar">
																<div class="bootstrap-timepicker">
																	<input type="text" class="form-control input-sm timepicker" />
																</div>
															</div>
														</td>
													</tr>
												</table>
												<div class="text-right" style="margin-top: 1%;">
													<button id="btnSaveMunicipalInfo" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
													<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
												</div>
											</div>

											<%-- Deregistration --%>
											<div id="tabDeregistration" class="tab-pane">
												<table class="table no-border">
													<tr>
														<td style="width: 30%;">Deregistration Required</td>
														<td>
															<select style="width: 17%;" class="form-control input-sm">
																<option value="true" selected="selected">Yes</option>
																<option value="false">No</option>
															</select>
														</td>
													</tr>
													<tr>
														<td>Only if Conveyed</td>
														<td>
															<select style="width: 17%;" class="form-control input-sm">
																<option value="true" selected="selected">Yes</option>
																<option value="false">No</option>
															</select>
														</td>
													</tr>
													<tr>
														<td>Only if Occupied</td>
														<td>
															<select style="width: 17%;" class="form-control input-sm">
																<option value="true" selected="selected">Yes</option>
																<option value="false">No</option>
															</select>
														</td>
													</tr>
													<tr>
														<td>Delist Requirements</td>
														<td>
															<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="dr"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
														</td>
													</tr>
													<tr>
														<td>How to Deregister</td>
														<td>
															<div class="col-xs-2 noPadMar">
																<select class="form-control input-sm">
																	<option value="pdf">PDF</option>
																	<option value="website">Website</option>
																	<option value="prochamp">Prochamp</option>
																</select>
															</div>
															<div id="tdUploadD" class="col-xs-10">
																<input  style="width: 17%;" id="inUploadFileD" type="file" class="file input-sm" data-show-preview="false" />
															</div>
														</td>
														
													</tr>
													<tr>
														<td>New Owner Information Required</td>
														<td>
															<select style="width: 17%;"   class="form-control input-sm">
																<option value="true" selected="selected">Yes</option>
																<option value="false">No</option>
															</select>
														</td>
													</tr>
													<tr>
														<td>Proof of Conveyance Required</td>
														<td>
															<select style="width: 17%;"   class="form-control input-sm">
																<option value="true" selected="selected">Yes</option>
																<option value="false">No</option>
															</select>
														</td>
													</tr>
													<tr>
														<td>Date of Sale Required</td>
														<td>
															<select style="width: 17%;"   class="form-control input-sm">
																<option value="true" selected="selected">Yes</option>
																<option value="false">No</option>
															</select>
														</td>
													</tr>
												</table>
												<div class="text-right" style="margin-top: 1%;">
													<button id="btnSaveDeregistration" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
													<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
												</div>
											</div>

											<%-- Ordinance Settings --%>
											<div id="tabOrdinance" class="tab-pane">
												<table id="tblOrdinance" class="table no-border table-responsive" data-pagination="true">
													<thead>
														<tr>
															<th data-field="ordinance_num">Ordinance Number</th>
															<th data-field="ordinance_name">Ordinance Name</th>
															<th data-field="description" data-cell-style="cellStyle">Description</th>
															<th data-field="section">Section</th>
															<th data-field="source">Source</th>
															<th data-field="enacted_date">Enacted Date</th>
															<th data-field="revision_date">Revision Date</th>
															<th data-field="id" data-formatter="action"></th>
														</tr>
													</thead>
													<tbody></tbody>
												</table>
												<div class="text-left" style="margin-top: 1%;">
													<button id="btnAddOrdinance" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add New</button>
													<button id="btnHistory" type="button" class="btn btn-primary btn-sm"><i class="fa fa-history"></i>&nbsp;History</button>
												</div>
												<div class="text-right" style="margin-top: 1%;">
													<%--<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>--%>
													<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
												</div>
											</div>

											<%-- Zip Code --%>
											<div id="tabZip" class="tab-pane">
												<table id="tblZip" class="table no-border">
													<tbody></tbody>
												</table>
												<div class="text-right" style="margin-top: 1%;">
													<button id="btnSaveZip" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
													<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
												</div>
											</div>

											<%-- Notification Setting --%>
											<div class="tab-pane" id="tabNotif">
												<fieldset>
													<legend>
														<label>Registration Notification</label></legend>
													<table class="table no-border">
														<tr>
															<td style="width: 15%">Send Reminder</td>
															<td>
																<input type="text" class="form-control input-sm" />
															</td>
															<td>
																<select class="form-control input-sm">

																	<option value="Business">Business</option>
																	<option value="Calendar">Calendar</option>
																</select>
															</td>

															<td>
																<select class="form-control input-sm">

																	<option value="Days">Days</option>
																	<option value="Months">Months</option>
																</select>
															</td>
															<td>
																<select class="form-control input-sm">

																	<option value="Before">Before</option>
																	<option value="After">After</option>
																</select>
															</td>
															<td>
																<select class="form-control input-sm">

																	<option value="Vacant Registration Time Frame">Vacant Registration Time Frame</option>
																	<option value="Renewal Time Frame">Renewal Time Frame</option>
																</select>
															</td>
														</tr>

													</table>
												</fieldset>
												<fieldset>
													<legend>
														<label>Renewal Notification</label></legend>
													<table class="table no-border">
														<tr>
															<td style="width: 15%">Send Reminder</td>
															<td>
																<input type="text" class="form-control input-sm" />
															</td>
															<td>
																<select class="form-control input-sm">

																	<option value="Business">Business</option>
																	<option value="Calendar">Calendar</option>
																</select>
															</td>

															<td>
																<select class="form-control input-sm">

																	<option value="Days">Days</option>
																	<option value="Months">Months</option>
																</select>
															</td>
															<td>
																<select class="form-control input-sm">

																	<option value="Before">Before</option>
																	<option value="After">After</option>
																</select>
															</td>
															<td>
																<select class="form-control input-sm">

																	<option value="Vacant Registration Time Frame">Vacant Registration Time Frame</option>
																	<option value="Renewal Time Frame">Renewal Time Frame</option>
																</select>
															</td>
														</tr>

													</table>
												</fieldset>
												<fieldset>
													<legend>
														<label>Web Notification</label></legend>
													<table class="table no-border">
														<tr>
															<td style="width: 15%">Url</td>
															<td>
																<input type="text" class="form-control input-sm" />
															</td>
														</tr>
													</table>
												</fieldset>
												<div class="text-right" style="margin-top: 1%;">
													<button id="btnSaveNotif" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
													<button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<div class="modal fade" id="modalEdit">
		<div class="modal-dialog" role="document">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<label id="lblEdit"></label>
				</div>
				<div id="sr" class="panel-body" style="display:none;">
					<textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
				<div id="ai" class="panel-body" style="display:none;">
					<textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
				<div id="pd" class="panel-body" style="display:none;">
					<textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
				<div id="lci" class="panel-body" style="display:none;">
					<table class="table no-border">
						<tr>
							<th>Contact Person</th>
							<td>
								<input type="text" class="form-control input-sm" />
							</td>
						</tr>
						<tr>
							<th>Title</th>
							<td>
								<input type="text" class="form-control input-sm" />
							</td>
						</tr>
						<tr>
							<th>Department</th>
							<td>
								<input type="text" class="form-control input-sm" />
							</td>
						</tr>
						<tr>
							<th>Phone Number</th>
							<td>
								<input type="text" class="form-control input-sm" />
							</td>
						</tr>
						<tr>
							<th>Email Address</th>
							<td>
								<input type="text" class="form-control input-sm" />
							</td>
						</tr>
						<tr>
							<th>Mailing Address</th>
							<td>
								<input type="text" class="form-control input-sm" />
							</td>
						</tr>
						<tr>
							<th>Hours of Operation</th>
							<td>
								<div class="col-xs-1 noPadMar">from:</div>
								<div class="col-xs-2 noPadMar">
									<div class="bootstrap-timepicker">
										<input type="text" class="form-control input-sm timepicker" />
									</div>
								</div>
								<div class="col-xs-1">to:</div>
								<div class="col-xs-2 noPadMar">
									<div class="bootstrap-timepicker">
										<input type="text" class="form-control input-sm timepicker" />
									</div>
								</div>
							</td>
						</tr>
					</table>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
				<div id="rr" class="panel-body" style="display:none;">
					<textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
				<div id="ico" class="panel-body" style="display:none;">
					<textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
				<div id="icv" class="panel-body" style="display:none;">
					<textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
				<div id="dr" class="panel-body" style="display:none;">
					<textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
					<div class="text-center" style="margin-top: 1%;">
						<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalAddOrdinance">
		<div class="modal-dialog" role="document">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<label>Add Ordinance</label>
				</div>
				<div class="panel-body">
					<label id="ordID" style="display: none;"></label>
					<table id="tblAddOrd" class="table no-border">
						<tbody>
							<tr>
								<th>Ordinance Number:</th>
								<td>
									<input type="text" class="form-control input-sm" />
								</td>
							</tr>
							<tr>
								<th>Ordinance Name:</th>
								<td>
									<input type="text" class="form-control input-sm" />
								</td>
							</tr>
							<tr>
								<th>Description:</th>
								<td>
									<textarea class="form-control input-sm" style="max-height: 140px; max-width: 400px; min-height: 140px; min-width: 400px;"></textarea>
								</td>
							</tr>
							<tr>
								<th>Section:</th>
								<td>
									<input type="text" class="form-control input-sm" />
								</td>
							</tr>
							<tr>
								<th>Source:</th>
								<td>
									<input type="text" class="form-control input-sm" />
								</td>
							</tr>
							<tr>
								<th>Enacted Date:</th>
								<td>
									<input type="text" class="form-control input-sm datepicker" />
								</td>
							</tr>
							<tr>
								<th>Revision Date:</th>
								<td>
									<input type="text" class="form-control input-sm datepicker" />
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<input id="inUpload" type="file" class="file input-sm" data-show-preview="false" />
								</td>
							</tr>
							<tr>
								<td colspan="2" class="text-right">
									<button id="btnApplyOrdinance" type="button" class="btn btn-primary btn-sm"><i class="fa fa-check"></i>&nbsp;Apply</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalOrdinanceHist">
		<div class="modal-dialog" role="document" style="width: 1000px;">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<label>Add Ordinance</label>
				</div>
				<div class="panel-body">
					<table id="tblOrdinanceHist" class="table no-border">
						<thead>
							<tr>
								<th data-field="ordinance_num">Ordinance Number</th>
								<th data-field="ordinance_name">Ordinance Name</th>
								<th data-field="description">Description</th>
								<th data-field="section">Section</th>
								<th data-field="source">Source</th>
								<th data-field="enacted_date">Enacted Date</th>
								<th data-field="revision_date">Revision Date</th>
								<th data-field="added_by">Added By</th>
								<th data-field="date_added">Date Added</th>
								<th data-field="modified_by">Modified By</th>
								<th data-field="date_modified">Date Modified</th>
								<th data-field="deleted_by">Deleted By</th>
								<th data-field="date_deleted">Date Deleted</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
					<div class="text-center" style="margin-top: 1%;">
						<button class="btn btn-primary btn-sm" type="button" data-dismiss="modal" aria-label="Close"><i class="fa fa-apply"></i>Done</button>
					</div>
				</div>
			</div>
		</div>
	</div>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
	<script src="js/Workable.js"></script>
</asp:Content>
