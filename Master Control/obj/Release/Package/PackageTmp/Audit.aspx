﻿<%@ Page Title="Audit Trail" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Audit.aspx.cs" Inherits="Master_Control.Audit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Audit Trail</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-bar-search"></i>&nbsp;Audit Trail</span>
            </li>
        </ol>
    </section>

    <section class="content">
        <div class="box box-default">
            <div class="box-body">
                <table class="table no-border">
                    <tr>
                        <th>Time Frame:</th>
                        <td>
                            <select id="slctTime" class="form-control input-sm">
								<option value="" selected="selected">--Select One Below--</option>
                                <option value="year">This Year</option>
								<option value="month">This Month</option>
								<option value="week">This Week</option>
								<option value="yesterday">Yesterday</option>
								<option value="today">Today</option>
                            </select>
                        </td>
                        <th>or</th>
                        <th>From</th>
                        <td>
                            <input id="inDateFrom" type="text" class="form-control input-sm datepicker" />
                        </td>
                        <th>To</th>
                        <td>
                            <input id="inDateTo" type="text" class="form-control input-sm datepicker" />
                        </td>
                    </tr>
                    <tr>
                        <th>By:</th>
                        <td>
                            <select class="form-control input-sm">
                                <option>All</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <div class="text-right">
                    <button id="btnApply" type="button" class="btn btn-primary btn-sm"><i class="fa fa-check"></i>&nbsp;Apply</button>
                </div>

                <div class="col-xs-12" style="margin-top: 1%;">
                    <table id="tblAudit" class="table table-bordered table-hover" data-single-select="true" style="display: none">
                        <thead>
                            <tr>
                                <th data-sortable="true" data-field="page">Page</th>
                                <th data-sortable="true" data-field="sub_page">Sub Page</th>
                                <th data-sortable="true" data-field="sub_sub_page">Sub-Sub Page</th>
                                <th data-sortable="true" data-field="action">Action</th>
                                <th data-sortable="true" data-field="action_by">Action By</th>
                                <th data-sortable="true" data-field="action_date">Action Date</th>
                                <th data-sortable="true" data-field="action_time">Action Time</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
    <script src="js/Audit.js"></script>

</asp:Content>