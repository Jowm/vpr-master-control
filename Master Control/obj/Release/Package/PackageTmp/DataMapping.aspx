﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="DataMapping.aspx.cs" Inherits="Master_Control.DataMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<link href="dist/css/dropzone.css" rel="stylesheet" />
	<style>
		table tbody tr:hover {
			background: #81c341;
			cursor: pointer;
		}

		table tbody tr.selected {
			background: #81c341;
		}

		#ulSETTINGS li input[type='number'] {
			width: 51px;
			padding: 3px 8px;
			height: 28px;
		}

		select {
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;
			padding: 1px 10px;
		}

		.ultraselect-1 {
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;
			padding: 3px 10px;
		}

		* {
			margin: 0;
			padding: 0;
		}

		.divbox {
			float: left;
			width: 100%;
			padding-left: 0;
			text-align: center;
			border: 5px solid #999;
		}

		.slot {
			position: relative;
			width: 100%;
			padding: 5px;
			border: 1px dotted;
			padding: 5px;
			background: #9cd09c;
			height: 37px !important;
			font-size: 16px;
		}

		.item {
			height: 28px;
			word-wrap: break-word;
			z-index: 1;
			background-color: #CCC;
			position: absolute;
			width: 97% !important;
		}

		.ui-selecting {
			background: #FECA40;
		}

		.ui-selected {
			background-color: #F90;
		}

		.green3 {
			background-color: #D9FFE2;
		}

		span {
			-webkit-animation-duration: 3s !important;
			-moz-animation-duration: 3s !important;
			-o-animation-duration: 3s !important;
			animation-duration: 3s !important;
			display: inline-block;
		}

		#divUPLOADER {
			-webkit-animation-duration: 1s !important;
			-moz-animation-duration: 1s !important;
			-o-animation-duration: 1s !important;
			animation-duration: 1s !important;
			display: inline-block;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
	<section class="content-header">
		<h1>Data Mapping</h1>
		<ol class="breadcrumb">
			<li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
				<span><i class="fa fa-map-marker"></i>&nbsp;Mappings</span>
			</li>
			<li class="active">
				<span><i class="fa fa-sitemap"></i>&nbsp;Data Mapping</span>
			</li>
		</ol>
	</section>
	<section class="content">
		<div class="box box-default">
			<div class="box-body">
				<div class="col-xs-12" style="padding-left: 0px;">
					<div class="panel panel-primary">
						<div class="panel-heading text-center">
							<label>Data Mapping</label>
						</div>
						<div class="panel-body">
							<div class="row " id="divUPLOADER" style="display: none;">
								<div class="container">
									<div class='row'>
										<div class='col-md-8 col-md-offset-2'>
											<div class="panel panel-default ">
												<div class="panel-heading text-center">Upload File</div>
												<div class="panel-body" style="padding: 36px;">
													<div class="col-md-12 dropzone">
														<div class="dz-default dz-message">Click or DropFile Here</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-3">
															<label>File Name:</label>
														</div>
														<div class="col-md-4">
															<label id="lblFN"></label>
														</div>
													</div>
													<div class="col-md-12">
														<div class="col-md-3">
															<label>Sheet Name:</label>
														</div>
														<div class="col-md-4">
															<input id="inSN" type="text" class="form-control input-sm" />
														</div>
													</div>
													<div class="col-md-12">
														<button id="btnGetHead" type="button" class="btn btn-primary btn-sm">Get Headers</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" id="divCONTENT">
								<div class="panel panel-default">
									<div class="panel-heading text-center">Field Mapping</div>
									<div class="panel-body">
										<div class='row col-md-12'>
											<div class="col-md-4 text-left" style="padding: 6px;">
												<button type="button" class="btn btnse" id="btnBACK">Back</button>
											</div>
											<div class="col-md-4 text-left">
												Please map Fields to match system field resources: 
											</div>
											<div class="col-md-4 text-right" style="padding: 6px;">
												<button type="button" class="btn btnse" data-toggle="modal" data-target="#mdlMAPPING" id="submitFILE">Save</button>
											</div>
										</div>
										<div class='row col-md-12'>
											<div class="col-md-3 ">
												<div class="panel panel-primary">

													<div class="panel-heading text-center">
														<label>Client's Excel Header</label>
													</div>
													<div class="panel-body">
														<div class='col-md-12' id="listB">
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-9 " id="divMAIN">
												<div class="panel panel-primary multiplier">
													<div class="panel-heading text-center">
														<%--<button type="button" class="btn btn-link pull-right" aria-label="Close">
															<span aria-hidden="true"></span>
															<i class="fa fa-close"></i>
														</button>--%>
														<label>Data Grouping</label>
													</div>
													<div class="panel-body">
														<div class='col-md-12'>
															<div class="form-group row">
																<div class="col-md-2 text-right">
																	<label class="text-black">GroupName</label>
																</div>
																<div class="col-md-4">
																	<input type="text" class="txtGROUPNAME" style="width: 100%;" />
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class=" panel panel-primary">
																<div class="panel-heading text-center">
																	<label>Header</label>
																</div>
																<div class="panel-body">
																	<div class='col-md-12 divbox divSTANDARD'>
																		<div id="B1" class="slot col-md-2 ui-droppable"></div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="panel panel-primary">
																<div class="panel-heading text-center">
																	<label>Preview</label>
																</div>
																<div class="panel-body">
																	<div class='col-md-12 divbox divSAMPLEDATA'>
																		<div id="B1" class="slot col-md-2 ui-droppable"></div>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-md-4 col-md-offset-8">
															<button type="button" class="btn btnADD">Add New Group</button>
															<button type="button" class="btn btnRMV" style="display: none;">Remove Group</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="mdlMAPPING">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">×</span></button>
					<h4 class="modal-title">Mapping Name</h4>
				</div>
				<div class="modal-body">
					<div class="form-group row">
						<div class="col-md-2 text-right">
							<label class="text-black">Name</label>
						</div>
						<div class="col-md-9">
							<input type="text" style="width: 100%;" id="txtNAME" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 text-right">
							<label class="text-black">Source</label>
						</div>
						<div class="col-md-9">
							<input type="text" style="width: 100%;" id="txtSOURCE" />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 text-right">
							<label class="text-black">Location</label>
						</div>
						<div class="col-md-9">
							<input type="text" style="width: 100%;" id="txtLOCATION" />
						</div>
					</div>-
					<div class="form-group row">
						<div class="col-md-2 text-right">
							<label class="text-black">Description</label>
						</div>
						<div class="col-md-9">
							<textarea style="width: 100%; height: 80px;" id="txtDESCRIPTION"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-2 text-right">
							<label class="text-black">Settings</label>
						</div>
						<div class="col-md-9" style="padding-left: 0; padding-right: 0;">
							<div class="form-group" style="margin-bottom: 40px;">
								<div class="col-md-12">
									<select style="width: 100px;" id="optOCCURENCE">
										<option value="D">Daily</option>
										<option value="W">Weekly</option>
										<option value="M">Monthly</option>
										<option value="Y">Yearly</option>
									</select>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6">
									<div class="col-md-4 text-right">
										<label class="text-black">Start Date</label>
									</div>
									<div class="col-md-8">
										<input type="text" style="width: 70%;" id="txtSTARTDATE" class="text-date" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="col-md-4 text-right">
										<label class="text-black">End Date</label>
									</div>
									<div class="col-md-8">
										<input type="text" style="width: 80%;" id="txtENDDATE" class="text-date" />
										<input type="checkbox" title="Check if no End date" id="chkNOENDDATE" />
									</div>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-6">
									<div class="col-md-4 text-right">
										<label class="text-black">Start Time</label>
									</div>
									<div class="col-md-8">
										<input type="time" style="width: 60%;" id="txtSTARTTIME" />
										<select style="width: 35%; display: none;">
											<option>AM</option>
											<option>PM</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="col-md-4 text-right">
										<label class="text-black">End Time</label>
									</div>
									<div class="col-md-8">
										<input type="time" style="width: 60%;" id="txtENDTIME" />
										<select style="width: 35%; display: none;">
											<option>AM</option>
											<option>PM</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="btnSUBMIT">Submit</button>
				</div>
			</div>
		</div>
	</div>
	<div id="divLOADER" style="display: none; position: absolute; top: 1px; width: 100%; min-height: 1000px; text-align: center; background-color: rgba(0,0,0,.5); z-index: 10000">
		<div id="container" style="margin-top: 20%;">
			<div class="ring green"></div>
			<div id="content">
				<span>L   O   A   D   I  N  G </span>
			</div>
		</div>
	</div>
	<div class="clones" style="display: none;">
		<div class="panel panel-primary multiplier">
			<div class="panel-heading text-center">
				<label>Data Grouping</label>
			</div>
			<div class="panel-body">
				<div class='col-md-12'>
					<div class="form-group row">
						<div class="col-md-2 text-right">
							<label class="text-black">GroupName</label>
						</div>
						<div class="col-md-4">
							<input type="text" class="txtGROUPNAME" style="width: 100%;" id="" />
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class=" panel panel-primary">
						<div class="panel-heading text-center">
							<label>Header</label>
						</div>
						<div class="panel-body">
							<div class='col-md-12 divbox divSTANDARD'>
								<div id="B1" class="slot col-md-2 ui-droppable"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="panel panel-primary">
						<div class="panel-heading text-center">
							<label>Preview</label>
						</div>
						<div class="panel-body">
							<div class='col-md-12 divbox divSAMPLEDATA'>
								<div id="B1" class="slot col-md-2 ui-droppable"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-md-offset-8">
					<button type="button" class="btn btnADD">Add New Group</button>
					<button type="button" class="btn btnRMV">Remove Group</button>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="script">
	<script src="js/dropzone.js"></script>
	<script src="js/ajax.js"></script>
	<script src="js/DataMapping.js"></script>
</asp:Content>
