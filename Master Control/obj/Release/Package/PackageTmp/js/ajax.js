﻿function busconnect(url, data, funcname, myresult) {
    if (data == '1') {
        $.ajax({
            type: "POST",
            url: url + funcname,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                myresult(result.d);
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    } else {
        $.ajax({
            type: "POST",
            url: url + funcname,
            data: data,
            contentType: 'application/json; charset=utf-8',
            success: function (result) {
                myresult(result.d);
            },
            failure: function (response) {
                alert(response.d);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });
    }

    
}
