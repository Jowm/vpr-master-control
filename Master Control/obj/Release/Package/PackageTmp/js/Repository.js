﻿$(document).ready(function () {
	getPDF();
	getData();
	getCustom();
});

$('#ulNav li button').click(function () {
	var btn = $(this);
	var alllibtn = btn.closest('#ulNav').find('li .btn');
	var closeli = btn.closest('li');

	alllibtn.removeClass('active');
	btn.addClass('active');

});

function dateFormat(value, row, index) {
	if (value != null) {
		return moment(value).format('MM-DD-YYYY HH:MM:SSS');
	} else {
		return '-';
	}
}

function getPDF() {
	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/GetPDF',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;

				$('#tblPDF').bootstrapTable('destroy');
				$('#tblPDF').bootstrapTable({
				    data: records,
				    search: true,
				    pagination: true,
				    pageSize: 5,
				    pageList: [5, 10, 15]
				});
			}
		},
		error: function (reponse) {
			console.log(reponse.reponseText);
		}
	});
}

function getData() {
	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/GetData',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;

				$('#tblData').bootstrapTable('destroy');
				$('#tblData').bootstrapTable({
				    data: records,
				    search: true,
				    pagination: true,
				    pageSize: 5,
				    pageList: [5, 10, 15]
				});
			}

			$('#modalLoading').modal('hide');
		},
		error: function (reponse) {
			console.log(reponse.reponseText);
		}
	});
}

function getCustom() {
	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/GetCustom',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;

				$('#tblCustom').bootstrapTable('destroy');
				$('#tblCustom').bootstrapTable({
				    data: records,
				    search: true,
				    pagination: true,
				    pageSize: 5,
				    pageList: [5, 10, 15]
				});
			}
		},
		error: function (reponse) {
			console.log(reponse.reponseText);
		}
	});
}

function pdflink(val) {
	return '<a data-id="' + val + '" class="btn btn-link" onclick="openPDF(this);">' + val + '</a>';
}

function datalink(val) {
	return '<a class="btn btn-link" onclick="openData(this);">' + val + '</a>';
}

function customlink(val) {
	return '<a class="btn btn-link" onclick="openCustom(this);">' + val + '</a>';
}

function openPDF(val) {
	$('#modalPDF').modal({ backdrop: 'static', keyboard: false });
	$('#lblPDF').text('PDF Name: ' + $(val).text());
	var descVal = $(val).closest('td').next();
	console.log(descVal);
	if (descVal.text() != "-") {
		$('#pdf').replaceWith('<iframe id="pdf" style="width: 100%; height: 682px; border: 0;"></iframe>');
		$('#pdf').attr('src', '../Files/Mapped Files/' + $(val).attr('data-id'));
	} else {
		$('#pdf').replaceWith('<div id="pdf" class="text-center"><h2 class="text-red">No Saved PDF Found!</h2></div>');
	}
}

function openData(val) {
	$('#modalData').modal({ backdrop: 'static', keyboard: false });

	var groupname = $(val).text();

	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/GetFieldsData',
		data: '{groupname: "' + groupname + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				console.log(records);

				//var icon = (records.length > 1) ? "fa-plus" : "fa-minus";

				$('#divFieldsData').empty();
				var headers = [];
				var values = [];

				var group = [];
				var ct = 0;
				$.each(records, function (indx, grp) {
					if ($.inArray(grp.GroupName, group) == -1) {
						ct++;
						group.push(grp.GroupName);
						var tblCount = (indx + 1);
						//var cust = (grp.custom_tag == 'True') ? 'custom' : '';
						$('#divFieldsData').append(
							'<div class="box box-default data">' +
								'<div class="box-header text-center">' +
									'<label class="box-title">' + grp.GroupName + '</label>' +
									'<div class="box-tools pull-right">' +
										'<button type="button" class="btn btn-box-tool" data-widget="collapse">' +
											'<i class="fa"></i>' +
										'</button>' +
									'</div>' +
								'</div>' +
								'<div id="divGroupFieldsData_' + tblCount + '" class="box-body">' +
									'<div class="col-xs-6 text-center">' +
										'<label>Headers</label>' +
									'</div>' +
									'<div class="col-xs-6 text-center">' +
										'<label>Values</label>' +
									'</div>' +
									//'<div class="col-xs-4 text-center">' +
									//	'<label>Action</label>' +
									//'</div>' +
								'</div>' +
							'</div>'
						);

						$.each(records, function (idx, val) {
							if (grp.GroupName == val.GroupName) {
								//var updt = '<button type="button" class="btn btn-link" data-id="' + val.FieldId + '" onclick="editData(this);"><i class="fa fa-pencil-square-o"></i></button>';
								//var del = '<button type="button" class="btn btn-link" data-id="' + val.FieldId + '" onclick="delData(this);"><i class="fa fa-trash-o"></i></button>';

								$('#divGroupFieldsData_' + tblCount).append(
									'<div class="row noPadMar">' +
										'<div class="col-xs-6 text-center header"><span>' + val.FieldName + '</span></div>' +
										'<div class="col-xs-6 text-center value"><span>' + val.SampleData + '</span></div>' +
										//'<div class="col-xs-4 text-center">' + updt + '&nbsp;' + del + '</div>' +
									'</div>'
								);
							}
						});
					}
				});

				$('div.data').addClass((ct > 1) ? 'collapsed-box' : '');
				$('div.data i').addClass((ct > 1) ? 'fa-plus' : 'fa-minus');
				
			}
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		}
	});
}

function openCustom(val) {
	$('#modalCustom').modal({ backdrop: 'static', keyboard: false });

	var groupname = $(val).text();

	$('#modalLoading').modal({ backdrop: 'static', keyboard: false });
	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/GetFieldsCust',
		data: '{groupname: "' + groupname + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			var d = $.parseJSON(data.d);

			if (d.Success) {
				var records = d.data.record;
				console.log(records);

				var icon = (records.length > 1) ? "fa-plus" : "fa-minus";

				$('#divFieldsCustom').empty();
				var headers = [];
				var values = [];

				var group = [];

				$.each(records, function (indx, grp) {
					if ($.inArray(grp.GroupName, group) == -1) {
						group.push(grp.GroupName);
						var tblCount = (indx + 1);
						var cust = (grp.custom_tag == 'True') ? 'custom' : '';
						$('#divFieldsCustom').append(
							'<div class="box box-default ' + cust + '">' +
								'<div class="box-header text-center">' +
									'<label class="box-title">' + grp.GroupName + '</label>' +
								'</div>' +
								'<div id="divGroupFieldsCustom_' + tblCount + '" class="box-body">' +
									'<div class="col-xs-4 text-center">' +
										'<label>Headers</label>' +
									'</div>' +
									'<div class="col-xs-4 text-center">' +
										'<label>Values</label>' +
									'</div>' +
									'<div class="col-xs-4 text-center">' +
										'<label>Action</label>' +
									'</div>' +
								'</div>' +
							'</div>'
						);
						
						$.each(records, function (idx, val) {
							if (grp.GroupName == val.GroupName) {
								var updt = '<button type="button" class="btn btn-link" data-id="' + val.FieldId + '" onclick="editCust(this);"><i class="fa fa-pencil-square-o"></i></button>';
								var del = '<button type="button" class="btn btn-link" data-id="' + val.FieldId + '" onclick="delCust(this);"><i class="fa fa-trash-o"></i></button>';

								$('#divGroupFieldsCustom_' + tblCount).append(
									'<div class="row noPadMar">' +
										'<div class="col-xs-4 text-center header"><span>' + val.FieldName + '</span></div>' +
										'<div class="col-xs-4 text-center value"><span>' + val.SampleData + '</span></div>' +
										'<div class="col-xs-4 text-center">' + updt + '&nbsp;' + del + '</div>' +
									'</div>'
								);
							}
						});
					}
				});
			}
			$('#modalLoading').modal('hide');
		},
		error: function (response) {
			console.log(response.responseText);
		}
	});
}

//function editData(val) {
//	var id = $(val).attr('data-id');
//	var header = $(val).closest('.row').find('.header span').html();
//	var value = $(val).closest('.row').find('.value span').html();

//	$(val).closest('.row').find('.header span').replaceWith('<input type="text" class="form-control input-sm" value="' + header + '" />')
//	$(val).closest('.row').find('.value span').replaceWith('<input type="text" class="form-control input-sm" value="' + value + '" />')
//	$(val).next().replaceWith('<button type="button" class="btn btn-link" data-id="' + header + '_' + value + '_' + id + '" onclick="cancelEditData(this);"><i class="fa fa-ban"></i></button>');
//	$(val).replaceWith('<button type="button" class="btn btn-link" data-id="' + id + '" onclick="saveEditData(this);"><i class="fa fa-save"></i></button>');
//}

//function delData(val) {
//	var id = $(val).attr('data-id');

//	$.ajax({
//		type: 'POST',
//		url: 'Repository.aspx/DeleteEditData',
//		data: '{id: "' + id + '"}',
//		contentType: 'application/json; charset=utf-8',
//		success: function (data) {
//			if (data.d == '1') {
//				alert('Record Deleted!');

//				$(val).closest('.row').remove();
//			}
//		},
//		error: function (response) {
//			console.log(response.responseText);
//		}
//	});
//}

//function saveEditData(val) {
//	var id = $(val).attr('data-id');
//	var header = $(val).closest('.row').find('.header input').val();
//	var value = $(val).closest('.row').find('.value input').val();

//	var myData = [id, header, value];

//	$.ajax({
//		type: 'POST',
//		url: 'Repository.aspx/SaveEditData',
//		data: '{myData: ' + JSON.stringify(myData) + '}',
//		contentType: 'application/json; charset=utf-8',
//		success: function (data) {
//			if (data.d == '1') {
//				alert('Record Updated!');

//				var updt = '<button type="button" class="btn btn-link" data-id="' + id + '" onclick="editData(this);"><i class="fa fa-pencil-square-o"></i></button>';
//				var del = '<button type="button" class="btn btn-link" data-id="' + id + '" onclick="delEditData(this);"><i class="fa fa-trash-o"></i></button>';

//				$(val).closest('.row').find('.header input').replaceWith('<span>' + header + '</span>');
//				$(val).closest('.row').find('.value input').replaceWith('<span>' + value + '</span>');
//				$(val).next().replaceWith(del);
//				$(val).replaceWith(updt);
//			}
//		},
//		error: function (response) {
//			console.log(response.responseText);
//		}
//	});
//}

//function cancelEditData(val) {
//	var data = $(val).attr('data-id');
//	var arr = data.split('_');

//	var updt = '<button type="button" class="btn btn-link" data-id="' + arr[2] + '" onclick="editData(this);"><i class="fa fa-pencil-square-o"></i></button>';
//	var del = '<button type="button" class="btn btn-link" data-id="' + arr[2] + '" onclick="delData(this);"><i class="fa fa-trash-o"></i></button>';
	
//	$(val).closest('.row').find('.header input').replaceWith('<span>' + arr[0] + '</span>');
//	$(val).closest('.row').find('.value input').replaceWith('<span>' + arr[1] + '</span>');
//	$(val).prev().replaceWith(updt);
//	$(val).replaceWith(del);
//}

function editCust(val) {
	var id = $(val).attr('data-id');
	var header = $(val).closest('.row').find('.header span').html();
	var value = $(val).closest('.row').find('.value span').html();

	$(val).closest('.row').find('.header span').replaceWith('<input type="text" class="form-control input-sm" value="' + header + '" />')
	$(val).closest('.row').find('.value span').replaceWith('<input type="text" class="form-control input-sm" value="' + value + '" />')
	$(val).next().replaceWith('<button type="button" class="btn btn-link" data-id="' + header + '_' + value + '_' + id + '" onclick="cancelEditCust(this);"><i class="fa fa-ban"></i></button>');
	$(val).replaceWith('<button type="button" class="btn btn-link" data-id="' + id + '" onclick="saveEditCust(this);"><i class="fa fa-save"></i></button>');
}

function delCust(val) {
	var id = $(val).attr('data-id');
	var usr = $('#usr').text();
	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/DeleteEditCust',
		data: '{id: "' + id + '", usr: "' + usr + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			if (data.d == '1') {
				alert('Record Deleted!');

				$(val).closest('.row').remove();
			}
		},
		error: function (response) {
			console.log(response.responseText);
		}
	});
}

function saveEditCust(val) {
    var id = $(val).attr('data-id');
	var header = $(val).closest('.row').find('.header input').val();
	var value = $(val).closest('.row').find('.value input').val();
	var usr = $('#usr').text();

	var myData = [id, header, value];

	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/SaveEditCust',
		data: '{myData: ' + JSON.stringify(myData) + ', usr: "' + usr + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			if (data.d == '1') {
				alert('Record Updated!');

				var updt = '<button type="button" class="btn btn-link" data-id="' + id + '" onclick="editCust(this);"><i class="fa fa-pencil-square-o"></i></button>';
				var del = '<button type="button" class="btn btn-link" data-id="' + id + '" onclick="delEditCust(this);"><i class="fa fa-trash-o"></i></button>';

				$(val).closest('.row').find('.header input').replaceWith('<span>' + header + '</span>');
				$(val).closest('.row').find('.value input').replaceWith('<span>' + value + '</span>');
				$(val).next().replaceWith(del);
				$(val).replaceWith(updt);
			}
		},
		error: function (response) {
			console.log(response.responseText);
		}
	});
}

function cancelEditCust(val) {
	var data = $(val).attr('data-id');
	var arr = data.split('_');

	var updt = '<button type="button" class="btn btn-link" data-id="' + arr[2] + '" onclick="editCust(this);"><i class="fa fa-pencil-square-o"></i></button>';
	var del = '<button type="button" class="btn btn-link" data-id="' + arr[2] + '" onclick="delCust(this);"><i class="fa fa-trash-o"></i></button>';

	$(val).closest('.row').find('.header input').replaceWith('<span>' + arr[0] + '</span>');
	$(val).closest('.row').find('.value input').replaceWith('<span>' + arr[1] + '</span>');
	$(val).prev().replaceWith(updt);
	$(val).replaceWith(del);
}

function action(val) {
	var updt = '<button type="button" class="btn btn-link" data-id="' + val + '" onclick="editRow(this);"><i class="fa fa-pencil-square-o"></i></button>';
	var del = '<button type="button" class="btn btn-link" data-id="' + val + '" onclick="delRow(this);"><i class="fa fa-trash-o"></i></button>';

	return updt + '&nbsp;' + del;
}

function delRow(val) {
	var active = $('#ulNav li .btn.active').text();
	var param = $(val).attr('data-id');
	var usr = $('#usr').text();
	
	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/DeleteRow',
		data: '{active: "' + active + '", param: "' + param + '", usr: "' + usr + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			if (data.d == '1') {
				getPDF();
				getData();
				getCustom();
				alert('Record deleted!');
			} else {
				alert(data.d);
			}
		},
		error: function (response) {
			console.log(response.responseText);
		}
	});
}

function editRow(val) {
	$('#modalEdit').modal({ backdrop: 'static', keyboard: false });

	var id = $(val).attr('data-id');
	var name = $(val).closest('tr').find('td:eq(0)').text();
	var desc = $(val).closest('tr').find('td:eq(1)').text();

	$('#inName').val(name);

	$('#btnSave').attr('data-id', id);

	var active = $('#ulNav li .btn.active').text();

	if (active == 'PDF' || active == 'Data') {
		$('#inName').closest('.row').find('label').text('File Name:');

		if (active == 'PDF') {
			if (desc != '-') {
				$('#taDesc').val(desc);
				$('#taDesc').prop('disabled', false);
			} else {
				$('#taDesc').val(desc);
				$('#taDesc').prop('disabled', true);
			}
		} else {
			$('#taDesc').val(desc);
			$('#taDesc').prop('disabled', false);
		}
	} else if (active == 'Custom Data') {
		$('#inName').closest('.row').find('label').text('Group Name:');
		$('#taDesc').val(desc);
		$('#taDesc').prop('disabled', false);
	}
}

$('#btnSave').click(function () {

    var id = $(this).attr('data-id');
	var name = $('#inName').val();
	var desc = $('#taDesc').val();
	var active = $('#ulNav li .btn.active').text();
	var usr = $('#usr').text();

	var myData = [id, name, desc, active];

	$.ajax({
		type: 'POST',
		url: 'Repository.aspx/UpdateRep',
		data: '{myData: ' + JSON.stringify(myData) + ', usr: "' + usr + '"}',
		contentType: 'application/json; charset=utf-8',
		success: function (data) {
			if (data.d == '1') {
				$('#modalEdit').modal('hide');
				alert('Record Updated!');
				getPDF();
				getData();
				getCustom();
			} else {
				alert(data.d);
			}
		},
		error: function (response) {
			console.log(response.responseText);
		}
	});
});