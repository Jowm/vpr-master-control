﻿$(document).ready(function () {
    //getVal();
    //getList();
});

$('#slct').change(function () {

    var val = $('#slct').val();

    $.ajax({
        type: 'POST',
        url: 'Standardization.aspx/GetList',
        data: '{header: "' + val + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                if (val == "occupancy") {
                    $('.prop').hide();
                    $('.occu').show();

                    $('#tblOccupancy tbody').empty();
                    $.each(records, function (idx, val) {
                        $('#tblOccupancy tbody').append(
                            '<tr>' +
                                '<td><label class="lblOcc">' + val.Normalize + '</label><input type="text" class="form-control input-sm tbOcc" style="display:none;" value="'+val.Normalize+'" /></td>' +
                                '<td>' + dropDown1() + '</td>' +
                                '<td>' + deleteRow(val.id) + '</td>' +
                            '</tr>'
                        );
                    });                    

                } else if (val == "property") {
                    $('.prop').show();
                    $('.occu').hide();

                    $('#tblProperty tbody').empty();
                    $.each(records, function (idx, val) {
                        $('#tblProperty tbody').append(
                            '<tr>' +
                                '<td><label class="lblProp">' + val.Normalize + '</label><input type="text" class="form-control input-sm tbProp" style="display:none;" value="' + val.Normalize + '" /></td>' +
                                '<td>' + dropDown1() + '</td>' +
                                '<td>' + dropDown2() + '</td>' +
                                '<td>' + deleteRow(val.id) + '</td>' +
                            '</tr>'
                        );
                    });
                } else {
                    $('.occu').hide();
                    $('.prop').hide();
                }
                getLabels(val);
            }
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
});

function dropDown1() {

    var select = '<select class="form-control input-sm noPadMar slctLabel"></select>';

    return select;
}

function dropDown2() {
    var select1 = '<select class="form-control input-sm noPadMar slctLabel2">';
    select1 += '<option values="Commercial">Commercial</option>';
    select1 += '<option values="Condo">Condo</option>';
    select1 += '<option values="Mobile Home">Mobile Home</option>';
    select1 += '<option values="Residential">Residential</option>';
    select1 += '<option values="Townhome">Townhome</option>';
    select1 += '<option values="Vacant Lot">Vacant Lot</option>';
    select1 += '</select>';

    return select1;
}

function getLabels(val) {

    $.ajax({
        type: 'POST',
        url: 'Standardization.aspx/GetNormalize',
        data: '{normalize: "' + val + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            var d = $.parseJSON(data.d);

            if (d.Success) {
                var records = d.data.record;

                $('.slctLabel').empty();

                $.each(records, function (idx, val) {
                    $('.slctLabel').append('<option value="' + val.Normalize + '">' + val.Normalize + '</option>');
                });
            }
        }

    });
}

//function getSelect() {

//    $.ajax({
//        type: "POST",
//        url: "Standardization.aspx/GetNormalize",
//        contentType: "application/json; charset=utf-8",
//        success: function (data) {
//            var d = $.parseJSON(data.d);

//            if (d.Success) {
//                var records = d.data.record;

//                $.each(records, function (idx, val) {
//                    $('#municipality').append(
//                        '<option value="' + val.Normalize + '">' + val.Normalize + '</option>'
//                    );
//                });
//            }

//        }
//    });
//}

//function saveRow() {

//}

function deleteRow(val) {
    var updt = '<button type="button" class="btn btn-link btnEdit" data-id="' + val + '" onclick="updtRow($(this))"><i class="fa fa-pencil-square-o"></i></button>';
    var cancel = '<button type="button" class="btn btn-link text-red btncancel" data-id="' + val + '" onclick="cancel($(this))" style = "display: none;"><i class="fa fa-ban"></i></button>';
    var del = '<button type="button" class="btn btn-link text-red" data-id="' + val + '" onclick="delRow($(this))"><i class="fa fa-trash"></i></button>';
    var save = '<button type="button" class="btn btn-link text-green" data-id="' + val + '" onclick="save($(this))"><i class="fa fa-save"></i></button>';

    return updt +cancel+ save;
}

function updtRow(val) {


    $(val).closest('tr').find('td:eq(0) .lblOcc').hide();
    $(val).closest('tr').find('td:eq(0) .tbOcc').show();

    $(val).closest('tr').find('td:eq(0) .lblProp').hide();
    $(val).closest('tr').find('td:eq(0) .tbProp').show();

    $(val).hide();
    $(val).next().show();
}

function cancel(val) {
    $(val).closest('tr').find('td:eq(0) .lblOcc').show();
    $(val).closest('tr').find('td:eq(0) .tbOcc').hide();

    $(val).closest('tr').find('td:eq(0) .lblProp').show();
    $(val).closest('tr').find('td:eq(0) .tbProp').hide();

    $(val).hide();
    $(val).prev().show();
}

function save(val) {
    var tbOcc = $(val).closest('tr').find('td:eq(0) .tbOcc').val();
    var lblOcc = $(val).closest('tr').find('td:eq(0) .lblOcc');

    var tbProp = $(val).closest('tr').find('td:eq(0) .tbProp').val();
    var lblProp = $(val).closest('tr').find('td:eq(0) .lblProp');

    lblProp.text(tbProp);
    $(val).closest('tr').find('td:eq(0) .tbProp').hide();
    $(val).closest('tr').find('td:eq(0) .lblProp').show();

    lblOcc.text(tbOcc);
    $(val).closest('tr').find('td:eq(0) .tbOcc').hide();
    $(val).closest('tr').find('td:eq(0) .lblOcc').show();

    $(val).prevAll('.btncancel').hide();

    $(val).prevAll('.btnEdit').show();

    var slctHdr = $('#slct').val();
    var dataid = dataid = $(val).attr('data-id');
    var occNorm = '', stLbl = '', regCondClass = '';
    var usr = $('#usr').text();

    var myData = [];

    if (slctHdr == 'occupancy') {
        occNorm = $(val).closest('tr').find('td:eq(1) select').val();

        myData = [dataid, tbOcc, occNorm];
    } else {
        stLbl = $(val).closest('tr').find('td:eq(1) select').val();
        regCondClass = $(val).closest('tr').find('td:eq(2) select').val();

        myData = [dataid, tbProp, stLbl, regCondClass]
    }

    $.ajax({
        type: 'POST',
        url: 'Standardization.aspx/Save',
        data: '{myData: ' + JSON.stringify(myData) + ', slctHdr: "' + slctHdr + '", usr: "' + usr + '"}',
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            console.log(data);
        }
    });
}