﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string GetList(string header)
        {
            clsConnection cls = new clsConnection();

            string query = "";
                
            if (header == "occupancy") {
                query = "select id, occupancy_status [Normalize], normalize from tbl_VPR_OccupancyStatus_Normalize group by id, occupancy_status, normalize";
            }
            else if (header == "property")
            {
                query = "select id, property_type [Normalize], normalize,col_name from tbl_VPR_PropertyType_Normalize order by id";
            }

            DataTable dt = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string GetNormalize(string normalize)
        {
            clsConnection cls = new clsConnection();

            string qry = "";

            if(normalize == "occupancy")
            {
                qry = "select id,Name [Normalize] from tbl_VPR_Standardized where Type = 'Occupancy Status' order by Name";
            }
            else if(normalize == "property")
            {
                qry = "select id,Name [Normalize] from tbl_VPR_Standardized where Type = 'Property Type' order by Name";
            }
            

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }


        //[WebMethod]
        //public static string GetColumn()
        //{
        //    clsConnection cls = new clsConnection();

        //    string qry = "select distinct(col_name) [ColName] from tbl_VPR_PropertyType_Normalize";

        //    DataTable dt = cls.GetData(qry);

        //    return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        //}

        [WebMethod]
        public static void Save(ArrayList myData, string slctHdr, string usr)
        {
            clsConnection cls = new clsConnection();

            string qry = "";

            if (slctHdr == "occupancy")
            {
                qry = "update tbl_VPR_OccupancyStatus_Normalize set occupancy_status = '" + myData[1] + "', normalize = '" + myData[2] + "' where id = " + myData[0];
            }
            else
            {
                qry = "update tbl_VPR_PropertyType_Normalize set property_type = '" + myData[1] + "', normalize = '" + myData[2] + "', col_name = '" + myData[3] + "' where id = " + myData[0];
            }

            cls.ExecuteQuery(qry);
        }
    }
}