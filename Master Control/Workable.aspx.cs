﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class Workable : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		[WebMethod]
		public static string GetState()
		{
			clsConnection cls = new clsConnection();
			DataTable dtState = new DataTable();

			string query = "select DISTINCT(LTRIM(RTRIM(State))) [State] from tbl_DB_US_States (nolock) where State is not null and State <> '' order by [State]";

			dtState = cls.GetData(query);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtState } });
		}

		[WebMethod]
		public static string GetCity(string state)
		{
			clsConnection cls = new clsConnection();
			DataTable dtCity = new DataTable();

			string query = "";

			if (state != "")
			{
				query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where LTRIM(RTRIM(State)) = '" + state + "' and City is not null and City <> '' order by [City]";
			}
			else
			{
				query = "select DISTINCT(LTRIM(RTRIM(City))) [City] from tbl_DB_US_States (nolock) where City is not null and City <> '' order by [City]";
			}

			dtCity = cls.GetData(query);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtCity } });
		}

		[WebMethod]
		public static string GetZip(string city)
		{
			clsConnection cls = new clsConnection();
			DataTable dtZip = new DataTable();

			string query = "";

			if (city != "")
			{
				query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where LTRIM(RTRIM(City)) = '" + city + "' and ZipCode is not null and ZipCode <> '' order by [ZipCode]";
			}
			else
			{
				query = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [ZipCode], isZipActive from tbl_DB_US_States (nolock) where ZipCode is not null and ZipCode <> '' order by [ZipCode]";
			}


			dtZip = cls.GetData(query);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtZip } });
		}

		[WebMethod]
		public static string GetOrdinance(string state, string city, string hist)
		{
			clsConnection cls = new clsConnection();
			DataTable dtOrdinance = new DataTable();

			string query = "";

			if (hist != "" && hist != "undefined")
			{
				if (hist == "hist")
				{
					query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date], added_by, date_added, modified_by, date_modified, deleted_by, date_deleted from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "'";
				}
				else
				{
					query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date] from tbl_VPR_Workable_Ordinance (nolock) where id = '" + hist + "'";
				}
			}
			else
			{
				query = "select id, ordinance_num, ordinance_name, description, section, source, Convert(varchar, CAST(enacted_date as date), 101) [enacted_date], Convert(varchar, CAST(revision_date as date), 101) [revision_date] from tbl_VPR_Workable_Ordinance (nolock) where state = '" + state + "' and city = '" + city + "' and date_deleted is null";
			}

			dtOrdinance = cls.GetData(query);

			return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtOrdinance } });
		}

		[WebMethod]
		public static void DeleteOrdinance(string id)
		{
			clsConnection cls = new clsConnection();

			string query = "update tbl_VPR_Workable_Ordinance set date_deleted = GETDATE() where id = '" + id + "'";

			try
			{
				cls.ExecuteQuery(query);

				ArrayList arrPages = new ArrayList();
				arrPages.Add("Ordinance Settings");
				arrPages.Add("");

				cls.FUNC.Audit("Deleted Ordinance Id: " + id, arrPages);
			}
			catch (Exception)
			{

			}
		}

		//[WebMethod]
		//public static void DeleteOrdinance(string id)
		//{
		//	clsConnection cls = new clsConnection();

		//	string query = "update tbl_VPR_Workable_Ordinance set date_deleted = GETDATE() where id = '" + id + "'";

		//	try
		//	{
		//		cls.ExecuteQuery(query);

		//		ArrayList arrPages = new ArrayList();
		//		arrPages.Add("Ordinance Settings");
		//		arrPages.Add("");

		//		cls.FUNC.Audit("Deleted Ordinance Id: " + id, arrPages);
		//	}
		//	catch (Exception)
		//	{

		//	}
		//}

		[WebMethod]
		public static void SaveWorkable(List<string> data)
		{
			clsConnection cls = new clsConnection();
			DataTable dt = new DataTable();
			string query = "";
			if (data[0] == "regCriteria")
			{
				dt = cls.GetData("select * from tbl_VPR_Workable_Registration_Criteria (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Registration_Criteria set " +
							"foreclosure_registration = '" + data[3].ToString() + "', " +
							"foreclosure_registration_time = '" + data[4].ToString() + "', " +
							"foreclosure_registration_type = '" + data[5].ToString() + "', " +
							"foreclosure_registration_days = '" + data[6].ToString() + "', " +
							"foreclosure_registration_type2 = '" + data[7].ToString() + "', " +
							"vacant_registration = '" + data[8].ToString() + "', " +
							"vacant_registration_time = '" + data[9].ToString() + "', " +
							"vacant_registration_type = '" + data[10].ToString() + "', " +
							"vacant_registration_days = '" + data[11].ToString() + "', " +
							"vacant_registration_type2 = '" + data[12].ToString() + "', " +
							"foreclosure_vacant_registration = '" + data[13].ToString() + "', " +
							"foreclosure_vacant_registration_time = '" + data[14].ToString() + "', " +
							"foreclosure_vacant_registration_type = '" + data[15].ToString() + "', " +
							"foreclosure_vacant_registration_days = '" + data[16].ToString() + "', " +
							"foreclosure_vacant_registration_type2 = '" + data[17].ToString() + "', " +
							"pre_sale_registration = '" + data[18].ToString() + "', " +
							"past_filed_exclusions = '" + data[19].ToString() + "', " +
							"past_filed_exclusions_date = '" + data[20].ToString() + "', " +
							"post_sale_registration = '" + data[21].ToString() + "', " +
							"city_notice_only = '" + data[22].ToString() + "', " +
							"boarded_only = '" + data[23].ToString() + "', " +
							"special_requirements_txt = '" + data[24].ToString() + "', " +
							"how_to_reg = '" + data[25].ToString() + "', " +
							"upload_path = '" + data[26].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Registration");
						arrPages.Add("Condition");

						cls.FUNC.Audit("Updated Condition settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Registration_Criteria ([state], city, foreclosure_registration, foreclosure_registration_time, " +
							"foreclosure_registration_type, foreclosure_registration_days, foreclosure_registration_type2, vacant_registration, " +
							"vacant_registration_time, vacant_registration_type, vacant_registration_days, vacant_registration_type2, " +
							"foreclosure_vacant_registration, foreclosure_vacant_registration_time, foreclosure_vacant_registration_type, " +
							"foreclosure_vacant_registration_days, foreclosure_vacant_registration_type2, pre_sale_registration, past_filed_exclusions, " + 
							"past_filed_exclusions_date, post_sale_registration, city_notice_only, boarded_only, special_requirements_txt, how_to_reg, " +
							"upload_path) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
							"'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
							"'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
							"'" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', " +
							"'" + data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', " +
							"'" + data[25].ToString() + "', '" + data[26].ToString() + "')";
				}

				try
				{
					cls.ExecuteQuery(query);

					ArrayList arrPages = new ArrayList();
					arrPages.Add("Registration");
					arrPages.Add("Condition");

					cls.FUNC.Audit("Saved New Condition settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
				}
				catch (Exception)
				{

				}

			}
			else if (data[0] == "regProperty")
			{

				dt = cls.GetData("select * from tbl_VPR_Workable_Registration_Property (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Registration_Property set " +
							"residential = '" + data[3].ToString() + "', rental = '" + data[4].ToString() + "', " +
							"commercial = '" + data[5].ToString() + "', condo = '" + data[6].ToString() + "', " +
							"townhome = '" + data[7].ToString() + "', vacant_lot = '" + data[8].ToString() + "', " +
							"mobile_home = '" + data[9].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Registration");
						arrPages.Add("Property Type");

						cls.FUNC.Audit("Updated Property Type settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Registration_Property ([state], city, residential,rental,commercial,condo, " +
							"townhome,vacant_lot,mobile_home) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
							"'" + data[9].ToString() + "')";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Registration");
						arrPages.Add("Property Type");

						cls.FUNC.Audit("Saved New Property Type settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}

			}
			else if (data[0] == "regPropReq")
			{

				dt = cls.GetData("select * from tbl_VPR_Workable_Registration_PropReq (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Registration_PropReq set " +
							"addn_info = '" + data[3].ToString() + "', first_time_vacancy_date = '" + data[4].ToString() + "', " +
							"presale_definition = '" + data[5].ToString() + "', secured_required = '" + data[6].ToString() + "', " +
							"local_contact_required = '" + data[7].ToString() + "', additional_signage_required = '" + data[8].ToString() + "', " +
							"pictures_required = '" + data[9].ToString() + "', gse_exclusion = '" + data[10].ToString() + "', " +
							"mobile_vin_number_required = '" + data[11].ToString() + "', insurance_required = '" + data[12].ToString() + "', " +
							"parcel_number_required = '" + data[13].ToString() + "', foreclosure_action_information_needed = '" + data[14].ToString() + "', " +
							"legal_description_required = '" + data[15].ToString() + "', foreclosure_case_information_needed = '" + data[16].ToString() + "', " +
							"block_and_lot_number_required = '" + data[17].ToString() + "', foreclosure_deed_required = '" + data[18].ToString() + "', " +
							"attorney_information_required = '" + data[19].ToString() + "', bond_required = '" + data[20].ToString() + "', " +
							"broker_information_required_if_reo = '" + data[21].ToString() + "', bond_amount = '" + data[22].ToString() + "', " +
							"mortgage_contact_name_required = '" + data[23].ToString() + "', maintenance_plan_required = '" + data[24].ToString() + "', " +
							"client_tax_number_required = '" + data[25].ToString() + "', no_trespass_form_required = '" + data[26].ToString() + "', " +
							"signature_required = '" + data[27].ToString() + "', utility_information_required = '" + data[28].ToString() + "', " +
							"notarization_required = '" + data[29].ToString() + "', winterization_required = '" + data[30].ToString() + "', " +
							"recent_inspection_date = '" + data[31].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

					query += "update tbl_VPR_Workable_Registration_PropReq_Contact set " +
							"contact_person = '" + data[32].ToString() + "', " +
							"title = '" + data[33].ToString() + "', " +
							"department = '" + data[34].ToString() + "', " +
							"phone_number = '" + data[35].ToString() + "', " +
							"email_address = '" + data[36].ToString() + "', " +
							"mailing_address = '" + data[37].ToString() + "', " +
							"hours_op_from = '" + data[38].ToString() + "', " +
							"hours_op_to = '" + data[39].ToString() + "' " +
							"where prop_req_id = " + dt.Rows[0]["id"].ToString();

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Registration");
						arrPages.Add("Property Requirements");

						cls.FUNC.Audit("Updated Property Requirements settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Registration_PropReq ([state], city,  addn_info, first_time_vacancy_date, presale_definition, " +
							"secured_required, local_contact_required, additional_signage_required, pictures_required, gse_exclusion, mobile_vin_number_required, " +
							"insurance_required, parcel_number_required, foreclosure_action_information_needed, legal_description_required, " +
							"foreclosure_case_information_needed, block_and_lot_number_required, foreclosure_deed_required, attorney_information_required, " +
							"bond_required, broker_information_required_if_reo, bond_amount, mortgage_contact_name_required, maintenance_plan_required, " +
							"client_tax_number_required, no_trespass_form_required, signature_required, utility_information_required, notarization_required, " +
							"winterization_required, recent_inspection_date) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
							"'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
							"'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
							"'" + data[17].ToString() + "', '" + data[18].ToString() + "', '" + data[19].ToString() + "', '" + data[20].ToString() + "', " +
							"'" + data[21].ToString() + "', '" + data[22].ToString() + "', '" + data[23].ToString() + "', '" + data[24].ToString() + "', " +
							"'" + data[25].ToString() + "', '" + data[26].ToString() + "', '" + data[27].ToString() + "', '" + data[28].ToString() + "', " +
							"'" + data[29].ToString() + "', '" + data[30].ToString() + "', '" + data[31].ToString() + "');";

					query += "insert into tbl_VPR_Workable_Registration_PropReq_Contact (prop_req_id, contact_person, title, department, phone_number, " +
								"email_address, mailing_address, hours_op_from, hours_op_to) " +
								"select CASE when MAX(id) is not null then MAX(id) else '1' end [id], " +
								"'" + data[32].ToString() + "', '" + data[33].ToString() + "', '" + data[34].ToString() + "', '" + data[35].ToString() + "', " +
								"'" + data[36].ToString() + "', '" + data[37].ToString() + "', '" + data[38].ToString() + "', '" + data[39].ToString() + "' " +
								"from tbl_VPR_Workable_Registration_PropReq;";
					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Registration");
						arrPages.Add("Property Requirements");

						cls.FUNC.Audit("Saved New Property Requirements settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception) { }
				}

			}
			else if (data[0] == "cost")
			{

				dt = cls.GetData("select * from tbl_VPR_Workable_Registration_Cost (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Registration_Cost set " +
							"reg_fee_req = '" + data[3].ToString() + "', " +
							"reg_fee = '" + data[4].ToString() + "', " +
							"renew_fee_req = '" + data[5].ToString() + "', " +
							"renew_timeframe = '" + data[6].ToString() + "', " +
							"renew_req = '" + data[7].ToString() + "', " +
							"renew_amt = '" + data[8].ToString() + "', " +
							"comm_reg_fee = '" + data[9].ToString() + "', " +
							"monthly_monitor_fee_req = '" + data[10].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Registration");
						arrPages.Add("Cost");

						cls.FUNC.Audit("Updated Cost settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Registration_Cost ([state], city,  reg_fee_req, reg_fee, renew_fee_req, renew_timeframe, renew_req, " +
							"renew_amt, comm_reg_fee, monthly_monitor_fee_req) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
							"'" + data[9].ToString() + "', '" + data[10].ToString() + "');";
					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Registration");
						arrPages.Add("Cost");

						cls.FUNC.Audit("Saved New Cost settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception) { }
				}

			}
			else if (data[0] == "inspect")
			{

				dt = cls.GetData("select * from tbl_VPR_Workable_Inspection (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Inspection set " +
							"inspection_update_req = '" + data[3].ToString() + "', " +
							"inspection_criteria_occ = '" + data[4].ToString() + "', " +
							"inspection_criteria_vac = '" + data[5].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "';";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Inspection");
						arrPages.Add("");

						cls.FUNC.Audit("Updated Inspection settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Inspection ([state], city,  inspection_update_req, inspection_criteria_occ, inspection_criteria_vac) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "');";
					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Inspection");
						arrPages.Add("");

						cls.FUNC.Audit("Saved New Inspection settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception) { }
				}

			}
			else if (data[0] == "municipalityInfo")
			{

				dt = cls.GetData("select * from tbl_VPR_Workable_Municipality (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Municipality set " +
							"municipality_department = '" + data[3].ToString() + "', municipality_phone_num = '" + data[4].ToString() + "', " +
							"municipality_email = '" + data[5].ToString() + "', municipality_st = '" + data[6].ToString() + "', " +
							"municipality_city = '" + data[7].ToString() + "', municipality_state = '" + data[8].ToString() + "', " +
							"municipality_zip = '" + data[9].ToString() + "', contact_person = '" + data[10].ToString() + "', " +
							"title = '" + data[11].ToString() + "', department = '" + data[12].ToString() + "', phone_num = '" + data[13].ToString() + "', " +
							"email = '" + data[14].ToString() + "', address = '" + data[15].ToString() + "', ops_hrs_from = '" + data[16].ToString() + "', " +
							"ops_hrs_to = '" + data[17].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Municipality Contact Information");
						arrPages.Add("");

						cls.FUNC.Audit("Updated Municipality Contact Information settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Municipality ([state], city, municipality_department,municipality_phone_num, " +
							"municipality_email,municipality_st,municipality_city,municipality_state,municipality_zip,contact_person, " +
							"title,department,phone_num,email,address,ops_hrs_from,ops_hrs_to) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
							"'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
							"'" + data[13].ToString() + "', '" + data[14].ToString() + "', '" + data[15].ToString() + "', '" + data[16].ToString() + "', " +
							"'" + data[17].ToString() + "')";
					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Municipality Contact Information");
						arrPages.Add("");

						cls.FUNC.Audit("Saved Municipality Contact Information settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}

			}
			else if (data[0] == "dereg")
			{

				dt = cls.GetData("select * from tbl_VPR_Workable_Deregistration (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Deregistration set " +
							"dereg_req = '" + data[3].ToString() + "', " +
							"conveyed = '" + data[4].ToString() + "', " +
							"occupied = '" + data[5].ToString() + "', " +
							"delist_req = '" + data[6].ToString() + "', " +
							"how_to_dereg = '" + data[7].ToString() + "', " +
							"upload_file = '" + data[8].ToString() + "', " +
							"new_owner_info_req = '" + data[9].ToString() + "', " +
							"proof_of_conveyance_req = '" + data[10].ToString() + "', " +
							"date_of_sale_req = '" + data[11].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Deregistration");
						arrPages.Add("");

						cls.FUNC.Audit("Updated Deregistration settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Deregistration ([state], city, dereg_req, conveyed, occupied, delist_req, how_to_dereg, upload_file, " +
							"new_owner_info_req, proof_of_conveyance_req, date_of_sale_req) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
							"'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "')";
					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Deregistration");
						arrPages.Add("");

						cls.FUNC.Audit("Saved Deregistration settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
					}
					catch (Exception)
					{

					}
				}

			}
			else if (data[0] == "ordinance")
			{
				query = "insert into tbl_VPR_Workable_Ordinance (state, city, ordinance_num, ordinance_name, description, section, source, enacted_date, revision_date, ordinance_file) values " +
								"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
								"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
								"'" + data[9].ToString() + "', '" + data[10].ToString() + "')";
				try
				{
					cls.ExecuteQuery(query);

					ArrayList arrPages = new ArrayList();
					arrPages.Add("Ordinance Settings");
					arrPages.Add("");

					cls.FUNC.Audit("Added new Ordinance settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
				}
				catch (Exception)
				{

				}

			}
			else if (data[0] == "ordinance2")
			{
				query = "update tbl_VPR_Workable_Ordinance set state = '" + data[1].ToString() + "', city = '" + data[2].ToString() + "', " +
								"ordinance_num = '" + data[3].ToString() + "', ordinance_name = '" + data[4].ToString() + "', description = '" + data[5].ToString() + "', " +
								"section = '" + data[6].ToString() + "', source = '" + data[7].ToString() + "', enacted_date = '" + data[8].ToString() + "', " +
								"revision_date = '" + data[9].ToString() + "', ordinance_file = '" + data[10].ToString() + "', date_modified = GETDATE() where id = '" + data[11].ToString() + "'";
				try
				{
					cls.ExecuteQuery(query);

					ArrayList arrPages = new ArrayList();
					arrPages.Add("Ordinance Settings");
					arrPages.Add("");

					cls.FUNC.Audit("Updated Ordinance settings for " + data[2].ToString() + ", " + data[1].ToString(), arrPages);
				}
				catch (Exception)
				{

				}

			}
			else if (data[0] == "zip")
			{
				query = "update tbl_DB_US_States set " +
								"isZipActive = " + data[4].ToString() + " " +
								"where state = '" + data[1].ToString() + "' and " +
								"city = '" + data[2].ToString() + "' and " +
								"ZipCode = '" + data[3].ToString() + "'";
				try
				{
					cls.ExecuteQuery(query);

					ArrayList arrPages = new ArrayList();
					arrPages.Add("Zip Codes");
					arrPages.Add("");

					if (data[4].ToString() == "1")
					{
						cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Active", arrPages);
					}
					else
					{
						cls.FUNC.Audit("Set the zip code of " + data[2].ToString() + ", " + data[1].ToString() + " to Inactive", arrPages);
					}
				}
				catch (Exception)
				{

				}

			}
			else if (data[0] == "notif")
			{

				dt = cls.GetData("select * from tbl_VPR_Workable_Notification (nolock) where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'");

				if (dt.Rows.Count > 0)
				{
					query = "update tbl_VPR_Workable_Notification set " +
							"reg_send_reminder_num = '" + data[3].ToString() + "', reg_send_reminder_type = '" + data[4].ToString() + "', " +
							"reg_send_reminder_days = '" + data[5].ToString() + "', reg_send_reminder_before = '" + data[6].ToString() + "', " +
							"reg_send_reminder_time_frame = '" + data[7].ToString() + "', ren_send_reminder_num = '" + data[8].ToString() + "', " +
							"ren_send_reminder_type = '" + data[9].ToString() + "', ren_send_reminder_days = '" + data[10].ToString() + "', " +
							"ren_send_reminder_before = '" + data[11].ToString() + "', ren_send_reminder_time_frame = '" + data[12].ToString() + "', " +
							"web_notif_url = '" + data[13].ToString() + "' " +
							"where state = '" + data[1].ToString() + "' and city = '" + data[2].ToString() + "'";
					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Notification Settings");
						arrPages.Add("");

						cls.FUNC.Audit("Updated notification settings of " + data[2].ToString() + ", " + data[1].ToString() + " to Active", arrPages);
					}
					catch (Exception)
					{

					}
				}
				else
				{
					query = "insert into tbl_VPR_Workable_Notification ([state], city, reg_send_reminder_num,reg_send_reminder_type, " +
							"reg_send_reminder_days,reg_send_reminder_before,reg_send_reminder_time_frame,ren_send_reminder_num, " +
							"ren_send_reminder_type,ren_send_reminder_days,ren_send_reminder_before,ren_send_reminder_time_frame, " +
							"web_notif_url) values " +
							"('" + data[1].ToString() + "', '" + data[2].ToString() + "', '" + data[3].ToString() + "', '" + data[4].ToString() + "', " +
							"'" + data[5].ToString() + "', '" + data[6].ToString() + "', '" + data[7].ToString() + "', '" + data[8].ToString() + "', " +
							"'" + data[9].ToString() + "', '" + data[10].ToString() + "', '" + data[11].ToString() + "', '" + data[12].ToString() + "', " +
							"'" + data[13].ToString() + "')";

					try
					{
						cls.ExecuteQuery(query);

						ArrayList arrPages = new ArrayList();
						arrPages.Add("Notification Settings");
						arrPages.Add("");

						cls.FUNC.Audit("Set the notification settings of " + data[2].ToString() + ", " + data[1].ToString() + " to Active", arrPages);
					}
					catch (Exception)
					{

					}
				}
			}
		}

		[WebMethod]
		public static string GetData(string state, string city)
		{
			clsConnection cls = new clsConnection();
			string qryRegCrit, qryRegPropType, qryRegPropReq, qryCost, qryInspect, qryMunicipal, qryDereg, qryFees, qryNotif = "";

			DataTable dtRegCrit, dtRegPropType, dtRegPropReq, dtCost, dtInspect, dtMunicipal, dtDereg, dtFees, dtNotif = new DataTable();

			qryRegCrit = "select * from tbl_VPR_Workable_Registration_Criteria (nolock) where state = '" + state + "' and city = '" + city + "'";
			dtRegCrit = cls.GetData(qryRegCrit);

			qryRegPropType = "select * from tbl_VPR_Workable_Registration_Property (nolock) where state = '" + state + "' and city = '" + city + "'";
			dtRegPropType = cls.GetData(qryRegPropType);

			qryRegPropReq = "select a.*, b.contact_person, b.title, b.department, b.phone_number, b.email_address, b.mailing_address, b.hours_op_from, " +
							"b.hours_op_to from tbl_VPR_Workable_Registration_PropReq a (nolock) join tbl_VPR_Workable_Registration_PropReq_Contact b (nolock) " +
							"on a.id = b.prop_req_id where a.state = '" + state + "' and a.city = '" + city + "'";
			dtRegPropReq = cls.GetData(qryRegPropReq);

			qryCost = "select * from tbl_VPR_Workable_Registration_Cost (nolock) where state = '" + state + "' and city = '" + city + "'";
			dtCost = cls.GetData(qryCost);

			qryInspect = "select * from tbl_VPR_Workable_Inspection (nolock) where state = '" + state + "' and city = '" + city + "'";
			dtInspect = cls.GetData(qryInspect);

			qryMunicipal = "select * from tbl_VPR_Workable_Municipality (nolock) where state = '" + state + "' and city = '" + city + "'";
			dtMunicipal = cls.GetData(qryMunicipal);

			qryDereg = "select * from tbl_VPR_Workable_Deregistration (nolock) where state = '" + state + "' and city = '" + city + "'";
			dtDereg = cls.GetData(qryDereg);

			qryNotif = "select * from tbl_VPR_Workable_Notification (nolock) where state = '" + state + "' and city = '" + city + "'";
			dtNotif = cls.GetData(qryNotif);

			return JsonConvert.SerializeObject(new
			{
				Success = true,
				Message = "Success",
				data = new
				{
					regCrit = dtRegCrit,
					regPropType = dtRegPropType,
					regPropReq = dtRegPropReq,
					regCost = dtCost,
					inspect = dtInspect,
					municipal = dtMunicipal,
					dereg = dtDereg,
					notif = dtNotif
				}
			});
		}
	}
}