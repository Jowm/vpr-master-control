﻿using iTextSharp.text.pdf;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PDFCreator
{
    public partial class PDFMap : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //dropdown pdf mapping
        [WebMethod]
        public static string GetPDF()
        {
            clsConnection cls = new clsConnection();
            DataTable dtPDF = new DataTable();

            string query = "select * from tbl_VPR_Municipality where date_deleted is null";

            dtPDF = cls.GetData(query);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtPDF } });
        }

        [WebMethod]
        public static string GetMapping()
        {
            clsConnection cls = new clsConnection();
            DataTable dtMap = new DataTable();
            DataTable dtCustom = new DataTable();

            string data = "select * from tbl_VPR_Mapping_Main where DateDeleted is null order by GroupId";

            dtMap = cls.GetData(data);

            string custom = "select Distinct(GroupName), CAST(description as varchar(MAX)) [description], modified_by, date_modified from tbl_VPR_Mapping_Group where custom_tag = 1 and date_deleted is null order by GroupName";

            dtCustom = cls.GetData(custom);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { dataMap = dtMap, customData = dtCustom } });
        }

        [WebMethod]
        public static string GetFields(ArrayList id)
        {
            clsConnection cls = new clsConnection();
            //DataTable dtFields = new DataTable();

            //if (tag == "source")
            //{
            //	string query = "select * from tbl_VPR_Mapping_Group where GroupId = " + id[0].ToString() + " order by FieldId, GroupName";

            //	dtFields = cls.GetData(query);
            //}
            //else if (tag == "cust")
            //{
            string val = "";

            foreach (string group in id)
            {
                val += "'" + group.Trim() + "',";
            }

            val = val.Remove(val.Length - 1, 1);

            string query = "select * from tbl_VPR_Mapping_Group where GroupId in (" + val + ") OR GroupName in (" + val + ") order by FieldId, GroupName";

            DataTable dtFields = cls.GetData(query);
            //}

            string query2 = "select * from tbl_VPR_Mapping_Conditions order by field_name";

            DataTable dtFields2 = cls.GetData(query2);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dtFields, record2 = dtFields2 } });
        }

        [WebMethod]
        public static void SaveCustom(ArrayList myData, string usr)
        {
            clsConnection cls = new clsConnection();

            string query = "insert into tbl_VPR_Mapping_Group (GroupId, GroupName, FieldName, SampleData, custom_tag, description, modified_date, date_modified) values ";

            string[] field = myData[2].ToString().Split(',');
            string[] value = myData[3].ToString().Split(',');

            int index = 0;

            foreach (string f in field)
            {
                query += "('" + myData[0] + "', '" + myData[1] + "', '" + f + "', '" + value[index] + "', 1, '" + myData[4] + "', '" + usr + "', GETDATE()),";
                index += 1;
            }

            query = query.Remove(query.Length - 1, 1);

            try
            {
                int exec = cls.ExecuteQuery(query);

                if (exec != 0)
                {
                    ArrayList arrPages = new ArrayList();
                    arrPages.Add("PDF Mapping");
                    arrPages.Add("");

                    cls.FUNC.Audit("Save Custom", arrPages);
                }
            }
            catch (Exception) { }
        }

        [WebMethod]
        public static string UploadFile(ArrayList myData)
        {
            clsConnection cls = new clsConnection();

            string qryFN = "select * from tbl_VPR_Municipality where municipality_code = '" + myData[1].ToString() + "' and ";
            qryFN += "client = '" + myData[2].ToString() + "' and reg_type = '" + myData[3].ToString() + "' and date_deleted is null";
            DataTable dtFN = cls.GetData(qryFN);

            string client = "";
            string regType = "";

            if (myData[2].ToString() == "PFC")
            {
                client = "P";
            }
            else if (myData[2].ToString() == "REO")
            {
                client = "R";
            }
            else if (myData[2].ToString() == "RESI")
            {
                client = "RE";
            }

            if (myData[3].ToString() == "Property Registration")
            {
                regType = "I";
            }
            else if (myData[3].ToString() == "Delist")
            {
                regType = "D";
            }
            else if (myData[3].ToString().IndexOf("Renewal") > -1)
            {
                //string num = myData[3].ToString().Substring(myData[3].ToString().Length - 1, 1);
                //regType = "R" + num;
                regType = "R";
            }

            string fn = client + regType + "_" + myData[1].ToString() + "_" + myData[0].ToString() + ".pdf";

            if (myData[5].ToString() == "new")
            {
                if (dtFN.Rows.Count > 0 && (dtFN.Rows[0]["file_name"].ToString() != "" && dtFN.Rows[0]["file_name"] != DBNull.Value))
                {
                    return "1";
                }
                else
                {
                    string qryDel = "select * from tbl_VPR_Municipality where municipality_code = '" + myData[1].ToString() + "' and ";
                    qryDel += "client = '" + myData[2].ToString() + "' and reg_type = '" + myData[3].ToString() + "' and date_deleted is not null";
                    DataTable dtDel = cls.GetData(qryDel);

                    if (dtFN.Rows.Count == 0 && dtDel.Rows.Count == 0)
                    {
                        string ins = "insert into tbl_VPR_Municipality (municipality_name, municipality_code, file_name, client, reg_type, modified_by, modified_date, tag) ";
                        ins += "values ('" + myData[0].ToString() + "', '" + myData[1].ToString() + "', '" + fn + "', ";
                        ins += "'" + myData[2].ToString() + "', '" + myData[3].ToString() + "','" + myData[4].ToString() + "', GETDATE(), 'uploaded')";

                        try
                        {
                            int exec = cls.ExecuteQuery(ins);

                            if (exec != 0)
                            {
                                ArrayList arrPages = new ArrayList();
                                arrPages.Add("PDF Mapping");
                                arrPages.Add("");

                                cls.FUNC.Audit("Uplaod PDF", arrPages);

                                return "0";
                            }
                            else
                            {
                                return "Error";
                            }
                        }
                        catch (Exception)
                        {
                            return "Error";
                        }
                    }
                    else
                    {
                        string updt = "";

                        //updt = "update tbl_VPR_Municipality set ";
                        //updt += "file_name = '" + fn + "', client = '" + myData[2].ToString() + "', reg_type = '" + myData[3].ToString() + "', ";
                        //updt += "modified_by = '" + myData[4].ToString() + "', modified_date = GETDATE(), tag = 'uploaded' where municipality_code = '" + myData[1].ToString() + "' and ";
                        //updt += "client = '" + myData[2].ToString() + "' and reg_type = '" + myData[3].ToString() + "'";

                        if (myData[0].ToString().IndexOf("CT - ") > -1)
                        {
                            string muniCode = "000000053";
                            string muniName = "CT - Statewide";

                            string ctFN = client + regType + "_" + muniCode + "_" + muniName + ".pdf";

                            string qryID = "select id from tbl_VPR_Municipality where municipality_name like '%CT - %' and ";
                            qryID += "municipality_name not in ('CT - Deep River','CT - Derby','CT - New Haven (2)','CT - Waterbury') and client = '" + myData[2].ToString() + "' ";
                            qryID += "and reg_type = '" + myData[3].ToString() + "'";

                            DataTable dtID = cls.GetData(qryID);

                            string ids = "";

                            for (int x = 0; x < dtID.Rows.Count; x++)
                            {
                                ids += dtID.Rows[x]["id"].ToString() + ",";
                            }

                            ids = ids.Remove(ids.Length - 1, 1);

                            updt = "update tbl_VPR_Municipality set ";
                            updt += "file_name = '" + ctFN + "', client = '" + myData[2].ToString() + "', reg_type = '" + myData[3].ToString() + "', ";
                            updt += "modified_by = '" + myData[4].ToString() + "', modified_date = GETDATE(), tag = 'uploaded' where id in (" + ids + ")";
                        }
                        else
                        {
                            updt = "update tbl_VPR_Municipality set ";
                            updt += "file_name = '" + fn + "', client = '" + myData[2].ToString() + "', reg_type = '" + myData[3].ToString() + "', ";
                            updt += "modified_by = '" + myData[4].ToString() + "', modified_date = GETDATE(), date_deleted = null, tag = 'uploaded' ";
                            updt += "where municipality_code = '" + myData[1].ToString() + "' and client = '" + myData[2].ToString() + "' and ";
                            updt += "reg_type = '" + myData[3].ToString() + "'";
                        }

                        try
                        {
                            int exec = cls.ExecuteQuery(updt);

                            if (exec != 0)
                            {
                                ArrayList arrPages = new ArrayList();
                                arrPages.Add("PDF Mapping");
                                arrPages.Add("");

                                cls.FUNC.Audit("Uplaod PDF", arrPages);

                                return "0";
                            }
                            else
                            {
                                return "Error";
                            }
                        }
                        catch (Exception)
                        {
                            return "Error";
                        }
                    }
                }
            }
            else
            {
                string updt = "";

                //updt = "update tbl_VPR_Municipality set ";
                //updt += "file_name = '" + fn + "', client = '" + myData[2].ToString() + "', reg_type = '" + myData[3].ToString() + "', ";
                //updt += "modified_by = '" + myData[4].ToString() + "', modified_date = GETDATE(), tag = 'overwrite' where municipality_code = '" + myData[1].ToString() + "' and ";
                //updt += "client = '" + myData[2].ToString() + "' and reg_type = '" + myData[3].ToString() + "' and date_deleted is null";

                if (myData[0].ToString().IndexOf("CT - ") > -1 &&
                    (myData[0].ToString().IndexOf("Deep River") <= -1 &&
                    myData[0].ToString().IndexOf("Derby") <= -1 &&
                    myData[0].ToString().IndexOf("New Haven") <= -1 &&
                    myData[0].ToString().IndexOf("Waterbury") <= -1))
                {
                    string muniCode = "000000053";
                    string muniName = "CT - Statewide";

                    string ctFN = client + regType + "_" + muniCode + "_" + muniName + ".pdf";

                    string qryID = "select id from tbl_VPR_Municipality where municipality_name like '%CT - %' and ";
                    qryID += "municipality_name not in ('CT - Deep River','CT - Derby','CT - New Haven (2)','CT - Waterbury') and client = '" + myData[2].ToString() + "' ";
                    qryID += "and reg_type = '" + myData[3].ToString() + "'";

                    DataTable dtID = cls.GetData(qryID);

                    string ids = "";

                    for (int x = 0; x < dtID.Rows.Count; x++)
                    {
                        ids += dtID.Rows[x]["id"].ToString() + ",";
                    }

                    ids = ids.Remove(ids.Length - 1, 1);

                    updt = "update tbl_VPR_Municipality set ";
                    updt += "file_name = '" + ctFN + "', client = '" + myData[2].ToString() + "', reg_type = '" + myData[3].ToString() + "', ";
                    updt += "modified_by = '" + myData[4].ToString() + "', modified_date = GETDATE(), tag = 'overwrite' where id in (" + ids + ") ";
                    updt += "and date_deleted is null";
                }
                else
                {
                    updt = "update tbl_VPR_Municipality set ";
                    updt += "file_name = '" + fn + "', client = '" + myData[2].ToString() + "', reg_type = '" + myData[3].ToString() + "', ";
                    updt += "modified_by = '" + myData[4].ToString() + "', modified_date = GETDATE(), tag = 'overwrite' where municipality_code = '" + myData[1].ToString() + "' and ";
                    updt += "client = '" + myData[2].ToString() + "' and reg_type = '" + myData[3].ToString() + "' and date_deleted is null";
                }

                try
                {
                    int exec = cls.ExecuteQuery(updt);

                    if (exec != 0)
                    {
                        ArrayList arrPages = new ArrayList();
                        arrPages.Add("PDF Mapping");
                        arrPages.Add("");

                        cls.FUNC.Audit("Upload PDF", arrPages);

                        return "0";
                    }
                    else
                    {
                        return "Error";
                    }
                }
                catch (Exception)
                {
                    return "Error";
                }
            }
        }

        [WebMethod]
        public static string GetHeaders(string pdf)
        {
            clsConnection cls = new clsConnection();

            string lockedMap = "select * from tbl_VPR_Municipality where file_name = '" + pdf + "'";

            DataTable dtLocked = cls.GetData(lockedMap);

            string lck = "0";

            if (dtLocked.Rows.Count > 0)
            {
                if (dtLocked.Rows[0]["lockedMap"].ToString() == "True")
                {
                    lck = "1";
                }
            }

            string headers = "";

            string source = HttpContext.Current.Server.MapPath("~/Files/PDF Files/" + pdf);
            string extSrc = HttpContext.Current.Server.MapPath("~/Files/Extracted Files/");

            if (IsValidPdf(source))
            {
                PdfReader pdfReader = new PdfReader(source);

                string filename = pdf;

                using (FileStream stream = new FileStream(string.Concat(extSrc, filename), FileMode.Create))
                {
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);

                    AcroFields formFields = default(AcroFields);
                    formFields = pdfStamper.AcroFields;
                    AcroFields.Item item = new AcroFields.Item();
                    PdfDictionary dict = new PdfDictionary();

                    int? flags;

                    headers += "/Files/Extracted Files/" + filename + "," + lck + ",";

                    foreach (var field in formFields.Fields)
                    {
                        headers += field.Key + ",";
                        formFields.SetField(field.Key, field.Key);

                        item = formFields.Fields[field.Key];
                        dict = item.GetMerged(0);

                        try
                        {
                            flags = dict.GetAsNumber(PdfName.FF).IntValue;

                            if ((flags & BaseField.MULTILINE) > 0)
                            {
                                formFields.SetFieldProperty(field.Key, "textsize", 6f, null);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    //string qryForm = "select * from tbl_VPR_Municipality ";

                    if (dtLocked.Rows.Count > 0)
                    {
                        if (dtLocked.Rows[0]["lockedMap"].ToString() == "True")
                        {
                            pdfStamper.FormFlattening = true;
                        }
                        else
                        {
                            pdfStamper.FormFlattening = false;
                        }
                    }
                    pdfStamper.Close();
                    stream.Close();
                }
                pdfReader.Close();
            }

            return headers;
        }

        [WebMethod]
        public static string PreviewPDF(Object fields, Object values, string pdf, string filename)
        {
            string fn = filename;

            string source = HttpContext.Current.Server.MapPath("~/Files/PDF Files/" + fn);
            string extSrc1 = HttpContext.Current.Server.MapPath("~/Files/Extracted Files/");

            if (IsValidPdf(source))
            {
                PdfReader pdfReader = new PdfReader(source);

                using (FileStream stream = new FileStream(string.Concat(extSrc1, fn), FileMode.Create))
                {
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);

                    AcroFields formFields = default(AcroFields);
                    formFields = pdfStamper.AcroFields;
                    AcroFields.Item item = new AcroFields.Item();
                    PdfDictionary dict = new PdfDictionary();

                    int? flags;

                    ArrayList arr = new ArrayList();
                    ArrayList arr2 = new ArrayList();
                    ArrayList arr3 = new ArrayList();

                    // Default Form Fields
                    foreach (var field in formFields.Fields)
                    {
                        formFields.SetField(field.Key, "");

                        arr.Add(field.Key);
                    }


                    int b = 0;

                    // Changed Fields/Values
                    foreach (var obj in (dynamic)values)
                    {
                        foreach (var k in (dynamic)obj)
                        {
                            object key = k.Key;
                            object value = k.Value;
                            int z = 1;

                            formFields.SetField(key.ToString(), value.ToString());

                            item = formFields.Fields[key.ToString()];
                            dict = item.GetMerged(0);

                            try
                            {
                                flags = dict.GetAsNumber(PdfName.FF).IntValue;

                                if ((flags & BaseField.MULTILINE) > 0)
                                {
                                    formFields.SetFieldProperty(key.ToString(), "textsize", 6f, null);
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                    }

                    int y = 0;

                    foreach (var objF in (dynamic)fields)
                    {
                        foreach (var f in (dynamic)objF)
                        {
                            object key = f.Key;
                            object value = f.Value;

                            int x = 1;

                            string n = "";
                            Regex rx = new Regex(@"(_+[0-9])", RegexOptions.IgnoreCase);

                            Match mt = rx.Match(value.ToString());

                            if (mt.Success)
                            {
                                n = value.ToString().Replace(mt.Groups[1].Value, "");
                            }
                            else
                            {
                                n = value.ToString();
                            }

                            if (!arr.Contains(n) && !arr2.Contains(n))
                            {
                                formFields.RenameField(key.ToString(), n);
                                arr.Add(n);
                                arr2.Add(n);
                            }
                            else
                            {
                                n = n + "__" + (x + 1);

                                while (arr.Contains(n))
                                {
                                    n = n.Remove(n.Length - 1, 1);
                                    x = x + 1;
                                    n = n + x;
                                }

                                formFields.RenameField(key.ToString(), n);
                                arr.Add(n);
                                arr2.Add(n);
                            }
                        }
                    }

                    foreach (var field in formFields.Fields)
                    {
                        arr3.Add(field);
                    }

                    pdfStamper.FormFlattening = false;
                    pdfStamper.Close();
                    stream.Close();
                }

                pdfReader.Close();
            }

            return "/Files/Extracted Files/" + fn;
        }

        [WebMethod]
        public static string SavePDF(ArrayList myData, string usr)
        {

            clsConnection cls = new clsConnection();

            if (myData[2].ToString() != "new")
            {
                //if (myData[2].ToString() == "overwrite")
                //{
                //    string qry = "update tbl_VPR_Municipality set deleted_by = '" + usr + "', date_deleted = GETDATE() where ";
                //    qry += "file_name = '" + myData[0].ToString() + "' and ";
                //    qry += "date_deleted is null";

                //    cls.ExecuteQuery(qry);
                //}

                string source1 = HttpContext.Current.Server.MapPath("~/Files/Extracted Files/" + myData[0].ToString());
                string source2 = HttpContext.Current.Server.MapPath("~/Files/PDF Files/" + myData[0].ToString());
                string dest = HttpContext.Current.Server.MapPath("~/Files/Mapped Files/" + myData[0].ToString());
                string shared = HttpContext.Current.Server.MapPath("~/Files/Shared Files/" + myData[0].ToString());

                try
                {
                    try
                    {
                        File.Copy(source1, dest, true);
                        File.Copy(source1, source2, true);
                    }
                    catch (Exception ex)
                    {
                        return "Error: " + ex.ToString();
                    }

                    string updt = "update tbl_VPR_Municipality set description = '" + myData[1].ToString() + "', ";
                    updt += "modified_by = '" + usr + "', modified_date = GETDATE(), tag = 'mapped' where file_name = '" + myData[0].ToString() + "'";

                    try
                    {
                        int exec = cls.ExecuteQuery(updt);

                        if (exec != 0)
                        {
                            ArrayList arrPages = new ArrayList();
                            arrPages.Add("PDF Mapping");
                            arrPages.Add("");

                            cls.FUNC.Audit("Save PDF", arrPages);
                        }

                        if (IsValidPdf(source1))
                        {
                            PdfReader pdfReader = new PdfReader(source1);

                            using (FileStream stream = new FileStream(source2, FileMode.Create))
                            {
                                PdfStamper pdfStamper = new PdfStamper(pdfReader, stream);

                                AcroFields formFields = default(AcroFields);
                                formFields = pdfStamper.AcroFields;
                                AcroFields.Item item = new AcroFields.Item();
                                PdfDictionary dict = new PdfDictionary();

                                int? flags;

                                foreach (var field in formFields.Fields)
                                {
                                    formFields.SetField(field.Key, "");

                                    item = formFields.Fields[field.Key];
                                    dict = item.GetMerged(0);

                                    try
                                    {
                                        flags = dict.GetAsNumber(PdfName.FF).IntValue;

                                        if ((flags & BaseField.MULTILINE) > 0)
                                        {
                                            formFields.SetFieldProperty(field.Key, "textsize", 0f, null);
                                        }
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }

                                pdfStamper.FormFlattening = false;
                                pdfStamper.Close();
                                stream.Close();
                            }
                            pdfReader.Close();
                        }

                        //File.Copy(source2, source1, true);
                        try
                        {
                            File.Copy(source2, shared, true);
                        }
                        catch (Exception ex)
                        {
                            return "Error: " + ex.ToString();
                        }

                        return "1";
                    }
                    catch (Exception ex)
                    {
                        return "Error: " + ex.ToString();
                    }
                }
                catch (Exception ex)
                {
                    return "Error: " + ex.ToString();
                }
            }
            else
            {
                string qry = "select * from tbl_VPR_Municipality where ";
                qry += "file_name = '" + myData[0].ToString() + "' and date_deleted is null";

                DataTable dt = cls.GetData(qry);

                if (dt.Rows.Count > 0)
                {
                    return "2";
                }
                else
                {
                    return "3";
                }
            }
        }

        public static bool IsValidPdf(string filepath)
        {
            bool Ret = true;

            PdfReader reader = null;

            try
            {
                reader = new PdfReader(filepath);
                reader.Close();
            }
            catch
            {
                Ret = false;
            }

            return Ret;
        }

        public static int CountStringOccurrences(string text, string pattern)
        {
            // Loop through all instances of the string 'text'.
            int count = 0;
            int i = 0;
            //int x = 0;
            while ((i = text.IndexOf(pattern, i)) != -1)
            {
                i += pattern.Length;
                count++;
            }

            return count;
        }

        [WebMethod]
        public static string SelectState()
        {
            clsConnection cls = new clsConnection();

            string qry = "select DISTINCT(LTRIM(RTRIM(State))) [State] from tbl_DB_US_States order by [State]";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string SelectMunicipality()
        {
            clsConnection cls = new clsConnection();

            string qry = "select DISTINCT(LTRIM(RTRIM(municipality_name))) [Municipality], municipality_code from tbl_VPR_Municipality order by [Municipality]";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }

        [WebMethod]
        public static string SelectZip(string state, string municipality)
        {
            clsConnection cls = new clsConnection();

            string qry = "select DISTINCT(LTRIM(RTRIM(ZipCode))) [Zip] from tbl_DB_US_States where LTRIM(RTRIM(State)) = '" + state.Trim() + "' and LTRIM(RTRIM(Municipality)) = '" + municipality.Trim() + "' order by Zip";

            DataTable dt = cls.GetData(qry);

            return JsonConvert.SerializeObject(new { Success = true, Message = "Success", data = new { record = dt } });
        }
    }
}