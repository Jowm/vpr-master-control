﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Master_Control
{
	public partial class DataMapping : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}
		[WebMethod]
		public static List<MyFields> getUPLOADEDDATA(string filename, string sheetname)
		{
			List<MyFields> myListoffields = new List<MyFields>();
			DataSet ds = new DataSet();
			string pathToSave_100;
			pathToSave_100 = HttpContext.Current.Server.MapPath("~/flatfiles/") + filename;
			string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + pathToSave_100 + ";" + "Extended Properties='Excel 8.0';";

			string excelConnStr = String.Format(connectionString, pathToSave_100, "Yes");
			OleDbConnection oleDb = new OleDbConnection(excelConnStr);

			oleDb.Open();

			DataTable dtExcel = oleDb.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
			OleDbDataAdapter excelData = new OleDbDataAdapter("SELECT * FROM [" + sheetname + "$]", connectionString);
			excelData.TableMappings.Add("Table", "ExcelSheet");
			excelData.Fill(ds);
			int x = 0;
			DataTable dt = new DataTable();
			dt = ds.Tables[0];
			do
			{
				MyFields MyFields = new MyFields();
				string myitems;
				myitems = dt.Columns[x].ColumnName;
				MyFields.FieldName = myitems;
				MyFields.SampleData = dt.Rows[0][myitems].ToString();
				myListoffields.Add(MyFields);
				x++;
			} while (x != dt.Columns.Count);
			return myListoffields;
		}
		[WebMethod]
		public static string SaveGroup(string originalFile, string generatedFile, string sheetname, GroupSetting GroupSetting)
		{
			string path = @"\\ascorp.com\data\bangalore\CommonShare\Strategic$\Internal\Operations\VPR Manila\PPI-MIS Manila\Dump - Raw File\";

			Connection Connection = new Connection();
			DataTable dtFields = new DataTable();
			dtFields.Columns.Add("FieldName");
			dtFields.Columns.Add("FieldData");
			dtFields.Columns.Add("GroupName");
			foreach (var Fields in GroupSetting.Fields)
			{
				DataRow dr = dtFields.NewRow();
				dr["FieldName"] = Fields.FieldName;
                dr["FieldData"] = Fields.SampleData;
				dr["GroupName"] = Fields.GroupName;
				dtFields.Rows.Add(dr);
			}
			Connection.myparameters.Add(new myParameters { ParameterName = "@FIELDS", mytype = SqlDbType.Structured, Value = dtFields });
			Connection.myparameters.Add(new myParameters { ParameterName = "@NAME", mytype = SqlDbType.NVarChar, Value = GroupSetting.Name.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@SOURCE", mytype = SqlDbType.NVarChar, Value = GroupSetting.Source.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@LOCATION", mytype = SqlDbType.NVarChar, Value = GroupSetting.Location.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@DESCRIPTION", mytype = SqlDbType.NVarChar, Value = GroupSetting.Description.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@OCCURENCE", mytype = SqlDbType.NVarChar, Value = GroupSetting.Occurence.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = GroupSetting.StartTime.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = GroupSetting.EndTime.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = GroupSetting.StartDate.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = GroupSetting.EndDate.Trim() });
			Connection.myparameters.Add(new myParameters { ParameterName = "@FILENAME", mytype = SqlDbType.NVarChar, Value = originalFile });
			Connection.myparameters.Add(new myParameters { ParameterName = "@PATH", mytype = SqlDbType.NVarChar, Value = path });

            Connection.ExecuteNonQuery("spSAVEFIELDS");

			return "Success";
		}
		public class Fields
		{
			public string GroupName { get; set; }
			public string FieldName { get; set; }
			public string SampleData { get; set; }
		}
		public class GroupSetting
		{
			public List<Fields> Fields { get; set; }
			public string GroupName { get; set; }
			public string Name { get; set; }
			public string Source { get; set; }
			public string Location { get; set; }
			public string Description { get; set; }
			public string Occurence { get; set; }
			public string StartTime { get; set; }
			public string EndTime { get; set; }
			public string StartDate { get; set; }
			public string EndDate { get; set; }
		}
	}


}