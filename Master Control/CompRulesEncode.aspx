﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="CompRulesEncode.aspx.cs" Inherits="Master_Control.CompRulesEncode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tblState tbody td, #tblCity tbody td {
            padding: 0px;
            margin: 0px;
        }

        .rowHover:hover {
            cursor: pointer;
            color: #000;
            background-color: #eff8b3;
        }

        .activeRow {
            color: #fff;
            background-color: #4d6578;
        }

        #navReg {
            list-style: none;
            display: inline-flex;
        }

            #navReg li {
                padding: 5px;
            }

                #navReg li.active {
                    border-top-color: #fff !important;
                }

                    #navReg li.active button {
                        color: #fff !important;
                    }

        #ulWorkSettings li.active a {
            color: #1e3d99;
            text-decoration: underline;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <section class="content-header">
        <h1>Compliance Rules</h1>
        <ol class="breadcrumb">
            <li><a href="Index.aspx"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">
                <span><i class="fa fa-tasks"></i>&nbsp;Compliance Rules</span>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-default" style="margin-top: 1%;">
            <div class="box-body">
                <div class="col-xs-2" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>States</label>
                        </div>
                        <div class="panel-body" style="min-height: 760px; max-height: 760px; overflow-y: auto;">
                            <table id="tblState" class="table no-border">
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-xs-10" style="padding-left: 0px;">
                    <div class="panel panel-primary">
                        <div class="panel-heading text-center">
                            <label>Workable Settings</label>
                        </div>
                        <div class="panel-body" style="padding: 5px;">
                            <div class="col-xs-2 noPadMar">
                                <div class="panel panel-default noPadMar">
                                    <%--<div class="panel-heading text-center">
                                        <label>City</label>
                                    </div>
                                    <div class="panel-body" style="min-height: 710px; max-height: 710px; overflow-y: auto;">
                                        <table id="tblCity" class="table no-border">
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>--%>
                                    <div class="panel-heading text-center">
                                        <label>Municipality</label>
                                    </div>
                                    <div class="panel-body" style="min-height: 710px; max-height: 710px; overflow-y: auto;">
                                        <table id="tblMunicipality" class="table no-border">
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="divSettings" class="col-xs-10 noPadMar" style="display: none;">
                                <div class="panel panel-default noPadMar">
                                    <div class="panel-body noPadMar">
                                        <div class="nav-tabs-custom noPadMar">
                                            <ul id="ulWorkSettings" class="nav nav-tabs text-nowrap" style="display: flex;">
                                                <li class="active" id="liRegistration">
                                                    <a href="#tabRegistration" data-toggle="tab" id="idRegistration">Registration Criteria</a>
                                                </li>
                                                <li id="liInspection" onclick="clickIns(this)">
                                                    <a href="#tabInspection" data-toggle="tab" id="idInspection">Inspection</a>
                                                </li>
                                                <li  id="liMunicipality" onclick="clickMunicipality(this)">
                                                    <a href="#tabMunicipality" data-toggle="tab" id="idMunicipality">Municipality Contact Information</a>
                                                </li>

                                                <li id="liDeRegistration" onclick="clickDereg(this)">
                                                    <a href="#tabDeregistration" data-toggle="tab" id="idDeregistration">Deregistration</a>
                                                </li>
                                                <li id="liOrdinance">
                                                    <a href="#tabOrdinance" data-toggle="tab" id="idOrdinance">Ordinance Settings</a>
                                                </li>

                                                <li id="liZip">
                                                    <a href="#tabZip" data-toggle="tab" id="idZipCodes">Zip Codes</a>
                                                </li>
                                                <li id="liNotif" onclick="clickNotif(this)">
                                                    <a href="#tabNotif" data-toggle="tab" id="idNotif">Notification Settings</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="tab-content">
                                            <%-- Registration --%>
                                            <div id="tabRegistration" class="tab-pane active">
                                                <div class="panel panel-default noPadMar">
                                                    <div class="panel-body noPadMar">
                                                        <%-- HIDE for NOW --%>
                                                       <%-- <div class="col-xs-12 copyfrom" style="margin-top: 1%;">
                                                            <div class="col-xs-2">
                                                                <label>Copy from:</label>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <select class="form-control input-sm slctCopyMuni"></select>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyReg" id="populateRegistration" <!--onclick="copySettings(this);-->"><i class="fa fa-chevron-circle-right"></i></button>
                                                            </div>
                                                        </div>--%>
                                                        <%-- END --%>
                                                        <div class="nav-tabs-custom noPadMar">
                                                            <ul class="nav nav-tabs" id="navReg">
                                                                <li>
                                                                    <button type="button" class="btn btn-primary" href="#tabPFC" id="btntabPFC" data-toggle="tab">PFC</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabREO" id="btntabREO" data-toggle="tab">REO</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabPropType" id="btntabProperty" data-toggle="tab">Property Type</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabPropReq" id="btntabRequirements" data-toggle="tab">Requirements</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabCost" id="btntabCost" data-toggle="tab">Cost</button>
                                                                </li>
                                                                <li>
                                                                    <button type="button" class="btn btn-default" href="#tabContReg" id="ContReg" data-toggle="tab">Continuing Registration</button>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="tab-content">
                                                            <%-- TAB PFC DESIGN --%>
                                                            <div id="tabPFC" class="tab-pane active">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        <input id="pfcReg" type="checkbox" />
                                                                        <label>No PFC Registrations</label>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <input id="pfcStatewideReg" type="checkbox" />
                                                                        <label>Statewide Registration</label>
                                                                    </div>
                                                                </div>
                                                                <%-- PFC DEFAULT --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Default
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcDefault" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Default Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="inDefRegTimeline1" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="slctDefRegTimeline2" type="number" min="-1" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctDefRegTimeline3" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctDefRegTimeline4" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>

                                                                    <div class="col-xs-1">
                                                                        <select id="slctDefRegTimeline5" class="form-control input-sm" style="width: 90px">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC DEFAULT --%>

                                                                <%-- PFC VACANT --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Vacant
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcVacantTimeline" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -0px">
                                                                        <select id="inPfcVacantTimeline1" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -37PX">
                                                                        <input id="slctPfcVacantTimeline2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcVacantTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcVacantTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2" style="width: 126px">
                                                                        <select id="slctPfcVacantTimeline5" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC VACANT --%>

                                                                <%-- PFC FORECLOSURE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Foreclosure
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcForeclosure" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Foreclosure Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="inPfcDefForclosureTimeline1" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -35px">
                                                                        <input id="slctPfcDefForclosureTimeline2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcDefForclosureTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcDefForclosureTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctPfcDefForclosureTimeline5" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC FORECLOSURE --%>

                                                                <%-- FORECLOSURE AND VACANT --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Foreclosure and Vacant
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -26px">
                                                                        <select id="slctPfcForeclosureVacant" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Foreclosure and Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="inPfcForeclosureVacantTimeline1" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="slctPfcForeclosureVacantTimeline2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcForeclosureVacantTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcForeclosureVacantTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-2" style="width: 120px">
                                                                        <select id="slctPfcForeclosureVacantTimeline5" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END FORECLOSURE AND VACANT --%>

                                                                <%-- PFC CITY NOTICE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC City Notice
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcCityNotice" class="form-control input-sm">

                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC City Notice Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="inPfcCityNoticeTimeline1" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -35px">
                                                                        <input id="slctPfcCityNoticeTimeline2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcCityNoticeTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcCityNoticeTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctPfcCityNoticeTimeline5" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC CITY NOTICE --%>

                                                                <%-- PFC CODE VIOLATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Code Violation
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcCodeViolation" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Code Violation Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="inPfcCodeViolationTimeline1" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -35px">
                                                                        <input id="slctPfcCodeViolationTimeline2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcCodeViolationTimeline3" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcCodeViolationTimeline4" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctPfcCodeViolationTimeline5" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC CODE VIOLATION --%>

                                                                <%-- PFC BOARDED --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Boarded
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcBoarded" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC Boarded Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="inPfcBoardedTimeline1" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -35px">
                                                                        <input id="slctPfcBoardedTimeline2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcBoardedTimeline3" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcBoardedTimeline4" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctPfcBoardedTimeline5" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC BOARDED --%>



                                                                <%-- PFC OTHERS --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC OTHER
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPfcOther" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        PFC OTHER Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="inPfcOtherTimeline1" class="form-control input-sm">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -11PX">
                                                                        <input id="slctPfcOtherTimeline2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcOtherTimeline3" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctPfcOtherTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctPfcOtherTimeline5" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END PFC OTHERS --%>
                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Special Requirements
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctSpcReq" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="A">A</option>
                                                                            <option value="B">B</option>
                                                                            <option value="C">C</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Payment Type
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctPaymentType" class="form-control input-sm">
                                                                            <option value="">None</option>
                                                                            <option value="check">Check</option>
                                                                            <option value="credit card online">Credit Card - Online</option>
                                                                            <option value="credit card phone">Credit Card - Phone</option>
                                                                            <option value="dpp">DPP</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Type of Registration
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctTypeOfRegistration" style="width: 100%" class="form-control input-sm">
                                                                            <option value="">N/A</option>
                                                                            <option value="PDF">City Registration Form</option>
                                                                            <option value="Online">City Website</option>
                                                                            <option value="Prochamps">ProChamps Online</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-6" style="display: none;">
                                                                        <input id="inUploadPath" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                                        <button id="linkPDF" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                    </div>
                                                                    <div class="col-xs-1" style="padding-left: 0px;">
                                                                        <button id="btnSavePDF" type="button" class="btn btn-primary" title="Upload File" style="display: none;"><i class="fa fa-upload" aria-hidden="true"></i> Save</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Renewals
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctVmsRenewal" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar renewYes">
                                                                        <input type="radio" id="RDEVERY" name="rdRenewal" value="Every" checked="checked" />
                                                                        Every
                                                                    </div>
                                                                    <div class="col-xs-2 renewYes">
                                                                        <select id="slctRenewEveryNum" class="form-control input-sm"></select>
                                                                    </div>
                                                                    <div class="col-xs-2 renewYes">
                                                                        <select id="slctRenewEveryYears" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="Year/s">Year/s</option>
                                                                            <option value="Month/s">Month/s</option>
                                                                            <option value="Week/s">Week/s</option>
                                                                            <option value="Day/s">Day/s</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 renewYes" style="margin-top: 1%;">
                                                                    <div class="col-xs-5">
                                                                        &nbsp;
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar" style="margin-left: -25px">
                                                                        <input type="radio" id="RDON" name="rdRenewal" value="On" />
                                                                        On
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRenewOnMonths" class="form-control input-sm"></select>
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRenewOnNum" class="form-control input-sm"></select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                    </div>
                                                                    <div class="col-xs-6 renewYes renewupload" style="display: none;">
                                                                        <input id="inUploadPath1" name="upload1" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                                        <button id="linkPDFRenewal" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                    </div>
                                                                    <div class="col-xs-1" style="padding-left: 0px;">
                                                                        <button id="btnSavePDFRenew" type="button" class="btn btn-primary" title="Upload File" style="display: none;"><i class="fa fa-upload" aria-hidden="true"></i> Save</button>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegPFC" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- END TAB PFC DESIGN --%>

                                                            <%-- TAB REO DESIGN --%>
                                                            <div id="tabREO" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        <input id="reoReg" type="checkbox" />
                                                                        <label>No REO Registrations</label>
                                                                    </div>
                                                                    <div class="col-xs-3">
                                                                        <input id="reoStatewideReg" type="checkbox" />
                                                                        <label>Statewide Registration</label>
                                                                    </div>
                                                                </div>
                                                                <%-- REO BANKED-OWED --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Banked-owed
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoBankOwed" class="form-control input-sm">
                                                                            <%--<option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Banked-owed Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="Drpreobanktime" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -35px">
                                                                        <input id="inReoBankOwed1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoBankOwed2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoBankOwed3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReoBankOwed4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO BANKED-OWED --%>

                                                                <%-- REO VACANT --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Vacant
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoVacant" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Vacant Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -0px">
                                                                        <select id="slctReowith" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="inReoVacantTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoVacantTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoVacantTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReoVacantTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO VACANT --%>

                                                                <%-- REO CITY NOTICE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO City Notice
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoCityNotice" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO City Notice Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -0px">
                                                                        <select id="slctReocitynoticewith" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="slctReocitytimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoBusinesstimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReodaystimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReovacancytimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO CITY NOTICE --%>

                                                                <%-- CODE VIOLATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Code Violation
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoCodeViolation" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Code Violation Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -0px">
                                                                        <select id="slctReoviolationwith" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="slctReoViolationtimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoviolationtimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoviolationdaystimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReoviolationvacancytimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO CODE VIOLATION --%>

                                                                <%-- REO BOARDED ONLY --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Boarded Only
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoBoardedOnly" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Boarded Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -0px">
                                                                        <select id="slctReoBoardedwith" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="slctReoBoardedtimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoBoardedtimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoBoardeddaystimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReoBoardedtimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO BOARDED ONLY --%>

                                                                <%-- REO DISTRESSED ABANDONED --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Distressed/Abandoned
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoDistressedAbandoned" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO Distressed/Abandoned Timeline
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="DrpReoAbandoned" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="inReoDistressedAbandoned1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoDistressedAbandoned2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoDistressedAbandoned3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReoDistressedAbandoned4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO DISTRESSED ABANDONED --%>

                                                                <%-- REO RENTAL REGISTRATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Rental Registration
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctRentalRegistration" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Rental Registration Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -0px">
                                                                        <select id="slctRentalFormwithin" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="slctReorentaltimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReorentaltimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReorentaltimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReorentaltimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO RENTAL REGISTRATION --%>

                                                                <%-- REO OTHERS --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO OTHER
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctReoOther" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        REO OTHER Timeline
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -0px">
                                                                        <select id="slctreootherwithin" class="form-control input-sm" style="margin-left: -25PX">
                                                                            <%-- <option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Within</option>
                                                                            <option value="false">After</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-xs-2" style="margin-left: -35PX">
                                                                        <input id="inReoOtherTimeline1" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoOtherTimeline2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="business">Business</option>
                                                                            <option value="calendar">Calendar</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25PX">
                                                                        <select id="slctReoOtherTimeline3" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="days">Days</option>
                                                                            <option value="months">Months</option>
                                                                        </select>
                                                                    </div>
                                                                    <span style="float: left;">From</span>
                                                                    <div class="col-xs-1" style="width: 120px">
                                                                        <select id="slctReoOtherTimeline4" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="vacancy">Vacancy</option>
                                                                            <option value="assumption of ownership">Assumption of Ownership</option>
                                                                            <option value="recording of deed">Recording of Deed</option>
                                                                            <option value="upon municipal notice">Upon Municipal Notice</option>
                                                                            <option value="inspection">Inspection</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <%-- END REO OTHERS --%>
                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Special Requirements
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctREOSpcReq" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="A">A</option>
                                                                            <option value="B">B</option>
                                                                            <option value="C">C</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>

                                                                <%-- REO PAYMENT TYPE --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Payment Type
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctREOPaymentType" class="form-control input-sm">
                                                                            <option value="" selected="selected">None</option>
                                                                            <option value="check">Check</option>
                                                                            <option value="credit card online">Credit Card - Online</option>
                                                                            <option value="credit card phone">Credit Card - Phone</option>
                                                                            <option value="dpp">DPP</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <%-- REO TYPE OF REGISTRATION --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Type of Registration
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctREOTypeOfRegistration" style="width: 100%" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="PDF">City Registration Form</option>
                                                                            <option value="Online">City Website</option>
                                                                            <option value="Prochamps">ProChamps Online</option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="col-xs-6" style="display: none;">
                                                                        <input id="inREOUploadPath" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                                        <button id="linkPDFREO" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                    </div>
                                                                    <div class="col-xs-1" style="padding-left: 0px;">
                                                                        <button type="button" class="btn btn-primary" id="btnSavePDF2" title="Upload File">
                                                                            <i class="fa fa-upload" aria-hidden="true"></i> Save</button>
                                                                    </div>
                                                                </div>

                                                                <%-- REO RENEWAL --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Renewal
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                        <select id="slctREOVmsRenewal" class="form-control input-sm">
                                                                            <%--<option value="" selected="selected">N/A</option>--%>
                                                                            <option value="">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                                                    <div class="col-xs-1 noPadMar renewREOYes" style="margin-left: 25px">
                                                                        <input type="radio" id="Reordvery" name="rdREORenewal" value="Every" checked="checked" />
                                                                        Every
                                                                    </div>
                                                                    <div class="col-xs-2 renewREOYes renewalss">
                                                                        <select id="slctREORenewEveryNum" class="form-control input-sm"></select>
                                                                    </div>
                                                                    <div class="col-xs-2 renewREOYes renewalss">
                                                                        <select id="slctREORenewEveryYears" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="Year/s">Year/s</option>
                                                                            <option value="Month/s">Month/s</option>
                                                                            <option value="Week/s">Week/s</option>
                                                                            <option value="Day/s">Day/s</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 renewREOYes renewalss" style="margin-top: 1%;">
                                                                    <div class="col-xs-5">
                                                                        &nbsp;
                                                                    </div>
                                                                    <div class="col-xs-1 noPadMar renewalss">
                                                                        <input type="radio" id="Reordon" name="rdREORenewal" value="On" />
                                                                        On
                                                                    </div>
                                                                    <div class="col-xs-2 renewalss">
                                                                        <select id="slctREORenewOnMonths" class="form-control input-sm"></select>
                                                                    </div>
                                                                    <div class="col-xs-2 renewalss">
                                                                        <select id="slctREORenewOnNum" class="form-control input-sm"></select>
                                                                    </div>


                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                    </div>
                                                                    <div class="col-xs-2" style="margin-left: -25px">
                                                                    </div>
                                                                    <div class="col-xs-6 renewREOYes renewUpload" style="display: none;">
                                                                        <input id="Reoupload1" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                                        <button id="linkPDFRenewalREO" type="button" class="btn btn-link input-sm" onclick="openPDF(this);"></button>
                                                                    </div>
                                                                    <div class="col-xs-1" style="padding-left: 0px;">
                                                                        <button id="btnSavePDFRenewREO" type="button" class="btn btn-primary" title="Upload File"><i class="fa fa-upload" aria-hidden="true"></i> Save</button>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegREO" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>

                                                            <div id="tabPropType" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Residential
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctPropResidential" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">

                                                                    <div class="col-xs-1"></div>
                                                                    <div class="col-xs-3">
                                                                        Single Family
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctSingleFamily" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>

                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-1"></div>
                                                                    <div class="col-xs-3">
                                                                        Multi Family
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctMultiFamily" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3">
                                                                        2 Units
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slct2units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3">
                                                                        3 Units
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slct3units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3">
                                                                        4 Units
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slct4units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-2"></div>
                                                                    <div class="col-xs-3">
                                                                        5 Units or more
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slct5units" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Rental
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctPropRental" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Commercial
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctPropCommercial" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Condo
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctPropCondo" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Townhome
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctPropTownhome" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Vacant Lot
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctPropVacantLot" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Mobile Home
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctPropMobilehome" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegProperty" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <%-- END TAB REO DESIGN --%>
                                                            <div id="tabPropReq" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            ADDN. FORM/INFO
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctAddnInfo" class="form-control input-sm" multiple="multiple">
                                                                                <option value="2016 Occupancy Registration form">2016 Occupancy Registration form</option>
                                                                                <option value="Bond form for Foreclosure + Vacant">Bond form for Foreclosure + Vacant</option>
                                                                                <option value="Insurance Certificate">Insurance Certificate</option>
                                                                                <option value="Insurance Plan">Insurance Plan</option>
                                                                                <option value="Maintenance Plan">Maintenance Plan</option>
                                                                                <option value="Maintenance Plan and Timetable">Maintenance Plan and Timetable</option>
                                                                                <option value="No Trespass Form">No Trespass Form</option>
                                                                                <option value="Non-Owner-Occupied License Application">Non-Owner-Occupied License Application</option>
                                                                                <option value="Out-of-County form and fee ">Out-of-County form and fee </option>
                                                                                <option value="Proof of insurance">Proof of insurance</option>
                                                                                <option value="Proof of utilities connection/deconnection">Proof of utilities connection/deconnection</option>
                                                                                <option value="Property Maintenance form">Property Maintenance form</option>
                                                                                <option value="Signage Form">Signage Form</option>
                                                                                <option value="Statement of Intent">Statement of Intent</option>
                                                                                <option value="Structure Checklist">Structure Checklist</option>
                                                                                <option value="Trespass Affidavit Form">Trespass Affidavit Form</option>
                                                                                <option value="Vacant Building Plan Form">Vacant Building Plan Form</option>
                                                                                <option value="Vacant Property Plan">Vacant Property Plan</option>
                                                                                <option value="Trespass form Foreclosure and Vacant">Trespass form Foreclosure and Vacant</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            First Time Vacancy Date
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctFirstTimeVacancyDate" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Presale "Owner" Definition
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <%--<button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="pd"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>--%>
                                                                            <select id="slctPresaleDefinition" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="Current Owner on Recorded Deed">Current Owner on Recorded Deed</option>
                                                                                <option value="Trustee/Mortgagee">Trustee/Mortgagee</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Secured Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctSecuredRequired" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Local Contact Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctLocalContactRequired" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Additional Signage Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctAddSignReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Local Contact Information
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <button type="button" class="btn btn-default btn-sm criteriaEdit" data-id="lci"><i class="fa fa-pencil-square-o"></i>&nbsp;Edit</button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Pictures Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctPicReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            GSE Exclusion
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctGseExclusion" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Mobile VIN Number Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctMobileVINReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Insurance Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctInsuranceReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Parcel Number Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctParcelReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Foreclosure Action Information Needed
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctForeclosureActInfo" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Legal Description Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctLegalDescReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Foreclosure Case Information Needed
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctForeclosureCaseInfo" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Block and Lot Number Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctBlockLotReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Foreclosure Deed Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctForeclosureDeedReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Attorney Information Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctAttyInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Bond Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctBondReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Broker Information Required If REO
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctBrkInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Bond Amount
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <input id="inBondAmount" type="number" class="form-control input-sm" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Mortgage Contact Name Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctMortContactNameReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Maintenance Plan Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctMaintePlanReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Client Tax Number Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctClientTaxReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            No Trespass Form Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctNoTrespassReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Signature Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctSignReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Utility Information Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctUtilityInfoReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Notarization Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctNotarizationReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Winterization Required
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctWinterReq" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-6">
                                                                        <div class="col-xs-6">
                                                                            Recent Inspection Date
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <select id="slctRecInsDate" class="form-control input-sm">
                                                                                <option value="" selected="selected">N/A</option>
                                                                                <option value="true">Yes</option>
                                                                                <option value="false">No</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegPropReq" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                            <div id="tabCost" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Registration Cost Y/N
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRegCost" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Registration Cost
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <input id="inRegCost" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRegCostCurr" class="form-control input-sm">
                                                                            <option value="USD" selected="selected">USD</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Registration Cost Standard
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRegCostStandard" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Is Renewal Cost Escalating
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRenewCostEscal" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <input id="inRenewAmt" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRenewAmtCurr1" class="form-control input-sm">
                                                                            <option value="USD">USD</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Escalating Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <button id="btnViewEscalRenewAmt" data-toggle="collapse" data-target="#divEscalRenewal" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                                    </div>
                                                                </div>
                                                                <div id="divEscalRenewal" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                        <button type="button" data-toggle="collapse" data-target="#divEscalRenewal" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-12">
                                                                            <div class="col-xs-2">
                                                                                <label>Service Type</label>
                                                                            </div>
                                                                            <div class="col-xs-3">
                                                                                <label>Amount</label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-2">
                                                                                <label>Renewal</label>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <input type="number" class="form-control input-sm" />
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <select class="form-control input-sm">
                                                                                    <option value="USD">USD</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <button id="btnAddNew1" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add New</button>
                                                                    </div>
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%; margin-bottom: 1%;">
                                                                        <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    </div>
                                                                </div>
<%-- COMMERCIAL REGISTRATION COST Y/N --%>
                                                               <%-- <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Commercial Registration Cost Y/N
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctComRegCost" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>
<%-- END COMMERCIAL REGISTRATION COST Y/N --%>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Commercial Registration Fee
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <input id="inComRegFee" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctComRegFee" class="form-control input-sm">
                                                                            <option value="USD">USD</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Commercial Fee Standard?
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctComFeeStandard" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Is Renewal Cost Escalating
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRenewCostEscal2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <input id="inRenewAmt2" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRenewAmtCurr2" class="form-control input-sm">
                                                                            <option value="USD">USD</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Escalating Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <button id="btnViewEscalRenewAmt2" data-toggle="collapse" data-target="#divEscalRenewal2" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                                    </div>
                                                                </div>
                                                                <div id="divEscalRenewal2" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                        <button type="button" data-toggle="collapse" data-target="#divEscalRenewal2" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-2">
                                                                            <label>Service Type</label>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <label>Amount</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-2">
                                                                                <label>Renewal</label>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <input type="number" class="form-control input-sm" />
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <select class="form-control input-sm">
                                                                                    <option value="USD">USD</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <button id="btnAddNew2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add New</button>
                                                                    </div>
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%; margin-bottom: 1%;">
                                                                        <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Is Registration Cost Standard? Y/N
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctRegCostStandard2" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
<%-- Vacant Lot Registration Cost Y/N --%>
                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Vacant Lot Registration Cost Y/N
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctVacLotRegCost" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>
<%-- END Vacant Lot Registration Cost Y/N --%>

<%-- Vacant Lot Registration Cost --%>
                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Vacant Lot Registration Cost
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <input id="inVacLotRegCost" type="number" class="form-control input-sm" />
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctVacLotRegCost2" class="form-control input-sm">
                                                                            <option value="USD">USD</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>
<%-- END Vacant Lot Registration Cost --%>

<%-- Vacant Lot Registration Cost Standard --%>
                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Vacant Lot Registration Cost Standard
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctVacLotRegCostStan" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>
<%-- END Vacant Lot Registration Cost Standard --%>

<%-- Is Renewal Cost Escalating --%>
                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Is Renewal Cost Escalating
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctVacRenCostEsc" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>--%>
<%-- END Is Renewal Cost Escalating --%>

<%-- Escalating Renewal Fee Amount --%>
                                                                <%--<div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Escalating Renewal Fee Amount
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <button id="btnViewEscalRenewAmt3" data-toggle="collapse" data-target="#divEscalRenewal3" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                                    </div>
                                                                </div>--%>
<%-- END Escalating Renewal Fee Amount --%>

<%-- Escalating Renewal Fee Amount BOX --%>
                                                                <%--<div id="divEscalRenewal3" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                        <button type="button" data-toggle="collapse" data-target="#divEscalRenewal3" class="btn btn-link text-red"><i class="fa fa-times fa-2x"></i></button>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <div class="col-xs-2">
                                                                            <label>Service Type</label>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <label>Amount</label>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12 escalInput" style="margin-top: 1%;">
                                                                        <div class="col-xs-12" style="margin-top: 1%;">
                                                                            <div class="col-xs-2">
                                                                                <label>Renewal</label>
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <input type="number" class="form-control input-sm" />
                                                                            </div>
                                                                            <div class="col-xs-2">
                                                                                <select class="form-control input-sm">
                                                                                    <option value="USD">USD</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                                        <button id="btnAddNew2" type="button" class="btn btn-link"><i class="fa fa-plus"></i>&nbsp;Add New</button>
                                                                    </div>
                                                                    <div class="col-xs-12 text-right" style="margin-top: 1%; margin-bottom: 1%;">
                                                                        <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    </div>
                                                                </div>--%>
<%-- END Escalating Renewal Fee Amount BOX --%>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegCost" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>

                                                            <div id="tabContReg" class="tab-pane">
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Continue Registration from PFC to REO?
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctContReg" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                                    <div class="col-xs-3">
                                                                        Update Registration with Municipality
                                                                    </div>
                                                                    <div class="col-xs-2">
                                                                        <select id="slctUpdateReg" class="form-control input-sm">
                                                                            <option value="" selected="selected">N/A</option>
                                                                            <option value="true">Yes</option>
                                                                            <option value="false">No</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                                    <button id="btnSaveRegCont" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <%-- Inspection --%>
                                            <div id="tabInspection" class="tab-pane">
                                              <%--  <div class="col-xs-12 copyfrom" style="margin-top: 1%;">
                                                    <div class="col-xs-2">
                                                        <label>Copy from:</label>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <select class="form-control input-sm slctCopyInpection"></select>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyIns" id="populateInspection" <!--onclick="copySettings(this);-->;"><i class="fa fa-chevron-circle-right"></i></button>
                                                    </div>
                                                </div>--%>
                                                <%-- Backup --%>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Update Required
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctInsUpReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Criteria Occupied
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctInsCriOcc" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button id="btnViewInsCriteriaOcc" data-toggle="collapse" data-target="#divInsCriteriaOcc" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                    </div>
                                                </div>
                                                <div id="divInsCriteriaOcc" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-2">
                                                            Select Frequency:
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" value="daily" />Daily
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" value="weekly" />&nbsp;Weekly
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" value="monthly" />&nbsp;Monthly
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaOccFreq" value="yearly" />&nbsp;Yearly
                                                        </div>
                                                        <div class="col-xs-2 text-right">
                                                            <button type="button" data-toggle="collapse" data-target="#divInsCriteriaOcc" class="btn btn-link btn-lg text-red noPadMar"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            What is the reference for this cycle?
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctInsCriteriaOccCycle1" class="form-control input-sm">
                                                                <option value="before">Before</option>
                                                                <option value="after">After</option>
                                                                <option value="Upon">Upon</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctInsCriteriaOccCycle2" class="form-control input-sm">
                                                                <option value="vacancy">Vacancy</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccDaily" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12" style="margin-top: 1%">
                                                            <div class="col-xs-3">
                                                                Set frequency of the cycle:
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" value="every1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreq" value="Monday" />&nbsp;Monday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreq" value="Tuesday" />&nbsp;Tuesday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreq" value="Wednesday" />&nbsp;Wednesday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreq" value="Thursday" />&nbsp;Thursday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreq" value="Friday" />&nbsp;Friday
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" value="every2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaOccDay" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">Day/s</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccWeekly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaOccWeek" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">week/s</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctInsCriteriaOccWeek" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the week</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccMonthly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaOccMonth" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">month/s</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctInsCriteriaMonth1" class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctInsCriteriaMonth2" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                        <option value="6th">6th</option>
                                                                        <option value="7th">7th</option>
                                                                        <option value="8th">8th</option>
                                                                        <option value="9th">9th</option>
                                                                        <option value="10th">10th</option>
                                                                        <option value="11th">11th</option>
                                                                        <option value="12th">12th</option>
                                                                        <option value="13th">13th</option>
                                                                        <option value="14th">14th</option>
                                                                        <option value="15th">15th</option>
                                                                        <option value="16th">16th</option>
                                                                        <option value="17th">17th</option>
                                                                        <option value="18th">18th</option>
                                                                        <option value="19th">19th</option>
                                                                        <option value="20th">20th</option>
                                                                        <option value="21st">21st</option>
                                                                        <option value="22nd">22nd</option>
                                                                        <option value="23rd">23rd</option>
                                                                        <option value="24th">24th</option>
                                                                        <option value="25th">25th</option>
                                                                        <option value="26th">26th</option>
                                                                        <option value="27th">27th</option>
                                                                        <option value="28th">28th</option>
                                                                        <option value="29th">29th</option>
                                                                        <option value="30th">30th</option>
                                                                        <option value="31st">31st</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaOccYearly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreqYear1" value="Anually" />&nbsp;Anually
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreqYear1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaOccYear" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">years</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            <input type="checkbox" name="cbRefresh" />&nbsp;Set refresh time of cycle:
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input id="inInsCriteriaOccTime" type="text" class="form-control input-sm" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 text-right" style="margin-bottom: 1%;">
                                                        <button id="btnSaveInsCriteriaOcc" data-toggle="collapse" data-target="#divInsCriteriaOcc" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Criteria Vacant
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctInsCriVac" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button id="btnViewInsCriteriaVac" data-toggle="collapse" data-target="#divInsCriteriaVac" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                    </div>
                                                </div>
                                                <div id="divInsCriteriaVac" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-2">
                                                            Select Frequency:
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" value="daily" />&nbsp;Daily
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" value="weekly" />&nbsp;Weekly
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" value="monthly" />&nbsp;Monthly
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsCriteriaVacFreq" value="yearly" />&nbsp;Yearly
                                                        </div>
                                                        <div class="col-xs-2 text-right">
                                                            <button type="button" data-toggle="collapse" data-target="#divInsCriteriaVac" class="btn btn-link btn-lg text-red noPadMar"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            What is the reference for this cycle?
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctInsCriteriaVacCycle1" class="form-control input-sm">
                                                                <option value="before">Before</option>
                                                                <option value="after">After</option>
                                                                <option value="Upon">Upon</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctInsCriteriaVacCycle2" class="form-control input-sm">
                                                                <option value="vacancy">Vacancy</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacDaily" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12" style="margin-top: 1%">
                                                            <div class="col-xs-3">
                                                                Set frequency of the cycle:
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" value="every1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqVac" value="Monday" />&nbsp;Monday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqVac" value="Tuesday" />&nbsp;Tuesday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqVac" value="Wednesday" />&nbsp;Wednesday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqVac" value="Thursday" />&nbsp;Thursday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqVac" value="Friday" />&nbsp;Friday
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" value="every2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaVacDay" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">Day/s</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacWeekly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaVacWeek" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">week/s</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctInsCriteriaVacWeek" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the week</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacMonthly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaVacMonth" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">month/s</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctInsCriteriaVacMonth1" class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctInsCriteriaVacMonth2" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                        <option value="6th">6th</option>
                                                                        <option value="7th">7th</option>
                                                                        <option value="8th">8th</option>
                                                                        <option value="9th">9th</option>
                                                                        <option value="10th">10th</option>
                                                                        <option value="11th">11th</option>
                                                                        <option value="12th">12th</option>
                                                                        <option value="13th">13th</option>
                                                                        <option value="14th">14th</option>
                                                                        <option value="15th">15th</option>
                                                                        <option value="16th">16th</option>
                                                                        <option value="17th">17th</option>
                                                                        <option value="18th">18th</option>
                                                                        <option value="19th">19th</option>
                                                                        <option value="20th">20th</option>
                                                                        <option value="21st">21st</option>
                                                                        <option value="22nd">22nd</option>
                                                                        <option value="23rd">23rd</option>
                                                                        <option value="24th">24th</option>
                                                                        <option value="25th">25th</option>
                                                                        <option value="26th">26th</option>
                                                                        <option value="27th">27th</option>
                                                                        <option value="28th">28th</option>
                                                                        <option value="29th">29th</option>
                                                                        <option value="30th">30th</option>
                                                                        <option value="31st">31st</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsCriteriaVacYearly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreqVacYear1" value="Anually" />&nbsp;Anually
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input id="inInsCriteriaVacYear" type="radio" name="rdFreq2" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">years</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            <input type="checkbox" name="cbRefreshVac" />&nbsp;Set refresh time of cycle:
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input id="inInsCriteriaVacTime" type="text" class="form-control input-sm" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 text-right" style="margin-bottom: 1%;">
                                                        <button id="btnSaveInsCriteriaVac" data-toggle="collapse" data-target="#divInsCriteriaVac" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection/Monitoring Fee Payment Frequency
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctInsFeePay" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <button id="btnViewInsFeePaymentFreq" data-toggle="collapse" data-target="#divInsFeePayFreq" type="button" class="btn btn-default btn-sm"><i class="fa fa-eye"></i>&nbsp;View</button>
                                                    </div>
                                                </div>
                                                <div id="divInsFeePayFreq" class="col-xs-12 collapse" style="margin-top: 1%; border: 1px solid black;">
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-2">
                                                            Select Frequency:
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsFeePayFreq" value="daily" />&nbsp;Daily
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsFeePayFreq" value="weekly" />&nbsp;Weekly
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsFeePayFreq" value="monthly" />&nbsp;Monthly
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input type="radio" name="rdInsFeePayFreq" value="yearly" />&nbsp;Yearly
                                                        </div>
                                                        <div class="col-xs-2 text-right">
                                                            <button type="button" data-toggle="collapse" data-target="#divInsFeePayFreq" class="btn btn-link btn-lg text-red noPadMar"><i class="fa fa-times"></i></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            What is the reference for this cycle?
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctFeePayCycle1" class="form-control input-sm">
                                                                <option value="before">Before</option>
                                                                <option value="after">After</option>
                                                                <option value="Upon">Upon</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctFeePayCycle2" class="form-control input-sm">
                                                                <option value="vacancy">Vacancy</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqDaily" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12" style="margin-top: 1%">
                                                            <div class="col-xs-3">
                                                                Set frequency of the cycle:
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqFeePay" value="Monday" />&nbsp;Monday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqFeePay" value="Tuesday" />&nbsp;Tuesday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqFeePay" value="Wednesday" />&nbsp;Wednesday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqFeePay" value="Thursday" />&nbsp;Thursday
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input type="checkbox" name="cbFreqFeePay" value="Friday" />&nbsp;Friday
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inFeePayFreqDay" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">Day/s</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqWeekly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inFeePayFreqWeek" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">week/s</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctFeePayFreqWeek" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the week</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqMonthly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inFeePayFreqMonth" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">month/s</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctFeePayFreqMonth1" class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select id="slctFeePayFreqMonth2" class="form-control input-sm">
                                                                        <option value="Sunday">Sunday</option>
                                                                        <option value="Monday">Monday</option>
                                                                        <option value="Tuesday">Tuesday</option>
                                                                        <option value="Wednesday">Wednesday</option>
                                                                        <option value="Thursday">Thursday</option>
                                                                        <option value="Friday">Friday</option>
                                                                        <option value="Saturday">Saturday</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%;">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreq3" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <select class="form-control input-sm">
                                                                        <option value="1st">1st</option>
                                                                        <option value="2nd">2nd</option>
                                                                        <option value="3rd">3rd</option>
                                                                        <option value="4th">4th</option>
                                                                        <option value="5th">5th</option>
                                                                        <option value="6th">6th</option>
                                                                        <option value="7th">7th</option>
                                                                        <option value="8th">8th</option>
                                                                        <option value="9th">9th</option>
                                                                        <option value="10th">10th</option>
                                                                        <option value="11th">11th</option>
                                                                        <option value="12th">12th</option>
                                                                        <option value="13th">13th</option>
                                                                        <option value="14th">14th</option>
                                                                        <option value="15th">15th</option>
                                                                        <option value="16th">16th</option>
                                                                        <option value="17th">17th</option>
                                                                        <option value="18th">18th</option>
                                                                        <option value="19th">19th</option>
                                                                        <option value="20th">20th</option>
                                                                        <option value="21st">21st</option>
                                                                        <option value="22nd">22nd</option>
                                                                        <option value="23rd">23rd</option>
                                                                        <option value="24th">24th</option>
                                                                        <option value="25th">25th</option>
                                                                        <option value="26th">26th</option>
                                                                        <option value="27th">27th</option>
                                                                        <option value="28th">28th</option>
                                                                        <option value="29th">29th</option>
                                                                        <option value="30th">30th</option>
                                                                        <option value="31st">31st</option>
                                                                    </select>
                                                                </div>
                                                                <span style="float: left;">of the month</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="freqInsFeePayFreqYearly" class="col-xs-12" style="display: none;">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreqFeePayYear1" value="Anually" />&nbsp;Anually
                                                                </div>
                                                            </div>
                                                            <div class="col-xs-12" style="margin-top: 1%">
                                                                <div class="col-xs-2">
                                                                    <input type="radio" name="rdFreqFeePayYear1" />&nbsp;Every
                                                                </div>
                                                                <div class="col-xs-2">
                                                                    <input id="inFeePayFreqYear" type="text" class="form-control input-sm" />
                                                                </div>
                                                                <span style="float: left;">years</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12" style="margin-top: 1%">
                                                        <div class="col-xs-3">
                                                            <input type="checkbox" />&nbsp;Set refresh time of cycle:
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input id="inFeePayTime" type="text" class="form-control input-sm" />
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 text-right" style="margin-bottom: 1%;">
                                                        <button id="btnSaveInsFeePayFreq" data-toggle="collapse" data-target="#divInsFeePayFreq" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection/Monitoring Fee Required Y/N
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctInsFeeReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection/Monitoring Fee Amount
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inInsFeeAmt" type="number" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctInsFeeAmt" class="form-control input-sm">
                                                            <option value="USD" selected="selected">USD</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Inspection Reporting Frequency
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctInsRepFreq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveInspection" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Municipality --%>
                                            <div id="tabMunicipality" class="tab-pane">
                                                <%--<div class="col-xs-12 copyfrom" style="margin-top: 1%;">
                                                            <div class="col-xs-2">
                                                                <label>Copy from:</label>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <select class="form-control input-sm slctCopyMunicipality"></select>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyReg" id="populateMunicipality" <!--onclick="copySettings(this);-->"><i class="fa fa-chevron-circle-right"></i></button>
                                                            </div>
                                                        </div>--%>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Department
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inMuniDept" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Phone Number
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inMuniPhone" type="number" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Email Address
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inMuniEmail" type="email" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Municipality Mailing Address
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inMuniMailStr" type="text" class="form-control input-sm" placeholder="Street Name" />
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctMuniMailSta" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctMuniMailCt" class="form-control input-sm"></select>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctMuniMailZip" class="form-control input-sm"></select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Contact Person
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inContact" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Title
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inTitle" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Department
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inDept" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Phone Number
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inPhone" type="number" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Email Address
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inEmail" type="email" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Mailing Address
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <input id="inMailing" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Hours of Operation
                                                    </div>
                                                    <div class="col-xs-1">from:</div>
                                                    <div class="col-xs-2">
                                                        <input id="inHrsFrom" type="text" class="form-control input-sm" />
                                                    </div>
                                                    <div class="col-xs-1">to:</div>
                                                    <div class="col-xs-2">
                                                        <input id="inHrsTo" type="text" class="form-control input-sm" />
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveMunicipalInfo" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Deregistration --%>
                                            <div id="tabDeregistration" class="tab-pane">
                                                <%--<div class="col-xs-12 copyfrom" style="margin-top: 1%;">
                                                    <div class="col-xs-2">
                                                        <label>Copy from:</label>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <select class="form-control input-sm slctCopyDeregistration"></select>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyDereg" id="populateDereg" <!--onclick="copySettings(this);-->;"><i class="fa fa-chevron-circle-right"></i></button>
                                                    </div>
                                                </div>--%>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Deregistration Required
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctDeregRequired" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Only if Conveyed
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctConveyed" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Only if Occupied
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctOccupied" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        How to Deregister
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctHowToDereg" style="width: 100%" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="PDF">City Registration Form</option>
                                                            <option value="Online">City Website</option>
                                                            <option value="Prochamps">ProChamps Online</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-xs-7" style="display: none;">
                                                        <input id="inUploadPathD" type="file" class="file form-control input-sm" data-show-preview="false" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        New Owner Information Required
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctNewOwnerInfoReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Proof of Conveyance Required
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctProofOfConveyReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12" style="margin-top: 1%;">
                                                    <div class="col-xs-3">
                                                        Date of Sale Required
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <select id="slctDateOfSaleReq" class="form-control input-sm">
                                                            <option value="" selected="selected">N/A</option>
                                                            <option value="true">Yes</option>
                                                            <option value="false">No</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveDeregistration" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Ordinance Settings --%>
                                            <div id="tabOrdinance" class="tab-pane">
                                                <table id="tblOrdinance" class="table no-border table-responsive" data-pagination="true">
                                                    <thead>
                                                        <tr>
                                                            <th data-field="ordinance_num">Ordinance Number</th>
                                                            <th data-field="ordinance_name">Ordinance Name</th>
                                                            <th data-field="description" data-cell-style="cellStyle">Description</th>
                                                            <th data-field="section">Section</th>
                                                            <th data-field="source">Source</th>
                                                            <th data-field="enacted_date">Enacted Date</th>
                                                            <th data-field="revision_date">Revision Date</th>
                                                            <th data-field="id" data-formatter="action"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                                <div class="text-left" style="margin-top: 1%;">
                                                    <button id="btnAddOrdinance" type="button" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i>&nbsp;Add New</button>
                                                    <button id="btnHistory" type="button" class="btn btn-primary btn-sm"><i class="fa fa-history"></i>&nbsp;History</button>
                                                </div>
                                                <div class="text-right" style="margin-top: 1%;">
                                                    <%--<button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>--%>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Zip Code --%>
                                            <div id="tabZip" class="tab-pane" style="max-height: 710px; overflow: auto;">
                                                <table id="tblZip" class="table no-border">
                                                    <tbody></tbody>
                                                </table>
                                                <div class="text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveZip" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>

                                            <%-- Notification Setting --%>
                                            <div class="tab-pane" id="tabNotif">
                                                <%--<div class="col-xs-12 copyfrom" style="margin-top: 1%;">
                                                    <div class="col-xs-2">
                                                        <label>Copy from:</label>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <select class="form-control input-sm slctCopyNotif"></select>
                                                    </div>
                                                    <div class="col-xs-4">
                                                        <button type="button" class="btn btn-link btn-lg noPadMar" data-id="copyNotif" id="populateNotif" <!--onclick="copySettings(this);-->;"><i class="fa fa-chevron-circle-right"></i></button>
                                                    </div>
                                                </div>--%>
                                                <fieldset>
                                                    <legend>
                                                        <label>Registration Notification</label></legend>
                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                        <div class="col-xs-2">
                                                            Send Reminder
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input id="inRegSendRem" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRegSendRem1" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="business">Business</option>
                                                                <option value="calendar">Calendar</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRegSendRem2" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Days">Days</option>
                                                                <option value="Months">Months</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRegSendRem3" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="before">Before</option>
                                                                <option value="after">After</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRegSendRem4" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Vacant Registration Time Frame">Vacant Registration Time Frame</option>
                                                                <option value="Renewal Time Frame">Renewal Time Frame</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <legend>
                                                        <label>Renewal Notification</label></legend>
                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                        <div class="col-xs-2">
                                                            Send Reminder
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <input id="inRenewSendRem" type="text" class="form-control input-sm" />
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRenewSendRem1" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="business">Business</option>
                                                                <option value="calendar">Calendar</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRenewSendRem2" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="days">Days</option>
                                                                <option value="months">Months</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRenewSendRem3" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="before">Before</option>
                                                                <option value="after">After</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <select id="slctRenewSendRem4" class="form-control input-sm">
                                                                <option value="" selected="selected">N/A</option>
                                                                <option value="Vacant Registration Time Frame">Vacant Registration Time Frame</option>
                                                                <option value="Renewal Time Frame">Renewal Time Frame</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <fieldset>
                                                    <legend>
                                                        <label>Web Notification</label></legend>
                                                    <div class="col-xs-12" style="margin-top: 1%;">
                                                        <div class="col-xs-2">
                                                            Send Reminder
                                                        </div>
                                                        <div class="col-xs-10">
                                                            <input id="inWebSendRem" type="text" class="form-control input-sm" />
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <div class="col-xs-12 text-right" style="margin-top: 1%;">
                                                    <button id="btnSaveNotif" type="button" class="btn btn-primary btn-sm"><i class="fa fa-save"></i>&nbsp;Save</button>
                                                    <button type="button" class="btn btn-primary btn-sm btnCancel"><i class="fa fa-times"></i>&nbsp;Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="modalPDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 1000px;">
            <div class="modal-content">
                <div class="modal-header text-center" style="background-color: #4d6578; color: #fff;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Document Preview</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <label id="lblPDF"></label>
                        </div>
                        <div class="col-xs-12">
                            <iframe id="pdf" style="width: 100%; height: 682px; border: 0;"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEdit">
        <div class="modal-dialog" role="document">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <label id="lblEdit"></label>
                </div>
                <div id="lci" class="panel-body" style="display: none;">
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Company</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciCompany" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Contact First Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciFirstName" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Contact Last Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciLastName" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Title</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciTitle" class="form-control input-sm">
                                <option value="">N/A</option>
                                <option value="Broker">Broker</option>
                                <option value="SPI Vendor">SPI Vendor</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Business License Number</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciBusinessLicenseNum" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Phone Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciPhoneNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciPhoneNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Business Phone Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciBusinessPhoneNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciBusinessPhoneNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>24-Hour Emergency Phone</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciEmrPhone1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciEmrPhone2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Fax Number</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciFaxNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciFaxNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Cell</label>
                        </div>
                        <div class="col-xs-4">
                            <input id="lciCellNum1" type="text" class="form-control input-sm" placeholder="(Primary)" />
                        </div>
                        <div class="col-xs-4">
                            <input id="lciCellNum2" type="text" class="form-control input-sm" placeholder="(Secondary)" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Email Address</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciEmail" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Street Name</label>
                        </div>
                        <div class="col-xs-8">
                            <input id="lciStreet" type="text" class="form-control input-sm" />
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>State</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciState" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>City</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciCity" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Zip</label>
                        </div>
                        <div class="col-xs-8">
                            <select id="lciZip" class="form-control input-sm"></select>
                        </div>
                    </div>
                    <div class="col-xs-12" style="margin-top: 1%;">
                        <div class="col-xs-4">
                            <label>Hours of Operation</label>
                        </div>
                        <div class="col-xs-1">
                            from:
                        </div>
                        <div class="col-xs-3">
                            <div class="bootstrap-timepicker">
                                <input id="lciHrsFrom" type="text" class="form-control input-sm timepicker" />
                            </div>
                        </div>
                        <div class="col-xs-1">
                            to:
                        </div>
                        <div class="col-xs-3">
                            <div class="bootstrap-timepicker">
                                <input id="lciHrsTo" type="text" class="form-control input-sm timepicker" />
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="rr" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="ico" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="icv" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
                <div id="dr" class="panel-body" style="display: none;">
                    <textarea class="form-control input-sm" style="max-height: 200px; min-height: 200px; resize: none;"></textarea>
                    <div class="text-center" style="margin-top: 1%;">
                        <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAddOrdinance">
        <div class="modal-dialog" role="document">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <label>Add Ordinance</label>
                </div>
                <div class="panel-body">
                    <label id="ordID" style="display: none;"></label>
                    <table id="tblAddOrd" class="table no-border">
                        <tbody>
                            <tr>
                                <th>Ordinance Number:</th>
                                <td>
                                    <input type="text" class="form-control input-sm" />
                                </td>
                            </tr>
                            <tr>
                                <th>Ordinance Name:</th>
                                <td>
                                    <input type="text" class="form-control input-sm" />
                                </td>
                            </tr>
                            <tr>
                                <th>Description:</th>
                                <td>
                                    <textarea class="form-control input-sm" style="max-height: 140px; max-width: 400px; min-height: 140px; min-width: 400px;"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>Section:</th>
                                <td>
                                    <input type="text" class="form-control input-sm" />
                                </td>
                            </tr>
                            <tr>
                                <th>Source:</th>
                                <td>
                                    <input type="text" class="form-control input-sm" />
                                </td>
                            </tr>
                            <tr>
                                <th>Enacted Date:</th>
                                <td>
                                    <input type="text" class="form-control input-sm datepicker" />
                                </td>
                            </tr>
                            <tr>
                                <th>Revision Date:</th>
                                <td>
                                    <input type="text" class="form-control input-sm datepicker" />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input id="inUpload" type="file" class="file input-sm" data-show-preview="false" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right">
                                    <button id="btnApplyOrdinance" type="button" class="btn btn-primary btn-sm"><i class="fa fa-check"></i>&nbsp;Apply</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalOrdinanceHist">
        <div class="modal-dialog" role="document" style="width: 1000px;">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <label>Add Ordinance</label>
                </div>
                <div class="panel-body">
                    <table id="tblOrdinanceHist" class="table no-border">
                        <thead>
                            <tr>
                                <th data-field="ordinance_num">Ordinance Number</th>
                                <th data-field="ordinance_name">Ordinance Name</th>
                                <th data-field="description">Description</th>
                                <th data-field="section">Section</th>
                                <th data-field="source">Source</th>
                                <th data-field="enacted_date">Enacted Date</th>
                                <th data-field="revision_date">Revision Date</th>
                                <th data-field="added_by">Added By</th>
                                <th data-field="date_added">Date Added</th>
                                <th data-field="modified_by">Modified By</th>
                                <th data-field="date_modified">Date Modified</th>
                                <th data-field="deleted_by">Deleted By</th>
                                <th data-field="date_deleted">Date Deleted</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                    <div class="text-center" style="margin-top: 1%;">
                        <button class="btn btn-primary btn-sm" type="button" data-dismiss="modal" aria-label="Close"><i class="fa fa-apply"></i>Done</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="script" runat="server">
    <script src="js/CompRulesEncode.js"></script>
</asp:Content>
